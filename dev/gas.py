#
# Google Apps Script Referenceをスクレイピング
#

import time
import re
import json
import random
from selenium import webdriver

driver = webdriver.Firefox( )
driver.get( "https://developers.google.com/apps-script/reference/" )

# クラスリストを作成
classes = []
known = {}
sections = driver.find_elements_by_css_selector("nav.devsite-section-nav>ul>li")
for section in sections:
    titles = section.find_elements_by_css_selector("span.devsite-nav-title")
    if len(titles) == 0: continue
    if titles[0].get_attribute("textContent").strip( ) == "Advanced Google Services": continue
    uls = section.find_elements_by_css_selector("ul")
    if len(uls) == 0: continue
    ul = uls[0]
    section.click( )
    for li in ul.find_elements_by_css_selector("li.devsite-nav-item-section-expandable"):
        li.click( )
        mode = "static"
        for item in li.find_elements_by_css_selector("ul>li"):
            if item.get_attribute( "class" ) != None and item.get_attribute( "class" ).find( "devsite-nav-item-heading" ) != -1:
                item_type = item.get_attribute("textContent")
                print( item_type )
                if item_type == "Classes":
                    mode = "class"
                elif item_type == "Interface":
                    mode = "interface"
                elif item_type == "Enums":
                    mode = "enum"
                continue
            a_tags = item.find_elements_by_css_selector("a")
            if len(a_tags) == 0: continue
            a_tag = a_tags[0]
            if a_tag.text == "概要": continue
            url = a_tag.get_attribute("href")
            if url not in known:
                known[url] = True
                cl = {"name": a_tag.text.strip( ), "url": url, "mode": mode, "deprecated": False}
                if item.get_attribute( "class" ) != None and item.get_attribute( "class" ).find("devsite-nav-deprecated") != -1:
                    cl["deprecated"] = True
                if cl["name"] == "":
                    print( "**********************************" )
                else:
                    print( cl )
                classes.append( cl )

# 情報取得
for cl in classes:
    print( "*", cl["name"] )
    driver.get( cl["url"] )
    article = driver.find_element_by_css_selector("article.devsite-article-inner")
    # プロパティ
    properties = article.find_elements_by_css_selector("table.members.property")
    cl["properties"] = []
    for prop in properties:
        deprecated = False
        prop_parent = prop.find_element_by_xpath("../..")
        if prop_parent.get_attribute("class") != None and prop_parent.get_attribute("class").find("expandable") != -1:
            deprecated = True
            prop_parent.find_element_by_css_selector("a.exw-control").click( )
        trs = prop.find_elements_by_css_selector("tbody>tr")
        if 1 < len( trs ):
            trs.pop(0)
            for tr in trs:
                tds = tr.find_elements_by_css_selector("td")
                name = tds[0].get_attribute("textContent").strip( )
                type_name = tds[1].get_attribute("textContent").strip( )
                a_tag = tds[1].find_elements_by_css_selector("a")
                if 0 < len(a_tag):
                    type_url = a_tag[0].get_attribute("href")
                else:
                    type_url = None
                cl["properties"].append({"name":name, "type": type_name, "type_url": type_url, "deprecated": deprecated})
                print("\tprop\t",name,deprecated,type_name)
    # メソッド
    funcs = article.find_elements_by_css_selector("div.function.doc")
    cl["funcs"] = []
    for func in funcs:
        if func.get_attribute('class').find('expandable') != -1:
            func.find_element_by_css_selector("a.exw-control").click( )
            deprecated = True
        else:
            deprecated = False
        # メソッド名
        name = re.match("[A-Za-z0-9]+", func.find_element_by_css_selector("h3.showalways").get_attribute("textContent").strip( )).group( )
        # 引数
        trs = func.find_elements_by_css_selector("table.function.param>tbody>tr")
        if 1 < len( trs ):
            trs.pop(0)
            params = []
            for tr in trs:
                tds = tr.find_elements_by_css_selector("td")
                arg_name = tds[0].get_attribute("textContent").strip( )
                arg_type = tds[1].get_attribute("textContent").strip( )
                a_tag = tds[1].find_elements_by_css_selector("a")
                if 0 < len(a_tag):
                    type_url = a_tag[0].get_attribute("href")
                else:
                    type_url = None
                params.append({"name": arg_name, "type": arg_type, "type_url": type_url})
        # 追加オプション
        trs = func.find_elements_by_css_selector("table.function.advancedparam>tbody>tr")
        if 1 < len( trs ):
            trs.pop(0)
            advanced_params = []
            for tr in trs:
                tds = tr.find_elements_by_css_selector("td")
                arg_name = tds[0].get_attribute("textContent").strip( )
                arg_type = tds[1].get_attribute("textContent").strip( )
                a_tag = tds[1].find_elements_by_css_selector("a")
                if 0 < len(a_tag):
                    type_url = a_tag[0].get_attribute("href")
                else:
                    type_url = None
                advanced_params.append({"name": arg_name, "type": arg_type, "type_url": type_url})
        else:
            advanced_params = None
        # 返り値
        ret_tag = func.find_elements_by_css_selector("h4+p")
        if 0 < len(ret_tag):
            a_tag = ret_tag[0].find_elements_by_css_selector("a")
            if 0 < len(a_tag):
                type_url = a_tag[0].get_attribute("href")
            else:
                type_url = None
            ret_code = ret_tag[0].find_elements_by_css_selector("code")
            if 0 < len(ret_code):
                ret = {"type": ret_code[0].get_attribute("textContent").strip( ), "type_url": type_url}
            else:
                ret = None
        else:
            ret = None
        cl["funcs"].append({"name": name, "params": params, "advanced_params": advanced_params, "return": ret, "deprecated": deprecated})
        print("\tfunc\t",name,deprecated)

with open( 'objects.json', 'w' ) as f:
    f.write( json.dumps( classes ) )

driver.quit( )
