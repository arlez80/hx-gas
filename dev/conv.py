#
# Google Apps Script RefereceからHaxe Bindを生成する
#

import os
import json

with open('objects.json', 'r') as f:
    src = json.loads(f.read( ))

# グルーピングする
groups = {}
byurl = {}
prefix = "https://developers.google.com/apps-script/reference/"
for t in src:
    byurl[t["url"]] = t
    if t["url"].find(prefix) == -1: continue
    group_name = t["url"].replace(prefix,"").split("/")[0].replace("-","_")
    t["group"] = group_name
    if group_name not in groups:
        groups[group_name] = {}
    groups[group_name][t["name"]] = t
    target_dir = "../google/apps/script/%s"  % (t["group"])
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)

def gen_enum( f, enum, p ):
    if p != "": p += "."

    f.write('@:require(hx_gas)\n')
    f.write('@:native("%s%s")\n' % (p,enum["name"]))
    if enum["deprecated"]: f.write('@:deprecated\n')
    f.write('@:enum abstract %s(String) from String to String\n{\n' % (enum["name"]))
    for prop in enum["properties"]:
        if prop["deprecated"]: f.write('@:deprecated\n')
        f.write('\tvar %s;\n' % (prop["name"]))
    f.write('}\n\n')

def gen_type( t ):
    if t == None: return "Void"

    # 配列
    array_prefix = ""
    array_postfix = ""
    for i in range( 0, t["type"].count("[]") ):
        array_prefix += "Array<"
        array_postfix += ">"
    type_name = t["type"].replace("[]","")

    # Haxeでの名称に変更するもの
    table = {
        "void":"Void",
        "Object":"Dynamic",
        "Byte":"Int",
        "Char":"String",
        "Integer":"Int",
        "Number":"Float",
        "BigNumber":"Float",
        "Boolean":"Bool",
        "String":"String",
        "Date":"Date",

        # TODO: これらはなんとかせろ
        "Object...":"Dynamic",
        "Function":"Dynamic",
    }
    if type_name in table:
        return array_prefix + table[type_name] + array_postfix

    if ( "type_url" not in t ) or ( t["type_url"] == None ):
        print( "Unknown", t )
        return "Dynamic"

    # js.Error
    if t["type_url"] == "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error":
        return "js.Error"

    # GASで定義されたClass
    type_url = t["type_url"].replace(".html","").replace("#","")

    if type_url in byurl:
        target = byurl[type_url]
        return "%sgoogle.apps.script.%s.%s%s" % ( array_prefix, target["group"], target["name"], array_postfix )

    # 特定できない
    print( "Unknown", t )
    return "Dynamic"

def gen_func( func, output_name = True ):
    options = None
    if func["advanced_params"] != None:
        options = []
        for t in func["advanced_params"]:
            options.append("?%s:%s" % (t["name"],gen_type(t)))
        options = ",".join(options)
    args = []
    for t in func["params"]:
        if t["name"] == "options" and options != None:
            args.append("%s:{%s}" % (t["name"],options))
        else:
            args.append("%s:%s" % (t["name"],gen_type(t)))

    return 'function %s(%s):%s' % (func["name"] if output_name else "", ",".join(args),gen_type(func["return"]))

def gen_class( f, cl ):
    # properties
    consts = []
    for t in cl["properties"]:
        if t["type_url"] != None:
            type_url = t["type_url"].replace(".html","").replace("#","")
            if type_url in byurl:
                gen_enum( f, byurl[type_url], cl["name"] )
            else:
                print(t)
                raise "unknown!!!"
        elif t["type_url"] != None:
            pass
        elif t["type"] == "Enum":
            consts.append(t["name"])
        else:
            print(t)
            raise "unknown!!!"
    # class
    f.write('@:native("%s")\n' % (cl["name"]))
    if cl["deprecated"]: f.write('@:deprecated\n')
    f.write('extern class %s\n{\n' % (cl["name"]))
    static = 'static ' if cl["mode"] == "static" else ""
    # consts
    for t in consts:
        f.write('\t' + static + 'var %s(default, null);\n' % t )
    # method
    overloaded = {}
    for func in cl["funcs"]:
        if func["name"] not in overloaded:
            overloaded[func["name"]] = []
        overloaded[func["name"]].append( func )
    for funcs in overloaded.values( ):
        if len(funcs) == 1:
            if funcs[0]["deprecated"]: f.write('\t@:deprecated\n')
            f.write('\t' + static + gen_func( funcs[0] ) + ';\n' )
        else:
            if funcs[0]["deprecated"]: f.write('\t@:deprecated\n')
            for func in funcs[1:]:
                f.write('\t@:overload(' + gen_func( func, False ) + ' {})\n' )
            f.write('\t' + static + gen_func( funcs[0] ) + ';\n' )
    f.write('}\n')

# 生産する
for t in src:
    if "group" not in t: continue
    print(t["name"], t["group"], t["mode"])
    with open('../google/apps/script/%s/%s.hx' % (t["group"], t["name"]), 'w') as f:
        f.write("package google.apps.script.%s;\n\n" % t["group"])
        if t["mode"] == "enum":
            gen_enum( f, t, "" )
        else:
            gen_class( f, t )

