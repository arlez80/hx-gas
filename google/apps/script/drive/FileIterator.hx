package google.apps.script.drive;

@:native("FileIterator")
extern class FileIterator
{
	function next(trashed:Bool):google.apps.script.drive.File;
	function getContinuationToken(trashed:Bool):String;
	function hasNext(trashed:Bool):Bool;
}
