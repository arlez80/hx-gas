package google.apps.script.drive;

@:native("User")
extern class User
{
	function getName(trashed:Bool):String;
	@:deprecated
	function getUserLoginId(trashed:Bool):String;
	function getEmail(trashed:Bool):String;
	function getDomain(trashed:Bool):String;
	function getPhotoUrl(trashed:Bool):String;
}
