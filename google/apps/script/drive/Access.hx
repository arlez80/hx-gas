package google.apps.script.drive;

@:require(hx_gas)
@:native("Access")
@:enum abstract Access(String) from String to String
{
	var ANYONE;
	var ANYONE_WITH_LINK;
	var DOMAIN;
	var DOMAIN_WITH_LINK;
	var PRIVATE;
}

