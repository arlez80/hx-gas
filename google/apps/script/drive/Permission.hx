package google.apps.script.drive;

@:require(hx_gas)
@:native("Permission")
@:enum abstract Permission(String) from String to String
{
	var VIEW;
	var EDIT;
	var COMMENT;
	var OWNER;
	var ORGANIZER;
	var NONE;
}

