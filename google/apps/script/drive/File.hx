package google.apps.script.drive;

@:native("File")
extern class File
{
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.File {})
	function removeCommenter(emailAddress:String):google.apps.script.drive.File;
	function addEditors(emailAddresses:Array<String>):google.apps.script.drive.File;
	function getSharingAccess(contentType:String):google.apps.script.drive.Access;
	function getSize(contentType:String):Int;
	function setContent(content:String):google.apps.script.drive.File;
	function getParents(contentType:String):google.apps.script.drive.FolderIterator;
	function getName(contentType:String):String;
	function setDescription(description:String):google.apps.script.drive.File;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.File {})
	function addCommenter(emailAddress:String):google.apps.script.drive.File;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.File {})
	function addViewer(emailAddress:String):google.apps.script.drive.File;
	function getViewers(contentType:String):Array<google.apps.script.drive.User>;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.File {})
	function setOwner(emailAddress:String):google.apps.script.drive.File;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.File {})
	function removeEditor(emailAddress:String):google.apps.script.drive.File;
	function getDescription(contentType:String):String;
	function getUrl(contentType:String):String;
	function getLastUpdated(contentType:String):Date;
	function getMimeType(contentType:String):String;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function addViewers(emailAddresses:Array<String>):google.apps.script.drive.File;
	function addCommenters(emailAddresses:Array<String>):google.apps.script.drive.File;
	function getDateCreated(contentType:String):Date;
	@:overload(function (destination:google.apps.script.drive.Folder):google.apps.script.drive.File {})
	@:overload(function (name:String):google.apps.script.drive.File {})
	@:overload(function (name:String,destination:google.apps.script.drive.Folder):google.apps.script.drive.File {})
	function makeCopy(contentType:String):google.apps.script.drive.File;
	function setShareableByEditors(shareable:Bool):google.apps.script.drive.File;
	function getId(contentType:String):String;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.File {})
	function revokePermissions(user:String):google.apps.script.drive.File;
	function getThumbnail(contentType:String):google.apps.script.base.Blob;
	function isTrashed(contentType:String):Bool;
	function getAs(contentType:String):google.apps.script.base.Blob;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.Permission {})
	function getAccess(email:String):google.apps.script.drive.Permission;
	function setName(name:String):google.apps.script.drive.File;
	function setTrashed(trashed:Bool):google.apps.script.drive.File;
	function getDownloadUrl(contentType:String):String;
	function getEditors(contentType:String):Array<google.apps.script.drive.User>;
	function setSharing(accessType:google.apps.script.drive.Access,permissionType:google.apps.script.drive.Permission):google.apps.script.drive.File;
	function isStarred(contentType:String):Bool;
	function setStarred(starred:Bool):google.apps.script.drive.File;
	function getOwner(contentType:String):google.apps.script.drive.User;
	function isShareableByEditors(contentType:String):Bool;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.File {})
	function addEditor(emailAddress:String):google.apps.script.drive.File;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.File {})
	function removeViewer(emailAddress:String):google.apps.script.drive.File;
	function getSharingPermission(contentType:String):google.apps.script.drive.Permission;
}
