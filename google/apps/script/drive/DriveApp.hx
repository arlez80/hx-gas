package google.apps.script.drive;

@:require(hx_gas)
@:native("DriveApp.Access")
@:enum abstract Access(String) from String to String
{
	var ANYONE;
	var ANYONE_WITH_LINK;
	var DOMAIN;
	var DOMAIN_WITH_LINK;
	var PRIVATE;
}

@:require(hx_gas)
@:native("DriveApp.Permission")
@:enum abstract Permission(String) from String to String
{
	var VIEW;
	var EDIT;
	var COMMENT;
	var OWNER;
	var ORGANIZER;
	var NONE;
}

@:native("DriveApp")
extern class DriveApp
{
	static function addFolder(child:google.apps.script.drive.Folder):google.apps.script.drive.Folder;
	static function getTrashedFiles(name:String):google.apps.script.drive.FileIterator;
	static function continueFileIterator(continuationToken:String):google.apps.script.drive.FileIterator;
	static function createFolder(name:String):google.apps.script.drive.Folder;
	static function getFileById(id:String):google.apps.script.drive.File;
	static function searchFolders(params:String):google.apps.script.drive.FolderIterator;
	static function removeFolder(child:google.apps.script.drive.Folder):google.apps.script.drive.Folder;
	static function getFiles(id:String):google.apps.script.drive.FileIterator;
	static function addFile(child:google.apps.script.drive.File):google.apps.script.drive.Folder;
	static function getStorageUsed(name:String):Int;
	static function removeFile(child:google.apps.script.drive.File):google.apps.script.drive.Folder;
	static function getFoldersByName(name:String):google.apps.script.drive.FolderIterator;
	static function continueFolderIterator(continuationToken:String):google.apps.script.drive.FolderIterator;
	static function getTrashedFolders(name:String):google.apps.script.drive.FolderIterator;
	static function getStorageLimit(name:String):Int;
	static function getFolderById(id:String):google.apps.script.drive.Folder;
	@:overload(function (name:String,content:String):google.apps.script.drive.File {})
	@:overload(function (name:String,content:String,mimeType:String):google.apps.script.drive.File {})
	static function createFile(blob:google.apps.script.base.BlobSource):google.apps.script.drive.File;
	static function getFilesByType(mimeType:String):google.apps.script.drive.FileIterator;
	static function getFolders(id:String):google.apps.script.drive.FolderIterator;
	static function getFilesByName(name:String):google.apps.script.drive.FileIterator;
	static function searchFiles(params:String):google.apps.script.drive.FileIterator;
	static function getRootFolder(name:String):google.apps.script.drive.Folder;
}
