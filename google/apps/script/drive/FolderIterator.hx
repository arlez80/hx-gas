package google.apps.script.drive;

@:native("FolderIterator")
extern class FolderIterator
{
	function next(trashed:Bool):google.apps.script.drive.Folder;
	function getContinuationToken(trashed:Bool):String;
	function hasNext(trashed:Bool):Bool;
}
