package google.apps.script.drive;

@:native("Folder")
extern class Folder
{
	function getUrl(name:String):String;
	function getSize(name:String):Int;
	function getEditors(user:google.apps.script.base.User):Array<google.apps.script.drive.User>;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.Folder {})
	function addViewer(emailAddress:String):google.apps.script.drive.Folder;
	function getViewers(name:String):Array<google.apps.script.drive.User>;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.Folder {})
	function removeEditor(emailAddress:String):google.apps.script.drive.Folder;
	function getFoldersByName(name:String):google.apps.script.drive.FolderIterator;
	function createFolder(name:String):google.apps.script.drive.Folder;
	function getDateCreated(user:google.apps.script.base.User):Date;
	function searchFolders(params:String):google.apps.script.drive.FolderIterator;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.Folder {})
	function revokePermissions(user:String):google.apps.script.drive.Folder;
	function getFolders(mimeType:String):google.apps.script.drive.FolderIterator;
	function getParents(name:String):google.apps.script.drive.FolderIterator;
	function getLastUpdated(name:String):Date;
	function setSharing(accessType:google.apps.script.drive.Access,permissionType:google.apps.script.drive.Permission):google.apps.script.drive.Folder;
	function getOwner(name:String):google.apps.script.drive.User;
	function isShareableByEditors(name:String):Bool;
	@:overload(function (name:String,content:String):google.apps.script.drive.File {})
	@:overload(function (name:String,content:String,mimeType:String):google.apps.script.drive.File {})
	function createFile(blob:google.apps.script.base.BlobSource):google.apps.script.drive.File;
	function getFilesByType(mimeType:String):google.apps.script.drive.FileIterator;
	function removeFolder(child:google.apps.script.drive.Folder):google.apps.script.drive.Folder;
	function getFilesByName(name:String):google.apps.script.drive.FileIterator;
	function searchFiles(params:String):google.apps.script.drive.FileIterator;
	function isStarred(name:String):Bool;
	function getSharingAccess(name:String):google.apps.script.drive.Access;
	function addFolder(child:google.apps.script.drive.Folder):google.apps.script.drive.Folder;
	function getName(name:String):String;
	function setDescription(description:String):google.apps.script.drive.Folder;
	function addEditors(emailAddresses:Array<String>):google.apps.script.drive.Folder;
	function getDescription(user:google.apps.script.base.User):String;
	function setTrashed(trashed:Bool):google.apps.script.drive.Folder;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.Folder {})
	function addEditor(emailAddress:String):google.apps.script.drive.Folder;
	function addViewers(emailAddresses:Array<String>):google.apps.script.drive.Folder;
	function setShareableByEditors(shareable:Bool):google.apps.script.drive.Folder;
	function getId(name:String):String;
	function setStarred(starred:Bool):google.apps.script.drive.Folder;
	function getFiles(user:google.apps.script.base.User):google.apps.script.drive.FileIterator;
	function addFile(child:google.apps.script.drive.File):google.apps.script.drive.Folder;
	function removeFile(child:google.apps.script.drive.File):google.apps.script.drive.Folder;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.Permission {})
	function getAccess(email:String):google.apps.script.drive.Permission;
	function setName(name:String):google.apps.script.drive.Folder;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.Folder {})
	function setOwner(emailAddress:String):google.apps.script.drive.Folder;
	function isTrashed(name:String):Bool;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.drive.Folder {})
	function removeViewer(emailAddress:String):google.apps.script.drive.Folder;
	function getSharingPermission(name:String):google.apps.script.drive.Permission;
}
