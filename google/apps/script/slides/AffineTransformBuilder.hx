package google.apps.script.slides;

@:native("AffineTransformBuilder")
extern class AffineTransformBuilder
{
	function setTranslateX(translateX:Float):google.apps.script.slides.AffineTransformBuilder;
	function setShearX(shearX:Float):google.apps.script.slides.AffineTransformBuilder;
	function setShearY(shearY:Float):google.apps.script.slides.AffineTransformBuilder;
	function setScaleX(scaleX:Float):google.apps.script.slides.AffineTransformBuilder;
	function setTranslateY(translateY:Float):google.apps.script.slides.AffineTransformBuilder;
	function build(url:String):google.apps.script.slides.AffineTransform;
	function setScaleY(scaleY:Float):google.apps.script.slides.AffineTransformBuilder;
}
