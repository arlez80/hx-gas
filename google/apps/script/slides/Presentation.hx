package google.apps.script.slides;

@:native("Presentation")
extern class Presentation
{
	function getMasters(slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):Array<google.apps.script.slides.Master>;
	function getNotesMaster(slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):google.apps.script.slides.NotesMaster;
	function getPageElementById(id:String):google.apps.script.slides.PageElement;
	function getEditors(slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):Array<google.apps.script.base.User>;
	function saveAndClose(findText:String,replaceText:String,matchCase:Bool):Void;
	function addEditors(emailAddresses:Array<String>):google.apps.script.slides.Presentation;
	function getViewers(id:String):Array<google.apps.script.base.User>;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.slides.Presentation {})
	function removeEditor(emailAddress:String):google.apps.script.slides.Presentation;
	function getUrl(id:String):String;
	function getPageWidth(id:String):Float;
	function getSelection(id:String):google.apps.script.slides.Selection;
	function getSlideById(id:String):google.apps.script.slides.Slide;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.slides.Presentation {})
	function addViewer(emailAddress:String):google.apps.script.slides.Presentation;
	@:overload(function (layout:google.apps.script.slides.Layout):google.apps.script.slides.Slide {})
	@:overload(function (predefinedLayout:google.apps.script.slides.PredefinedLayout):google.apps.script.slides.Slide {})
	@:overload(function (slide:google.apps.script.slides.Slide):google.apps.script.slides.Slide {})
	@:overload(function (slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):google.apps.script.slides.Slide {})
	function appendSlide(emailAddresses:Array<String>):google.apps.script.slides.Slide;
	function getPageHeight(id:String):Float;
	function getNotesPageWidth(slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):Float;
	function getId(slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):String;
	function getSlides(id:String):Array<google.apps.script.slides.Slide>;
	@:overload(function (insertionIndex:Int,layout:google.apps.script.slides.Layout):google.apps.script.slides.Slide {})
	@:overload(function (insertionIndex:Int,predefinedLayout:google.apps.script.slides.PredefinedLayout):google.apps.script.slides.Slide {})
	@:overload(function (insertionIndex:Int,slide:google.apps.script.slides.Slide):google.apps.script.slides.Slide {})
	@:overload(function (insertionIndex:Int,slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):google.apps.script.slides.Slide {})
	function insertSlide(insertionIndex:Int):google.apps.script.slides.Slide;
	@:overload(function (findText:String,replaceText:String,matchCase:Bool):Int {})
	function replaceAllText(findText:String,replaceText:String):Int;
	function getName(slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):String;
	function getNotesPageHeight(slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):Float;
	function setName(name:String):Void;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.slides.Presentation {})
	function addEditor(emailAddress:String):google.apps.script.slides.Presentation;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.slides.Presentation {})
	function removeViewer(emailAddress:String):google.apps.script.slides.Presentation;
	function addViewers(emailAddresses:Array<String>):google.apps.script.slides.Presentation;
	function getLayouts(slide:google.apps.script.slides.Slide,linkingMode:google.apps.script.slides.SlideLinkingMode):Array<google.apps.script.slides.Layout>;
}
