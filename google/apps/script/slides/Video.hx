package google.apps.script.slides;

@:native("Video")
extern class Video
{
	function getHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function setDescription(description:String):google.apps.script.slides.Video;
	function getUrl(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function setTitle(title:String):google.apps.script.slides.Video;
	function getObjectId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getBorder(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Border;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Video;
	function setRotation(angle:Float):google.apps.script.slides.Video;
	function getDescription(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Video;
	function getPageElementType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElementType;
	function getParentGroup(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getLeft(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getConnectionSites(alignmentPosition:google.apps.script.slides.AlignmentPosition):Array<google.apps.script.slides.ConnectionSite>;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Video;
	function setLeft(left:Float):google.apps.script.slides.Video;
	function setTop(top:Float):google.apps.script.slides.Video;
	function setWidth(width:Float):google.apps.script.slides.Video;
	function sendBackward(replace:Bool):google.apps.script.slides.Video;
	function setHeight(height:Float):google.apps.script.slides.Video;
	function getInherentWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getInherentHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Video;
	function getRotation(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getParentPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Page;
	function getTop(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function sendToBack(replace:Bool):google.apps.script.slides.Video;
	function getTitle(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getTransform(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.AffineTransform;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Video;
	function scaleHeight(ratio:Float):google.apps.script.slides.Video;
	function getThumbnailUrl(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getSource(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.VideoSourceType;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	function getVideoId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function scaleWidth(ratio:Float):google.apps.script.slides.Video;
}
