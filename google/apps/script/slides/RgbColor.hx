package google.apps.script.slides;

@:native("RgbColor")
extern class RgbColor
{
	function getGreen(name:String):Int;
	function asHexString(name:String):String;
	function getColorType(name:String):google.apps.script.slides.ColorType;
	function getRed(name:String):Int;
	function getBlue(name:String):Int;
}
