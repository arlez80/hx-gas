package google.apps.script.slides;

@:require(hx_gas)
@:native("TextDirection")
@:enum abstract TextDirection(String) from String to String
{
	var UNSUPPORTED;
	var LEFT_TO_RIGHT;
	var RIGHT_TO_LEFT;
}

