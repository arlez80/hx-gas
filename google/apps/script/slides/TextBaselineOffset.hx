package google.apps.script.slides;

@:require(hx_gas)
@:native("TextBaselineOffset")
@:enum abstract TextBaselineOffset(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var SUPERSCRIPT;
	var SUBSCRIPT;
}

