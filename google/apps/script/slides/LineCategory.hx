package google.apps.script.slides;

@:require(hx_gas)
@:native("LineCategory")
@:enum abstract LineCategory(String) from String to String
{
	var UNSUPPORTED;
	var STRAIGHT;
	var BENT;
	var CURVED;
}

