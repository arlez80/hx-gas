package google.apps.script.slides;

@:native("ThemeColor")
extern class ThemeColor
{
	function getColorType(underline:Bool):google.apps.script.slides.ColorType;
	function getThemeColorType(underline:Bool):google.apps.script.slides.ThemeColorType;
}
