package google.apps.script.slides;

@:require(hx_gas)
@:native("CellMergeState")
@:enum abstract CellMergeState(String) from String to String
{
	var NORMAL;
	var HEAD;
	var MERGED;
}

