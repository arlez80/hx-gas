package google.apps.script.slides;

@:native("PictureFill")
extern class PictureFill
{
	function getAs(contentType:String):google.apps.script.base.Blob;
	function getSourceUrl(contentType:String):String;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function getContentUrl(contentType:String):String;
}
