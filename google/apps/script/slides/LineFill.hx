package google.apps.script.slides;

@:native("LineFill")
extern class LineFill
{
	@:overload(function (color:google.apps.script.slides.Color,alpha:Float):Void {})
	@:overload(function (red:Int,green:Int,blue:Int):Void {})
	@:overload(function (red:Int,green:Int,blue:Int,alpha:Float):Void {})
	@:overload(function (hexString:String):Void {})
	@:overload(function (hexString:String,alpha:Float):Void {})
	@:overload(function (color:google.apps.script.slides.ThemeColorType):Void {})
	@:overload(function (color:google.apps.script.slides.ThemeColorType,alpha:Float):Void {})
	function setSolidFill(color:google.apps.script.slides.Color):Void;
	function getSolidFill(width:Float):google.apps.script.slides.SolidFill;
	function getFillType(width:Float):google.apps.script.slides.LineFillType;
}
