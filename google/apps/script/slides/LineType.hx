package google.apps.script.slides;

@:require(hx_gas)
@:native("LineType")
@:enum abstract LineType(String) from String to String
{
	var UNSUPPORTED;
	var STRAIGHT_CONNECTOR_1;
	var BENT_CONNECTOR_2;
	var BENT_CONNECTOR_3;
	var BENT_CONNECTOR_4;
	var BENT_CONNECTOR_5;
	var CURVED_CONNECTOR_2;
	var CURVED_CONNECTOR_3;
	var CURVED_CONNECTOR_4;
	var CURVED_CONNECTOR_5;
	var STRAIGHT_LINE;
}

