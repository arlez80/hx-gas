package google.apps.script.slides;

@:require(hx_gas)
@:native("FillType")
@:enum abstract FillType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var SOLID;
}

