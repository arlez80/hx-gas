package google.apps.script.slides;

@:native("TableRow")
extern class TableRow
{
	function getParentTable(cellIndex:Int):google.apps.script.slides.Table;
	function getIndex(cellIndex:Int):Int;
	function getMinimumHeight(cellIndex:Int):Float;
	function getCell(cellIndex:Int):google.apps.script.slides.TableCell;
	function getNumCells(cellIndex:Int):Int;
	function remove(cellIndex:Int):Void;
}
