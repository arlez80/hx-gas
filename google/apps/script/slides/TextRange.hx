package google.apps.script.slides;

@:native("TextRange")
extern class TextRange
{
	function asString(text:String):String;
	function asRenderedString(text:String):String;
	function isEmpty(startOffset:Int,text:String):Bool;
	function setText(newText:String):google.apps.script.slides.TextRange;
	function getRange(startOffset:Int,endOffset:Int):google.apps.script.slides.TextRange;
	function getParagraphs(pattern:String,startOffset:Int):Array<google.apps.script.slides.Paragraph>;
	function getRuns(startOffset:Int,endOffset:Int):Array<google.apps.script.slides.TextRange>;
	function getStartIndex(startOffset:Int,endOffset:Int):Int;
	function getParagraphStyle(pattern:String,startOffset:Int):google.apps.script.slides.ParagraphStyle;
	function getLinks(pattern:String,startOffset:Int):Array<google.apps.script.slides.TextRange>;
	function appendText(text:String):google.apps.script.slides.TextRange;
	@:overload(function (startOffset:Int,endOffset:Int):Void {})
	function clear(text:String):Void;
	function insertParagraph(startOffset:Int,text:String):google.apps.script.slides.Paragraph;
	@:overload(function (pattern:String,startOffset:Int):Array<google.apps.script.slides.TextRange> {})
	function find(pattern:String):Array<google.apps.script.slides.TextRange>;
	function getListParagraphs(pattern:String,startOffset:Int):Array<google.apps.script.slides.Paragraph>;
	function getEndIndex(pattern:String,startOffset:Int):Int;
	function getListStyle(pattern:String,startOffset:Int):google.apps.script.slides.ListStyle;
	function getLength(pattern:String,startOffset:Int):Int;
	function getTextStyle(startOffset:Int,endOffset:Int):google.apps.script.slides.TextStyle;
	@:overload(function (startOffset:Int,textRange:google.apps.script.slides.TextRange,matchSourceFormatting:Bool):google.apps.script.slides.TextRange {})
	function insertRange(startOffset:Int,textRange:google.apps.script.slides.TextRange):google.apps.script.slides.TextRange;
	@:overload(function (textRange:google.apps.script.slides.TextRange,matchSourceFormatting:Bool):google.apps.script.slides.TextRange {})
	function appendRange(textRange:google.apps.script.slides.TextRange):google.apps.script.slides.TextRange;
	@:overload(function (findText:String,replaceText:String,matchCase:Bool):Int {})
	function replaceAllText(findText:String,replaceText:String):Int;
	function getAutoTexts(pattern:String,startOffset:Int):Array<google.apps.script.slides.AutoText>;
	function insertText(startOffset:Int,text:String):google.apps.script.slides.TextRange;
	function appendParagraph(text:String):google.apps.script.slides.Paragraph;
	function select(findText:String,replaceText:String,matchCase:Bool):Void;
}
