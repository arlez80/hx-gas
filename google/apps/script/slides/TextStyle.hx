package google.apps.script.slides;

@:native("TextStyle")
extern class TextStyle
{
	@:overload(function (red:Int,green:Int,blue:Int):google.apps.script.slides.TextStyle {})
	@:overload(function (hexColor:String):google.apps.script.slides.TextStyle {})
	@:overload(function (color:google.apps.script.slides.ThemeColorType):google.apps.script.slides.TextStyle {})
	function setBackgroundColor(color:google.apps.script.slides.Color):google.apps.script.slides.TextStyle;
	function setUnderline(underline:Bool):google.apps.script.slides.TextStyle;
	function getForegroundColor(newText:String):google.apps.script.slides.Color;
	function getFontSize(newText:String):Float;
	function setBold(bold:Bool):google.apps.script.slides.TextStyle;
	function isUnderline(newText:String):Bool;
	function isBold(newText:String):Bool;
	function isItalic(newText:String):Bool;
	function removeLink(newText:String):google.apps.script.slides.TextStyle;
	function setBackgroundColorTransparent(color:google.apps.script.slides.ThemeColorType):google.apps.script.slides.TextStyle;
	function setFontSize(fontSize:Float):google.apps.script.slides.TextStyle;
	function getBackgroundColor(newText:String):google.apps.script.slides.Color;
	function setFontFamily(fontFamily:String):google.apps.script.slides.TextStyle;
	function isBackgroundTransparent(newText:String):Bool;
	function setItalic(italic:Bool):google.apps.script.slides.TextStyle;
	function getLink(newText:String):google.apps.script.slides.Link;
	function isStrikethrough(newText:String):Bool;
	function getFontFamily(newText:String):String;
	function setFontFamilyAndWeight(fontFamily:String,fontWeight:Int):google.apps.script.slides.TextStyle;
	function setSmallCaps(smallCaps:Bool):google.apps.script.slides.TextStyle;
	@:overload(function (red:Int,green:Int,blue:Int):google.apps.script.slides.TextStyle {})
	@:overload(function (hexColor:String):google.apps.script.slides.TextStyle {})
	@:overload(function (color:google.apps.script.slides.ThemeColorType):google.apps.script.slides.TextStyle {})
	function setForegroundColor(foregroundColor:google.apps.script.slides.Color):google.apps.script.slides.TextStyle;
	function isSmallCaps(newText:String):Bool;
	function setBaselineOffset(offset:google.apps.script.slides.TextBaselineOffset):google.apps.script.slides.TextStyle;
	function getBaselineOffset(newText:String):google.apps.script.slides.TextBaselineOffset;
	function getFontWeight(newText:String):Int;
	function setLinkUrl(url:String):google.apps.script.slides.TextStyle;
	@:overload(function (slide:google.apps.script.slides.Slide):google.apps.script.slides.TextStyle {})
	@:overload(function (slidePosition:google.apps.script.slides.SlidePosition):google.apps.script.slides.TextStyle {})
	function setLinkSlide(slideIndex:Int):google.apps.script.slides.TextStyle;
	function setStrikethrough(strikethrough:Bool):google.apps.script.slides.TextStyle;
	function hasLink(newText:String):Bool;
}
