package google.apps.script.slides;

@:native("AffineTransform")
extern class AffineTransform
{
	function getScaleY(url:String):Float;
	function getTranslateX(url:String):Float;
	function toBuilder(url:String):google.apps.script.slides.AffineTransformBuilder;
	function getShearX(url:String):Float;
	function getScaleX(url:String):Float;
	function getTranslateY(url:String):Float;
	function getShearY(url:String):Float;
}
