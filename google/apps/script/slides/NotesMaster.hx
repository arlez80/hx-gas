package google.apps.script.slides;

@:native("NotesMaster")
extern class NotesMaster
{
	function getSheetsCharts(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.SheetsChart>;
	function getPlaceholders(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.PageElement>;
	function getShapes(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Shape>;
	function getPageElementById(id:String):google.apps.script.slides.PageElement;
	@:overload(function (placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):google.apps.script.slides.PageElement {})
	function getPlaceholder(placeholderType:google.apps.script.slides.PlaceholderType):google.apps.script.slides.PageElement;
	function getPageElements(id:String):Array<google.apps.script.slides.PageElement>;
	function getGroups(findText:String,replaceText:String,matchCase:Bool):Array<google.apps.script.slides.Group>;
	function getImages(findText:String,replaceText:String,matchCase:Bool):Array<google.apps.script.slides.Image>;
	function getWordArts(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.WordArt>;
	function getLines(findText:String,replaceText:String,matchCase:Bool):Array<google.apps.script.slides.Line>;
	function getVideos(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Video>;
	function getObjectId(findText:String,replaceText:String,matchCase:Bool):String;
	function getTables(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Table>;
}
