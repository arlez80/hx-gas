package google.apps.script.slides;

@:native("PageRange")
extern class PageRange
{
	function getPages(width:Float):Array<google.apps.script.slides.Page>;
}
