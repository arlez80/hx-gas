package google.apps.script.slides;

@:require(hx_gas)
@:native("SheetsChartEmbedType")
@:enum abstract SheetsChartEmbedType(String) from String to String
{
	var UNSUPPORTED;
	var IMAGE;
}

