package google.apps.script.slides;

@:native("SheetsChart")
extern class SheetsChart
{
	function getHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function scaleHeight(ratio:Float):google.apps.script.slides.SheetsChart;
	function sendBackward(replace:Bool):google.apps.script.slides.SheetsChart;
	function getObjectId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.SheetsChart;
	function setRotation(angle:Float):google.apps.script.slides.SheetsChart;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	function removeLink(transform:google.apps.script.slides.AffineTransform):Void;
	function getDescription(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.SheetsChart;
	function getPageElementType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElementType;
	function getParentGroup(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getLeft(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getConnectionSites(alignmentPosition:google.apps.script.slides.AlignmentPosition):Array<google.apps.script.slides.ConnectionSite>;
	function getInherentWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function asImage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Image;
	function setLeft(left:Float):google.apps.script.slides.SheetsChart;
	function setTop(top:Float):google.apps.script.slides.SheetsChart;
	function setWidth(width:Float):google.apps.script.slides.SheetsChart;
	function getLink(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Link;
	function getChartId(alignmentPosition:google.apps.script.slides.AlignmentPosition):Int;
	function setHeight(height:Float):google.apps.script.slides.SheetsChart;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.SheetsChart;
	function getEmbedType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.SheetsChartEmbedType;
	function getInherentHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.SheetsChart;
	function getRotation(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function setTitle(title:String):google.apps.script.slides.SheetsChart;
	function getParentPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Page;
	function getTop(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function sendToBack(replace:Bool):google.apps.script.slides.SheetsChart;
	function getTitle(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getSpreadsheetId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.SheetsChart;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
	function setDescription(description:String):google.apps.script.slides.SheetsChart;
	function refresh(transform:google.apps.script.slides.AffineTransform):Void;
	function getTransform(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.AffineTransform;
	function setLinkUrl(url:String):google.apps.script.slides.Link;
	@:overload(function (slide:google.apps.script.slides.Slide):google.apps.script.slides.Link {})
	@:overload(function (slidePosition:google.apps.script.slides.SlidePosition):google.apps.script.slides.Link {})
	function setLinkSlide(slideIndex:Int):google.apps.script.slides.Link;
	function getWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function scaleWidth(ratio:Float):google.apps.script.slides.SheetsChart;
}
