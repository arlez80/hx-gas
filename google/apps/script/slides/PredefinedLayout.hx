package google.apps.script.slides;

@:require(hx_gas)
@:native("PredefinedLayout")
@:enum abstract PredefinedLayout(String) from String to String
{
	var UNSUPPORTED;
	var BLANK;
	var CAPTION_ONLY;
	var TITLE;
	var TITLE_AND_BODY;
	var TITLE_AND_TWO_COLUMNS;
	var TITLE_ONLY;
	var SECTION_HEADER;
	var SECTION_TITLE_AND_DESCRIPTION;
	var ONE_COLUMN_TEXT;
	var MAIN_POINT;
	var BIG_NUMBER;
}

