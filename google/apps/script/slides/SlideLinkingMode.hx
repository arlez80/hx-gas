package google.apps.script.slides;

@:require(hx_gas)
@:native("SlideLinkingMode")
@:enum abstract SlideLinkingMode(String) from String to String
{
	var UNSUPPORTED;
	var LINKED;
	var NOT_LINKED;
}

