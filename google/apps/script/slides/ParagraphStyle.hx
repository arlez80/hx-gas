package google.apps.script.slides;

@:native("ParagraphStyle")
extern class ParagraphStyle
{
	function getTextDirection(width:Float):google.apps.script.slides.TextDirection;
	function setSpaceAbove(space:Float):google.apps.script.slides.ParagraphStyle;
	function setIndentStart(indent:Float):google.apps.script.slides.ParagraphStyle;
	function getSpaceBelow(width:Float):Float;
	function setSpacingMode(mode:google.apps.script.slides.SpacingMode):google.apps.script.slides.ParagraphStyle;
	function setParagraphAlignment(alignment:google.apps.script.slides.ParagraphAlignment):google.apps.script.slides.ParagraphStyle;
	function getIndentEnd(width:Float):Float;
	function getSpaceAbove(width:Float):Float;
	function setSpaceBelow(space:Float):google.apps.script.slides.ParagraphStyle;
	function setTextDirection(direction:google.apps.script.slides.TextDirection):google.apps.script.slides.ParagraphStyle;
	function getSpacingMode(width:Float):google.apps.script.slides.SpacingMode;
	function getParagraphAlignment(width:Float):google.apps.script.slides.ParagraphAlignment;
	function setIndentEnd(indent:Float):google.apps.script.slides.ParagraphStyle;
	function getLineSpacing(width:Float):Float;
	function getIndentStart(width:Float):Float;
	function setLineSpacing(spacing:Float):google.apps.script.slides.ParagraphStyle;
	function getIndentFirstLine(width:Float):Float;
	function setIndentFirstLine(indent:Float):google.apps.script.slides.ParagraphStyle;
}
