package google.apps.script.slides;

@:native("Shape")
extern class Shape
{
	function getHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function replaceWithSheetsChartAsImage(sourceChart:google.apps.script.spreadsheet.EmbeddedChart):google.apps.script.slides.Image;
	function setTitle(title:String):google.apps.script.slides.Shape;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Shape;
	function setRotation(angle:Float):google.apps.script.slides.Shape;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Shape;
	function setLeft(left:Float):google.apps.script.slides.Shape;
	function getLink(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Link;
	function sendBackward(replace:Bool):google.apps.script.slides.Shape;
	function getInherentHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getShapeType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.ShapeType;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getParentPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Page;
	function getWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	function setTop(top:Float):google.apps.script.slides.Shape;
	function replaceWithSheetsChart(sourceChart:google.apps.script.spreadsheet.EmbeddedChart):google.apps.script.slides.SheetsChart;
	function setDescription(description:String):google.apps.script.slides.Shape;
	function getTransform(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.AffineTransform;
	function scaleWidth(ratio:Float):google.apps.script.slides.Shape;
	function getPlaceholderType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PlaceholderType;
	function getText(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.TextRange;
	function getBorder(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Border;
	function getParentPlaceholder(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function removeLink(transform:google.apps.script.slides.AffineTransform):Void;
	function getDescription(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Shape;
	function getPageElementType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElementType;
	function getParentGroup(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getLeft(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getConnectionSites(alignmentPosition:google.apps.script.slides.AlignmentPosition):Array<google.apps.script.slides.ConnectionSite>;
	function setContentAlignment(contentAlignment:google.apps.script.slides.ContentAlignment):google.apps.script.slides.Shape;
	function setWidth(width:Float):google.apps.script.slides.Shape;
	function setHeight(height:Float):google.apps.script.slides.Shape;
	function getInherentWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Shape;
	function getRotation(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getObjectId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getTop(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getPlaceholderIndex(alignmentPosition:google.apps.script.slides.AlignmentPosition):Int;
	function sendToBack(replace:Bool):google.apps.script.slides.Shape;
	function getTitle(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Shape;
	function scaleHeight(ratio:Float):google.apps.script.slides.Shape;
	function getFill(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Fill;
	@:overload(function (blobSource:google.apps.script.base.BlobSource,crop:Bool):google.apps.script.slides.Image {})
	@:overload(function (imageUrl:String):google.apps.script.slides.Image {})
	@:overload(function (imageUrl:String,crop:Bool):google.apps.script.slides.Image {})
	function replaceWithImage(blobSource:google.apps.script.base.BlobSource):google.apps.script.slides.Image;
	function setLinkUrl(url:String):google.apps.script.slides.Link;
	@:overload(function (slide:google.apps.script.slides.Slide):google.apps.script.slides.Link {})
	@:overload(function (slidePosition:google.apps.script.slides.SlidePosition):google.apps.script.slides.Link {})
	function setLinkSlide(slideIndex:Int):google.apps.script.slides.Link;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
	function getContentAlignment(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.ContentAlignment;
}
