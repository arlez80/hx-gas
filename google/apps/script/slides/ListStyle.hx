package google.apps.script.slides;

@:native("ListStyle")
extern class ListStyle
{
	function getGlyph(listPreset:google.apps.script.slides.ListPreset):String;
	function getList(listPreset:google.apps.script.slides.ListPreset):google.apps.script.slides.List;
	function removeFromList(listPreset:google.apps.script.slides.ListPreset):google.apps.script.slides.ListStyle;
	function applyListPreset(listPreset:google.apps.script.slides.ListPreset):google.apps.script.slides.ListStyle;
	function getNestingLevel(listPreset:google.apps.script.slides.ListPreset):Int;
	function isInList(listPreset:google.apps.script.slides.ListPreset):Bool;
}
