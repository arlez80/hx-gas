package google.apps.script.slides;

@:native("Fill")
extern class Fill
{
	function setTransparent(color:google.apps.script.slides.ThemeColorType,alpha:Float):Void;
	function isVisible(theme:google.apps.script.slides.ThemeColorType):Bool;
	function getSolidFill(theme:google.apps.script.slides.ThemeColorType):google.apps.script.slides.SolidFill;
	function getType(theme:google.apps.script.slides.ThemeColorType):google.apps.script.slides.FillType;
	@:overload(function (color:google.apps.script.slides.Color,alpha:Float):Void {})
	@:overload(function (red:Int,green:Int,blue:Int):Void {})
	@:overload(function (red:Int,green:Int,blue:Int,alpha:Float):Void {})
	@:overload(function (hexString:String):Void {})
	@:overload(function (hexString:String,alpha:Float):Void {})
	@:overload(function (color:google.apps.script.slides.ThemeColorType):Void {})
	@:overload(function (color:google.apps.script.slides.ThemeColorType,alpha:Float):Void {})
	function setSolidFill(color:google.apps.script.slides.Color):Void;
}
