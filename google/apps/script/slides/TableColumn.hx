package google.apps.script.slides;

@:native("TableColumn")
extern class TableColumn
{
	function getParentTable(cellIndex:Int):google.apps.script.slides.Table;
	function getIndex(cellIndex:Int):Int;
	function getCell(cellIndex:Int):google.apps.script.slides.TableCell;
	function remove(cellIndex:Int):Void;
	function getNumCells(cellIndex:Int):Int;
	function getWidth(cellIndex:Int):Float;
}
