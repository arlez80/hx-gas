package google.apps.script.slides;

@:native("SolidFill")
extern class SolidFill
{
	function getAlpha(findText:String,replaceText:String,matchCase:Bool):Float;
	function getColor(findText:String,replaceText:String,matchCase:Bool):google.apps.script.slides.Color;
}
