package google.apps.script.slides;

@:native("Group")
extern class Group
{
	function getHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function setTitle(title:String):google.apps.script.slides.Group;
	function ungroup(width:Float):Void;
	function getObjectId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getChildren(alignmentPosition:google.apps.script.slides.AlignmentPosition):Array<google.apps.script.slides.PageElement>;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Group;
	function setRotation(angle:Float):google.apps.script.slides.Group;
	function getDescription(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getPageElementType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElementType;
	function getParentGroup(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getLeft(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getConnectionSites(alignmentPosition:google.apps.script.slides.AlignmentPosition):Array<google.apps.script.slides.ConnectionSite>;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Group;
	function setLeft(left:Float):google.apps.script.slides.Group;
	function setTop(top:Float):google.apps.script.slides.Group;
	function setWidth(width:Float):google.apps.script.slides.Group;
	function sendBackward(replace:Bool):google.apps.script.slides.Group;
	function setHeight(height:Float):google.apps.script.slides.Group;
	function getInherentWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getInherentHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getRotation(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getParentPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Page;
	function getTop(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function sendToBack(replace:Bool):google.apps.script.slides.Group;
	function getTitle(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getTransform(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.AffineTransform;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function scaleHeight(ratio:Float):google.apps.script.slides.Group;
	function setDescription(description:String):google.apps.script.slides.Group;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	function getWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function scaleWidth(ratio:Float):google.apps.script.slides.Group;
}
