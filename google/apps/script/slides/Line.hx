package google.apps.script.slides;

@:native("Line")
extern class Line
{
	function setWidth(width:Float):google.apps.script.slides.Line;
	function getHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function setTitle(title:String):google.apps.script.slides.Line;
	@:overload(function (point:google.apps.script.slides.Point):google.apps.script.slides.Line {})
	function setStart(left:Float,top:Float):google.apps.script.slides.Line;
	function getLineCategory(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.LineCategory;
	function setDashStyle(style:google.apps.script.slides.DashStyle):google.apps.script.slides.Line;
	function getLineFill(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.LineFill;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Line;
	function setEndConnection(connectionSite:google.apps.script.slides.ConnectionSite):google.apps.script.slides.Line;
	function getLink(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Link;
	function sendBackward(replace:Bool):google.apps.script.slides.Line;
	function getInherentHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getDashStyle(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.DashStyle;
	function setLeft(left:Float):google.apps.script.slides.Line;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getParentPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Page;
	function getStartArrow(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.ArrowStyle;
	function setStartConnection(connectionSite:google.apps.script.slides.ConnectionSite):google.apps.script.slides.Line;
	function getWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getStartConnection(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.ConnectionSite;
	function getTransform(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.AffineTransform;
	function setTop(top:Float):google.apps.script.slides.Line;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	@:overload(function (point:google.apps.script.slides.Point):google.apps.script.slides.Line {})
	function setEnd(left:Float,top:Float):google.apps.script.slides.Line;
	function scaleWidth(ratio:Float):google.apps.script.slides.Line;
	function reroute(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Line;
	function getEndArrow(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.ArrowStyle;
	function setLineCategory(lineCategory:google.apps.script.slides.LineCategory):google.apps.script.slides.Line;
	function setWeight(points:Float):google.apps.script.slides.Line;
	function setDescription(description:String):google.apps.script.slides.Line;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Line;
	function getEnd(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Point;
	function setRotation(angle:Float):google.apps.script.slides.Line;
	function removeLink(transform:google.apps.script.slides.AffineTransform):Void;
	function getDescription(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Line;
	function getPageElementType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElementType;
	function getParentGroup(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getLeft(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getConnectionSites(alignmentPosition:google.apps.script.slides.AlignmentPosition):Array<google.apps.script.slides.ConnectionSite>;
	function getWeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getStart(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Point;
	function setHeight(height:Float):google.apps.script.slides.Line;
	function getInherentWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Line;
	function getRotation(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function setStartArrow(style:google.apps.script.slides.ArrowStyle):google.apps.script.slides.Line;
	function getObjectId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getTop(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function isConnector(alignmentPosition:google.apps.script.slides.AlignmentPosition):Bool;
	function sendToBack(replace:Bool):google.apps.script.slides.Line;
	function getTitle(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Line;
	function scaleHeight(ratio:Float):google.apps.script.slides.Line;
	function getLineType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.LineType;
	function getEndConnection(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.ConnectionSite;
	function setLinkUrl(url:String):google.apps.script.slides.Link;
	@:overload(function (slide:google.apps.script.slides.Slide):google.apps.script.slides.Link {})
	@:overload(function (slidePosition:google.apps.script.slides.SlidePosition):google.apps.script.slides.Link {})
	function setLinkSlide(slideIndex:Int):google.apps.script.slides.Link;
	function setEndArrow(style:google.apps.script.slides.ArrowStyle):google.apps.script.slides.Line;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
}
