package google.apps.script.slides;

@:native("Selection")
extern class Selection
{
	function getSelectionType(name:String):google.apps.script.slides.SelectionType;
	function getTableCellRange(name:String):google.apps.script.slides.TableCellRange;
	function getTextRange(name:String):google.apps.script.slides.TextRange;
	function getPageRange(name:String):google.apps.script.slides.PageRange;
	function getCurrentPage(name:String):google.apps.script.slides.Page;
	function getPageElementRange(name:String):google.apps.script.slides.PageElementRange;
}
