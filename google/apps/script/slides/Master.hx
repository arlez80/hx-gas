package google.apps.script.slides;

@:native("Master")
extern class Master
{
	@:overload(function (shapeType:google.apps.script.slides.ShapeType):google.apps.script.slides.Shape {})
	@:overload(function (shapeType:google.apps.script.slides.ShapeType,left:Float,top:Float,width:Float,height:Float):google.apps.script.slides.Shape {})
	function insertShape(shape:google.apps.script.slides.Shape):google.apps.script.slides.Shape;
	function insertWordArt(wordArt:google.apps.script.slides.WordArt):google.apps.script.slides.WordArt;
	function insertGroup(group:google.apps.script.slides.Group):google.apps.script.slides.Group;
	function getPageElementById(id:String):google.apps.script.slides.PageElement;
	@:overload(function (lineCategory:google.apps.script.slides.LineCategory,startConnectionSite:google.apps.script.slides.ConnectionSite,endConnectionSite:google.apps.script.slides.ConnectionSite):google.apps.script.slides.Line {})
	@:overload(function (lineCategory:google.apps.script.slides.LineCategory,startLeft:Float,startTop:Float,endLeft:Float,endTop:Float):google.apps.script.slides.Line {})
	function insertLine(line:google.apps.script.slides.Line):google.apps.script.slides.Line;
	function getPageElements(id:String):Array<google.apps.script.slides.PageElement>;
	function insertPageElement(pageElement:google.apps.script.slides.PageElement):google.apps.script.slides.PageElement;
	function group(pageElements:Array<google.apps.script.slides.PageElement>):google.apps.script.slides.Group;
	function getImages(listPreset:google.apps.script.slides.ListPreset):Array<google.apps.script.slides.Image>;
	@:overload(function (videoUrl:String,left:Float,top:Float,width:Float,height:Float):google.apps.script.slides.Video {})
	@:overload(function (video:google.apps.script.slides.Video):google.apps.script.slides.Video {})
	function insertVideo(videoUrl:String):google.apps.script.slides.Video;
	@:overload(function (blobSource:google.apps.script.base.BlobSource,left:Float,top:Float,width:Float,height:Float):google.apps.script.slides.Image {})
	@:overload(function (image:google.apps.script.slides.Image):google.apps.script.slides.Image {})
	@:overload(function (imageUrl:String):google.apps.script.slides.Image {})
	@:overload(function (imageUrl:String,left:Float,top:Float,width:Float,height:Float):google.apps.script.slides.Image {})
	function insertImage(blobSource:google.apps.script.base.BlobSource):google.apps.script.slides.Image;
	function getPageType(id:String):google.apps.script.slides.PageType;
	function getLines(listPreset:google.apps.script.slides.ListPreset):Array<google.apps.script.slides.Line>;
	function getVideos(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Video>;
	function selectAsCurrentPage(findText:String,replaceText:String,matchCase:Bool):Void;
	function getSheetsCharts(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.SheetsChart>;
	function getPlaceholders(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.PageElement>;
	@:overload(function (numRows:Int,numColumns:Int,left:Float,top:Float,width:Float,height:Float):google.apps.script.slides.Table {})
	@:overload(function (table:google.apps.script.slides.Table):google.apps.script.slides.Table {})
	function insertTable(numRows:Int,numColumns:Int):google.apps.script.slides.Table;
	function getShapes(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Shape>;
	@:overload(function (placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):google.apps.script.slides.PageElement {})
	function getPlaceholder(placeholderType:google.apps.script.slides.PlaceholderType):google.apps.script.slides.PageElement;
	function getColorScheme(listPreset:google.apps.script.slides.ListPreset):google.apps.script.slides.ColorScheme;
	function getObjectId(listPreset:google.apps.script.slides.ListPreset):String;
	function getGroups(listPreset:google.apps.script.slides.ListPreset):Array<google.apps.script.slides.Group>;
	function getBackground(listPreset:google.apps.script.slides.ListPreset):google.apps.script.slides.PageBackground;
	@:overload(function (text:String,left:Float,top:Float,width:Float,height:Float):google.apps.script.slides.Shape {})
	function insertTextBox(text:String):google.apps.script.slides.Shape;
	@:overload(function (sourceChart:google.apps.script.spreadsheet.EmbeddedChart,left:Float,top:Float,width:Float,height:Float):google.apps.script.slides.SheetsChart {})
	@:overload(function (sheetsChart:google.apps.script.slides.SheetsChart):google.apps.script.slides.SheetsChart {})
	function insertSheetsChart(sourceChart:google.apps.script.spreadsheet.EmbeddedChart):google.apps.script.slides.SheetsChart;
	function getWordArts(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.WordArt>;
	@:overload(function (findText:String,replaceText:String,matchCase:Bool):Int {})
	function replaceAllText(findText:String,replaceText:String):Int;
	function getTables(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Table>;
	@:overload(function (sourceChart:google.apps.script.spreadsheet.EmbeddedChart,left:Float,top:Float,width:Float,height:Float):google.apps.script.slides.Image {})
	function insertSheetsChartAsImage(sourceChart:google.apps.script.spreadsheet.EmbeddedChart):google.apps.script.slides.Image;
	function remove(wordArt:google.apps.script.slides.WordArt):Void;
	function getLayouts(listPreset:google.apps.script.slides.ListPreset):Array<google.apps.script.slides.Layout>;
}
