package google.apps.script.slides;

@:native("WordArt")
extern class WordArt
{
	function getHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function setTitle(title:String):google.apps.script.slides.WordArt;
	function getObjectId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.WordArt;
	function getRenderedText(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function removeLink(transform:google.apps.script.slides.AffineTransform):Void;
	function getDescription(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.WordArt;
	function getPageElementType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElementType;
	function getParentGroup(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getLeft(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getConnectionSites(alignmentPosition:google.apps.script.slides.AlignmentPosition):Array<google.apps.script.slides.ConnectionSite>;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.WordArt;
	function setLeft(left:Float):google.apps.script.slides.WordArt;
	function setTop(top:Float):google.apps.script.slides.WordArt;
	function setWidth(width:Float):google.apps.script.slides.WordArt;
	function getLink(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Link;
	function sendBackward(replace:Bool):google.apps.script.slides.WordArt;
	function setHeight(height:Float):google.apps.script.slides.WordArt;
	function getInherentWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getInherentHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.WordArt;
	function getRotation(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getParentPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Page;
	function getTop(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function sendToBack(replace:Bool):google.apps.script.slides.WordArt;
	function getTitle(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getTransform(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.AffineTransform;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.WordArt;
	function scaleHeight(ratio:Float):google.apps.script.slides.WordArt;
	function setDescription(description:String):google.apps.script.slides.WordArt;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	function setRotation(angle:Float):google.apps.script.slides.WordArt;
	function setLinkUrl(url:String):google.apps.script.slides.Link;
	@:overload(function (slide:google.apps.script.slides.Slide):google.apps.script.slides.Link {})
	@:overload(function (slidePosition:google.apps.script.slides.SlidePosition):google.apps.script.slides.Link {})
	function setLinkSlide(slideIndex:Int):google.apps.script.slides.Link;
	function getWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function scaleWidth(ratio:Float):google.apps.script.slides.WordArt;
}
