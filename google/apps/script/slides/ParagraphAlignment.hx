package google.apps.script.slides;

@:require(hx_gas)
@:native("ParagraphAlignment")
@:enum abstract ParagraphAlignment(String) from String to String
{
	var UNSUPPORTED;
	var START;
	var CENTER;
	var END;
	var JUSTIFIED;
}

