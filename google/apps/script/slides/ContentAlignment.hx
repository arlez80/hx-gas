package google.apps.script.slides;

@:require(hx_gas)
@:native("ContentAlignment")
@:enum abstract ContentAlignment(String) from String to String
{
	var UNSUPPORTED;
	var TOP;
	var MIDDLE;
	var BOTTOM;
}

