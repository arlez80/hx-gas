package google.apps.script.slides;

@:native("NotesPage")
extern class NotesPage
{
	function getSheetsCharts(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.SheetsChart>;
	function getPlaceholders(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.PageElement>;
	function getShapes(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Shape>;
	function getPageElementById(id:String):google.apps.script.slides.PageElement;
	@:overload(function (placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):google.apps.script.slides.PageElement {})
	function getPlaceholder(placeholderType:google.apps.script.slides.PlaceholderType):google.apps.script.slides.PageElement;
	function getPageElements(id:String):Array<google.apps.script.slides.PageElement>;
	function getGroups(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Group>;
	function getImages(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Image>;
	function getSpeakerNotesShape(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):google.apps.script.slides.Shape;
	function getWordArts(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.WordArt>;
	function getLines(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Line>;
	@:overload(function (findText:String,replaceText:String,matchCase:Bool):Int {})
	function replaceAllText(findText:String,replaceText:String):Int;
	function getVideos(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Video>;
	function getObjectId(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):String;
	function getTables(placeholderType:google.apps.script.slides.PlaceholderType,placeholderIndex:Int):Array<google.apps.script.slides.Table>;
}
