package google.apps.script.slides;

@:require(hx_gas)
@:native("SlidesApp.AlignmentPosition")
@:enum abstract AlignmentPosition(String) from String to String
{
	var CENTER;
	var HORIZONTAL_CENTER;
	var VERTICAL_CENTER;
}

@:require(hx_gas)
@:native("SlidesApp.ArrowStyle")
@:enum abstract ArrowStyle(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var STEALTH_ARROW;
	var FILL_ARROW;
	var FILL_CIRCLE;
	var FILL_SQUARE;
	var FILL_DIAMOND;
	var OPEN_ARROW;
	var OPEN_CIRCLE;
	var OPEN_SQUARE;
	var OPEN_DIAMOND;
}

@:require(hx_gas)
@:native("SlidesApp.AutoTextType")
@:enum abstract AutoTextType(String) from String to String
{
	var UNSUPPORTED;
	var SLIDE_NUMBER;
}

@:require(hx_gas)
@:native("SlidesApp.CellMergeState")
@:enum abstract CellMergeState(String) from String to String
{
	var NORMAL;
	var HEAD;
	var MERGED;
}

@:require(hx_gas)
@:native("SlidesApp.ColorType")
@:enum abstract ColorType(String) from String to String
{
	var UNSUPPORTED;
	var RGB;
	var THEME;
}

@:require(hx_gas)
@:native("SlidesApp.ContentAlignment")
@:enum abstract ContentAlignment(String) from String to String
{
	var UNSUPPORTED;
	var TOP;
	var MIDDLE;
	var BOTTOM;
}

@:require(hx_gas)
@:native("SlidesApp.DashStyle")
@:enum abstract DashStyle(String) from String to String
{
	var UNSUPPORTED;
	var SOLID;
	var DOT;
	var DASH;
	var DASH_DOT;
	var LONG_DASH;
	var LONG_DASH_DOT;
}

@:require(hx_gas)
@:native("SlidesApp.FillType")
@:enum abstract FillType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var SOLID;
}

@:require(hx_gas)
@:native("SlidesApp.LineCategory")
@:enum abstract LineCategory(String) from String to String
{
	var UNSUPPORTED;
	var STRAIGHT;
	var BENT;
	var CURVED;
}

@:require(hx_gas)
@:native("SlidesApp.LineFillType")
@:enum abstract LineFillType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var SOLID;
}

@:require(hx_gas)
@:native("SlidesApp.LineType")
@:enum abstract LineType(String) from String to String
{
	var UNSUPPORTED;
	var STRAIGHT_CONNECTOR_1;
	var BENT_CONNECTOR_2;
	var BENT_CONNECTOR_3;
	var BENT_CONNECTOR_4;
	var BENT_CONNECTOR_5;
	var CURVED_CONNECTOR_2;
	var CURVED_CONNECTOR_3;
	var CURVED_CONNECTOR_4;
	var CURVED_CONNECTOR_5;
	var STRAIGHT_LINE;
}

@:require(hx_gas)
@:native("SlidesApp.LinkType")
@:enum abstract LinkType(String) from String to String
{
	var UNSUPPORTED;
	var URL;
	var SLIDE_POSITION;
	var SLIDE_ID;
	var SLIDE_INDEX;
}

@:require(hx_gas)
@:native("SlidesApp.ListPreset")
@:enum abstract ListPreset(String) from String to String
{
	var DISC_CIRCLE_SQUARE;
	var DIAMONDX_ARROW3D_SQUARE;
	var CHECKBOX;
	var ARROW_DIAMOND_DISC;
	var STAR_CIRCLE_SQUARE;
	var ARROW3D_CIRCLE_SQUARE;
	var LEFTTRIANGLE_DIAMOND_DISC;
	var DIAMONDX_HOLLOWDIAMOND_SQUARE;
	var DIAMOND_CIRCLE_SQUARE;
	var DIGIT_ALPHA_ROMAN;
	var DIGIT_ALPHA_ROMAN_PARENS;
	var DIGIT_NESTED;
	var UPPERALPHA_ALPHA_ROMAN;
	var UPPERROMAN_UPPERALPHA_DIGIT;
	var ZERODIGIT_ALPHA_ROMAN;
}

@:require(hx_gas)
@:native("SlidesApp.PageBackgroundType")
@:enum abstract PageBackgroundType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var SOLID;
	var PICTURE;
}

@:require(hx_gas)
@:native("SlidesApp.PageElementType")
@:enum abstract PageElementType(String) from String to String
{
	var UNSUPPORTED;
	var SHAPE;
	var IMAGE;
	var VIDEO;
	var TABLE;
	var GROUP;
	var LINE;
	var WORD_ART;
	var SHEETS_CHART;
}

@:require(hx_gas)
@:native("SlidesApp.PageType")
@:enum abstract PageType(String) from String to String
{
	var UNSUPPORTED;
	var SLIDE;
	var LAYOUT;
	var MASTER;
}

@:require(hx_gas)
@:native("SlidesApp.ParagraphAlignment")
@:enum abstract ParagraphAlignment(String) from String to String
{
	var UNSUPPORTED;
	var START;
	var CENTER;
	var END;
	var JUSTIFIED;
}

@:require(hx_gas)
@:native("SlidesApp.PlaceholderType")
@:enum abstract PlaceholderType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var BODY;
	var CHART;
	var CLIP_ART;
	var CENTERED_TITLE;
	var DIAGRAM;
	var DATE_AND_TIME;
	var FOOTER;
	var HEADER;
	var MEDIA;
	var OBJECT;
	var PICTURE;
	var SLIDE_NUMBER;
	var SUBTITLE;
	var TABLE;
	var TITLE;
	var SLIDE_IMAGE;
}

@:require(hx_gas)
@:native("SlidesApp.PredefinedLayout")
@:enum abstract PredefinedLayout(String) from String to String
{
	var UNSUPPORTED;
	var BLANK;
	var CAPTION_ONLY;
	var TITLE;
	var TITLE_AND_BODY;
	var TITLE_AND_TWO_COLUMNS;
	var TITLE_ONLY;
	var SECTION_HEADER;
	var SECTION_TITLE_AND_DESCRIPTION;
	var ONE_COLUMN_TEXT;
	var MAIN_POINT;
	var BIG_NUMBER;
}

@:require(hx_gas)
@:native("SlidesApp.SelectionType")
@:enum abstract SelectionType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var TEXT;
	var TABLE_CELL;
	var PAGE;
	var PAGE_ELEMENT;
	var CURRENT_PAGE;
}

@:require(hx_gas)
@:native("SlidesApp.ShapeType")
@:enum abstract ShapeType(String) from String to String
{
	var UNSUPPORTED;
	var TEXT_BOX;
	var RECTANGLE;
	var ROUND_RECTANGLE;
	var ELLIPSE;
	var ARC;
	var BENT_ARROW;
	var BENT_UP_ARROW;
	var BEVEL;
	var BLOCK_ARC;
	var BRACE_PAIR;
	var BRACKET_PAIR;
	var CAN;
	var CHEVRON;
	var CHORD;
	var CLOUD;
	var CORNER;
	var CUBE;
	var CURVED_DOWN_ARROW;
	var CURVED_LEFT_ARROW;
	var CURVED_RIGHT_ARROW;
	var CURVED_UP_ARROW;
	var DECAGON;
	var DIAGONAL_STRIPE;
	var DIAMOND;
	var DODECAGON;
	var DONUT;
	var DOUBLE_WAVE;
	var DOWN_ARROW;
	var DOWN_ARROW_CALLOUT;
	var FOLDED_CORNER;
	var FRAME;
	var HALF_FRAME;
	var HEART;
	var HEPTAGON;
	var HEXAGON;
	var HOME_PLATE;
	var HORIZONTAL_SCROLL;
	var IRREGULAR_SEAL_1;
	var IRREGULAR_SEAL_2;
	var LEFT_ARROW;
	var LEFT_ARROW_CALLOUT;
	var LEFT_BRACE;
	var LEFT_BRACKET;
	var LEFT_RIGHT_ARROW;
	var LEFT_RIGHT_ARROW_CALLOUT;
	var LEFT_RIGHT_UP_ARROW;
	var LEFT_UP_ARROW;
	var LIGHTNING_BOLT;
	var MATH_DIVIDE;
	var MATH_EQUAL;
	var MATH_MINUS;
	var MATH_MULTIPLY;
	var MATH_NOT_EQUAL;
	var MATH_PLUS;
	var MOON;
	var NO_SMOKING;
	var NOTCHED_RIGHT_ARROW;
	var OCTAGON;
	var PARALLELOGRAM;
	var PENTAGON;
	var PIE;
	var PLAQUE;
	var PLUS;
	var QUAD_ARROW;
	var QUAD_ARROW_CALLOUT;
	var RIBBON;
	var RIBBON_2;
	var RIGHT_ARROW;
	var RIGHT_ARROW_CALLOUT;
	var RIGHT_BRACE;
	var RIGHT_BRACKET;
	var ROUND_1_RECTANGLE;
	var ROUND_2_DIAGONAL_RECTANGLE;
	var ROUND_2_SAME_RECTANGLE;
	var RIGHT_TRIANGLE;
	var SMILEY_FACE;
	var SNIP_1_RECTANGLE;
	var SNIP_2_DIAGONAL_RECTANGLE;
	var SNIP_2_SAME_RECTANGLE;
	var SNIP_ROUND_RECTANGLE;
	var STAR_10;
	var STAR_12;
	var STAR_16;
	var STAR_24;
	var STAR_32;
	var STAR_4;
	var STAR_5;
	var STAR_6;
	var STAR_7;
	var STAR_8;
	var STRIPED_RIGHT_ARROW;
	var SUN;
	var TRAPEZOID;
	var TRIANGLE;
	var UP_ARROW;
	var UP_ARROW_CALLOUT;
	var UP_DOWN_ARROW;
	var UTURN_ARROW;
	var VERTICAL_SCROLL;
	var WAVE;
	var WEDGE_ELLIPSE_CALLOUT;
	var WEDGE_RECTANGLE_CALLOUT;
	var WEDGE_ROUND_RECTANGLE_CALLOUT;
	var FLOW_CHART_ALTERNATE_PROCESS;
	var FLOW_CHART_COLLATE;
	var FLOW_CHART_CONNECTOR;
	var FLOW_CHART_DECISION;
	var FLOW_CHART_DELAY;
	var FLOW_CHART_DISPLAY;
	var FLOW_CHART_DOCUMENT;
	var FLOW_CHART_EXTRACT;
	var FLOW_CHART_INPUT_OUTPUT;
	var FLOW_CHART_INTERNAL_STORAGE;
	var FLOW_CHART_MAGNETIC_DISK;
	var FLOW_CHART_MAGNETIC_DRUM;
	var FLOW_CHART_MAGNETIC_TAPE;
	var FLOW_CHART_MANUAL_INPUT;
	var FLOW_CHART_MANUAL_OPERATION;
	var FLOW_CHART_MERGE;
	var FLOW_CHART_MULTIDOCUMENT;
	var FLOW_CHART_OFFLINE_STORAGE;
	var FLOW_CHART_OFFPAGE_CONNECTOR;
	var FLOW_CHART_ONLINE_STORAGE;
	var FLOW_CHART_OR;
	var FLOW_CHART_PREDEFINED_PROCESS;
	var FLOW_CHART_PREPARATION;
	var FLOW_CHART_PROCESS;
	var FLOW_CHART_PUNCHED_CARD;
	var FLOW_CHART_PUNCHED_TAPE;
	var FLOW_CHART_SORT;
	var FLOW_CHART_SUMMING_JUNCTION;
	var FLOW_CHART_TERMINATOR;
	var ARROW_EAST;
	var ARROW_NORTH_EAST;
	var ARROW_NORTH;
	var SPEECH;
	var STARBURST;
	var TEARDROP;
	var ELLIPSE_RIBBON;
	var ELLIPSE_RIBBON_2;
	var CLOUD_CALLOUT;
	var CUSTOM;
}

@:require(hx_gas)
@:native("SlidesApp.SheetsChartEmbedType")
@:enum abstract SheetsChartEmbedType(String) from String to String
{
	var UNSUPPORTED;
	var IMAGE;
}

@:require(hx_gas)
@:native("SlidesApp.SlideLinkingMode")
@:enum abstract SlideLinkingMode(String) from String to String
{
	var UNSUPPORTED;
	var LINKED;
	var NOT_LINKED;
}

@:require(hx_gas)
@:native("SlidesApp.SlidePosition")
@:enum abstract SlidePosition(String) from String to String
{
	var NEXT_SLIDE;
	var PREVIOUS_SLIDE;
	var FIRST_SLIDE;
	var LAST_SLIDE;
}

@:require(hx_gas)
@:native("SlidesApp.SpacingMode")
@:enum abstract SpacingMode(String) from String to String
{
	var UNSUPPORTED;
	var NEVER_COLLAPSE;
	var COLLAPSE_LISTS;
}

@:require(hx_gas)
@:native("SlidesApp.TextBaselineOffset")
@:enum abstract TextBaselineOffset(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var SUPERSCRIPT;
	var SUBSCRIPT;
}

@:require(hx_gas)
@:native("SlidesApp.TextDirection")
@:enum abstract TextDirection(String) from String to String
{
	var UNSUPPORTED;
	var LEFT_TO_RIGHT;
	var RIGHT_TO_LEFT;
}

@:require(hx_gas)
@:native("SlidesApp.ThemeColorType")
@:enum abstract ThemeColorType(String) from String to String
{
	var UNSUPPORTED;
	var DARK1;
	var LIGHT1;
	var DARK2;
	var LIGHT2;
	var ACCENT1;
	var ACCENT2;
	var ACCENT3;
	var ACCENT4;
	var ACCENT5;
	var ACCENT6;
	var HYPERLINK;
	var FOLLOWED_HYPERLINK;
}

@:require(hx_gas)
@:native("SlidesApp.VideoSourceType")
@:enum abstract VideoSourceType(String) from String to String
{
	var UNSUPPORTED;
	var YOUTUBE;
}

@:native("SlidesApp")
extern class SlidesApp
{
	static function newAffineTransformBuilder(name:String):google.apps.script.slides.AffineTransformBuilder;
	static function openById(id:String):google.apps.script.slides.Presentation;
	static function getUi(name:String):google.apps.script.base.Ui;
	static function openByUrl(url:String):google.apps.script.slides.Presentation;
	static function getActivePresentation(name:String):google.apps.script.slides.Presentation;
	static function create(name:String):google.apps.script.slides.Presentation;
}
