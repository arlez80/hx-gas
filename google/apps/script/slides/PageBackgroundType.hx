package google.apps.script.slides;

@:require(hx_gas)
@:native("PageBackgroundType")
@:enum abstract PageBackgroundType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var SOLID;
	var PICTURE;
}

