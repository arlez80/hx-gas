package google.apps.script.slides;

@:require(hx_gas)
@:native("ThemeColorType")
@:enum abstract ThemeColorType(String) from String to String
{
	var UNSUPPORTED;
	var DARK1;
	var LIGHT1;
	var DARK2;
	var LIGHT2;
	var ACCENT1;
	var ACCENT2;
	var ACCENT3;
	var ACCENT4;
	var ACCENT5;
	var ACCENT6;
	var HYPERLINK;
	var FOLLOWED_HYPERLINK;
}

