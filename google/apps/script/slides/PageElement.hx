package google.apps.script.slides;

@:native("PageElement")
extern class PageElement
{
	function getHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function setTitle(title:String):google.apps.script.slides.PageElement;
	function asLine(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Line;
	function getParentPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Page;
	function asShape(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Shape;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.PageElement;
	function setHeight(height:Float):google.apps.script.slides.PageElement;
	function asTable(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Table;
	function getDescription(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getPageElementType(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElementType;
	function getParentGroup(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function getLeft(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function getConnectionSites(alignmentPosition:google.apps.script.slides.AlignmentPosition):Array<google.apps.script.slides.ConnectionSite>;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
	function asImage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Image;
	function setLeft(left:Float):google.apps.script.slides.PageElement;
	function setTop(top:Float):google.apps.script.slides.PageElement;
	function setWidth(width:Float):google.apps.script.slides.PageElement;
	function getInherentHeight(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function sendBackward(replace:Bool):google.apps.script.slides.PageElement;
	function asSheetsChart(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.SheetsChart;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.PageElement;
	function getInherentWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function asVideo(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Video;
	function asGroup(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Group;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getRotation(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getObjectId(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function asWordArt(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.WordArt;
	function getTop(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function sendToBack(replace:Bool):google.apps.script.slides.PageElement;
	function getTitle(alignmentPosition:google.apps.script.slides.AlignmentPosition):String;
	function getTransform(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.AffineTransform;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function scaleHeight(ratio:Float):google.apps.script.slides.PageElement;
	function setDescription(description:String):google.apps.script.slides.PageElement;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	function setRotation(angle:Float):google.apps.script.slides.PageElement;
	function getWidth(alignmentPosition:google.apps.script.slides.AlignmentPosition):Float;
	function scaleWidth(ratio:Float):google.apps.script.slides.PageElement;
}
