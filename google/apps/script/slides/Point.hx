package google.apps.script.slides;

@:native("Point")
extern class Point
{
	function getX(contentType:String):Float;
	function getY(contentType:String):Float;
}
