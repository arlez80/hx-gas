package google.apps.script.slides;

@:require(hx_gas)
@:native("AutoTextType")
@:enum abstract AutoTextType(String) from String to String
{
	var UNSUPPORTED;
	var SLIDE_NUMBER;
}

