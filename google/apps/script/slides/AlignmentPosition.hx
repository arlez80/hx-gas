package google.apps.script.slides;

@:require(hx_gas)
@:native("AlignmentPosition")
@:enum abstract AlignmentPosition(String) from String to String
{
	var CENTER;
	var HORIZONTAL_CENTER;
	var VERTICAL_CENTER;
}

