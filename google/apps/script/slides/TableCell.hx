package google.apps.script.slides;

@:native("TableCell")
extern class TableCell
{
	function getParentTable(width:Float):google.apps.script.slides.Table;
	function getHeadCell(width:Float):google.apps.script.slides.TableCell;
	function getColumnIndex(width:Float):Int;
	function getParentColumn(width:Float):google.apps.script.slides.TableColumn;
	function getRowIndex(width:Float):Int;
	function setContentAlignment(contentAlignment:google.apps.script.slides.ContentAlignment):google.apps.script.slides.TableCell;
	function getMergeState(width:Float):google.apps.script.slides.CellMergeState;
	function getColumnSpan(width:Float):Int;
	function getFill(width:Float):google.apps.script.slides.Fill;
	function getText(width:Float):google.apps.script.slides.TextRange;
	function getRowSpan(width:Float):Int;
	function getContentAlignment(width:Float):google.apps.script.slides.ContentAlignment;
	function getParentRow(width:Float):google.apps.script.slides.TableRow;
}
