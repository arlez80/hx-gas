package google.apps.script.slides;

@:native("TableCellRange")
extern class TableCellRange
{
	function getTableCells(contentAlignment:google.apps.script.slides.ContentAlignment):Array<google.apps.script.slides.TableCell>;
}
