package google.apps.script.slides;

@:native("Paragraph")
extern class Paragraph
{
	function getRange(width:Float):google.apps.script.slides.TextRange;
	function getIndex(width:Float):Int;
}
