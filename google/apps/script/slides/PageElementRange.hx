package google.apps.script.slides;

@:native("PageElementRange")
extern class PageElementRange
{
	function getPageElements(width:Float):Array<google.apps.script.slides.PageElement>;
}
