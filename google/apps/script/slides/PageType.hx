package google.apps.script.slides;

@:require(hx_gas)
@:native("PageType")
@:enum abstract PageType(String) from String to String
{
	var UNSUPPORTED;
	var SLIDE;
	var LAYOUT;
	var MASTER;
}

