package google.apps.script.slides;

@:require(hx_gas)
@:native("SlidePosition")
@:enum abstract SlidePosition(String) from String to String
{
	var NEXT_SLIDE;
	var PREVIOUS_SLIDE;
	var FIRST_SLIDE;
	var LAST_SLIDE;
}

