package google.apps.script.slides;

@:require(hx_gas)
@:native("ColorType")
@:enum abstract ColorType(String) from String to String
{
	var UNSUPPORTED;
	var RGB;
	var THEME;
}

