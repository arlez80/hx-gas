package google.apps.script.slides;

@:native("Border")
extern class Border
{
	function isVisible(translateY:Float):Bool;
	function setDashStyle(style:google.apps.script.slides.DashStyle):google.apps.script.slides.Border;
	function getLineFill(translateY:Float):google.apps.script.slides.LineFill;
	function getDashStyle(translateY:Float):google.apps.script.slides.DashStyle;
	function setTransparent(style:google.apps.script.slides.DashStyle):google.apps.script.slides.Border;
	function getWeight(translateY:Float):Float;
	function setWeight(points:Float):google.apps.script.slides.Border;
}
