package google.apps.script.slides;

@:require(hx_gas)
@:native("LineFillType")
@:enum abstract LineFillType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var SOLID;
}

