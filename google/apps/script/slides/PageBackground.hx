package google.apps.script.slides;

@:native("PageBackground")
extern class PageBackground
{
	function isVisible(findText:String,replaceText:String,matchCase:Bool):Bool;
	function getPictureFill(findText:String,replaceText:String,matchCase:Bool):google.apps.script.slides.PictureFill;
	function setTransparent(color:google.apps.script.slides.ThemeColorType,alpha:Float):Void;
	@:overload(function (imageUrl:String):Void {})
	function setPictureFill(blobSource:google.apps.script.base.BlobSource):Void;
	@:overload(function (color:google.apps.script.slides.Color,alpha:Float):Void {})
	@:overload(function (red:Int,green:Int,blue:Int):Void {})
	@:overload(function (red:Int,green:Int,blue:Int,alpha:Float):Void {})
	@:overload(function (hexString:String):Void {})
	@:overload(function (hexString:String,alpha:Float):Void {})
	@:overload(function (color:google.apps.script.slides.ThemeColorType):Void {})
	@:overload(function (color:google.apps.script.slides.ThemeColorType,alpha:Float):Void {})
	function setSolidFill(color:google.apps.script.slides.Color):Void;
	function getSolidFill(findText:String,replaceText:String,matchCase:Bool):google.apps.script.slides.SolidFill;
	function getType(findText:String,replaceText:String,matchCase:Bool):google.apps.script.slides.PageBackgroundType;
}
