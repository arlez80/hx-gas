package google.apps.script.slides;

@:require(hx_gas)
@:native("ArrowStyle")
@:enum abstract ArrowStyle(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var STEALTH_ARROW;
	var FILL_ARROW;
	var FILL_CIRCLE;
	var FILL_SQUARE;
	var FILL_DIAMOND;
	var OPEN_ARROW;
	var OPEN_CIRCLE;
	var OPEN_SQUARE;
	var OPEN_DIAMOND;
}

