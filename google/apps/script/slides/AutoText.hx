package google.apps.script.slides;

@:native("AutoText")
extern class AutoText
{
	function getRange(translateY:Float):google.apps.script.slides.TextRange;
	function getIndex(translateY:Float):Int;
	function getAutoTextType(translateY:Float):google.apps.script.slides.AutoTextType;
}
