package google.apps.script.slides;

@:require(hx_gas)
@:native("DashStyle")
@:enum abstract DashStyle(String) from String to String
{
	var UNSUPPORTED;
	var SOLID;
	var DOT;
	var DASH;
	var DASH_DOT;
	var LONG_DASH;
	var LONG_DASH_DOT;
}

