package google.apps.script.slides;

@:native("ColorScheme")
extern class ColorScheme
{
	function getConcreteColor(theme:google.apps.script.slides.ThemeColorType):google.apps.script.slides.Color;
	function getThemeColors(theme:google.apps.script.slides.ThemeColorType):Array<google.apps.script.slides.ThemeColorType>;
}
