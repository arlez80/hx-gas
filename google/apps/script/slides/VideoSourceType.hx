package google.apps.script.slides;

@:require(hx_gas)
@:native("VideoSourceType")
@:enum abstract VideoSourceType(String) from String to String
{
	var UNSUPPORTED;
	var YOUTUBE;
}

