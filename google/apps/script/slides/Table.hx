package google.apps.script.slides;

@:native("Table")
extern class Table
{
	function setRotation(angle:Float):google.apps.script.slides.Table;
	function getHeight(columnIndex:Int):Float;
	function setTitle(title:String):google.apps.script.slides.Table;
	function insertColumn(index:Int):google.apps.script.slides.TableColumn;
	function getObjectId(columnIndex:Int):String;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Table;
	function appendColumn(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.TableColumn;
	function getColumn(columnIndex:Int):google.apps.script.slides.TableColumn;
	function appendRow(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.TableRow;
	function getDescription(columnIndex:Int):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Table;
	function getPageElementType(columnIndex:Int):google.apps.script.slides.PageElementType;
	function getParentGroup(columnIndex:Int):google.apps.script.slides.Group;
	function getLeft(columnIndex:Int):Float;
	function getConnectionSites(columnIndex:Int):Array<google.apps.script.slides.ConnectionSite>;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
	function getNumColumns(columnIndex:Int):Int;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Table;
	function setLeft(left:Float):google.apps.script.slides.Table;
	function setTop(top:Float):google.apps.script.slides.Table;
	function setWidth(width:Float):google.apps.script.slides.Table;
	function sendBackward(replace:Bool):google.apps.script.slides.Table;
	function setHeight(height:Float):google.apps.script.slides.Table;
	function getCell(rowIndex:Int,columnIndex:Int):google.apps.script.slides.TableCell;
	function getInherentWidth(columnIndex:Int):Float;
	function getInherentHeight(columnIndex:Int):Float;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Table;
	function getRotation(columnIndex:Int):Float;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getParentPage(columnIndex:Int):google.apps.script.slides.Page;
	function getTop(rowIndex:Int):Float;
	function sendToBack(replace:Bool):google.apps.script.slides.Table;
	function getTitle(rowIndex:Int):String;
	function getTransform(rowIndex:Int):google.apps.script.slides.AffineTransform;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Table;
	function scaleHeight(ratio:Float):google.apps.script.slides.Table;
	function setDescription(description:String):google.apps.script.slides.Table;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	function getNumRows(columnIndex:Int):Int;
	function insertRow(index:Int):google.apps.script.slides.TableRow;
	function getRow(rowIndex:Int):google.apps.script.slides.TableRow;
	function getWidth(rowIndex:Int):Float;
	function scaleWidth(ratio:Float):google.apps.script.slides.Table;
}
