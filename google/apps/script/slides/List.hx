package google.apps.script.slides;

@:native("List")
extern class List
{
	function getListParagraphs(color:google.apps.script.slides.ThemeColorType,alpha:Float):Array<google.apps.script.slides.Paragraph>;
	function getListId(color:google.apps.script.slides.ThemeColorType,alpha:Float):String;
}
