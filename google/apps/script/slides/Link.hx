package google.apps.script.slides;

@:native("Link")
extern class Link
{
	function getSlideIndex(color:google.apps.script.slides.ThemeColorType,alpha:Float):Int;
	function getLinkType(color:google.apps.script.slides.ThemeColorType,alpha:Float):google.apps.script.slides.LinkType;
	function getLinkedSlide(color:google.apps.script.slides.ThemeColorType,alpha:Float):google.apps.script.slides.Slide;
	function getUrl(color:google.apps.script.slides.ThemeColorType,alpha:Float):String;
	function getSlidePosition(color:google.apps.script.slides.ThemeColorType,alpha:Float):google.apps.script.slides.SlidePosition;
	function getSlideId(color:google.apps.script.slides.ThemeColorType,alpha:Float):String;
}
