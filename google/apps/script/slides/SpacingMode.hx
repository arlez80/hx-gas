package google.apps.script.slides;

@:require(hx_gas)
@:native("SpacingMode")
@:enum abstract SpacingMode(String) from String to String
{
	var UNSUPPORTED;
	var NEVER_COLLAPSE;
	var COLLAPSE_LISTS;
}

