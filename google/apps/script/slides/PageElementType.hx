package google.apps.script.slides;

@:require(hx_gas)
@:native("PageElementType")
@:enum abstract PageElementType(String) from String to String
{
	var UNSUPPORTED;
	var SHAPE;
	var IMAGE;
	var VIDEO;
	var TABLE;
	var GROUP;
	var LINE;
	var WORD_ART;
	var SHEETS_CHART;
}

