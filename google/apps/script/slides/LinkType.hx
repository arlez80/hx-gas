package google.apps.script.slides;

@:require(hx_gas)
@:native("LinkType")
@:enum abstract LinkType(String) from String to String
{
	var UNSUPPORTED;
	var URL;
	var SLIDE_POSITION;
	var SLIDE_ID;
	var SLIDE_INDEX;
}

