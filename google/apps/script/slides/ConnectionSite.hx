package google.apps.script.slides;

@:native("ConnectionSite")
extern class ConnectionSite
{
	function getIndex(theme:google.apps.script.slides.ThemeColorType):Int;
	function getPageElement(theme:google.apps.script.slides.ThemeColorType):google.apps.script.slides.PageElement;
}
