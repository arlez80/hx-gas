package google.apps.script.slides;

@:require(hx_gas)
@:native("PlaceholderType")
@:enum abstract PlaceholderType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var BODY;
	var CHART;
	var CLIP_ART;
	var CENTERED_TITLE;
	var DIAGRAM;
	var DATE_AND_TIME;
	var FOOTER;
	var HEADER;
	var MEDIA;
	var OBJECT;
	var PICTURE;
	var SLIDE_NUMBER;
	var SUBTITLE;
	var TABLE;
	var TITLE;
	var SLIDE_IMAGE;
}

