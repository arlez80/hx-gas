package google.apps.script.slides;

@:require(hx_gas)
@:native("SelectionType")
@:enum abstract SelectionType(String) from String to String
{
	var UNSUPPORTED;
	var NONE;
	var TEXT;
	var TABLE_CELL;
	var PAGE;
	var PAGE_ELEMENT;
	var CURRENT_PAGE;
}

