package google.apps.script.slides;

@:native("Image")
extern class Image
{
	function getHeight(contentType:String):Float;
	function setTitle(title:String):google.apps.script.slides.Image;
	function setTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Image;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	@:overload(function (blobSource:google.apps.script.base.BlobSource,crop:Bool):google.apps.script.slides.Image {})
	@:overload(function (imageUrl:String):google.apps.script.slides.Image {})
	@:overload(function (imageUrl:String,crop:Bool):google.apps.script.slides.Image {})
	function replace(blobSource:google.apps.script.base.BlobSource):google.apps.script.slides.Image;
	function preconcatenateTransform(transform:google.apps.script.slides.AffineTransform):google.apps.script.slides.Image;
	function setLeft(left:Float):google.apps.script.slides.Image;
	function getLink(contentType:String):google.apps.script.slides.Link;
	function sendBackward(replace:Bool):google.apps.script.slides.Image;
	function getInherentHeight(contentType:String):Float;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function duplicate(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.PageElement;
	function getParentPage(contentType:String):google.apps.script.slides.Page;
	function getWidth(contentType:String):Float;
	function getTransform(contentType:String):google.apps.script.slides.AffineTransform;
	function setTop(top:Float):google.apps.script.slides.Image;
	@:overload(function (replace:Bool):Void {})
	function select(ratio:Float):Void;
	function scaleWidth(ratio:Float):google.apps.script.slides.Image;
	function setDescription(description:String):google.apps.script.slides.Image;
	function getBorder(contentType:String):google.apps.script.slides.Border;
	function setHeight(height:Float):google.apps.script.slides.Image;
	function removeLink(transform:google.apps.script.slides.AffineTransform):Void;
	function getDescription(contentType:String):String;
	function bringToFront(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Image;
	function getPageElementType(contentType:String):google.apps.script.slides.PageElementType;
	function getParentGroup(contentType:String):google.apps.script.slides.Group;
	function getLeft(contentType:String):Float;
	function getConnectionSites(contentType:String):Array<google.apps.script.slides.ConnectionSite>;
	function setWidth(width:Float):google.apps.script.slides.Image;
	function getContentUrl(contentType:String):String;
	function getSourceUrl(contentType:String):String;
	function setRotation(angle:Float):google.apps.script.slides.Image;
	function getInherentWidth(contentType:String):Float;
	function alignOnPage(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Image;
	function getRotation(contentType:String):Float;
	function getObjectId(contentType:String):String;
	function getTop(contentType:String):Float;
	function sendToBack(replace:Bool):google.apps.script.slides.Image;
	function getTitle(contentType:String):String;
	function bringForward(alignmentPosition:google.apps.script.slides.AlignmentPosition):google.apps.script.slides.Image;
	function scaleHeight(ratio:Float):google.apps.script.slides.Image;
	function setLinkUrl(url:String):google.apps.script.slides.Link;
	@:overload(function (slide:google.apps.script.slides.Slide):google.apps.script.slides.Link {})
	@:overload(function (slidePosition:google.apps.script.slides.SlidePosition):google.apps.script.slides.Link {})
	function setLinkSlide(slideIndex:Int):google.apps.script.slides.Link;
	function remove(transform:google.apps.script.slides.AffineTransform):Void;
}
