package google.apps.script.slides;

@:native("Color")
extern class Color
{
	function asThemeColor(points:Float):google.apps.script.slides.ThemeColor;
	function getColorType(points:Float):google.apps.script.slides.ColorType;
	function asRgbColor(points:Float):google.apps.script.slides.RgbColor;
}
