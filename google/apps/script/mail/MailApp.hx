package google.apps.script.mail;

@:native("MailApp")
extern class MailApp
{
	static function getRemainingDailyQuota(timeoutInMillis:Int):Int;
	@:overload(function (recipient:String,subject:String,body:String):Void {})
	@:overload(function (recipient:String,subject:String,body:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?noReply:Bool,?replyTo:String}):Void {})
	@:overload(function (to:String,replyTo:String,subject:String,body:String):Void {})
	static function sendEmail(message:Dynamic):Void;
}
