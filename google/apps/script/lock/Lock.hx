package google.apps.script.lock;

@:native("Lock")
extern class Lock
{
	function tryLock(timeoutInMillis:Int):Bool;
	function waitLock(timeoutInMillis:Int):js.Error;
	function releaseLock(year:Int):Void;
	function hasLock(year:Int):Bool;
}
