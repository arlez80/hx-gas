package google.apps.script.lock;

@:native("LockService")
extern class LockService
{
	static function getUserLock(year:Int):google.apps.script.lock.Lock;
	static function getDocumentLock(year:Int):google.apps.script.lock.Lock;
	static function getScriptLock(year:Int):google.apps.script.lock.Lock;
}
