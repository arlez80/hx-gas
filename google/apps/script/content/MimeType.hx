package google.apps.script.content;

@:require(hx_gas)
@:native("MimeType")
@:enum abstract MimeType(String) from String to String
{
	var ATOM;
	var CSV;
	var ICAL;
	var JAVASCRIPT;
	var JSON;
	var RSS;
	var TEXT;
	var VCARD;
	var XML;
}

