package google.apps.script.content;

@:require(hx_gas)
@:native("ContentService.MimeType")
@:enum abstract MimeType(String) from String to String
{
	var ATOM;
	var CSV;
	var ICAL;
	var JAVASCRIPT;
	var JSON;
	var RSS;
	var TEXT;
	var VCARD;
	var XML;
}

@:native("ContentService")
extern class ContentService
{
	@:overload(function (content:String):google.apps.script.content.TextOutput {})
	static function createTextOutput(fontSize:Float):google.apps.script.content.TextOutput;
}
