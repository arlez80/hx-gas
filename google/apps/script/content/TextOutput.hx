package google.apps.script.content;

@:native("TextOutput")
extern class TextOutput
{
	function append(addedContent:String):google.apps.script.content.TextOutput;
	function clear(addedContent:String):google.apps.script.content.TextOutput;
	function downloadAsFile(filename:String):google.apps.script.content.TextOutput;
	function setContent(content:String):google.apps.script.content.TextOutput;
	function getMimeType(filename:String):google.apps.script.content.MimeType;
	function getContent(filename:String):String;
	function setMimeType(mimeType:google.apps.script.content.MimeType):google.apps.script.content.TextOutput;
	function getFileName(filename:String):String;
}
