package google.apps.script.base;

@:require(hx_gas)
@:native("Month")
@:enum abstract Month(String) from String to String
{
	var JANUARY;
	var FEBRUARY;
	var MARCH;
	var APRIL;
	var MAY;
	var JUNE;
	var JULY;
	var AUGUST;
	var SEPTEMBER;
	var OCTOBER;
	var NOVEMBER;
	var DECEMBER;
}

