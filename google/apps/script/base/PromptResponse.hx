package google.apps.script.base;

@:native("PromptResponse")
extern class PromptResponse
{
	function getResponseText(menu:google.apps.script.base.Menu):String;
	function getSelectedButton(menu:google.apps.script.base.Menu):google.apps.script.base.Button;
}
