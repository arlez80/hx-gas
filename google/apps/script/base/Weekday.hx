package google.apps.script.base;

@:require(hx_gas)
@:native("Weekday")
@:enum abstract Weekday(String) from String to String
{
	var SUNDAY;
	var MONDAY;
	var TUESDAY;
	var WEDNESDAY;
	var THURSDAY;
	var FRIDAY;
	var SATURDAY;
}

