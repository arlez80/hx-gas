package google.apps.script.base;

@:require(hx_gas)
@:native("Ui.Button")
@:enum abstract Button(String) from String to String
{
	var CLOSE;
	var OK;
	var CANCEL;
	var YES;
	var NO;
}

@:require(hx_gas)
@:native("Ui.ButtonSet")
@:enum abstract ButtonSet(String) from String to String
{
	var OK;
	var OK_CANCEL;
	var YES_NO;
	var YES_NO_CANCEL;
}

@:native("Ui")
extern class Ui
{
	function showModelessDialog(userInterface:Dynamic,title:String):Void;
	function createMenu(caption:String):google.apps.script.base.Menu;
	@:overload(function (prompt:String,buttons:google.apps.script.base.ButtonSet):google.apps.script.base.PromptResponse {})
	@:overload(function (title:String,prompt:String,buttons:google.apps.script.base.ButtonSet):google.apps.script.base.PromptResponse {})
	function prompt(prompt:String):google.apps.script.base.PromptResponse;
	@:deprecated
	function showDialog(userInterface:Dynamic):Void;
	function showModalDialog(userInterface:Dynamic,title:String):Void;
	@:overload(function (prompt:String,buttons:google.apps.script.base.ButtonSet):google.apps.script.base.Button {})
	@:overload(function (title:String,prompt:String,buttons:google.apps.script.base.ButtonSet):google.apps.script.base.Button {})
	function alert(prompt:String):google.apps.script.base.Button;
	function createAddonMenu(title:String,prompt:String,buttons:google.apps.script.base.ButtonSet):google.apps.script.base.Menu;
	function showSidebar(userInterface:Dynamic):Void;
}
