package google.apps.script.base;

@:native("User")
extern class User
{
	@:deprecated
	function getUserLoginId(userInterface:Dynamic):String;
	function getEmail(userInterface:Dynamic):String;
}
