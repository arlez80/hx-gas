package google.apps.script.base;

@:native("Logger")
extern class Logger
{
	static function clear(title:String,prompt:String,buttons:google.apps.script.base.ButtonSet):Void;
	static function getLog(title:String,prompt:String,buttons:google.apps.script.base.ButtonSet):String;
	@:overload(function (format:String,values:Dynamic):google.apps.script.base.Logger {})
	static function log(data:Dynamic):google.apps.script.base.Logger;
}
