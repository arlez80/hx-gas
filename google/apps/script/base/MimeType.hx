package google.apps.script.base;

@:native("MimeType")
extern class MimeType
{
	static var GOOGLE_APPS_SCRIPT(default, null);
	static var GOOGLE_DRAWINGS(default, null);
	static var GOOGLE_DOCS(default, null);
	static var GOOGLE_FORMS(default, null);
	static var GOOGLE_SHEETS(default, null);
	static var GOOGLE_SITES(default, null);
	static var GOOGLE_SLIDES(default, null);
	static var FOLDER(default, null);
	static var BMP(default, null);
	static var GIF(default, null);
	static var JPEG(default, null);
	static var PNG(default, null);
	static var SVG(default, null);
	static var PDF(default, null);
	static var CSS(default, null);
	static var CSV(default, null);
	static var HTML(default, null);
	static var JAVASCRIPT(default, null);
	static var PLAIN_TEXT(default, null);
	static var RTF(default, null);
	static var OPENDOCUMENT_GRAPHICS(default, null);
	static var OPENDOCUMENT_PRESENTATION(default, null);
	static var OPENDOCUMENT_SPREADSHEET(default, null);
	static var OPENDOCUMENT_TEXT(default, null);
	static var MICROSOFT_EXCEL(default, null);
	static var MICROSOFT_EXCEL_LEGACY(default, null);
	static var MICROSOFT_POWERPOINT(default, null);
	static var MICROSOFT_POWERPOINT_LEGACY(default, null);
	static var MICROSOFT_WORD(default, null);
	static var MICROSOFT_WORD_LEGACY(default, null);
	static var ZIP(default, null);
}
