package google.apps.script.base;

@:require(hx_gas)
@:native("Browser.ButtonSet")
@:enum abstract ButtonSet(String) from String to String
{
	var OK;
	var OK_CANCEL;
	var YES_NO;
	var YES_NO_CANCEL;
}

@:native("Browser")
extern class Browser
{
	@:overload(function (prompt:String,buttons:google.apps.script.base.ButtonSet):String {})
	@:overload(function (title:String,prompt:String,buttons:google.apps.script.base.ButtonSet):String {})
	static function msgBox(prompt:String):String;
	@:overload(function (prompt:String,buttons:google.apps.script.base.ButtonSet):String {})
	@:overload(function (title:String,prompt:String,buttons:google.apps.script.base.ButtonSet):String {})
	static function inputBox(prompt:String):String;
}
