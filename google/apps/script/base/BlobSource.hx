package google.apps.script.base;

@:native("BlobSource")
extern class BlobSource
{
	function getAs(contentType:String):google.apps.script.base.Blob;
	function getBlob(contentType:String):google.apps.script.base.Blob;
}
