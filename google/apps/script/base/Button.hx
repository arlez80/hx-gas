package google.apps.script.base;

@:require(hx_gas)
@:native("Button")
@:enum abstract Button(String) from String to String
{
	var CLOSE;
	var OK;
	var CANCEL;
	var YES;
	var NO;
}

