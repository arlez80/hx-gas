package google.apps.script.base;

@:native("Session")
extern class Session
{
	@:deprecated
	static function getUser(format:String,values:Dynamic):google.apps.script.base.User;
	static function getScriptTimeZone(format:String,values:Dynamic):String;
	@:deprecated
	static function getTimeZone(format:String,values:Dynamic):String;
	static function getTemporaryActiveUserKey(format:String,values:Dynamic):String;
	static function getActiveUser(format:String,values:Dynamic):google.apps.script.base.User;
	static function getEffectiveUser(format:String,values:Dynamic):google.apps.script.base.User;
	static function getActiveUserLocale(format:String,values:Dynamic):String;
}
