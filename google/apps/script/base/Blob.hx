package google.apps.script.base;

@:native("Blob")
extern class Blob
{
	function setContentTypeFromExtension(contentType:String):google.apps.script.base.Blob;
	@:overload(function (string:String,charset:String):google.apps.script.base.Blob {})
	function setDataFromString(string:String):google.apps.script.base.Blob;
	@:overload(function (charset:String):String {})
	function getDataAsString(contentType:String):String;
	@:deprecated
	function getAllBlobs(name:String):Array<google.apps.script.base.Blob>;
	function getName(charset:String):String;
	function copyBlob(formatOrObject:Dynamic,values:Dynamic):google.apps.script.base.Blob;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function setBytes(data:Array<Int>):google.apps.script.base.Blob;
	function getBytes(contentType:String):Array<Int>;
	function setContentType(contentType:String):google.apps.script.base.Blob;
	function getContentType(contentType:String):String;
	function setName(name:String):google.apps.script.base.Blob;
	function isGoogleType(charset:String):Bool;
}
