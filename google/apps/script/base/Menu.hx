package google.apps.script.base;

@:native("Menu")
extern class Menu
{
	function addItem(caption:String,functionName:String):google.apps.script.base.Menu;
	function addSeparator(caption:String,functionName:String):google.apps.script.base.Menu;
	function addToUi(menu:google.apps.script.base.Menu):Void;
	function addSubMenu(menu:google.apps.script.base.Menu):google.apps.script.base.Menu;
}
