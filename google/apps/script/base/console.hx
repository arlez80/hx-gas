package google.apps.script.base;

@:native("console")
extern class console
{
	@:overload(function (formatOrObject:Dynamic,values:Dynamic):Void {})
	static function warn(label:String):Void;
	@:overload(function (formatOrObject:Dynamic,values:Dynamic):Void {})
	static function info(formatOrObject:Dynamic,values:Dynamic):Void;
	@:overload(function (formatOrObject:Dynamic,values:Dynamic):Void {})
	static function log(formatOrObject:Dynamic,values:Dynamic):Void;
	@:overload(function (formatOrObject:Dynamic,values:Dynamic):Void {})
	static function error(format:String,values:Dynamic):Void;
	static function timeEnd(label:String):Void;
	static function time(label:String):Void;
}
