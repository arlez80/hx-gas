package google.apps.script.base;

@:require(hx_gas)
@:native("ButtonSet")
@:enum abstract ButtonSet(String) from String to String
{
	var OK;
	var OK_CANCEL;
	var YES_NO;
	var YES_NO_CANCEL;
}

