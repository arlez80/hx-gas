package google.apps.script.cache;

@:native("Cache")
extern class Cache
{
	@:overload(function (key:String,value:String,expirationInSeconds:Int):Void {})
	function put(key:String,value:String):Void;
	function get(key:String):String;
	@:overload(function (values:Dynamic,expirationInSeconds:Int):Void {})
	function putAll(values:Dynamic):Void;
	function getAll(keys:Array<String>):Dynamic;
	function removeAll(keys:Array<String>):Void;
	function remove(key:String):Void;
}
