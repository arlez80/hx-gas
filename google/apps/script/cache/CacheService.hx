package google.apps.script.cache;

@:native("CacheService")
extern class CacheService
{
	static function getDocumentCache(contentType:String):google.apps.script.cache.Cache;
	static function getScriptCache(contentType:String):google.apps.script.cache.Cache;
	static function getUserCache(contentType:String):google.apps.script.cache.Cache;
}
