package google.apps.script.spreadsheet;

@:native("Protection")
extern class Protection
{
	function setWarningOnly(warningOnly:Bool):google.apps.script.spreadsheet.Protection;
	function getProtectionType(emailAddresses:Array<String>):google.apps.script.spreadsheet.ProtectionType;
	function setRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Protection;
	function getRangeName(emailAddresses:Array<String>):String;
	function getRange(emailAddresses:Array<String>):google.apps.script.spreadsheet.Range;
	function setDescription(description:String):google.apps.script.spreadsheet.Protection;
	function addEditors(emailAddresses:Array<String>):google.apps.script.spreadsheet.Protection;
	function setRangeName(rangeName:String):google.apps.script.spreadsheet.Protection;
	function isWarningOnly(emailAddresses:Array<String>):Bool;
	function setDomainEdit(editable:Bool):google.apps.script.spreadsheet.Protection;
	function getEditors(emailAddresses:Array<String>):Array<google.apps.script.base.User>;
	function getDescription(emailAddresses:Array<String>):String;
	function canDomainEdit(emailAddresses:Array<String>):Bool;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.spreadsheet.Protection {})
	function removeEditor(emailAddress:String):google.apps.script.spreadsheet.Protection;
	function canEdit(emailAddresses:Array<String>):Bool;
	function getUnprotectedRanges(emailAddresses:Array<String>):Array<google.apps.script.spreadsheet.Range>;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.spreadsheet.Protection {})
	function addEditor(emailAddress:String):google.apps.script.spreadsheet.Protection;
	function setUnprotectedRanges(ranges:Array<google.apps.script.spreadsheet.Range>):google.apps.script.spreadsheet.Protection;
	function removeEditors(emailAddresses:Array<String>):google.apps.script.spreadsheet.Protection;
	function remove(emailAddresses:Array<String>):Void;
	function setNamedRange(namedRange:google.apps.script.spreadsheet.NamedRange):google.apps.script.spreadsheet.Protection;
}
