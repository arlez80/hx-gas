package google.apps.script.spreadsheet;

@:native("RichTextValue")
extern class RichTextValue
{
	function getStartIndex(strategy:google.apps.script.spreadsheet.WrapStrategy):Int;
	function copy(strategy:google.apps.script.spreadsheet.WrapStrategy):google.apps.script.spreadsheet.RichTextValueBuilder;
	@:overload(function (startOffset:Int,endOffset:Int):google.apps.script.spreadsheet.TextStyle {})
	function getTextStyle(strategy:google.apps.script.spreadsheet.WrapStrategy):google.apps.script.spreadsheet.TextStyle;
	function getText(strategy:google.apps.script.spreadsheet.WrapStrategy):String;
	function getRuns(strategy:google.apps.script.spreadsheet.WrapStrategy):Array<google.apps.script.spreadsheet.RichTextValue>;
	function getEndIndex(strategy:google.apps.script.spreadsheet.WrapStrategy):Int;
}
