package google.apps.script.spreadsheet;

@:native("DeveloperMetadataFinder")
extern class DeveloperMetadataFinder
{
	function withValue(value:String):google.apps.script.spreadsheet.DeveloperMetadataFinder;
	function withVisibility(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.DeveloperMetadataFinder;
	function withLocationType(locationType:google.apps.script.spreadsheet.DeveloperMetadataLocationType):google.apps.script.spreadsheet.DeveloperMetadataFinder;
	function onIntersectingLocations(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.DeveloperMetadataFinder;
	function withId(id:Int):google.apps.script.spreadsheet.DeveloperMetadataFinder;
	function find(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):Array<google.apps.script.spreadsheet.DeveloperMetadata>;
	function withKey(key:String):google.apps.script.spreadsheet.DeveloperMetadataFinder;
}
