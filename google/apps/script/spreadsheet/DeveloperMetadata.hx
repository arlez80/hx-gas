package google.apps.script.spreadsheet;

@:native("DeveloperMetadata")
extern class DeveloperMetadata
{
	function moveToRow(row:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.DeveloperMetadata;
	function moveToSpreadsheet(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.DeveloperMetadata;
	function getValue(criteria:google.apps.script.spreadsheet.DataValidationCriteria,args:Array<Dynamic>):String;
	function setVisibility(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.DeveloperMetadata;
	function setKey(key:String):google.apps.script.spreadsheet.DeveloperMetadata;
	function moveToColumn(column:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.DeveloperMetadata;
	function getVisibility(criteria:google.apps.script.spreadsheet.DataValidationCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.DeveloperMetadataVisibility;
	function getId(criteria:google.apps.script.spreadsheet.DataValidationCriteria,args:Array<Dynamic>):Int;
	function setValue(value:String):google.apps.script.spreadsheet.DeveloperMetadata;
	function moveToSheet(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.DeveloperMetadata;
	function getLocation(criteria:google.apps.script.spreadsheet.DataValidationCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.DeveloperMetadataLocation;
	function getKey(criteria:google.apps.script.spreadsheet.DataValidationCriteria,args:Array<Dynamic>):String;
	function remove(sheet:google.apps.script.spreadsheet.Sheet):Void;
}
