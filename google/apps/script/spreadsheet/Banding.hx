package google.apps.script.spreadsheet;

@:native("Banding")
extern class Banding
{
	function setFirstRowColor(color:String):google.apps.script.spreadsheet.Banding;
	function setFooterRowColor(color:String):google.apps.script.spreadsheet.Banding;
	function getFirstRowColor(range:google.apps.script.spreadsheet.Range):String;
	function setRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Banding;
	function setHeaderRowColor(color:String):google.apps.script.spreadsheet.Banding;
	function getRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	function getHeaderColumnColor(range:google.apps.script.spreadsheet.Range):String;
	function copyTo(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Banding;
	function getHeaderRowColor(range:google.apps.script.spreadsheet.Range):String;
	function setHeaderColumnColor(color:String):google.apps.script.spreadsheet.Banding;
	function getFooterColumnColor(range:google.apps.script.spreadsheet.Range):String;
	function setFirstColumnColor(color:String):google.apps.script.spreadsheet.Banding;
	function setFooterColumnColor(color:String):google.apps.script.spreadsheet.Banding;
	function setSecondColumnColor(color:String):google.apps.script.spreadsheet.Banding;
	function setSecondRowColor(color:String):google.apps.script.spreadsheet.Banding;
	function getSecondRowColor(range:google.apps.script.spreadsheet.Range):String;
	function getSecondColumnColor(range:google.apps.script.spreadsheet.Range):String;
	function getFooterRowColor(range:google.apps.script.spreadsheet.Range):String;
	function remove(range:google.apps.script.spreadsheet.Range):Void;
	function getFirstColumnColor(range:google.apps.script.spreadsheet.Range):String;
}
