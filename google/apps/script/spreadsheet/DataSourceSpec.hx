package google.apps.script.spreadsheet;

@:native("DataSourceSpec")
extern class DataSourceSpec
{
	function asBigQuery(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.BigQueryDataSourceSpec;
	function getParameters(spec:google.apps.script.spreadsheet.DataSourceSpec):Array<google.apps.script.spreadsheet.DataSourceParameter>;
	function copy(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.DataSourceSpecBuilder;
	function getType(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.DataSourceType;
}
