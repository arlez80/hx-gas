package google.apps.script.spreadsheet;

@:native("EmbeddedHistogramChartBuilder")
extern class EmbeddedHistogramChartBuilder
{
	function setBackgroundColor(cssValue:String):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function asLineChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedLineChartBuilder;
	function setColors(cssValues:Array<String>):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setMergeStrategy(mergeStrategy:google.apps.script.charts.ChartMergeStrategy):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function getContainer(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.ContainerInfo;
	function asHistogramChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setXAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function asScatterChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setChartType(type:google.apps.script.charts.ChartType):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asAreaChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedAreaChartBuilder;
	function setNumHeaders(headers:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function reverseCategories(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setXAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function removeRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setYAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function asComboChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedComboChartBuilder;
	function getRanges(range:google.apps.script.spreadsheet.Range):Array<google.apps.script.spreadsheet.Range>;
	function asColumnChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedColumnChartBuilder;
	function setTitle(chartTitle:String):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function useLogScale(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function asBarChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedBarChartBuilder;
	function setRange(start:Float,end:Float):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setHiddenDimensionStrategy(strategy:google.apps.script.charts.ChartHiddenDimensionStrategy):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setStacked(start:Float,end:Float):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setYAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function addRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setLegendTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setLegendPosition(position:google.apps.script.charts.Position):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setTransposeRowsAndColumns(transpose:Bool):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setYAxisTitle(title:String):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function getChartType(range:google.apps.script.spreadsheet.Range):google.apps.script.charts.ChartType;
	function setOption(option:String,value:Dynamic):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asTableChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function setPosition(anchorRowPos:Int,anchorColPos:Int,offsetX:Int,offsetY:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function build(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChart;
	function setXAxisTitle(title:String):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function asPieChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedPieChartBuilder;
}
