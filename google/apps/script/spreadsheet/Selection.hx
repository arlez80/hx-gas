package google.apps.script.spreadsheet;

@:native("Selection")
extern class Selection
{
	function getActiveSheet(textStyle:google.apps.script.spreadsheet.TextStyle):google.apps.script.spreadsheet.Sheet;
	function getCurrentCell(textStyle:google.apps.script.spreadsheet.TextStyle):google.apps.script.spreadsheet.Range;
	function getNextDataRange(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.Range;
	function getActiveRangeList(textStyle:google.apps.script.spreadsheet.TextStyle):google.apps.script.spreadsheet.RangeList;
	function getActiveRange(textStyle:google.apps.script.spreadsheet.TextStyle):google.apps.script.spreadsheet.Range;
}
