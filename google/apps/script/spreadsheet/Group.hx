package google.apps.script.spreadsheet;

@:native("Group")
extern class Group
{
	function isCollapsed(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Bool;
	function collapse(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.Group;
	function expand(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.Group;
	function getDepth(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Int;
	function getRange(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.Range;
	function remove(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Void;
	function getControlIndex(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Int;
}
