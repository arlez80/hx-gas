package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("WrapStrategy")
@:enum abstract WrapStrategy(String) from String to String
{
	var WRAP;
	var OVERFLOW;
	var CLIP;
}

