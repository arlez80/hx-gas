package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("DataSourceParameterType")
@:enum abstract DataSourceParameterType(String) from String to String
{
	var DATA_SOURCE_PARAMETER_TYPE_UNSUPPORTED;
	var CELL;
}

