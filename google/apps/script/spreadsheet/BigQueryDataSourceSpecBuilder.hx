package google.apps.script.spreadsheet;

@:native("BigQueryDataSourceSpecBuilder")
extern class BigQueryDataSourceSpecBuilder
{
	function getRawQuery(color:String):String;
	function setRawQuery(rawQuery:String):google.apps.script.spreadsheet.BigQueryDataSourceSpecBuilder;
	function setParameterFromCell(parameterName:String,sourceCell:String):google.apps.script.spreadsheet.BigQueryDataSourceSpecBuilder;
	function removeAllParameters(color:String):google.apps.script.spreadsheet.BigQueryDataSourceSpecBuilder;
	function getProjectId(color:String):String;
	function setProjectId(projectId:String):google.apps.script.spreadsheet.BigQueryDataSourceSpecBuilder;
	function removeParameter(parameterName:String):google.apps.script.spreadsheet.BigQueryDataSourceSpecBuilder;
	function build(color:String):google.apps.script.spreadsheet.DataSourceSpec;
	function copy(color:String):google.apps.script.spreadsheet.DataSourceSpecBuilder;
	function getParameters(color:String):Array<google.apps.script.spreadsheet.DataSourceParameter>;
	function getType(color:String):google.apps.script.spreadsheet.DataSourceType;
}
