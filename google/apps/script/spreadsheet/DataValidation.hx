package google.apps.script.spreadsheet;

@:native("DataValidation")
extern class DataValidation
{
	function getHelpText(timeoutInSeconds:Int):String;
	function getCriteriaType(timeoutInSeconds:Int):google.apps.script.spreadsheet.DataValidationCriteria;
	function copy(timeoutInSeconds:Int):google.apps.script.spreadsheet.DataValidationBuilder;
	function getCriteriaValues(timeoutInSeconds:Int):Array<Dynamic>;
	function getAllowInvalid(timeoutInSeconds:Int):Bool;
}
