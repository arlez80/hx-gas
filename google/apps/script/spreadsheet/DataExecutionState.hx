package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("DataExecutionState")
@:enum abstract DataExecutionState(String) from String to String
{
	var DATA_EXECUTION_STATE_UNSUPPORTED;
	var RUNNING;
	var SUCCESS;
	var ERROR;
	var NOT_STARTED;
}

