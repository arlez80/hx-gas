package google.apps.script.spreadsheet;

@:native("OverGridImage")
extern class OverGridImage
{
	function setWidth(width:Int):google.apps.script.spreadsheet.OverGridImage;
	function getHeight(functionName:String):Int;
	function getUrl(functionName:String):String;
	function getScript(functionName:String):String;
	function setAnchorCellYOffset(offset:Int):google.apps.script.spreadsheet.OverGridImage;
	function assignScript(functionName:String):google.apps.script.spreadsheet.OverGridImage;
	function setAnchorCellXOffset(offset:Int):google.apps.script.spreadsheet.OverGridImage;
	function setAltTextTitle(title:String):google.apps.script.spreadsheet.OverGridImage;
	@:overload(function (url:String):google.apps.script.spreadsheet.OverGridImage {})
	function replace(blob:google.apps.script.base.BlobSource):google.apps.script.spreadsheet.OverGridImage;
	function setAnchorCell(cell:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.OverGridImage;
	function setAltTextDescription(description:String):google.apps.script.spreadsheet.OverGridImage;
	function setHeight(height:Int):google.apps.script.spreadsheet.OverGridImage;
	function getInherentWidth(functionName:String):Int;
	function getInherentHeight(functionName:String):Int;
	function getAnchorCellXOffset(functionName:String):Int;
	function getAnchorCell(functionName:String):google.apps.script.spreadsheet.Range;
	function getSheet(functionName:String):google.apps.script.spreadsheet.Sheet;
	function getAnchorCellYOffset(functionName:String):Int;
	function getAltTextTitle(functionName:String):String;
	function getWidth(functionName:String):Int;
	function getAltTextDescription(functionName:String):String;
	function remove(functionName:String):Void;
	function resetSize(url:String):google.apps.script.spreadsheet.OverGridImage;
}
