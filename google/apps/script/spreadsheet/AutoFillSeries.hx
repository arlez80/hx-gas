package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("AutoFillSeries")
@:enum abstract AutoFillSeries(String) from String to String
{
	var DEFAULT_SERIES;
	var ALTERNATE_SERIES;
}

