package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("Direction")
@:enum abstract Direction(String) from String to String
{
	var UP;
	var DOWN;
	var PREVIOUS;
	var NEXT;
}

