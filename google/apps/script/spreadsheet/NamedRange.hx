package google.apps.script.spreadsheet;

@:native("NamedRange")
extern class NamedRange
{
	function getName(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):String;
	function getRange(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.Range;
	function setName(name:String):google.apps.script.spreadsheet.NamedRange;
	function setRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.NamedRange;
	function remove(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Void;
}
