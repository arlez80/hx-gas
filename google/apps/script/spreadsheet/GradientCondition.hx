package google.apps.script.spreadsheet;

@:native("GradientCondition")
extern class GradientCondition
{
	function getMaxValue(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):String;
	function getMidValue(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):String;
	function getMinColor(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):String;
	function getMaxColor(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):String;
	function getMaxType(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.InterpolationType;
	function getMinValue(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):String;
	function getMidType(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.InterpolationType;
	function getMidColor(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):String;
	function getMinType(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.InterpolationType;
}
