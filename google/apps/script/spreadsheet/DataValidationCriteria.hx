package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("DataValidationCriteria")
@:enum abstract DataValidationCriteria(String) from String to String
{
	var DATE_AFTER;
	var DATE_BEFORE;
	var DATE_BETWEEN;
	var DATE_EQUAL_TO;
	var DATE_IS_VALID_DATE;
	var DATE_NOT_BETWEEN;
	var DATE_ON_OR_AFTER;
	var DATE_ON_OR_BEFORE;
	var NUMBER_BETWEEN;
	var NUMBER_EQUAL_TO;
	var NUMBER_GREATER_THAN;
	var NUMBER_GREATER_THAN_OR_EQUAL_TO;
	var NUMBER_LESS_THAN;
	var NUMBER_LESS_THAN_OR_EQUAL_TO;
	var NUMBER_NOT_BETWEEN;
	var NUMBER_NOT_EQUAL_TO;
	var TEXT_CONTAINS;
	var TEXT_DOES_NOT_CONTAIN;
	var TEXT_EQUAL_TO;
	var TEXT_IS_VALID_EMAIL;
	var TEXT_IS_VALID_URL;
	var VALUE_IN_LIST;
	var VALUE_IN_RANGE;
	var CUSTOM_FORMULA;
	var CHECKBOX;
}

