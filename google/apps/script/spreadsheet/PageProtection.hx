package google.apps.script.spreadsheet;

@:native("PageProtection")
@:deprecated
extern class PageProtection
{
	@:deprecated
	function removeUser(user:String):Void;
	@:deprecated
	function isProtected(email:String):Bool;
	@:deprecated
	function getUsers(email:String):Array<String>;
	@:deprecated
	function setProtected(protection:Bool):Void;
	@:deprecated
	function addUser(email:String):Void;
}
