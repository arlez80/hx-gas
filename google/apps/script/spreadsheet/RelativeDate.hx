package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("RelativeDate")
@:enum abstract RelativeDate(String) from String to String
{
	var TODAY;
	var TOMORROW;
	var YESTERDAY;
	var PAST_WEEK;
	var PAST_MONTH;
	var PAST_YEAR;
}

