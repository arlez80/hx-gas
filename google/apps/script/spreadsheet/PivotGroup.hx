package google.apps.script.spreadsheet;

@:native("PivotGroup")
extern class PivotGroup
{
	function sortDescending(value:google.apps.script.spreadsheet.PivotValue,oppositeGroupValues:Array<Dynamic>):google.apps.script.spreadsheet.PivotGroup;
	function getPivotTable(groupName:String,groupMembers:Array<Dynamic>):google.apps.script.spreadsheet.PivotTable;
	function removeManualGroupingRule(groupName:String):google.apps.script.spreadsheet.PivotGroup;
	function moveToIndex(index:Int):google.apps.script.spreadsheet.PivotGroup;
	function sortAscending(showTotals:Bool):google.apps.script.spreadsheet.PivotGroup;
	function showRepeatedLabels(minValue:Int,maxValue:Int,intervalSize:Int):google.apps.script.spreadsheet.PivotGroup;
	function clearSort(groupName:String,groupMembers:Array<Dynamic>):google.apps.script.spreadsheet.PivotGroup;
	function clearGroupingRule(groupName:String,groupMembers:Array<Dynamic>):google.apps.script.spreadsheet.PivotGroup;
	function totalsAreShown(value:google.apps.script.spreadsheet.PivotValue,oppositeGroupValues:Array<Dynamic>):Bool;
	function showTotals(showTotals:Bool):google.apps.script.spreadsheet.PivotGroup;
	function getIndex(groupName:String,groupMembers:Array<Dynamic>):Int;
	function getSourceDataColumn(groupName:String,groupMembers:Array<Dynamic>):Int;
	function isSortAscending(groupName:String,groupMembers:Array<Dynamic>):Bool;
	function setDisplayName(name:String):google.apps.script.spreadsheet.PivotGroup;
	function getDimension(groupName:String,groupMembers:Array<Dynamic>):google.apps.script.spreadsheet.Dimension;
	function hideRepeatedLabels(groupName:String,groupMembers:Array<Dynamic>):google.apps.script.spreadsheet.PivotGroup;
	function addManualGroupingRule(groupName:String,groupMembers:Array<Dynamic>):google.apps.script.spreadsheet.PivotGroup;
	function setHistogramGroupingRule(minValue:Int,maxValue:Int,intervalSize:Int):google.apps.script.spreadsheet.PivotGroup;
	function sortBy(value:google.apps.script.spreadsheet.PivotValue,oppositeGroupValues:Array<Dynamic>):google.apps.script.spreadsheet.PivotGroup;
	function resetDisplayName(groupName:String):google.apps.script.spreadsheet.PivotGroup;
	function areLabelsRepeated(groupName:String,groupMembers:Array<Dynamic>):Bool;
	function remove(index:Int):Void;
}
