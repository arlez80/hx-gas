package google.apps.script.spreadsheet;

@:native("Filter")
extern class Filter
{
	function getColumnFilterCriteria(columnPosition:Int):google.apps.script.spreadsheet.FilterCriteria;
	function getRange(columnPosition:Int):google.apps.script.spreadsheet.Range;
	function setColumnFilterCriteria(columnPosition:Int,filterCriteria:google.apps.script.spreadsheet.FilterCriteria):google.apps.script.spreadsheet.Filter;
	function sort(columnPosition:Int,ascending:Bool):google.apps.script.spreadsheet.Filter;
	function removeColumnFilterCriteria(columnPosition:Int):google.apps.script.spreadsheet.Filter;
	function remove(columnPosition:Int):Void;
}
