package google.apps.script.spreadsheet;

@:native("DeveloperMetadataLocation")
extern class DeveloperMetadataLocation
{
	function getRow(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Range;
	function getSpreadsheet(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Spreadsheet;
	function getLocationType(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.DeveloperMetadataLocationType;
	function getColumn(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Range;
	function getSheet(visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Sheet;
}
