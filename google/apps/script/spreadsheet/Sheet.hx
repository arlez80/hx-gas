package google.apps.script.spreadsheet;

@:native("Sheet")
extern class Sheet
{
	function getLastRow(columnPosition:Int):Int;
	function unhideColumn(column:google.apps.script.spreadsheet.Range):Void;
	function insertRowAfter(afterPosition:Int):google.apps.script.spreadsheet.Sheet;
	function getFormUrl(columnPosition:Int):String;
	function createDeveloperMetadataFinder(spreadsheet:google.apps.script.spreadsheet.Spreadsheet):google.apps.script.spreadsheet.DeveloperMetadataFinder;
	function insertColumnBefore(beforePosition:Int):google.apps.script.spreadsheet.Sheet;
	function autoResizeRows(startRow:Int,numRows:Int):google.apps.script.spreadsheet.Sheet;
	function setColumnGroupControlPosition(position:google.apps.script.spreadsheet.GroupControlTogglePosition):google.apps.script.spreadsheet.Sheet;
	function getMaxColumns(columnPosition:Int):Int;
	function isRowHiddenByUser(rowPosition:Int):Bool;
	function getIndex(columnPosition:Int):Int;
	function moveRows(rowSpec:google.apps.script.spreadsheet.Range,destinationIndex:Int):Void;
	function getColumnGroupControlPosition(columnIndex:Int,groupDepth:Int):google.apps.script.spreadsheet.GroupControlTogglePosition;
	function setTabColor(color:String):google.apps.script.spreadsheet.Sheet;
	@:overload(function (key:String,visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Sheet {})
	@:overload(function (key:String,value:String):google.apps.script.spreadsheet.Sheet {})
	@:overload(function (key:String,value:String,visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Sheet {})
	function addDeveloperMetadata(key:String):google.apps.script.spreadsheet.Sheet;
	function getFrozenRows(columnPosition:Int):Int;
	function setRowHeight(rowPosition:Int,height:Int):google.apps.script.spreadsheet.Sheet;
	function getRowGroupDepth(rowIndex:Int):Int;
	function insertRowBefore(beforePosition:Int):google.apps.script.spreadsheet.Sheet;
	function clearFormats(options:Dynamic):google.apps.script.spreadsheet.Sheet;
	function isSheetHidden(rowPosition:Int):Bool;
	function deleteColumn(columnPosition:Int):google.apps.script.spreadsheet.Sheet;
	function getColumnGroupDepth(columnIndex:Int):Int;
	function setRowGroupControlPosition(position:google.apps.script.spreadsheet.GroupControlTogglePosition):google.apps.script.spreadsheet.Sheet;
	function deleteRow(rowPosition:Int):google.apps.script.spreadsheet.Sheet;
	function getParent(columnPosition:Int):google.apps.script.spreadsheet.Spreadsheet;
	function insertChart(chart:google.apps.script.spreadsheet.EmbeddedChart):Void;
	function setActiveRangeList(rangeList:google.apps.script.spreadsheet.RangeList):google.apps.script.spreadsheet.RangeList;
	function getDataSourceTables(columnPosition:Int):Array<google.apps.script.spreadsheet.DataSourceTable>;
	function setActiveRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	function removeChart(chart:google.apps.script.spreadsheet.EmbeddedChart):Void;
	function getCurrentCell(columnPosition:Int):google.apps.script.spreadsheet.Range;
	function deleteColumns(columnPosition:Int,howMany:Int):Void;
	function getSheetId(rowPosition:Int):Int;
	@:overload(function (row:Int,column:Int,numRows:Int):google.apps.script.spreadsheet.Range {})
	@:overload(function (row:Int,column:Int,numRows:Int,numColumns:Int):google.apps.script.spreadsheet.Range {})
	@:overload(function (a1Notation:String):google.apps.script.spreadsheet.Range {})
	function getRange(row:Int,column:Int):google.apps.script.spreadsheet.Range;
	function setName(name:String):google.apps.script.spreadsheet.Sheet;
	function setHiddenGridlines(hideGridlines:Bool):google.apps.script.spreadsheet.Sheet;
	@:overload(function (rowIndex:Int,numRows:Int):Void {})
	function insertRows(rowIndex:Int):Void;
	function setColumnWidth(columnPosition:Int,width:Int):google.apps.script.spreadsheet.Sheet;
	@:overload(function (columnIndex:Int,numColumns:Int):Void {})
	function showColumns(columnIndex:Int):Void;
	function setFrozenRows(rows:Int):Void;
	function clearNotes(options:Dynamic):google.apps.script.spreadsheet.Sheet;
	function isColumnHiddenByUser(columnPosition:Int):Bool;
	function isRowHiddenByFilter(rowPosition:Int):Bool;
	function collapseAllRowGroups(options:Dynamic):google.apps.script.spreadsheet.Sheet;
	function updateChart(chart:google.apps.script.spreadsheet.EmbeddedChart):Void;
	function clearConditionalFormatRules(options:Dynamic):Void;
	function hideColumn(column:google.apps.script.spreadsheet.Range):Void;
	function getActiveRangeList(groupDepth:Int):google.apps.script.spreadsheet.RangeList;
	function getDeveloperMetadata(columnPosition:Int):Array<google.apps.script.spreadsheet.DeveloperMetadata>;
	function protect(rowSpec:google.apps.script.spreadsheet.Range,destinationIndex:Int):google.apps.script.spreadsheet.Protection;
	@:deprecated
	function getSheetProtection(chart:google.apps.script.spreadsheet.EmbeddedChart):google.apps.script.spreadsheet.PageProtection;
	function setConditionalFormatRules(rules:Array<google.apps.script.spreadsheet.ConditionalFormatRule>):Void;
	function hideSheet(rowIndex:Int,numRows:Int):google.apps.script.spreadsheet.Sheet;
	function showSheet(rowIndex:Int,numRows:Int):google.apps.script.spreadsheet.Sheet;
	function isRightToLeft(columnPosition:Int):Bool;
	function insertColumnsBefore(beforePosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function getColumnWidth(columnPosition:Int):Int;
	function hasHiddenGridlines(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):Bool;
	function getLastColumn(columnPosition:Int):Int;
	@:deprecated
	function setSheetProtection(permissions:google.apps.script.spreadsheet.PageProtection):Void;
	function activate(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.Sheet;
	@:overload(function (rowIndex:Int,numRows:Int):Void {})
	function showRows(rowIndex:Int):Void;
	function expandColumnGroupsUpToDepth(groupDepth:Int):google.apps.script.spreadsheet.Sheet;
	function deleteRows(rowPosition:Int,howMany:Int):Void;
	function hideRow(row:google.apps.script.spreadsheet.Range):Void;
	@:overload(function (blobSource:google.apps.script.base.BlobSource,column:Int,row:Int,offsetX:Int,offsetY:Int):google.apps.script.spreadsheet.OverGridImage {})
	@:overload(function (url:String,column:Int,row:Int):google.apps.script.spreadsheet.OverGridImage {})
	@:overload(function (url:String,column:Int,row:Int,offsetX:Int,offsetY:Int):google.apps.script.spreadsheet.OverGridImage {})
	function insertImage(blobSource:google.apps.script.base.BlobSource,column:Int,row:Int):google.apps.script.spreadsheet.OverGridImage;
	function getBandings(groupDepth:Int):Array<google.apps.script.spreadsheet.Banding>;
	function getActiveRange(groupDepth:Int):google.apps.script.spreadsheet.Range;
	@:overload(function (options:{?contentsOnly:Bool,?formatOnly:Bool}):google.apps.script.spreadsheet.Sheet {})
	function clear(startRow:Int,numRows:Int):google.apps.script.spreadsheet.Sheet;
	function getTabColor(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):String;
	function setColumnWidths(startColumn:Int,numColumns:Int,width:Int):google.apps.script.spreadsheet.Sheet;
	function insertColumnAfter(afterPosition:Int):google.apps.script.spreadsheet.Sheet;
	function getRowGroup(rowIndex:Int,groupDepth:Int):google.apps.script.spreadsheet.Group;
	function copyTo(spreadsheet:google.apps.script.spreadsheet.Spreadsheet):google.apps.script.spreadsheet.Sheet;
	function getProtections(type:google.apps.script.spreadsheet.ProtectionType):Array<google.apps.script.spreadsheet.Protection>;
	function getNamedRanges(columnPosition:Int):Array<google.apps.script.spreadsheet.NamedRange>;
	function getColumnGroup(columnIndex:Int,groupDepth:Int):google.apps.script.spreadsheet.Group;
	function getDataRange(columnPosition:Int):google.apps.script.spreadsheet.Range;
	function getMaxRows(columnPosition:Int):Int;
	function getFilter(columnPosition:Int):google.apps.script.spreadsheet.Filter;
	@:overload(function (columnPosition:Int,ascending:Bool):google.apps.script.spreadsheet.Sheet {})
	function sort(columnPosition:Int):google.apps.script.spreadsheet.Sheet;
	function autoResizeColumns(startColumn:Int,numColumns:Int):google.apps.script.spreadsheet.Sheet;
	function getRowHeight(rowPosition:Int):Int;
	function getName(columnPosition:Int):String;
	@:overload(function (columnIndex:Int,numColumns:Int):Void {})
	function insertColumns(columnIndex:Int):Void;
	function setRowHeights(startRow:Int,numRows:Int,height:Int):google.apps.script.spreadsheet.Sheet;
	function moveColumns(columnSpec:google.apps.script.spreadsheet.Range,destinationIndex:Int):Void;
	function getActiveCell(groupDepth:Int):google.apps.script.spreadsheet.Range;
	function getImages(columnPosition:Int):Array<google.apps.script.spreadsheet.OverGridImage>;
	function getRangeList(a1Notations:Array<String>):google.apps.script.spreadsheet.RangeList;
	function expandAllRowGroups(rowPosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function getSelection(rowPosition:Int):google.apps.script.spreadsheet.Selection;
	function newChart(rowSpec:google.apps.script.spreadsheet.Range,destinationIndex:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function appendRow(rowContents:Array<Dynamic>):google.apps.script.spreadsheet.Sheet;
	function getSheetName(rowPosition:Int):String;
	function getRowGroupControlPosition(rowIndex:Int,groupDepth:Int):google.apps.script.spreadsheet.GroupControlTogglePosition;
	function getSheetValues(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):Array<Array<Dynamic>>;
	@:overload(function (columnIndex:Int,numColumns:Int):Void {})
	function hideColumns(columnIndex:Int):Void;
	function getConditionalFormatRules(columnPosition:Int):Array<google.apps.script.spreadsheet.ConditionalFormatRule>;
	function getFrozenColumns(columnPosition:Int):Int;
	function insertColumnsAfter(afterPosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function expandAllColumnGroups(rowPosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function autoResizeColumn(columnPosition:Int):google.apps.script.spreadsheet.Sheet;
	function unhideRow(row:google.apps.script.spreadsheet.Range):Void;
	function insertRowsBefore(beforePosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function getPivotTables(columnPosition:Int):Array<google.apps.script.spreadsheet.PivotTable>;
	@:overload(function (rowIndex:Int,numRows:Int):Void {})
	function hideRows(rowIndex:Int):Void;
	function setRightToLeft(rightToLeft:Bool):google.apps.script.spreadsheet.Sheet;
	function insertRowsAfter(afterPosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function clearContents(options:Dynamic):google.apps.script.spreadsheet.Sheet;
	@:overload(function (a1Notation:String):google.apps.script.spreadsheet.Range {})
	function setActiveSelection(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	function getCharts(groupDepth:Int):Array<google.apps.script.spreadsheet.EmbeddedChart>;
	function collapseAllColumnGroups(options:Dynamic):google.apps.script.spreadsheet.Sheet;
	function expandRowGroupsUpToDepth(groupDepth:Int):google.apps.script.spreadsheet.Sheet;
	function setCurrentCell(cell:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	function setFrozenColumns(columns:Int):Void;
}
