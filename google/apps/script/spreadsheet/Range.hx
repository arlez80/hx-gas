package google.apps.script.spreadsheet;

@:native("Range")
extern class Range
{
	function getHeight(firstRowIsHeader:Bool):Int;
	@:overload(function (sheet:google.apps.script.spreadsheet.Sheet,column:Int,columnEnd:Int,row:Int,rowEnd:Int):Void {})
	function copyValuesToRange(gridId:Int,column:Int,columnEnd:Int,row:Int,rowEnd:Int):Void;
	function getWrapStrategy(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.WrapStrategy;
	function setNote(note:String):google.apps.script.spreadsheet.Range;
	function getDataValidations(firstRowIsHeader:Bool):Array<Array<google.apps.script.spreadsheet.DataValidation>>;
	function setFontSizes(sizes:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function getTextDirections(direction:google.apps.script.spreadsheet.Direction):Array<Array<google.apps.script.spreadsheet.TextDirection>>;
	function canEdit(series:google.apps.script.spreadsheet.AutoFillSeries):Bool;
	function getLastRow(firstRowIsHeader:Bool):Int;
	function getDataValidation(firstRowIsHeader:Bool):google.apps.script.spreadsheet.DataValidation;
	function createDeveloperMetadataFinder(sheet:google.apps.script.spreadsheet.Sheet,column:Int,columnEnd:Int,row:Int,rowEnd:Int):google.apps.script.spreadsheet.DeveloperMetadataFinder;
	function shiftRowGroupDepth(delta:Int):google.apps.script.spreadsheet.Range;
	function getRichTextValues(direction:google.apps.script.spreadsheet.Direction):Array<Array<google.apps.script.spreadsheet.RichTextValue>>;
	@:overload(function (bandingTheme:google.apps.script.spreadsheet.BandingTheme):google.apps.script.spreadsheet.Banding {})
	@:overload(function (bandingTheme:google.apps.script.spreadsheet.BandingTheme,showHeader:Bool,showFooter:Bool):google.apps.script.spreadsheet.Banding {})
	function applyRowBanding(bandingTheme:google.apps.script.spreadsheet.BandingTheme,showHeader:Bool,showFooter:Bool):google.apps.script.spreadsheet.Banding;
	function setTextStyle(style:google.apps.script.spreadsheet.TextStyle):google.apps.script.spreadsheet.Range;
	function getBackgrounds(shiftDimension:google.apps.script.spreadsheet.Dimension):Array<Array<String>>;
	@:overload(function (delimiter:String):Void {})
	@:overload(function (delimiter:google.apps.script.spreadsheet.TextToColumnsDelimiter):Void {})
	function splitTextToColumns(sortSpecObj:Dynamic):Void;
	function setWrapStrategies(strategies:Array<Array<google.apps.script.spreadsheet.WrapStrategy>>):google.apps.script.spreadsheet.Range;
	function mergeVertically(shiftDimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.Range;
	function getGridId(firstRowIsHeader:Bool):Int;
	function expandGroups(shiftDimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.Range;
	function getTextDirection(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.TextDirection;
	function getWrapStrategies(direction:google.apps.script.spreadsheet.Direction):Array<Array<google.apps.script.spreadsheet.WrapStrategy>>;
	function getCell(row:Int,column:Int):google.apps.script.spreadsheet.Range;
	function setDataValidation(rule:google.apps.script.spreadsheet.DataValidation):google.apps.script.spreadsheet.Range;
	@:overload(function (key:String,visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Range {})
	@:overload(function (key:String,value:String):google.apps.script.spreadsheet.Range {})
	@:overload(function (key:String,value:String,visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Range {})
	function addDeveloperMetadata(key:String):google.apps.script.spreadsheet.Range;
	function getFontFamilies(firstRowIsHeader:Bool):Array<Array<String>>;
	function setVerticalAlignments(alignments:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function getFormulaR1C1(firstRowIsHeader:Bool):String;
	function getRichTextValue(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.RichTextValue;
	function setTextStyles(styles:Array<Array<google.apps.script.spreadsheet.TextStyle>>):google.apps.script.spreadsheet.Range;
	function getRowIndex(direction:google.apps.script.spreadsheet.Direction):Int;
	function setNumberFormat(numberFormat:String):google.apps.script.spreadsheet.Range;
	function getNumberFormats(direction:google.apps.script.spreadsheet.Direction):Array<Array<String>>;
	function setHorizontalAlignments(alignments:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function setFontSize(size:Int):google.apps.script.spreadsheet.Range;
	function clearNote(options:Dynamic):google.apps.script.spreadsheet.Range;
	function protect(rowOffset:Int,columnOffset:Int,numRows:Int,numColumns:Int):google.apps.script.spreadsheet.Protection;
	function getHorizontalAlignment(firstRowIsHeader:Bool):String;
	function shiftColumnGroupDepth(delta:Int):google.apps.script.spreadsheet.Range;
	function setWraps(isWrapEnabled:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function getFontWeight(firstRowIsHeader:Bool):String;
	function getA1Notation(shiftDimension:google.apps.script.spreadsheet.Dimension):String;
	function getHorizontalAlignments(firstRowIsHeader:Bool):Array<Array<String>>;
	function setWrap(isWrapEnabled:Bool):google.apps.script.spreadsheet.Range;
	function setShowHyperlink(showHyperlink:Bool):google.apps.script.spreadsheet.Range;
	function setFontLine(fontLine:String):google.apps.script.spreadsheet.Range;
	function setRichTextValues(values:Array<Array<google.apps.script.spreadsheet.RichTextValue>>):google.apps.script.spreadsheet.Range;
	function setVerticalAlignment(alignment:String):google.apps.script.spreadsheet.Range;
	function setFontColor(color:String):google.apps.script.spreadsheet.Range;
	function autoFill(destination:google.apps.script.spreadsheet.Range,series:google.apps.script.spreadsheet.AutoFillSeries):Void;
	function isBlank(shiftDimension:google.apps.script.spreadsheet.Dimension):Bool;
	function setBackground(color:String):google.apps.script.spreadsheet.Range;
	function setFontFamily(fontFamily:String):google.apps.script.spreadsheet.Range;
	@:overload(function (top:Bool,left:Bool,bottom:Bool,right:Bool,vertical:Bool,horizontal:Bool,color:String,style:google.apps.script.spreadsheet.BorderStyle):google.apps.script.spreadsheet.Range {})
	function setBorder(top:Bool,left:Bool,bottom:Bool,right:Bool,vertical:Bool,horizontal:Bool):google.apps.script.spreadsheet.Range;
	function setDataValidations(rules:Array<Array<google.apps.script.spreadsheet.DataValidation>>):google.apps.script.spreadsheet.Range;
	function setValues(values:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function setFontWeight(fontWeight:String):google.apps.script.spreadsheet.Range;
	function sort(sortSpecObj:Dynamic):google.apps.script.spreadsheet.Range;
	function setBackgrounds(color:Array<Array<String>>):google.apps.script.spreadsheet.Range;
	function getRow(direction:google.apps.script.spreadsheet.Direction):Int;
	function isEndColumnBounded(shiftDimension:google.apps.script.spreadsheet.Dimension):Bool;
	function getWrap(direction:google.apps.script.spreadsheet.Direction):Bool;
	function setFontStyles(fontStyles:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function clearDataValidations(options:Dynamic):google.apps.script.spreadsheet.Range;
	function getFontSize(firstRowIsHeader:Bool):Int;
	function setBackgroundRGB(red:Int,green:Int,blue:Int):google.apps.script.spreadsheet.Range;
	function getWraps(direction:google.apps.script.spreadsheet.Direction):Array<Array<Bool>>;
	function setRichTextValue(value:google.apps.script.spreadsheet.RichTextValue):google.apps.script.spreadsheet.Range;
	function activate(warningOnly:Bool):google.apps.script.spreadsheet.Range;
	function mergeAcross(shiftDimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.Range;
	function getDeveloperMetadata(firstRowIsHeader:Bool):Array<google.apps.script.spreadsheet.DeveloperMetadata>;
	function getLastColumn(firstRowIsHeader:Bool):Int;
	function setFormula(formula:String):google.apps.script.spreadsheet.Range;
	function getFormulasR1C1(firstRowIsHeader:Bool):Array<Array<String>>;
	function getFontLine(firstRowIsHeader:Bool):String;
	function isEndRowBounded(shiftDimension:google.apps.script.spreadsheet.Dimension):Bool;
	function setTextDirection(direction:google.apps.script.spreadsheet.TextDirection):google.apps.script.spreadsheet.Range;
	function clearContent(options:Dynamic):google.apps.script.spreadsheet.Range;
	function getFontStyles(firstRowIsHeader:Bool):Array<Array<String>>;
	function deleteCells(shiftDimension:google.apps.script.spreadsheet.Dimension):Void;
	function setTextRotations(rotations:Array<Array<google.apps.script.spreadsheet.TextRotation>>):google.apps.script.spreadsheet.Range;
	function getBandings(shiftDimension:google.apps.script.spreadsheet.Dimension):Array<google.apps.script.spreadsheet.Banding>;
	function getColumn(row:Int,column:Int):Int;
	function activateAsCurrentCell(warningOnly:Bool):google.apps.script.spreadsheet.Range;
	function clearFormat(options:Dynamic):google.apps.script.spreadsheet.Range;
	function createFilter(sheet:google.apps.script.spreadsheet.Sheet,column:Int,columnEnd:Int,row:Int,rowEnd:Int):google.apps.script.spreadsheet.Filter;
	function getFontStyle(firstRowIsHeader:Bool):String;
	function createPivotTable(sourceData:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.PivotTable;
	@:overload(function (sheet:google.apps.script.spreadsheet.Sheet,column:Int,columnEnd:Int,row:Int,rowEnd:Int):Void {})
	function copyFormatToRange(gridId:Int,column:Int,columnEnd:Int,row:Int,rowEnd:Int):Void;
	function getFormulas(firstRowIsHeader:Bool):Array<Array<String>>;
	function getDisplayValues(firstRowIsHeader:Bool):Array<Array<String>>;
	function getMergedRanges(firstRowIsHeader:Bool):Array<google.apps.script.spreadsheet.Range>;
	function autoFillToNeighbor(series:google.apps.script.spreadsheet.AutoFillSeries):Void;
	function setHorizontalAlignment(alignment:String):google.apps.script.spreadsheet.Range;
	function merge(shiftDimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.Range;
	function getTextRotation(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.TextRotation;
	function getNumberFormat(direction:google.apps.script.spreadsheet.Direction):String;
	function getFontColor(firstRowIsHeader:Bool):String;
	function getFontWeights(firstRowIsHeader:Bool):Array<Array<String>>;
	function setFontStyle(fontStyle:String):google.apps.script.spreadsheet.Range;
	function getDataSourceUrl(row:Int,column:Int):String;
	function getFontSizes(firstRowIsHeader:Bool):Array<Array<Int>>;
	function getTextStyle(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.TextStyle;
	function insertCells(shiftDimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.Range;
	function getFormula(firstRowIsHeader:Bool):String;
	function setFontFamilies(fontFamilies:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function getNumRows(direction:google.apps.script.spreadsheet.Direction):Int;
	function getFontColors(firstRowIsHeader:Bool):Array<Array<String>>;
	function setFontWeights(fontWeights:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function getDisplayValue(firstRowIsHeader:Bool):String;
	function setWrapStrategy(strategy:google.apps.script.spreadsheet.WrapStrategy):google.apps.script.spreadsheet.Range;
	function setFormulas(formulas:Array<Array<String>>):google.apps.script.spreadsheet.Range;
	@:overload(function (rowOffset:Int,columnOffset:Int,numRows:Int):google.apps.script.spreadsheet.Range {})
	@:overload(function (rowOffset:Int,columnOffset:Int,numRows:Int,numColumns:Int):google.apps.script.spreadsheet.Range {})
	function offset(rowOffset:Int,columnOffset:Int):google.apps.script.spreadsheet.Range;
	function getValue(direction:google.apps.script.spreadsheet.Direction):Dynamic;
	@:overload(function (firstRowIsHeader:Bool):google.apps.script.charts.DataTable {})
	function getDataTable(row:Int,column:Int):google.apps.script.charts.DataTable;
	function setFontColors(colors:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function getVerticalAlignments(direction:google.apps.script.spreadsheet.Direction):Array<Array<String>>;
	function getVerticalAlignment(direction:google.apps.script.spreadsheet.Direction):String;
	function setTextDirections(directions:Array<Array<google.apps.script.spreadsheet.TextDirection>>):google.apps.script.spreadsheet.Range;
	function getNote(direction:google.apps.script.spreadsheet.Direction):String;
	@:overload(function (options:{?commentsOnly:Bool,?contentsOnly:Bool,?formatOnly:Bool,?validationsOnly:Bool,?skipFilteredRows:Bool}):google.apps.script.spreadsheet.Range {})
	function clear(series:google.apps.script.spreadsheet.AutoFillSeries):google.apps.script.spreadsheet.Range;
	function setNumberFormats(numberFormats:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function getFontLines(firstRowIsHeader:Bool):Array<Array<String>>;
	function setFontLines(fontLines:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function breakApart(series:google.apps.script.spreadsheet.AutoFillSeries):google.apps.script.spreadsheet.Range;
	function isPartOfMerge(shiftDimension:google.apps.script.spreadsheet.Dimension):Bool;
	function getValues(direction:google.apps.script.spreadsheet.Direction):Array<Array<Dynamic>>;
	function getNumColumns(direction:google.apps.script.spreadsheet.Direction):Int;
	@:overload(function (bandingTheme:google.apps.script.spreadsheet.BandingTheme):google.apps.script.spreadsheet.Banding {})
	@:overload(function (bandingTheme:google.apps.script.spreadsheet.BandingTheme,showHeader:Bool,showFooter:Bool):google.apps.script.spreadsheet.Banding {})
	function applyColumnBanding(key:String,value:String,visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Banding;
	function randomize(rowOffset:Int,columnOffset:Int,numRows:Int,numColumns:Int):google.apps.script.spreadsheet.Range;
	function getTextRotations(direction:google.apps.script.spreadsheet.Direction):Array<Array<google.apps.script.spreadsheet.TextRotation>>;
	function getDataSourceTables(row:Int,column:Int):Array<google.apps.script.spreadsheet.DataSourceTable>;
	function collapseGroups(options:Dynamic):google.apps.script.spreadsheet.Range;
	function isStartRowBounded(shiftDimension:google.apps.script.spreadsheet.Dimension):Bool;
	function setVerticalText(isVertical:Bool):google.apps.script.spreadsheet.Range;
	function setNotes(notes:Array<Array<Dynamic>>):google.apps.script.spreadsheet.Range;
	function isStartColumnBounded(shiftDimension:google.apps.script.spreadsheet.Dimension):Bool;
	function setFormulasR1C1(formulas:Array<Array<String>>):google.apps.script.spreadsheet.Range;
	function getNotes(direction:google.apps.script.spreadsheet.Direction):Array<Array<String>>;
	function getFontFamily(firstRowIsHeader:Bool):String;
	function setFormulaR1C1(formula:String):google.apps.script.spreadsheet.Range;
	@:overload(function (rotation:google.apps.script.spreadsheet.TextRotation):google.apps.script.spreadsheet.Range {})
	function setTextRotation(degrees:Int):google.apps.script.spreadsheet.Range;
	function getBackground(shiftDimension:google.apps.script.spreadsheet.Dimension):String;
	function getSheet(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.Sheet;
	function getTextStyles(direction:google.apps.script.spreadsheet.Direction):Array<Array<google.apps.script.spreadsheet.TextStyle>>;
	function getFilter(firstRowIsHeader:Bool):google.apps.script.spreadsheet.Filter;
	function setValue(value:Dynamic):google.apps.script.spreadsheet.Range;
	function moveTo(target:google.apps.script.spreadsheet.Range):Void;
	@:overload(function (destination:google.apps.script.spreadsheet.Range,copyPasteType:google.apps.script.spreadsheet.CopyPasteType,transposed:Bool):Void {})
	@:overload(function (destination:google.apps.script.spreadsheet.Range,options:{?formatOnly:Bool,?contentsOnly:Bool}):Void {})
	function copyTo(destination:google.apps.script.spreadsheet.Range):Void;
	function getWidth(direction:google.apps.script.spreadsheet.Direction):Int;
	function getNextDataCell(direction:google.apps.script.spreadsheet.Direction):google.apps.script.spreadsheet.Range;
}
