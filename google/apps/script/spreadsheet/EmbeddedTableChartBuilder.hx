package google.apps.script.spreadsheet;

@:native("EmbeddedTableChartBuilder")
extern class EmbeddedTableChartBuilder
{
	function enableSorting(enableSorting:Bool):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function useAlternatingRowStyle(alternate:Bool):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function asLineChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedLineChartBuilder;
	function setInitialSortingAscending(column:Int):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function getContainer(enableSorting:Bool):google.apps.script.spreadsheet.ContainerInfo;
	function asHistogramChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function asScatterChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setChartType(type:google.apps.script.charts.ChartType):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asAreaChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedAreaChartBuilder;
	function setFirstRowNumber(number:Int):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function setNumHeaders(headers:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function removeRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asComboChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedComboChartBuilder;
	function getRanges(enableSorting:Bool):Array<google.apps.script.spreadsheet.Range>;
	function asColumnChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedColumnChartBuilder;
	function showRowNumberColumn(showRowNumber:Bool):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	@:overload(function (pageSize:Int):google.apps.script.spreadsheet.EmbeddedTableChartBuilder {})
	@:overload(function (pageSize:Int,startPage:Int):google.apps.script.spreadsheet.EmbeddedTableChartBuilder {})
	function enablePaging(enablePaging:Bool):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function asBarChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedBarChartBuilder;
	function setMergeStrategy(mergeStrategy:google.apps.script.charts.ChartMergeStrategy):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setHiddenDimensionStrategy(strategy:google.apps.script.charts.ChartHiddenDimensionStrategy):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function addRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setTransposeRowsAndColumns(transpose:Bool):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function getChartType(enableSorting:Bool):google.apps.script.charts.ChartType;
	function setOption(option:String,value:Dynamic):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asTableChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function setPosition(anchorRowPos:Int,anchorColPos:Int,offsetX:Int,offsetY:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function build(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChart;
	function asPieChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedPieChartBuilder;
	function setInitialSortingDescending(column:Int):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function enableRtlTable(rtlEnabled:Bool):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
}
