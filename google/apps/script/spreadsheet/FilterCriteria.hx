package google.apps.script.spreadsheet;

@:native("FilterCriteria")
extern class FilterCriteria
{
	function getVisibleValues(columnPosition:Int,ascending:Bool):Array<String>;
	function getHiddenValues(columnPosition:Int,ascending:Bool):Array<String>;
	function copy(columnPosition:Int,ascending:Bool):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function getCriteriaValues(columnPosition:Int,ascending:Bool):Array<Dynamic>;
	function getCriteriaType(columnPosition:Int,ascending:Bool):google.apps.script.spreadsheet.BooleanCriteria;
}
