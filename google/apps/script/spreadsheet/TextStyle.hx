package google.apps.script.spreadsheet;

@:native("TextStyle")
extern class TextStyle
{
	function isStrikethrough(permissions:google.apps.script.spreadsheet.PageProtection):Bool;
	function getForegroundColor(permissions:google.apps.script.spreadsheet.PageProtection):String;
	function getFontSize(permissions:google.apps.script.spreadsheet.PageProtection):Int;
	function isUnderline(permissions:google.apps.script.spreadsheet.PageProtection):Bool;
	function copy(permissions:google.apps.script.spreadsheet.PageProtection):google.apps.script.spreadsheet.TextStyleBuilder;
	function isBold(permissions:google.apps.script.spreadsheet.PageProtection):Bool;
	function isItalic(permissions:google.apps.script.spreadsheet.PageProtection):Bool;
	function getFontFamily(permissions:google.apps.script.spreadsheet.PageProtection):String;
}
