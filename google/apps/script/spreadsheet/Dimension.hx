package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("Dimension")
@:enum abstract Dimension(String) from String to String
{
	var COLUMNS;
	var ROWS;
}

