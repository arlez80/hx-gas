package google.apps.script.spreadsheet;

@:native("EmbeddedScatterChartBuilder")
extern class EmbeddedScatterChartBuilder
{
	function setBackgroundColor(cssValue:String):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function asLineChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedLineChartBuilder;
	function setColors(cssValues:Array<String>):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setMergeStrategy(mergeStrategy:google.apps.script.charts.ChartMergeStrategy):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function getContainer(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.ContainerInfo;
	function asHistogramChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function setXAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function asScatterChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setChartType(type:google.apps.script.charts.ChartType):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asAreaChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedAreaChartBuilder;
	function setYAxisRange(start:Float,end:Float):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setNumHeaders(headers:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setXAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setPointStyle(style:google.apps.script.charts.PointStyle):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function removeRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setYAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function asComboChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedComboChartBuilder;
	function getRanges(range:google.apps.script.spreadsheet.Range):Array<google.apps.script.spreadsheet.Range>;
	function asColumnChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedColumnChartBuilder;
	function setTitle(chartTitle:String):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function asBarChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedBarChartBuilder;
	function setXAxisRange(start:Float,end:Float):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setHiddenDimensionStrategy(strategy:google.apps.script.charts.ChartHiddenDimensionStrategy):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setYAxisLogScale(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setYAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function addRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setLegendPosition(position:google.apps.script.charts.Position):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setLegendTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setTransposeRowsAndColumns(transpose:Bool):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setYAxisTitle(title:String):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function getChartType(range:google.apps.script.spreadsheet.Range):google.apps.script.charts.ChartType;
	function setOption(option:String,value:Dynamic):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asTableChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function setPosition(anchorRowPos:Int,anchorColPos:Int,offsetX:Int,offsetY:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function build(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChart;
	function setXAxisLogScale(transpose:Bool):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setXAxisTitle(title:String):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function asPieChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedPieChartBuilder;
}
