package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("CopyPasteType")
@:enum abstract CopyPasteType(String) from String to String
{
	var PASTE_NORMAL;
	var PASTE_NO_BORDERS;
	var PASTE_FORMAT;
	var PASTE_FORMULA;
	var PASTE_DATA_VALIDATION;
	var PASTE_VALUES;
	var PASTE_CONDITIONAL_FORMATTING;
	var PASTE_COLUMN_WIDTHS;
}

