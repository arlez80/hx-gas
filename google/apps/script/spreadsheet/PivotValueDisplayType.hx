package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("PivotValueDisplayType")
@:enum abstract PivotValueDisplayType(String) from String to String
{
	var DEFAULT;
	var PERCENT_OF_ROW_TOTAL;
	var PERCENT_OF_COLUMN_TOTAL;
	var PERCENT_OF_GRAND_TOTAL;
}

