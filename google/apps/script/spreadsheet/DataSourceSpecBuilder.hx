package google.apps.script.spreadsheet;

@:native("DataSourceSpecBuilder")
extern class DataSourceSpecBuilder
{
	function asBigQuery(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.BigQueryDataSourceSpecBuilder;
	function setParameterFromCell(parameterName:String,sourceCell:String):google.apps.script.spreadsheet.DataSourceSpecBuilder;
	function removeAllParameters(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.DataSourceSpecBuilder;
	function removeParameter(parameterName:String):google.apps.script.spreadsheet.DataSourceSpecBuilder;
	function build(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.DataSourceSpec;
	function copy(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.DataSourceSpecBuilder;
	function getParameters(spec:google.apps.script.spreadsheet.DataSourceSpec):Array<google.apps.script.spreadsheet.DataSourceParameter>;
	function getType(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.DataSourceType;
}
