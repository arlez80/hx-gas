package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("DeveloperMetadataLocationType")
@:enum abstract DeveloperMetadataLocationType(String) from String to String
{
	var SPREADSHEET;
	var SHEET;
	var ROW;
	var COLUMN;
}

