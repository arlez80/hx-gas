package google.apps.script.spreadsheet;

@:native("FilterCriteriaBuilder")
extern class FilterCriteriaBuilder
{
	function whenNumberLessThan(number:Float):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function setVisibleValues(values:Array<String>):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function withCriteria(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenNumberNotBetween(start:Float,end:Float):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenFormulaSatisfied(formula:String):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	@:overload(function (date:google.apps.script.spreadsheet.RelativeDate):google.apps.script.spreadsheet.FilterCriteriaBuilder {})
	function whenDateBefore(date:Date):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenTextEndsWith(text:String):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function getCriteriaValues(columnPosition:Int,ascending:Bool):Array<Dynamic>;
	function whenCellEmpty(values:Array<String>):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function getCriteriaType(columnPosition:Int,ascending:Bool):google.apps.script.spreadsheet.BooleanCriteria;
	function whenTextEqualTo(text:String):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenTextStartsWith(text:String):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenNumberGreaterThan(number:Float):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function getHiddenValues(columnPosition:Int,ascending:Bool):Array<String>;
	function whenNumberEqualTo(number:Float):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function getVisibleValues(columnPosition:Int,ascending:Bool):Array<String>;
	function setHiddenValues(values:Array<String>):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenCellNotEmpty(values:Array<String>):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenNumberBetween(start:Float,end:Float):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenTextDoesNotContain(text:String):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenNumberGreaterThanOrEqualTo(number:Float):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	@:overload(function (date:google.apps.script.spreadsheet.RelativeDate):google.apps.script.spreadsheet.FilterCriteriaBuilder {})
	function whenDateAfter(date:Date):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenNumberLessThanOrEqualTo(number:Float):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function build(columnPosition:Int,ascending:Bool):google.apps.script.spreadsheet.FilterCriteria;
	function copy(columnPosition:Int,ascending:Bool):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	@:overload(function (date:google.apps.script.spreadsheet.RelativeDate):google.apps.script.spreadsheet.FilterCriteriaBuilder {})
	function whenDateEqualTo(date:Date):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenTextContains(text:String):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	function whenNumberNotEqualTo(number:Float):google.apps.script.spreadsheet.FilterCriteriaBuilder;
}
