package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("DataSourceType")
@:enum abstract DataSourceType(String) from String to String
{
	var DATA_SOURCE_TYPE_UNSUPPORTED;
	var BIGQUERY;
}

