package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("PivotTableSummarizeFunction")
@:enum abstract PivotTableSummarizeFunction(String) from String to String
{
	var CUSTOM;
	var SUM;
	var COUNTA;
	var COUNT;
	var COUNTUNIQUE;
	var AVERAGE;
	var MAX;
	var MIN;
	var MEDIAN;
	var PRODUCT;
	var STDEV;
	var STDEVP;
	var VAR;
	var VARP;
}

