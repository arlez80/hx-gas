package google.apps.script.spreadsheet;

@:native("ConditionalFormatRule")
extern class ConditionalFormatRule
{
	function getGradientCondition(rawQuery:String):google.apps.script.spreadsheet.GradientCondition;
	function copy(rawQuery:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function getBooleanCondition(rawQuery:String):google.apps.script.spreadsheet.BooleanCondition;
	function getRanges(rawQuery:String):Array<google.apps.script.spreadsheet.Range>;
}
