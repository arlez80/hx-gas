package google.apps.script.spreadsheet;

@:native("ConditionalFormatRuleBuilder")
extern class ConditionalFormatRuleBuilder
{
	function whenNumberLessThan(number:Float):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setUnderline(underline:Bool):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenTextContains(text:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenFormulaSatisfied(formula:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setBold(bold:Bool):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	@:overload(function (date:google.apps.script.spreadsheet.RelativeDate):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder {})
	function whenDateBefore(date:Date):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenNumberNotBetween(start:Float,end:Float):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenTextEndsWith(text:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setGradientMaxpoint(color:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setGradientMinpointWithValue(color:String,type:google.apps.script.spreadsheet.InterpolationType,value:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setFontColor(color:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenNumberLessThanOrEqualTo(number:Float):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenCellEmpty(underline:Bool):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function withCriteria(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenTextEqualTo(text:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenTextStartsWith(text:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenNumberGreaterThan(number:Float):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function getRanges(rawQuery:String):Array<google.apps.script.spreadsheet.Range>;
	function setRanges(ranges:Array<google.apps.script.spreadsheet.Range>):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function getBooleanCondition(rawQuery:String):google.apps.script.spreadsheet.BooleanCondition;
	function setBackground(color:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenCellNotEmpty(underline:Bool):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenNumberBetween(start:Float,end:Float):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setGradientMinpoint(color:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenTextDoesNotContain(text:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function getGradientCondition(rawQuery:String):google.apps.script.spreadsheet.GradientCondition;
	function build(rawQuery:String):google.apps.script.spreadsheet.ConditionalFormatRule;
	function setItalic(italic:Bool):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenNumberGreaterThanOrEqualTo(number:Float):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setGradientMaxpointWithValue(color:String,type:google.apps.script.spreadsheet.InterpolationType,value:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	@:overload(function (date:google.apps.script.spreadsheet.RelativeDate):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder {})
	function whenDateAfter(date:Date):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setGradientMidpointWithValue(color:String,type:google.apps.script.spreadsheet.InterpolationType,value:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function copy(rawQuery:String):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function setStrikethrough(strikethrough:Bool):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	@:overload(function (date:google.apps.script.spreadsheet.RelativeDate):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder {})
	function whenDateEqualTo(date:Date):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenNumberEqualTo(number:Float):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	function whenNumberNotEqualTo(number:Float):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
}
