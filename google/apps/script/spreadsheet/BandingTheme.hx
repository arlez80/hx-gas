package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("BandingTheme")
@:enum abstract BandingTheme(String) from String to String
{
	var LIGHT_GREY;
	var CYAN;
	var GREEN;
	var YELLOW;
	var ORANGE;
	var BLUE;
	var TEAL;
	var GREY;
	var BROWN;
	var LIGHT_GREEN;
	var INDIGO;
	var PINK;
}

