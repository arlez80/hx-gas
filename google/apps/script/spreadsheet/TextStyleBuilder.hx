package google.apps.script.spreadsheet;

@:native("TextStyleBuilder")
extern class TextStyleBuilder
{
	function setUnderline(underline:Bool):google.apps.script.spreadsheet.TextStyleBuilder;
	function setFontSize(fontSize:Int):google.apps.script.spreadsheet.TextStyleBuilder;
	function setBold(bold:Bool):google.apps.script.spreadsheet.TextStyleBuilder;
	function build(permissions:google.apps.script.spreadsheet.PageProtection):google.apps.script.spreadsheet.TextStyle;
	function setFontFamily(fontFamily:String):google.apps.script.spreadsheet.TextStyleBuilder;
	function setForegroundColor(cssString:String):google.apps.script.spreadsheet.TextStyleBuilder;
	function setStrikethrough(strikethrough:Bool):google.apps.script.spreadsheet.TextStyleBuilder;
	function setItalic(italic:Bool):google.apps.script.spreadsheet.TextStyleBuilder;
}
