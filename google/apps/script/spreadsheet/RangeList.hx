package google.apps.script.spreadsheet;

@:native("RangeList")
extern class RangeList
{
	function setWrapStrategy(strategy:google.apps.script.spreadsheet.WrapStrategy):google.apps.script.spreadsheet.RangeList;
	function setNote(note:String):google.apps.script.spreadsheet.RangeList;
	function setNumberFormat(numberFormat:String):google.apps.script.spreadsheet.RangeList;
	function clearContent(options:Dynamic):google.apps.script.spreadsheet.RangeList;
	function setWrap(isWrapEnabled:Bool):google.apps.script.spreadsheet.RangeList;
	function breakApart(delimiter:google.apps.script.spreadsheet.TextToColumnsDelimiter):google.apps.script.spreadsheet.RangeList;
	@:overload(function (top:Bool,left:Bool,bottom:Bool,right:Bool,vertical:Bool,horizontal:Bool,color:String,style:google.apps.script.spreadsheet.BorderStyle):google.apps.script.spreadsheet.RangeList {})
	function setBorder(top:Bool,left:Bool,bottom:Bool,right:Bool,vertical:Bool,horizontal:Bool):google.apps.script.spreadsheet.RangeList;
	function setFormula(formula:String):google.apps.script.spreadsheet.RangeList;
	function setVerticalAlignment(alignment:String):google.apps.script.spreadsheet.RangeList;
	function setValue(value:Dynamic):google.apps.script.spreadsheet.RangeList;
	function setFontSize(size:Int):google.apps.script.spreadsheet.RangeList;
	function setFontFamily(fontFamily:String):google.apps.script.spreadsheet.RangeList;
	function setFontLine(fontLine:String):google.apps.script.spreadsheet.RangeList;
	function clearFormat(options:Dynamic):google.apps.script.spreadsheet.RangeList;
	@:overload(function (options:{?commentsOnly:Bool,?contentsOnly:Bool,?formatOnly:Bool,?validationsOnly:Bool,?skipFilteredRows:Bool}):google.apps.script.spreadsheet.RangeList {})
	function clear(delimiter:google.apps.script.spreadsheet.TextToColumnsDelimiter):google.apps.script.spreadsheet.RangeList;
	function setFontColor(color:String):google.apps.script.spreadsheet.RangeList;
	function setBackground(color:String):google.apps.script.spreadsheet.RangeList;
	function setTextDirection(direction:google.apps.script.spreadsheet.TextDirection):google.apps.script.spreadsheet.RangeList;
	function setVerticalText(isVertical:Bool):google.apps.script.spreadsheet.RangeList;
	function setHorizontalAlignment(alignment:String):google.apps.script.spreadsheet.RangeList;
	function activate(delimiter:google.apps.script.spreadsheet.TextToColumnsDelimiter):google.apps.script.spreadsheet.RangeList;
	function setFormulaR1C1(formula:String):google.apps.script.spreadsheet.RangeList;
	function setTextRotation(degrees:Int):google.apps.script.spreadsheet.RangeList;
	function getRanges(options:Dynamic):Array<google.apps.script.spreadsheet.Range>;
	function setFontStyle(fontStyle:String):google.apps.script.spreadsheet.RangeList;
	function setBackgroundRGB(red:Int,green:Int,blue:Int):google.apps.script.spreadsheet.RangeList;
	function setShowHyperlink(showHyperlink:Bool):google.apps.script.spreadsheet.RangeList;
	function clearNote(options:Dynamic):google.apps.script.spreadsheet.RangeList;
	function clearDataValidations(options:Dynamic):google.apps.script.spreadsheet.RangeList;
	function setFontWeight(fontWeight:String):google.apps.script.spreadsheet.RangeList;
}
