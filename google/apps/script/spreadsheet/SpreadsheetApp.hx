package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("SpreadsheetApp.AutoFillSeries")
@:enum abstract AutoFillSeries(String) from String to String
{
	var DEFAULT_SERIES;
	var ALTERNATE_SERIES;
}

@:require(hx_gas)
@:native("SpreadsheetApp.BandingTheme")
@:enum abstract BandingTheme(String) from String to String
{
	var LIGHT_GREY;
	var CYAN;
	var GREEN;
	var YELLOW;
	var ORANGE;
	var BLUE;
	var TEAL;
	var GREY;
	var BROWN;
	var LIGHT_GREEN;
	var INDIGO;
	var PINK;
}

@:require(hx_gas)
@:native("SpreadsheetApp.BooleanCriteria")
@:enum abstract BooleanCriteria(String) from String to String
{
	var CELL_EMPTY;
	var CELL_NOT_EMPTY;
	var DATE_AFTER;
	var DATE_BEFORE;
	var DATE_EQUAL_TO;
	var DATE_AFTER_RELATIVE;
	var DATE_BEFORE_RELATIVE;
	var DATE_EQUAL_TO_RELATIVE;
	var NUMBER_BETWEEN;
	var NUMBER_EQUAL_TO;
	var NUMBER_GREATER_THAN;
	var NUMBER_GREATER_THAN_OR_EQUAL_TO;
	var NUMBER_LESS_THAN;
	var NUMBER_LESS_THAN_OR_EQUAL_TO;
	var NUMBER_NOT_BETWEEN;
	var NUMBER_NOT_EQUAL_TO;
	var TEXT_CONTAINS;
	var TEXT_DOES_NOT_CONTAIN;
	var TEXT_EQUAL_TO;
	var TEXT_STARTS_WITH;
	var TEXT_ENDS_WITH;
	var CUSTOM_FORMULA;
}

@:require(hx_gas)
@:native("SpreadsheetApp.BorderStyle")
@:enum abstract BorderStyle(String) from String to String
{
	var DOTTED;
	var DASHED;
	var SOLID;
	var SOLID_MEDIUM;
	var SOLID_THICK;
	var DOUBLE;
}

@:require(hx_gas)
@:native("SpreadsheetApp.CopyPasteType")
@:enum abstract CopyPasteType(String) from String to String
{
	var PASTE_NORMAL;
	var PASTE_NO_BORDERS;
	var PASTE_FORMAT;
	var PASTE_FORMULA;
	var PASTE_DATA_VALIDATION;
	var PASTE_VALUES;
	var PASTE_CONDITIONAL_FORMATTING;
	var PASTE_COLUMN_WIDTHS;
}

@:require(hx_gas)
@:native("SpreadsheetApp.DataExecutionErrorCode")
@:enum abstract DataExecutionErrorCode(String) from String to String
{
	var DATA_EXECUTION_ERROR_CODE_UNSUPPORTED;
	var NONE;
	var TIME_OUT;
	var TOO_MANY_ROWS;
	var TOO_MANY_CELLS;
	var ENGINE;
	var PARAMETER_INVALID;
	var UNSUPPORTED_DATA_TYPE;
	var DUPLICATE_COLUMN_NAMES;
	var INTERRUPTED;
	var OTHER;
	var TOO_MANY_CHARS_PER_CELL;
}

@:require(hx_gas)
@:native("SpreadsheetApp.DataExecutionState")
@:enum abstract DataExecutionState(String) from String to String
{
	var DATA_EXECUTION_STATE_UNSUPPORTED;
	var RUNNING;
	var SUCCESS;
	var ERROR;
	var NOT_STARTED;
}

@:require(hx_gas)
@:native("SpreadsheetApp.DataSourceParameterType")
@:enum abstract DataSourceParameterType(String) from String to String
{
	var DATA_SOURCE_PARAMETER_TYPE_UNSUPPORTED;
	var CELL;
}

@:require(hx_gas)
@:native("SpreadsheetApp.DataSourceType")
@:enum abstract DataSourceType(String) from String to String
{
	var DATA_SOURCE_TYPE_UNSUPPORTED;
	var BIGQUERY;
}

@:require(hx_gas)
@:native("SpreadsheetApp.DataValidationCriteria")
@:enum abstract DataValidationCriteria(String) from String to String
{
	var DATE_AFTER;
	var DATE_BEFORE;
	var DATE_BETWEEN;
	var DATE_EQUAL_TO;
	var DATE_IS_VALID_DATE;
	var DATE_NOT_BETWEEN;
	var DATE_ON_OR_AFTER;
	var DATE_ON_OR_BEFORE;
	var NUMBER_BETWEEN;
	var NUMBER_EQUAL_TO;
	var NUMBER_GREATER_THAN;
	var NUMBER_GREATER_THAN_OR_EQUAL_TO;
	var NUMBER_LESS_THAN;
	var NUMBER_LESS_THAN_OR_EQUAL_TO;
	var NUMBER_NOT_BETWEEN;
	var NUMBER_NOT_EQUAL_TO;
	var TEXT_CONTAINS;
	var TEXT_DOES_NOT_CONTAIN;
	var TEXT_EQUAL_TO;
	var TEXT_IS_VALID_EMAIL;
	var TEXT_IS_VALID_URL;
	var VALUE_IN_LIST;
	var VALUE_IN_RANGE;
	var CUSTOM_FORMULA;
	var CHECKBOX;
}

@:require(hx_gas)
@:native("SpreadsheetApp.DeveloperMetadataLocationType")
@:enum abstract DeveloperMetadataLocationType(String) from String to String
{
	var SPREADSHEET;
	var SHEET;
	var ROW;
	var COLUMN;
}

@:require(hx_gas)
@:native("SpreadsheetApp.DeveloperMetadataVisibility")
@:enum abstract DeveloperMetadataVisibility(String) from String to String
{
	var DOCUMENT;
	var PROJECT;
}

@:require(hx_gas)
@:native("SpreadsheetApp.Dimension")
@:enum abstract Dimension(String) from String to String
{
	var COLUMNS;
	var ROWS;
}

@:require(hx_gas)
@:native("SpreadsheetApp.Direction")
@:enum abstract Direction(String) from String to String
{
	var UP;
	var DOWN;
	var PREVIOUS;
	var NEXT;
}

@:require(hx_gas)
@:native("SpreadsheetApp.GroupControlTogglePosition")
@:enum abstract GroupControlTogglePosition(String) from String to String
{
	var BEFORE;
	var AFTER;
}

@:require(hx_gas)
@:native("SpreadsheetApp.InterpolationType")
@:enum abstract InterpolationType(String) from String to String
{
	var NUMBER;
	var PERCENT;
	var PERCENTILE;
	var MIN;
	var MAX;
}

@:require(hx_gas)
@:native("SpreadsheetApp.PivotTableSummarizeFunction")
@:enum abstract PivotTableSummarizeFunction(String) from String to String
{
	var CUSTOM;
	var SUM;
	var COUNTA;
	var COUNT;
	var COUNTUNIQUE;
	var AVERAGE;
	var MAX;
	var MIN;
	var MEDIAN;
	var PRODUCT;
	var STDEV;
	var STDEVP;
	var VAR;
	var VARP;
}

@:require(hx_gas)
@:native("SpreadsheetApp.PivotValueDisplayType")
@:enum abstract PivotValueDisplayType(String) from String to String
{
	var DEFAULT;
	var PERCENT_OF_ROW_TOTAL;
	var PERCENT_OF_COLUMN_TOTAL;
	var PERCENT_OF_GRAND_TOTAL;
}

@:require(hx_gas)
@:native("SpreadsheetApp.ProtectionType")
@:enum abstract ProtectionType(String) from String to String
{
	var RANGE;
	var SHEET;
}

@:require(hx_gas)
@:native("SpreadsheetApp.RelativeDate")
@:enum abstract RelativeDate(String) from String to String
{
	var TODAY;
	var TOMORROW;
	var YESTERDAY;
	var PAST_WEEK;
	var PAST_MONTH;
	var PAST_YEAR;
}

@:require(hx_gas)
@:native("SpreadsheetApp.TextDirection")
@:enum abstract TextDirection(String) from String to String
{
	var LEFT_TO_RIGHT;
	var RIGHT_TO_LEFT;
}

@:require(hx_gas)
@:native("SpreadsheetApp.TextToColumnsDelimiter")
@:enum abstract TextToColumnsDelimiter(String) from String to String
{
	var COMMA;
	var SEMICOLON;
	var PERIOD;
	var SPACE;
}

@:require(hx_gas)
@:native("SpreadsheetApp.WrapStrategy")
@:enum abstract WrapStrategy(String) from String to String
{
	var WRAP;
	var OVERFLOW;
	var CLIP;
}

@:native("SpreadsheetApp")
extern class SpreadsheetApp
{
	static function getActive(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.Spreadsheet;
	static function newConditionalFormatRule(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.ConditionalFormatRuleBuilder;
	static function flush(name:String,rows:Int,columns:Int):Void;
	static function newFilterCriteria(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.FilterCriteriaBuilder;
	static function getUi(name:String,rows:Int,columns:Int):google.apps.script.base.Ui;
	static function enableAllDataSourcesExecution(name:String,rows:Int,columns:Int):Void;
	static function setActiveRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	static function getCurrentCell(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.Range;
	static function newDataValidation(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.DataValidationBuilder;
	@:overload(function (name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.Spreadsheet {})
	static function create(name:String):google.apps.script.spreadsheet.Spreadsheet;
	static function setActiveRangeList(rangeList:google.apps.script.spreadsheet.RangeList):google.apps.script.spreadsheet.RangeList;
	static function enableBigQueryExecution(name:String,rows:Int,columns:Int):Void;
	@:overload(function (sheet:google.apps.script.spreadsheet.Sheet,restoreSelection:Bool):google.apps.script.spreadsheet.Sheet {})
	static function setActiveSheet(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.Sheet;
	static function newRichTextValue(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.RichTextValueBuilder;
	static function openByUrl(url:String):google.apps.script.spreadsheet.Spreadsheet;
	static function newDataSourceSpec(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.DataSourceSpecBuilder;
	static function newTextStyle(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.TextStyleBuilder;
	static function getActiveSheet(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.Sheet;
	static function setCurrentCell(cell:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	static function openById(id:String):google.apps.script.spreadsheet.Spreadsheet;
	static function setActiveSpreadsheet(newActiveSpreadsheet:google.apps.script.spreadsheet.Spreadsheet):Void;
	static function getActiveRangeList(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.RangeList;
	static function getActiveRange(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.Range;
	static function open(file:google.apps.script.drive.File):google.apps.script.spreadsheet.Spreadsheet;
	static function getActiveSpreadsheet(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.Spreadsheet;
	static function getSelection(name:String,rows:Int,columns:Int):google.apps.script.spreadsheet.Selection;
}
