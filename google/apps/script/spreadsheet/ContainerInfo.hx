package google.apps.script.spreadsheet;

@:native("ContainerInfo")
extern class ContainerInfo
{
	function getAnchorColumn(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Int;
	function getOffsetY(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Int;
	function getOffsetX(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Int;
	function getAnchorRow(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Int;
}
