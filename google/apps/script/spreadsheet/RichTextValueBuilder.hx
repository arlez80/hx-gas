package google.apps.script.spreadsheet;

@:native("RichTextValueBuilder")
extern class RichTextValueBuilder
{
	function setText(text:String):google.apps.script.spreadsheet.RichTextValueBuilder;
	function build(startOffset:Int,endOffset:Int):google.apps.script.spreadsheet.RichTextValue;
	@:overload(function (textStyle:google.apps.script.spreadsheet.TextStyle):google.apps.script.spreadsheet.RichTextValueBuilder {})
	function setTextStyle(startOffset:Int,endOffset:Int,textStyle:google.apps.script.spreadsheet.TextStyle):google.apps.script.spreadsheet.RichTextValueBuilder;
}
