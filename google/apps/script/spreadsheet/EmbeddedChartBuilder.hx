package google.apps.script.spreadsheet;

@:native("EmbeddedChartBuilder")
extern class EmbeddedChartBuilder
{
	function asLineChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedLineChartBuilder;
	function setMergeStrategy(mergeStrategy:google.apps.script.charts.ChartMergeStrategy):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function getContainer(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.ContainerInfo;
	function asHistogramChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedHistogramChartBuilder;
	function asScatterChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedScatterChartBuilder;
	function setChartType(type:google.apps.script.charts.ChartType):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asAreaChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedAreaChartBuilder;
	function setPosition(anchorRowPos:Int,anchorColPos:Int,offsetX:Int,offsetY:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function removeRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asComboChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedComboChartBuilder;
	function getRanges(range:google.apps.script.spreadsheet.Range):Array<google.apps.script.spreadsheet.Range>;
	function asColumnChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedColumnChartBuilder;
	function asBarChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedBarChartBuilder;
	function setHiddenDimensionStrategy(strategy:google.apps.script.charts.ChartHiddenDimensionStrategy):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function addRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function setTransposeRowsAndColumns(transpose:Bool):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function getChartType(range:google.apps.script.spreadsheet.Range):google.apps.script.charts.ChartType;
	function setOption(option:String,value:Dynamic):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function asTableChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedTableChartBuilder;
	function setNumHeaders(headers:Int):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function build(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedChart;
	function asPieChart(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.EmbeddedPieChartBuilder;
}
