package google.apps.script.spreadsheet;

@:native("DataSourceTable")
extern class DataSourceTable
{
	function refreshData(parameterName:String,sourceCell:String):google.apps.script.spreadsheet.DataSourceTable;
	function forceRefreshData(parameterName:String,sourceCell:String):google.apps.script.spreadsheet.DataSourceTable;
	function getRange(parameterName:String,sourceCell:String):google.apps.script.spreadsheet.Range;
	function getDataSource(parameterName:String,sourceCell:String):google.apps.script.spreadsheet.DataSource;
	function waitForCompletion(timeoutInSeconds:Int):google.apps.script.spreadsheet.DataExecutionStatus;
	function getStatus(parameterName:String,sourceCell:String):google.apps.script.spreadsheet.DataExecutionStatus;
}
