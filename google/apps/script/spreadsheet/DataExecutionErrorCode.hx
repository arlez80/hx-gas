package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("DataExecutionErrorCode")
@:enum abstract DataExecutionErrorCode(String) from String to String
{
	var DATA_EXECUTION_ERROR_CODE_UNSUPPORTED;
	var NONE;
	var TIME_OUT;
	var TOO_MANY_ROWS;
	var TOO_MANY_CELLS;
	var ENGINE;
	var PARAMETER_INVALID;
	var UNSUPPORTED_DATA_TYPE;
	var DUPLICATE_COLUMN_NAMES;
	var INTERRUPTED;
	var OTHER;
	var TOO_MANY_CHARS_PER_CELL;
}

