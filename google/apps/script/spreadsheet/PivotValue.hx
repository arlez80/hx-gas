package google.apps.script.spreadsheet;

@:native("PivotValue")
extern class PivotValue
{
	function getDisplayType(dimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.PivotValueDisplayType;
	function setFormula(formula:String):google.apps.script.spreadsheet.PivotValue;
	function getSummarizedBy(dimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.PivotTableSummarizeFunction;
	function summarizeBy(summarizeFunction:google.apps.script.spreadsheet.PivotTableSummarizeFunction):google.apps.script.spreadsheet.PivotValue;
	function getPivotTable(dimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.PivotTable;
	function setDisplayName(name:String):google.apps.script.spreadsheet.PivotValue;
	function getFormula(dimension:google.apps.script.spreadsheet.Dimension):String;
	function showAs(displayType:google.apps.script.spreadsheet.PivotValueDisplayType):google.apps.script.spreadsheet.PivotValue;
}
