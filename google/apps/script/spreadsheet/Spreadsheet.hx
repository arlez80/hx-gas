package google.apps.script.spreadsheet;

@:native("Spreadsheet")
extern class Spreadsheet
{
	function getLastRow(columnPosition:Int):Int;
	function getUrl(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):String;
	function unhideColumn(column:google.apps.script.spreadsheet.Range):Void;
	function getSpreadsheetLocale(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):String;
	function getProtections(type:google.apps.script.spreadsheet.ProtectionType):Array<google.apps.script.spreadsheet.Protection>;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.spreadsheet.Spreadsheet {})
	function addViewer(emailAddress:String):google.apps.script.spreadsheet.Spreadsheet;
	function getFormUrl(columnPosition:Int):String;
	function createDeveloperMetadataFinder(name:String):google.apps.script.spreadsheet.DeveloperMetadataFinder;
	function getSheetValues(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):Array<Array<Dynamic>>;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function addViewers(emailAddresses:Array<String>):google.apps.script.spreadsheet.Spreadsheet;
	function isRowHiddenByUser(rowPosition:Int):Bool;
	function removeMenu(name:String):Void;
	@:overload(function (key:String,visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Spreadsheet {})
	@:overload(function (key:String,value:String):google.apps.script.spreadsheet.Spreadsheet {})
	@:overload(function (key:String,value:String,visibility:google.apps.script.spreadsheet.DeveloperMetadataVisibility):google.apps.script.spreadsheet.Spreadsheet {})
	function addDeveloperMetadata(key:String):google.apps.script.spreadsheet.Spreadsheet;
	function getSheetByName(name:String):google.apps.script.spreadsheet.Sheet;
	function setNamedRange(name:String,range:google.apps.script.spreadsheet.Range):Void;
	function setRowHeight(rowPosition:Int,height:Int):google.apps.script.spreadsheet.Sheet;
	function deleteColumn(columnPosition:Int):google.apps.script.spreadsheet.Sheet;
	function setFrozenRows(rows:Int):Void;
	function updateMenu(name:String,subMenus:Array<Dynamic>):Void;
	function insertRowBefore(beforePosition:Int):google.apps.script.spreadsheet.Sheet;
	function getOwner(columnPosition:Int):google.apps.script.base.User;
	function deleteRow(rowPosition:Int):google.apps.script.spreadsheet.Sheet;
	function getDataSourceTables(columnPosition:Int):Array<google.apps.script.spreadsheet.DataSourceTable>;
	function setActiveRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	function getCurrentCell(columnPosition:Int):google.apps.script.spreadsheet.Range;
	function addEditors(emailAddresses:Array<String>):google.apps.script.spreadsheet.Spreadsheet;
	function setActiveRangeList(rangeList:google.apps.script.spreadsheet.RangeList):google.apps.script.spreadsheet.RangeList;
	function getSheetId(name:String):Int;
	function getRange(a1Notation:String):google.apps.script.spreadsheet.Range;
	function insertColumnBefore(beforePosition:Int):google.apps.script.spreadsheet.Sheet;
	function setColumnWidth(columnPosition:Int,width:Int):google.apps.script.spreadsheet.Sheet;
	function getId(columnPosition:Int):String;
	function insertSheetWithDataSourceTable(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.Sheet;
	function isColumnHiddenByUser(columnPosition:Int):Bool;
	function isRowHiddenByFilter(rowPosition:Int):Bool;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.spreadsheet.Spreadsheet {})
	function removeEditor(emailAddress:String):google.apps.script.spreadsheet.Spreadsheet;
	function show(userInterface:Dynamic):Void;
	function hideColumn(column:google.apps.script.spreadsheet.Range):Void;
	function getActiveRangeList(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.RangeList;
	function moveActiveSheet(pos:Int):Void;
	@:deprecated
	function getSheetProtection(name:String,subMenus:Array<Dynamic>):google.apps.script.spreadsheet.PageProtection;
	function deleteColumns(columnPosition:Int,howMany:Int):Void;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.spreadsheet.Spreadsheet {})
	function removeViewer(emailAddress:String):google.apps.script.spreadsheet.Spreadsheet;
	function insertColumnsBefore(beforePosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function getColumnWidth(columnPosition:Int):Int;
	function getFrozenRows(columnPosition:Int):Int;
	@:overload(function (sheet:google.apps.script.spreadsheet.Sheet,restoreSelection:Bool):google.apps.script.spreadsheet.Sheet {})
	function setActiveSheet(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.Sheet;
	function getLastColumn(columnPosition:Int):Int;
	@:deprecated
	function setSheetProtection(permissions:google.apps.script.spreadsheet.PageProtection):Void;
	function getEditors(columnPosition:Int):Array<google.apps.script.base.User>;
	function getViewers(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):Array<google.apps.script.base.User>;
	function deleteSheet(sheet:google.apps.script.spreadsheet.Sheet):Void;
	function deleteRows(rowPosition:Int,howMany:Int):Void;
	function hideRow(row:google.apps.script.spreadsheet.Range):Void;
	@:overload(function (blobSource:google.apps.script.base.BlobSource,column:Int,row:Int,offsetX:Int,offsetY:Int):google.apps.script.spreadsheet.OverGridImage {})
	@:overload(function (url:String,column:Int,row:Int):google.apps.script.spreadsheet.OverGridImage {})
	@:overload(function (url:String,column:Int,row:Int,offsetX:Int,offsetY:Int):google.apps.script.spreadsheet.OverGridImage {})
	function insertImage(blobSource:google.apps.script.base.BlobSource,column:Int,row:Int):google.apps.script.spreadsheet.OverGridImage;
	function getBandings(contentType:String):Array<google.apps.script.spreadsheet.Banding>;
	@:deprecated
	function isAnonymousWrite(name:String,subMenus:Array<Dynamic>):Bool;
	function getActiveRange(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.Range;
	function getActiveSheet(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.Sheet;
	@:overload(function (sheetIndex:Int):google.apps.script.spreadsheet.Sheet {})
	@:overload(function (sheetIndex:Int,options:{?template:google.apps.script.spreadsheet.Sheet}):google.apps.script.spreadsheet.Sheet {})
	@:overload(function (options:{?template:google.apps.script.spreadsheet.Sheet}):google.apps.script.spreadsheet.Sheet {})
	@:overload(function (sheetName:String):google.apps.script.spreadsheet.Sheet {})
	@:overload(function (sheetName:String,sheetIndex:Int):google.apps.script.spreadsheet.Sheet {})
	@:overload(function (sheetName:String,sheetIndex:Int,options:{?template:google.apps.script.spreadsheet.Sheet}):google.apps.script.spreadsheet.Sheet {})
	@:overload(function (sheetName:String,options:{?template:google.apps.script.spreadsheet.Sheet}):google.apps.script.spreadsheet.Sheet {})
	function insertSheet(beforePosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function getNumSheets(columnPosition:Int):Int;
	function removeNamedRange(name:String):Void;
	function insertColumnAfter(afterPosition:Int):google.apps.script.spreadsheet.Sheet;
	@:deprecated
	function isAnonymousView(name:String,subMenus:Array<Dynamic>):Bool;
	function insertRowAfter(afterPosition:Int):google.apps.script.spreadsheet.Sheet;
	function getNamedRanges(columnPosition:Int):Array<google.apps.script.spreadsheet.NamedRange>;
	@:deprecated
	function setAnonymousAccess(anonymousReadAllowed:Bool,anonymousWriteAllowed:Bool):Void;
	function getDataRange(columnPosition:Int):google.apps.script.spreadsheet.Range;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.spreadsheet.Spreadsheet {})
	function addEditor(emailAddress:String):google.apps.script.spreadsheet.Spreadsheet;
	@:overload(function (columnPosition:Int,ascending:Bool):google.apps.script.spreadsheet.Sheet {})
	function sort(columnPosition:Int):google.apps.script.spreadsheet.Sheet;
	function duplicateActiveSheet(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.Sheet;
	@:overload(function (msg:String,title:String):Void {})
	@:overload(function (msg:String,title:String,timeoutSeconds:Float):Void {})
	function toast(msg:String):Void;
	function getSheets(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):Array<google.apps.script.spreadsheet.Sheet>;
	function getRowHeight(rowPosition:Int):Int;
	function getName(columnPosition:Int):String;
	function renameActiveSheet(newName:String):Void;
	function getActiveCell(sheet:google.apps.script.spreadsheet.Sheet):google.apps.script.spreadsheet.Range;
	function getImages(columnPosition:Int):Array<google.apps.script.spreadsheet.OverGridImage>;
	function getRangeList(a1Notations:Array<String>):google.apps.script.spreadsheet.RangeList;
	function setSpreadsheetTimeZone(timezone:String):Void;
	function getSelection(rowPosition:Int):google.apps.script.spreadsheet.Selection;
	function addMenu(name:String,subMenus:Array<Dynamic>):Void;
	function appendRow(rowContents:Array<Dynamic>):google.apps.script.spreadsheet.Sheet;
	function getSheetName(name:String):String;
	function getSpreadsheetTimeZone(startRow:Int,startColumn:Int,numRows:Int,numColumns:Int):String;
	function getRangeByName(name:String):google.apps.script.spreadsheet.Range;
	function deleteActiveSheet(name:String):google.apps.script.spreadsheet.Sheet;
	function copy(name:String):google.apps.script.spreadsheet.Spreadsheet;
	function getFrozenColumns(columnPosition:Int):Int;
	function insertColumnsAfter(afterPosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function autoResizeColumn(columnPosition:Int):google.apps.script.spreadsheet.Sheet;
	function rename(newName:String):Void;
	function unhideRow(row:google.apps.script.spreadsheet.Range):Void;
	function insertRowsBefore(beforePosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function insertRowsAfter(afterPosition:Int,howMany:Int):google.apps.script.spreadsheet.Sheet;
	function getDeveloperMetadata(columnPosition:Int):Array<google.apps.script.spreadsheet.DeveloperMetadata>;
	@:overload(function (a1Notation:String):google.apps.script.spreadsheet.Range {})
	function setActiveSelection(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	function setSpreadsheetLocale(locale:String):Void;
	function setCurrentCell(cell:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.Range;
	function setFrozenColumns(columns:Int):Void;
}
