package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("TextToColumnsDelimiter")
@:enum abstract TextToColumnsDelimiter(String) from String to String
{
	var COMMA;
	var SEMICOLON;
	var PERIOD;
	var SPACE;
}

