package google.apps.script.spreadsheet;

@:native("DataSource")
extern class DataSource
{
	function getSpec(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.DataSourceSpec;
	function updateSpec(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.DataSource;
}
