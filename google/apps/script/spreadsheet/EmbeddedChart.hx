package google.apps.script.spreadsheet;

@:native("EmbeddedChart")
extern class EmbeddedChart
{
	function getNumHeaders(contentType:String):Int;
	function getOptions(contentType:String):google.apps.script.charts.ChartOptions;
	function getTransposeRowsAndColumns(contentType:String):Bool;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function setId(id:String):google.apps.script.charts.Chart;
	function getType(contentType:String):String;
	function getContainerInfo(contentType:String):google.apps.script.spreadsheet.ContainerInfo;
	function modify(contentType:String):google.apps.script.spreadsheet.EmbeddedChartBuilder;
	function getMergeStrategy(contentType:String):google.apps.script.charts.ChartMergeStrategy;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function getHiddenDimensionStrategy(contentType:String):google.apps.script.charts.ChartHiddenDimensionStrategy;
	function getRanges(contentType:String):Array<google.apps.script.spreadsheet.Range>;
	function getId(contentType:String):String;
}
