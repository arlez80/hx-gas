package google.apps.script.spreadsheet;

@:native("PivotTable")
extern class PivotTable
{
	function getRowGroups(sourceDataColumn:Int):Array<google.apps.script.spreadsheet.PivotGroup>;
	function addRowGroup(sourceDataColumn:Int):google.apps.script.spreadsheet.PivotGroup;
	function setValuesDisplayOrientation(dimension:google.apps.script.spreadsheet.Dimension):google.apps.script.spreadsheet.PivotTable;
	function getColumnGroups(sourceDataColumn:Int):Array<google.apps.script.spreadsheet.PivotGroup>;
	function getFilters(sourceDataColumn:Int):Array<google.apps.script.spreadsheet.PivotFilter>;
	function addPivotValue(sourceDataColumn:Int,summarizeFunction:google.apps.script.spreadsheet.PivotTableSummarizeFunction):google.apps.script.spreadsheet.PivotValue;
	function getValuesDisplayOrientation(sourceDataColumn:Int):google.apps.script.spreadsheet.Dimension;
	function addFilter(sourceDataColumn:Int,filterCriteria:google.apps.script.spreadsheet.FilterCriteria):google.apps.script.spreadsheet.PivotFilter;
	function addColumnGroup(sourceDataColumn:Int):google.apps.script.spreadsheet.PivotGroup;
	function getAnchorCell(sourceDataColumn:Int):google.apps.script.spreadsheet.Range;
	function getPivotValues(sourceDataColumn:Int):Array<google.apps.script.spreadsheet.PivotValue>;
	function remove(sourceDataColumn:Int):Void;
	function addCalculatedPivotValue(name:String,formula:String):google.apps.script.spreadsheet.PivotValue;
}
