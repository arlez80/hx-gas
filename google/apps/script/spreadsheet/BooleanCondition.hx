package google.apps.script.spreadsheet;

@:native("BooleanCondition")
extern class BooleanCondition
{
	function getStrikethrough(rawQuery:String):Bool;
	function getCriteriaValues(rawQuery:String):Array<Dynamic>;
	function getItalic(rawQuery:String):Bool;
	function getCriteriaType(rawQuery:String):google.apps.script.spreadsheet.BooleanCriteria;
	function getBold(rawQuery:String):Bool;
	function getFontColor(rawQuery:String):String;
	function getBackground(rawQuery:String):String;
	function getUnderline(rawQuery:String):Bool;
}
