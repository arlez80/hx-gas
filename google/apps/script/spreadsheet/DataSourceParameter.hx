package google.apps.script.spreadsheet;

@:native("DataSourceParameter")
extern class DataSourceParameter
{
	function getName(spec:google.apps.script.spreadsheet.DataSourceSpec):String;
	function getSourceCell(spec:google.apps.script.spreadsheet.DataSourceSpec):String;
	function getType(spec:google.apps.script.spreadsheet.DataSourceSpec):google.apps.script.spreadsheet.DataSourceParameterType;
}
