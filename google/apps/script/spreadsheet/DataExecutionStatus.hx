package google.apps.script.spreadsheet;

@:native("DataExecutionStatus")
extern class DataExecutionStatus
{
	function isTruncated(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Bool;
	function getErrorCode(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.DataExecutionErrorCode;
	function getLastRefreshedTime(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):Date;
	function getExecutionState(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.DataExecutionState;
	function getErrorMessage(criteria:google.apps.script.spreadsheet.BooleanCriteria,args:Array<Dynamic>):String;
}
