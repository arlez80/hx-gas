package google.apps.script.spreadsheet;

@:native("DataValidationBuilder")
extern class DataValidationBuilder
{
	function requireTextDoesNotContain(text:String):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireNumberGreaterThan(number:Float):google.apps.script.spreadsheet.DataValidationBuilder;
	function withCriteria(criteria:google.apps.script.spreadsheet.DataValidationCriteria,args:Array<Dynamic>):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireNumberLessThanOrEqualTo(number:Float):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireDateBetween(start:Date,end:Date):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireDateNotBetween(start:Date,end:Date):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireTextEqualTo(text:String):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireNumberNotEqualTo(number:Float):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireNumberLessThan(number:Float):google.apps.script.spreadsheet.DataValidationBuilder;
	function getCriteriaValues(timeoutInSeconds:Int):Array<Dynamic>;
	function requireDateOnOrBefore(date:Date):google.apps.script.spreadsheet.DataValidationBuilder;
	function getCriteriaType(timeoutInSeconds:Int):google.apps.script.spreadsheet.DataValidationCriteria;
	function requireTextIsEmail(text:String):google.apps.script.spreadsheet.DataValidationBuilder;
	function setHelpText(helpText:String):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireDateOnOrAfter(date:Date):google.apps.script.spreadsheet.DataValidationBuilder;
	function getHelpText(timeoutInSeconds:Int):String;
	function requireDate(timeoutInSeconds:Int):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireTextIsUrl(text:String):google.apps.script.spreadsheet.DataValidationBuilder;
	function setAllowInvalid(allowInvalidData:Bool):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireFormulaSatisfied(formula:String):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireDateAfter(date:Date):google.apps.script.spreadsheet.DataValidationBuilder;
	@:overload(function (range:google.apps.script.spreadsheet.Range,showDropdown:Bool):google.apps.script.spreadsheet.DataValidationBuilder {})
	function requireValueInRange(range:google.apps.script.spreadsheet.Range):google.apps.script.spreadsheet.DataValidationBuilder;
	function getAllowInvalid(timeoutInSeconds:Int):Bool;
	function requireDateBefore(date:Date):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireTextContains(text:String):google.apps.script.spreadsheet.DataValidationBuilder;
	@:overload(function (values:Array<String>,showDropdown:Bool):google.apps.script.spreadsheet.DataValidationBuilder {})
	function requireValueInList(values:Array<String>):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireDateEqualTo(date:Date):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireNumberBetween(start:Float,end:Float):google.apps.script.spreadsheet.DataValidationBuilder;
	function build(timeoutInSeconds:Int):google.apps.script.spreadsheet.DataValidation;
	function copy(timeoutInSeconds:Int):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireNumberGreaterThanOrEqualTo(number:Float):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireNumberNotBetween(start:Float,end:Float):google.apps.script.spreadsheet.DataValidationBuilder;
	function requireNumberEqualTo(number:Float):google.apps.script.spreadsheet.DataValidationBuilder;
}
