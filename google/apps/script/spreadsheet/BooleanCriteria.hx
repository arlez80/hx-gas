package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("BooleanCriteria")
@:enum abstract BooleanCriteria(String) from String to String
{
	var CELL_EMPTY;
	var CELL_NOT_EMPTY;
	var DATE_AFTER;
	var DATE_BEFORE;
	var DATE_EQUAL_TO;
	var DATE_AFTER_RELATIVE;
	var DATE_BEFORE_RELATIVE;
	var DATE_EQUAL_TO_RELATIVE;
	var NUMBER_BETWEEN;
	var NUMBER_EQUAL_TO;
	var NUMBER_GREATER_THAN;
	var NUMBER_GREATER_THAN_OR_EQUAL_TO;
	var NUMBER_LESS_THAN;
	var NUMBER_LESS_THAN_OR_EQUAL_TO;
	var NUMBER_NOT_BETWEEN;
	var NUMBER_NOT_EQUAL_TO;
	var TEXT_CONTAINS;
	var TEXT_DOES_NOT_CONTAIN;
	var TEXT_EQUAL_TO;
	var TEXT_STARTS_WITH;
	var TEXT_ENDS_WITH;
	var CUSTOM_FORMULA;
}

