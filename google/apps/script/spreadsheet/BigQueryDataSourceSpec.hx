package google.apps.script.spreadsheet;

@:native("BigQueryDataSourceSpec")
extern class BigQueryDataSourceSpec
{
	function getRawQuery(color:String):String;
	function getParameters(color:String):Array<google.apps.script.spreadsheet.DataSourceParameter>;
	function copy(color:String):google.apps.script.spreadsheet.DataSourceSpecBuilder;
	function getType(color:String):google.apps.script.spreadsheet.DataSourceType;
	function getProjectId(color:String):String;
}
