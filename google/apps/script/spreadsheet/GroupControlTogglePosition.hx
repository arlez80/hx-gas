package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("GroupControlTogglePosition")
@:enum abstract GroupControlTogglePosition(String) from String to String
{
	var BEFORE;
	var AFTER;
}

