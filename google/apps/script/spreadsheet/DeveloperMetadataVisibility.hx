package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("DeveloperMetadataVisibility")
@:enum abstract DeveloperMetadataVisibility(String) from String to String
{
	var DOCUMENT;
	var PROJECT;
}

