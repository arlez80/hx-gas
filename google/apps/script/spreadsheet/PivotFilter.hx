package google.apps.script.spreadsheet;

@:native("PivotFilter")
extern class PivotFilter
{
	function remove(protection:Bool):Void;
	function getFilterCriteria(protection:Bool):google.apps.script.spreadsheet.FilterCriteria;
	function setFilterCriteria(filterCriteria:google.apps.script.spreadsheet.FilterCriteria):google.apps.script.spreadsheet.PivotFilter;
	function getSourceDataColumn(protection:Bool):Int;
	function getPivotTable(protection:Bool):google.apps.script.spreadsheet.PivotTable;
}
