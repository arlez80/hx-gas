package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("ProtectionType")
@:enum abstract ProtectionType(String) from String to String
{
	var RANGE;
	var SHEET;
}

