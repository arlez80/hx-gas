package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("BorderStyle")
@:enum abstract BorderStyle(String) from String to String
{
	var DOTTED;
	var DASHED;
	var SOLID;
	var SOLID_MEDIUM;
	var SOLID_THICK;
	var DOUBLE;
}

