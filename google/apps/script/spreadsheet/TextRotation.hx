package google.apps.script.spreadsheet;

@:native("TextRotation")
extern class TextRotation
{
	function isVertical(permissions:google.apps.script.spreadsheet.PageProtection):Bool;
	function getDegrees(permissions:google.apps.script.spreadsheet.PageProtection):Int;
}
