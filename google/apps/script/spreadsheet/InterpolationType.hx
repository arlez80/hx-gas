package google.apps.script.spreadsheet;

@:require(hx_gas)
@:native("InterpolationType")
@:enum abstract InterpolationType(String) from String to String
{
	var NUMBER;
	var PERCENT;
	var PERCENTILE;
	var MIN;
	var MAX;
}

