package google.apps.script.card_service;

@:native("ActionResponse")
extern class ActionResponse
{
	function printJson(functionName:String):String;
}
