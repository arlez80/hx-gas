package google.apps.script.card_service;

@:require(hx_gas)
@:native("ContentType")
@:enum abstract ContentType(String) from String to String
{
	var TEXT;
	var MUTABLE_HTML;
	var IMMUTABLE_HTML;
}

