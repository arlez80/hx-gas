package google.apps.script.card_service;

@:native("ComposeActionResponseBuilder")
extern class ComposeActionResponseBuilder
{
	function build(numUncollapsibleWidgets:Int):google.apps.script.card_service.ComposeActionResponse;
	function setGmailDraft(draft:google.apps.script.gmail.GmailDraft):google.apps.script.card_service.ComposeActionResponseBuilder;
}
