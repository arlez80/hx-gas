package google.apps.script.card_service;

@:require(hx_gas)
@:native("Icon")
@:enum abstract Icon(String) from String to String
{
	var NONE;
	var AIRPLANE;
	var BOOKMARK;
	var BUS;
	var CAR;
	var CLOCK;
	var CONFIRMATION_NUMBER_ICON;
	var DOLLAR;
	var DESCRIPTION;
	var EMAIL;
	var EVENT_PERFORMER;
	var EVENT_SEAT;
	var FLIGHT_ARRIVAL;
	var FLIGHT_DEPARTURE;
	var HOTEL;
	var HOTEL_ROOM_TYPE;
	var INVITE;
	var MAP_PIN;
	var MEMBERSHIP;
	var MULTIPLE_PEOPLE;
	var OFFER;
	var PERSON;
	var PHONE;
	var RESTAURANT_ICON;
	var SHOPPING_CART;
	var STAR;
	var STORE;
	var TICKET;
	var TRAIN;
	var VIDEO_CAMERA;
	var VIDEO_PLAY;
}

