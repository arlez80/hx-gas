package google.apps.script.card_service;

@:native("UniversalActionResponse")
extern class UniversalActionResponse
{
	function printJson(text:String):String;
}
