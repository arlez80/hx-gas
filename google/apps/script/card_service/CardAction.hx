package google.apps.script.card_service;

@:native("CardAction")
extern class CardAction
{
	function setText(text:String):google.apps.script.card_service.CardAction;
	function setComposeAction(action:google.apps.script.card_service.Action,composedEmailType:google.apps.script.card_service.ComposedEmailType):google.apps.script.card_service.CardAction;
	function setOnClickOpenLinkAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.CardAction;
	function setOnClickAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.CardAction;
	function setOpenLink(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.CardAction;
	function setAuthorizationAction(action:google.apps.script.card_service.AuthorizationAction):google.apps.script.card_service.CardAction;
}
