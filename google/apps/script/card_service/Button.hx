package google.apps.script.card_service;

@:native("Button")
extern class Button
{
	function setComposeAction(action:google.apps.script.card_service.Action,composedEmailType:google.apps.script.card_service.ComposedEmailType):google.apps.script.card_service.Button;
	function setOnClickOpenLinkAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.Button;
	function setOnClickAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.Button;
	function setOpenLink(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.Button;
	function setAuthorizationAction(action:google.apps.script.card_service.AuthorizationAction):google.apps.script.card_service.Button;
}
