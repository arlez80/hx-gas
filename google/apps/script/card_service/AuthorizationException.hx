package google.apps.script.card_service;

@:native("AuthorizationException")
extern class AuthorizationException
{
	function setAuthorizationUrl(authUrl:String):google.apps.script.card_service.AuthorizationException;
	function setCustomUiCallback(callback:String):google.apps.script.card_service.AuthorizationException;
	function throwException(name:String):Void;
	function setResourceDisplayName(name:String):google.apps.script.card_service.AuthorizationException;
	function printJson(authorizationUrl:String):String;
}
