package google.apps.script.card_service;

@:native("UpdateDraftBodyAction")
extern class UpdateDraftBodyAction
{
	function addUpdateContent(content:String,contentType:google.apps.script.card_service.ContentType):google.apps.script.card_service.UpdateDraftBodyAction;
	function setUpdateType(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.card_service.UpdateDraftBodyAction;
}
