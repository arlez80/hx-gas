package google.apps.script.card_service;

@:require(hx_gas)
@:native("TextButtonStyle")
@:enum abstract TextButtonStyle(String) from String to String
{
	var TEXT;
	var FILLED;
}

