package google.apps.script.card_service;

@:native("AuthorizationAction")
extern class AuthorizationAction
{
	function setAuthorizationUrl(authorizationUrl:String):google.apps.script.card_service.AuthorizationAction;
}
