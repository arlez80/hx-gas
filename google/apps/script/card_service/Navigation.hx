package google.apps.script.card_service;

@:native("Navigation")
extern class Navigation
{
	function popCard(text:String):google.apps.script.card_service.Navigation;
	function pushCard(card:google.apps.script.card_service.Card):google.apps.script.card_service.Navigation;
	function popToRoot(cardName:String):google.apps.script.card_service.Navigation;
	function updateCard(card:google.apps.script.card_service.Card):google.apps.script.card_service.Navigation;
	function popToNamedCard(cardName:String):google.apps.script.card_service.Navigation;
	function printJson(cardName:String):String;
}
