package google.apps.script.card_service;

@:native("Notification")
extern class Notification
{
	function setText(text:String):google.apps.script.card_service.Notification;
	function setType(type:google.apps.script.card_service.NotificationType):google.apps.script.card_service.Notification;
}
