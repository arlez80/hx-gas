package google.apps.script.card_service;

@:require(hx_gas)
@:native("ComposedEmailType")
@:enum abstract ComposedEmailType(String) from String to String
{
	var REPLY_AS_DRAFT;
	var STANDALONE_DRAFT;
}

