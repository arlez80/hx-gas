package google.apps.script.card_service;

@:native("Switch")
extern class Switch
{
	function setOnChangeAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.Switch;
	function setValue(value:String):google.apps.script.card_service.Switch;
	function setSelected(selected:Bool):google.apps.script.card_service.Switch;
	function setFieldName(fieldName:String):google.apps.script.card_service.Switch;
}
