package google.apps.script.card_service;

@:native("SuggestionsResponseBuilder")
extern class SuggestionsResponseBuilder
{
	function setSuggestions(suggestions:google.apps.script.card_service.Suggestions):google.apps.script.card_service.SuggestionsResponseBuilder;
	function build(suggestions:Array<Dynamic>):google.apps.script.card_service.SuggestionsResponse;
}
