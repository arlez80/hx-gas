package google.apps.script.card_service;

@:require(hx_gas)
@:native("OnClose")
@:enum abstract OnClose(String) from String to String
{
	var NOTHING;
	var RELOAD_ADD_ON;
}

