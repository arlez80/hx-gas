package google.apps.script.card_service;

@:native("TextInput")
extern class TextInput
{
	function setSuggestionsAction(suggestionsAction:google.apps.script.card_service.Action):google.apps.script.card_service.TextInput;
	function setValue(value:String):google.apps.script.card_service.TextInput;
	function setTitle(title:String):google.apps.script.card_service.TextInput;
	function setHint(hint:String):google.apps.script.card_service.TextInput;
	function setSuggestions(suggestions:google.apps.script.card_service.Suggestions):google.apps.script.card_service.TextInput;
	function setMultiline(multiline:Bool):google.apps.script.card_service.TextInput;
	function setFieldName(fieldName:String):google.apps.script.card_service.TextInput;
	function setOnChangeAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.TextInput;
}
