package google.apps.script.card_service;

@:require(hx_gas)
@:native("CardService.ComposedEmailType")
@:enum abstract ComposedEmailType(String) from String to String
{
	var REPLY_AS_DRAFT;
	var STANDALONE_DRAFT;
}

@:require(hx_gas)
@:native("CardService.ContentType")
@:enum abstract ContentType(String) from String to String
{
	var TEXT;
	var MUTABLE_HTML;
	var IMMUTABLE_HTML;
}

@:require(hx_gas)
@:native("CardService.Icon")
@:enum abstract Icon(String) from String to String
{
	var NONE;
	var AIRPLANE;
	var BOOKMARK;
	var BUS;
	var CAR;
	var CLOCK;
	var CONFIRMATION_NUMBER_ICON;
	var DOLLAR;
	var DESCRIPTION;
	var EMAIL;
	var EVENT_PERFORMER;
	var EVENT_SEAT;
	var FLIGHT_ARRIVAL;
	var FLIGHT_DEPARTURE;
	var HOTEL;
	var HOTEL_ROOM_TYPE;
	var INVITE;
	var MAP_PIN;
	var MEMBERSHIP;
	var MULTIPLE_PEOPLE;
	var OFFER;
	var PERSON;
	var PHONE;
	var RESTAURANT_ICON;
	var SHOPPING_CART;
	var STAR;
	var STORE;
	var TICKET;
	var TRAIN;
	var VIDEO_CAMERA;
	var VIDEO_PLAY;
}

@:require(hx_gas)
@:native("CardService.ImageStyle")
@:enum abstract ImageStyle(String) from String to String
{
	var SQUARE;
	var CIRCLE;
}

@:require(hx_gas)
@:native("CardService.LoadIndicator")
@:enum abstract LoadIndicator(String) from String to String
{
	var SPINNER;
	var NONE;
}

@:require(hx_gas)
@:native("CardService.NotificationType")
@:enum abstract NotificationType(String) from String to String
{
	var INFO;
	var ERROR;
	var WARNING;
}

@:require(hx_gas)
@:native("CardService.OnClose")
@:enum abstract OnClose(String) from String to String
{
	var NOTHING;
	var RELOAD_ADD_ON;
}

@:require(hx_gas)
@:native("CardService.OpenAs")
@:enum abstract OpenAs(String) from String to String
{
	var FULL_SIZE;
	var OVERLAY;
}

@:require(hx_gas)
@:native("CardService.SelectionInputType")
@:enum abstract SelectionInputType(String) from String to String
{
	var CHECK_BOX;
	var RADIO_BUTTON;
	var DROPDOWN;
}

@:require(hx_gas)
@:native("CardService.TextButtonStyle")
@:enum abstract TextButtonStyle(String) from String to String
{
	var TEXT;
	var FILLED;
}

@:require(hx_gas)
@:native("CardService.UpdateDraftBodyType")
@:enum abstract UpdateDraftBodyType(String) from String to String
{
	var IN_PLACE_INSERT;
}

@:native("CardService")
extern class CardService
{
	static function newSelectionInput(keys:Array<String>):google.apps.script.card_service.SelectionInput;
	static function newAuthorizationAction(keys:Array<String>):google.apps.script.card_service.AuthorizationAction;
	static function newCardBuilder(keys:Array<String>):google.apps.script.card_service.CardBuilder;
	static function newTextParagraph(keys:Array<String>):google.apps.script.card_service.TextParagraph;
	static function newUpdateDraftActionResponseBuilder(keys:Array<String>):google.apps.script.card_service.UpdateDraftActionResponseBuilder;
	static function newSuggestions(keys:Array<String>):google.apps.script.card_service.Suggestions;
	static function newAuthorizationException(keys:Array<String>):google.apps.script.card_service.AuthorizationException;
	static function newButtonSet(keys:Array<String>):google.apps.script.card_service.ButtonSet;
	static function newUniversalActionResponseBuilder(keys:Array<String>):google.apps.script.card_service.UniversalActionResponseBuilder;
	static function newAction(keys:Array<String>):google.apps.script.card_service.Action;
	static function newSwitch(keys:Array<String>):google.apps.script.card_service.Switch;
	static function newCardSection(keys:Array<String>):google.apps.script.card_service.CardSection;
	static function newKeyValue(keys:Array<String>):google.apps.script.card_service.KeyValue;
	static function newActionResponseBuilder(keys:Array<String>):google.apps.script.card_service.ActionResponseBuilder;
	static function newComposeActionResponseBuilder(keys:Array<String>):google.apps.script.card_service.ComposeActionResponseBuilder;
	static function newTextInput(keys:Array<String>):google.apps.script.card_service.TextInput;
	static function newImageButton(keys:Array<String>):google.apps.script.card_service.ImageButton;
	static function newImage(keys:Array<String>):google.apps.script.card_service.Image;
	static function newOpenLink(keys:Array<String>):google.apps.script.card_service.OpenLink;
	static function newCardAction(keys:Array<String>):google.apps.script.card_service.CardAction;
	static function newNavigation(keys:Array<String>):google.apps.script.card_service.Navigation;
	static function newSuggestionsResponseBuilder(keys:Array<String>):google.apps.script.card_service.SuggestionsResponseBuilder;
	static function newCardHeader(keys:Array<String>):google.apps.script.card_service.CardHeader;
	static function newNotification(keys:Array<String>):google.apps.script.card_service.Notification;
	static function newTextButton(keys:Array<String>):google.apps.script.card_service.TextButton;
	static function newUpdateDraftBodyAction(keys:Array<String>):google.apps.script.card_service.UpdateDraftBodyAction;
}
