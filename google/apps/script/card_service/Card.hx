package google.apps.script.card_service;

@:native("Card")
extern class Card
{
	function printJson(button:google.apps.script.card_service.Button):String;
}
