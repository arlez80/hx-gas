package google.apps.script.card_service;

@:native("UniversalActionResponseBuilder")
extern class UniversalActionResponseBuilder
{
	function build(text:String):google.apps.script.card_service.UniversalActionResponse;
	function displayAddOnCards(cardObjects:Array<Dynamic>):google.apps.script.card_service.UniversalActionResponseBuilder;
	function setOpenLink(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.UniversalActionResponseBuilder;
}
