package google.apps.script.card_service;

@:native("KeyValue")
extern class KeyValue
{
	function setIconAltText(altText:String):google.apps.script.card_service.KeyValue;
	function setSwitch(switchToSet:google.apps.script.card_service.Switch):google.apps.script.card_service.KeyValue;
	function setBottomLabel(text:String):google.apps.script.card_service.KeyValue;
	function setButton(button:google.apps.script.card_service.Button):google.apps.script.card_service.KeyValue;
	function setTopLabel(text:String):google.apps.script.card_service.KeyValue;
	function setOnClickAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.KeyValue;
	function setMultiline(multiline:Bool):google.apps.script.card_service.KeyValue;
	function setIconUrl(url:String):google.apps.script.card_service.KeyValue;
	function setContent(text:String):google.apps.script.card_service.KeyValue;
	function setIcon(icon:google.apps.script.card_service.Icon):google.apps.script.card_service.KeyValue;
	function setOnClickOpenLinkAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.KeyValue;
	function setComposeAction(action:google.apps.script.card_service.Action,composedEmailType:google.apps.script.card_service.ComposedEmailType):google.apps.script.card_service.KeyValue;
	function setOpenLink(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.KeyValue;
	function setAuthorizationAction(action:google.apps.script.card_service.AuthorizationAction):google.apps.script.card_service.KeyValue;
}
