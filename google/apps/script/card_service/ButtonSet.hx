package google.apps.script.card_service;

@:native("ButtonSet")
extern class ButtonSet
{
	function addButton(button:google.apps.script.card_service.Button):google.apps.script.card_service.ButtonSet;
}
