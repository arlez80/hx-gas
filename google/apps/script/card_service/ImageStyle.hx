package google.apps.script.card_service;

@:require(hx_gas)
@:native("ImageStyle")
@:enum abstract ImageStyle(String) from String to String
{
	var SQUARE;
	var CIRCLE;
}

