package google.apps.script.card_service;

@:native("TextParagraph")
extern class TextParagraph
{
	function setText(text:String):google.apps.script.card_service.TextParagraph;
}
