package google.apps.script.card_service;

@:native("Suggestions")
extern class Suggestions
{
	function addSuggestion(suggestion:String):google.apps.script.card_service.Suggestions;
	function addSuggestions(suggestions:Array<Dynamic>):google.apps.script.card_service.Suggestions;
}
