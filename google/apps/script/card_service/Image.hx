package google.apps.script.card_service;

@:native("Image")
extern class Image
{
	function setImageUrl(url:String):google.apps.script.card_service.Image;
	function setAltText(altText:String):google.apps.script.card_service.Image;
	function setComposeAction(action:google.apps.script.card_service.Action,composedEmailType:google.apps.script.card_service.ComposedEmailType):google.apps.script.card_service.Image;
	function setOnClickOpenLinkAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.Image;
	function setOnClickAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.Image;
	function setOpenLink(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.Image;
	function setAuthorizationAction(action:google.apps.script.card_service.AuthorizationAction):google.apps.script.card_service.Image;
}
