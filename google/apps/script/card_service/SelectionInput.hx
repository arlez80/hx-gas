package google.apps.script.card_service;

@:native("SelectionInput")
extern class SelectionInput
{
	function setType(type:google.apps.script.card_service.SelectionInputType):google.apps.script.card_service.SelectionInput;
	function addItem(text:Dynamic,value:Dynamic,selected:Bool):google.apps.script.card_service.SelectionInput;
	function setOnChangeAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.SelectionInput;
	function setTitle(title:String):google.apps.script.card_service.SelectionInput;
	function setFieldName(fieldName:String):google.apps.script.card_service.SelectionInput;
}
