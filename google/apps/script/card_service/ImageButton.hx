package google.apps.script.card_service;

@:native("ImageButton")
extern class ImageButton
{
	function setOnClickAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.ImageButton;
	function setIcon(icon:google.apps.script.card_service.Icon):google.apps.script.card_service.ImageButton;
	function setAltText(altText:String):google.apps.script.card_service.ImageButton;
	function setComposeAction(action:google.apps.script.card_service.Action,composedEmailType:google.apps.script.card_service.ComposedEmailType):google.apps.script.card_service.ImageButton;
	function setOnClickOpenLinkAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.ImageButton;
	function setIconUrl(url:String):google.apps.script.card_service.ImageButton;
	function setOpenLink(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.ImageButton;
	function setAuthorizationAction(action:google.apps.script.card_service.AuthorizationAction):google.apps.script.card_service.ImageButton;
}
