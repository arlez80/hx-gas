package google.apps.script.card_service;

@:require(hx_gas)
@:native("LoadIndicator")
@:enum abstract LoadIndicator(String) from String to String
{
	var SPINNER;
	var NONE;
}

