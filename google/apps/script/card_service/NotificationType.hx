package google.apps.script.card_service;

@:require(hx_gas)
@:native("NotificationType")
@:enum abstract NotificationType(String) from String to String
{
	var INFO;
	var ERROR;
	var WARNING;
}

