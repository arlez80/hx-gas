package google.apps.script.card_service;

@:native("CardHeader")
extern class CardHeader
{
	function setImageAltText(imageAltText:String):google.apps.script.card_service.CardHeader;
	function setImageUrl(imageUrl:String):google.apps.script.card_service.CardHeader;
	function setTitle(title:String):google.apps.script.card_service.CardHeader;
	function setSubtitle(subtitle:String):google.apps.script.card_service.CardHeader;
	function setImageStyle(imageStyle:google.apps.script.card_service.ImageStyle):google.apps.script.card_service.CardHeader;
}
