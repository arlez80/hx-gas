package google.apps.script.card_service;

@:native("SuggestionsResponse")
extern class SuggestionsResponse
{
	function printJson(suggestions:Array<Dynamic>):String;
}
