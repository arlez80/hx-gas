package google.apps.script.card_service;

@:require(hx_gas)
@:native("SelectionInputType")
@:enum abstract SelectionInputType(String) from String to String
{
	var CHECK_BOX;
	var RADIO_BUTTON;
	var DROPDOWN;
}

