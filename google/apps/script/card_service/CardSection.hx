package google.apps.script.card_service;

@:native("CardSection")
extern class CardSection
{
	function setNumUncollapsibleWidgets(numUncollapsibleWidgets:Int):google.apps.script.card_service.CardSection;
	function setHeader(header:String):google.apps.script.card_service.CardSection;
	function setCollapsible(collapsible:Bool):google.apps.script.card_service.CardSection;
	function addWidget(widget:google.apps.script.card_service.Widget):google.apps.script.card_service.CardSection;
}
