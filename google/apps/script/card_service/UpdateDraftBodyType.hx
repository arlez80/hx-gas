package google.apps.script.card_service;

@:require(hx_gas)
@:native("UpdateDraftBodyType")
@:enum abstract UpdateDraftBodyType(String) from String to String
{
	var IN_PLACE_INSERT;
}

