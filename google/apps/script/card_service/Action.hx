package google.apps.script.card_service;

@:native("Action")
extern class Action
{
	function setLoadIndicator(loadIndicator:google.apps.script.card_service.LoadIndicator):google.apps.script.card_service.Action;
	function setFunctionName(functionName:String):google.apps.script.card_service.Action;
	function setParameters(parameters:Dynamic):google.apps.script.card_service.Action;
	@:deprecated
	function setMethodName(functionName:String):google.apps.script.card_service.Action;
}
