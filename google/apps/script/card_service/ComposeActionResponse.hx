package google.apps.script.card_service;

@:native("ComposeActionResponse")
extern class ComposeActionResponse
{
	function printJson(numUncollapsibleWidgets:Int):String;
}
