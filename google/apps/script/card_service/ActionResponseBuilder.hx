package google.apps.script.card_service;

@:native("ActionResponseBuilder")
extern class ActionResponseBuilder
{
	function setNavigation(navigation:google.apps.script.card_service.Navigation):google.apps.script.card_service.ActionResponseBuilder;
	function build(functionName:String):google.apps.script.card_service.ActionResponse;
	function setNotification(notification:google.apps.script.card_service.Notification):google.apps.script.card_service.ActionResponseBuilder;
	function setOpenLink(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.ActionResponseBuilder;
	function setStateChanged(stateChanged:Bool):google.apps.script.card_service.ActionResponseBuilder;
}
