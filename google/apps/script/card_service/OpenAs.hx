package google.apps.script.card_service;

@:require(hx_gas)
@:native("OpenAs")
@:enum abstract OpenAs(String) from String to String
{
	var FULL_SIZE;
	var OVERLAY;
}

