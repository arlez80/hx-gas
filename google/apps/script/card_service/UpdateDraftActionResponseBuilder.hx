package google.apps.script.card_service;

@:native("UpdateDraftActionResponseBuilder")
extern class UpdateDraftActionResponseBuilder
{
	function build(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.UpdateDraftActionResponse;
	function setUpdateDraftBodyAction(updateDraftBodyAction:google.apps.script.card_service.UpdateDraftBodyAction):google.apps.script.card_service.UpdateDraftActionResponseBuilder;
}
