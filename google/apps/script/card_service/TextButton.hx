package google.apps.script.card_service;

@:native("TextButton")
extern class TextButton
{
	function setText(text:String):google.apps.script.card_service.TextButton;
	function setBackgroundColor(backgroundColor:String):google.apps.script.card_service.TextButton;
	function setComposeAction(action:google.apps.script.card_service.Action,composedEmailType:google.apps.script.card_service.ComposedEmailType):google.apps.script.card_service.TextButton;
	function setTextButtonStyle(textButtonStyle:google.apps.script.card_service.TextButtonStyle):google.apps.script.card_service.TextButton;
	function setOnClickAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.TextButton;
	function setOnClickOpenLinkAction(action:google.apps.script.card_service.Action):google.apps.script.card_service.TextButton;
	function setDisabled(disabled:Bool):google.apps.script.card_service.TextButton;
	function setOpenLink(openLink:google.apps.script.card_service.OpenLink):google.apps.script.card_service.TextButton;
	function setAuthorizationAction(action:google.apps.script.card_service.AuthorizationAction):google.apps.script.card_service.TextButton;
}
