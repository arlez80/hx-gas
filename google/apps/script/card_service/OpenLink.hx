package google.apps.script.card_service;

@:native("OpenLink")
extern class OpenLink
{
	function setOpenAs(openAs:google.apps.script.card_service.OpenAs):google.apps.script.card_service.OpenLink;
	function setOnClose(onClose:google.apps.script.card_service.OnClose):google.apps.script.card_service.OpenLink;
	function setUrl(url:String):google.apps.script.card_service.OpenLink;
}
