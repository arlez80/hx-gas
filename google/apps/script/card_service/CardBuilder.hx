package google.apps.script.card_service;

@:native("CardBuilder")
extern class CardBuilder
{
	function addCardAction(cardAction:google.apps.script.card_service.CardAction):google.apps.script.card_service.CardBuilder;
	function build(section:google.apps.script.card_service.CardSection):google.apps.script.card_service.Card;
	function setHeader(cardHeader:google.apps.script.card_service.CardHeader):google.apps.script.card_service.CardBuilder;
	function addSection(section:google.apps.script.card_service.CardSection):google.apps.script.card_service.CardBuilder;
	function setName(name:String):google.apps.script.card_service.CardBuilder;
}
