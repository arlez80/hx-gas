package google.apps.script.card_service;

@:native("UpdateDraftActionResponse")
extern class UpdateDraftActionResponse
{
	function printJson(openLink:google.apps.script.card_service.OpenLink):String;
}
