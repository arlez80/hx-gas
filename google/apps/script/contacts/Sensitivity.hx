package google.apps.script.contacts;

@:require(hx_gas)
@:native("Sensitivity")
@:enum abstract Sensitivity(String) from String to String
{
	var CONFIDENTIAL;
	var NORMAL;
	var PERSONAL;
	var PRIVATE;
}

