package google.apps.script.contacts;

@:require(hx_gas)
@:native("ContactsApp.ExtendedField")
@:enum abstract ExtendedField(String) from String to String
{
	var HOBBY;
	var MILEAGE;
	var LANGUAGE;
	var GENDER;
	var BILLING_INFORMATION;
	var DIRECTORY_SERVER;
	var SENSITIVITY;
	var PRIORITY;
	var HOME;
	var WORK;
	var USER;
	var OTHER;
}

@:require(hx_gas)
@:native("ContactsApp.Field")
@:enum abstract Field(String) from String to String
{
	var FULL_NAME;
	var GIVEN_NAME;
	var MIDDLE_NAME;
	var FAMILY_NAME;
	var MAIDEN_NAME;
	var NICKNAME;
	var SHORT_NAME;
	var INITIALS;
	var PREFIX;
	var SUFFIX;
	var HOME_EMAIL;
	var WORK_EMAIL;
	var BIRTHDAY;
	var ANNIVERSARY;
	var HOME_ADDRESS;
	var WORK_ADDRESS;
	var ASSISTANT_PHONE;
	var CALLBACK_PHONE;
	var MAIN_PHONE;
	var PAGER;
	var HOME_FAX;
	var WORK_FAX;
	var HOME_PHONE;
	var WORK_PHONE;
	var MOBILE_PHONE;
	var GOOGLE_VOICE;
	var NOTES;
	var GOOGLE_TALK;
	var AIM;
	var YAHOO;
	var SKYPE;
	var QQ;
	var MSN;
	var ICQ;
	var JABBER;
	var BLOG;
	var FTP;
	var PROFILE;
	var HOME_PAGE;
	var WORK_WEBSITE;
	var HOME_WEBSITE;
	var JOB_TITLE;
	var COMPANY;
}

@:require(hx_gas)
@:native("ContactsApp.Gender")
@:enum abstract Gender(String) from String to String
{
	var MALE;
	var FEMALE;
}

@:require(hx_gas)
@:native("ContactsApp.Month")
@:enum abstract Month(String) from String to String
{
	var JANUARY;
	var FEBRUARY;
	var MARCH;
	var APRIL;
	var MAY;
	var JUNE;
	var JULY;
	var AUGUST;
	var SEPTEMBER;
	var OCTOBER;
	var NOVEMBER;
	var DECEMBER;
}

@:require(hx_gas)
@:native("ContactsApp.Priority")
@:enum abstract Priority(String) from String to String
{
	var HIGH;
	var LOW;
	var NORMAL;
}

@:require(hx_gas)
@:native("ContactsApp.Sensitivity")
@:enum abstract Sensitivity(String) from String to String
{
	var CONFIDENTIAL;
	var NORMAL;
	var PERSONAL;
	var PRIVATE;
}

@:native("ContactsApp")
extern class ContactsApp
{
	static function getContactGroups(id:String):Array<google.apps.script.contacts.ContactGroup>;
	static function createContactGroup(name:String):google.apps.script.contacts.ContactGroup;
	static function getContact(emailAddress:String):google.apps.script.contacts.Contact;
	static function getContactById(id:String):google.apps.script.contacts.Contact;
	static function getContactsByCustomField(query:Dynamic,label:google.apps.script.contacts.ExtendedField):Array<google.apps.script.contacts.Contact>;
	@:overload(function (query:String,label:google.apps.script.contacts.Field):Array<google.apps.script.contacts.Contact> {})
	@:overload(function (query:String,label:String):Array<google.apps.script.contacts.Contact> {})
	static function getContactsByAddress(query:String):Array<google.apps.script.contacts.Contact>;
	@:overload(function (query:String,label:google.apps.script.contacts.Field):Array<google.apps.script.contacts.Contact> {})
	@:overload(function (query:String,label:String):Array<google.apps.script.contacts.Contact> {})
	static function getContactsByEmailAddress(query:String):Array<google.apps.script.contacts.Contact>;
	@:deprecated
	static function findByEmailAddress(email:String):google.apps.script.contacts.Contact;
	static function getContactsByCompany(query:String):Array<google.apps.script.contacts.Contact>;
	static function getContactGroupById(id:String):google.apps.script.contacts.ContactGroup;
	static function getContactsByJobTitle(query:String):Array<google.apps.script.contacts.Contact>;
	static function createContact(givenName:String,familyName:String,email:String):google.apps.script.contacts.Contact;
	@:overload(function (query:String,label:google.apps.script.contacts.Field):Array<google.apps.script.contacts.Contact> {})
	static function getContactsByName(query:String):Array<google.apps.script.contacts.Contact>;
	@:overload(function (query:String,label:google.apps.script.contacts.Field):Array<google.apps.script.contacts.Contact> {})
	@:overload(function (query:String,label:String):Array<google.apps.script.contacts.Contact> {})
	static function getContactsByUrl(query:String):Array<google.apps.script.contacts.Contact>;
	static function getContactGroup(name:String):google.apps.script.contacts.ContactGroup;
	@:overload(function (query:String,label:google.apps.script.contacts.Field):Array<google.apps.script.contacts.Contact> {})
	@:overload(function (query:String,label:String):Array<google.apps.script.contacts.Contact> {})
	static function getContactsByIM(query:String):Array<google.apps.script.contacts.Contact>;
	static function getContactsByGroup(group:google.apps.script.contacts.ContactGroup):Array<google.apps.script.contacts.Contact>;
	static function getContacts(id:String):Array<google.apps.script.contacts.Contact>;
	static function getContactsByNotes(query:String):Array<google.apps.script.contacts.Contact>;
	@:overload(function (month:google.apps.script.base.Month,day:Int,year:Int,label:google.apps.script.contacts.Field):Array<google.apps.script.contacts.Contact> {})
	@:overload(function (month:google.apps.script.base.Month,day:Int,year:Int,label:String):Array<google.apps.script.contacts.Contact> {})
	@:overload(function (month:google.apps.script.base.Month,day:Int,label:String):Array<google.apps.script.contacts.Contact> {})
	static function getContactsByDate(month:google.apps.script.base.Month,day:Int,label:google.apps.script.contacts.Field):Array<google.apps.script.contacts.Contact>;
	@:overload(function (query:String,label:google.apps.script.contacts.Field):Array<google.apps.script.contacts.Contact> {})
	@:overload(function (query:String,label:String):Array<google.apps.script.contacts.Contact> {})
	static function getContactsByPhone(query:String):Array<google.apps.script.contacts.Contact>;
	@:deprecated
	static function findContactGroup(name:String):google.apps.script.contacts.ContactGroup;
	static function deleteContact(contact:google.apps.script.contacts.Contact):Void;
	@:deprecated
	static function getAllContacts(name:String):Array<google.apps.script.contacts.Contact>;
	static function deleteContactGroup(group:google.apps.script.contacts.ContactGroup):Void;
}
