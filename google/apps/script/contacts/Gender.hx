package google.apps.script.contacts;

@:require(hx_gas)
@:native("Gender")
@:enum abstract Gender(String) from String to String
{
	var MALE;
	var FEMALE;
}

