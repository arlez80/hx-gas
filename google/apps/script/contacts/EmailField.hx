package google.apps.script.contacts;

@:native("EmailField")
extern class EmailField
{
	function getLabel(label:String):Dynamic;
	function isPrimary(label:String):Bool;
	function getAddress(label:String):String;
	function setAddress(address:String):google.apps.script.contacts.EmailField;
	function setDisplayName(name:String):google.apps.script.contacts.EmailField;
	@:overload(function (label:String):google.apps.script.contacts.EmailField {})
	function setLabel(field:google.apps.script.contacts.Field):google.apps.script.contacts.EmailField;
	function setAsPrimary(address:String):google.apps.script.contacts.EmailField;
	function getDisplayName(label:String):String;
	function deleteEmailField(label:String):Void;
}
