package google.apps.script.contacts;

@:require(hx_gas)
@:native("Field")
@:enum abstract Field(String) from String to String
{
	var FULL_NAME;
	var GIVEN_NAME;
	var MIDDLE_NAME;
	var FAMILY_NAME;
	var MAIDEN_NAME;
	var NICKNAME;
	var SHORT_NAME;
	var INITIALS;
	var PREFIX;
	var SUFFIX;
	var HOME_EMAIL;
	var WORK_EMAIL;
	var BIRTHDAY;
	var ANNIVERSARY;
	var HOME_ADDRESS;
	var WORK_ADDRESS;
	var ASSISTANT_PHONE;
	var CALLBACK_PHONE;
	var MAIN_PHONE;
	var PAGER;
	var HOME_FAX;
	var WORK_FAX;
	var HOME_PHONE;
	var WORK_PHONE;
	var MOBILE_PHONE;
	var GOOGLE_VOICE;
	var NOTES;
	var GOOGLE_TALK;
	var AIM;
	var YAHOO;
	var SKYPE;
	var QQ;
	var MSN;
	var ICQ;
	var JABBER;
	var BLOG;
	var FTP;
	var PROFILE;
	var HOME_PAGE;
	var WORK_WEBSITE;
	var HOME_WEBSITE;
	var JOB_TITLE;
	var COMPANY;
}

