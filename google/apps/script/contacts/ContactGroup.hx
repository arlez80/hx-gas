package google.apps.script.contacts;

@:native("ContactGroup")
extern class ContactGroup
{
	@:deprecated
	function setGroupName(name:String):Void;
	function addContact(contact:google.apps.script.contacts.Contact):google.apps.script.contacts.ContactGroup;
	function getContacts(contact:google.apps.script.contacts.Contact):Array<google.apps.script.contacts.Contact>;
	function removeContact(contact:google.apps.script.contacts.Contact):google.apps.script.contacts.ContactGroup;
	function deleteGroup(contact:google.apps.script.contacts.Contact):Void;
	function setName(name:String):google.apps.script.contacts.ContactGroup;
	function getName(contact:google.apps.script.contacts.Contact):String;
	function isSystemGroup(contact:google.apps.script.contacts.Contact):Bool;
	function getId(contact:google.apps.script.contacts.Contact):String;
	@:deprecated
	function getGroupName(name:String):String;
}
