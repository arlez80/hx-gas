package google.apps.script.contacts;

@:native("PhoneField")
extern class PhoneField
{
	function deletePhoneField(label:String):Void;
	function getLabel(label:String):Dynamic;
	function isPrimary(label:String):Bool;
	function getPhoneNumber(label:String):String;
	@:overload(function (label:String):google.apps.script.contacts.PhoneField {})
	function setLabel(field:google.apps.script.contacts.Field):google.apps.script.contacts.PhoneField;
	function setPhoneNumber(number:String):google.apps.script.contacts.PhoneField;
	function setAsPrimary(label:String):google.apps.script.contacts.PhoneField;
}
