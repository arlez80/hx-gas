package google.apps.script.contacts;

@:native("DateField")
extern class DateField
{
	function getLabel(value:Dynamic):Dynamic;
	@:overload(function (month:google.apps.script.base.Month,day:Int,year:Int):google.apps.script.contacts.DateField {})
	function setDate(month:google.apps.script.base.Month,day:Int):google.apps.script.contacts.DateField;
	function getMonth(value:Dynamic):google.apps.script.base.Month;
	function getDay(value:Dynamic):Int;
	function getYear(value:Dynamic):Int;
	@:overload(function (label:String):google.apps.script.contacts.DateField {})
	function setLabel(label:google.apps.script.contacts.Field):google.apps.script.contacts.DateField;
	function deleteDateField(value:Dynamic):Void;
}
