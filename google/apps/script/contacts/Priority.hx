package google.apps.script.contacts;

@:require(hx_gas)
@:native("Priority")
@:enum abstract Priority(String) from String to String
{
	var HIGH;
	var LOW;
	var NORMAL;
}

