package google.apps.script.contacts;

@:require(hx_gas)
@:native("ExtendedField")
@:enum abstract ExtendedField(String) from String to String
{
	var HOBBY;
	var MILEAGE;
	var LANGUAGE;
	var GENDER;
	var BILLING_INFORMATION;
	var DIRECTORY_SERVER;
	var SENSITIVITY;
	var PRIORITY;
	var HOME;
	var WORK;
	var USER;
	var OTHER;
}

