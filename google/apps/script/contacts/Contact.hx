package google.apps.script.contacts;

@:native("Contact")
extern class Contact
{
	function getSuffix(label:Dynamic):String;
	function setSuffix(suffix:String):google.apps.script.contacts.Contact;
	function getPrimaryEmail(label:Dynamic):String;
	function addUrl(label:Dynamic,url:String):google.apps.script.contacts.UrlField;
	@:deprecated
	function getMobilePhone(suffix:String):String;
	function setShortName(shortName:String):google.apps.script.contacts.Contact;
	@:deprecated
	function getEmailAddresses(suffix:String):Array<String>;
	function setFullName(fullName:String):google.apps.script.contacts.Contact;
	function addCompany(company:String,title:String):google.apps.script.contacts.CompanyField;
	function getMiddleName(label:Dynamic):String;
	@:deprecated
	function setMobilePhone(phone:String):Void;
	@:overload(function (label:Dynamic):Array<google.apps.script.contacts.DateField> {})
	function getDates(label:Dynamic):Array<google.apps.script.contacts.DateField>;
	@:deprecated
	function setWorkPhone(phone:String):Void;
	function getFamilyName(label:Dynamic):String;
	@:deprecated
	function getWorkFax(key:String):String;
	function addCustomField(label:Dynamic,content:Dynamic):google.apps.script.contacts.CustomField;
	function setInitials(initials:String):google.apps.script.contacts.Contact;
	function getPrefix(label:Dynamic):String;
	@:deprecated
	function getPager(suffix:String):String;
	function setPrefix(prefix:String):google.apps.script.contacts.Contact;
	@:deprecated
	function getUserDefinedFields(key:String):Dynamic;
	@:deprecated
	function getHomeAddress(suffix:String):String;
	function setMiddleName(middleName:String):google.apps.script.contacts.Contact;
	@:overload(function (label:Dynamic):Array<google.apps.script.contacts.PhoneField> {})
	function getPhones(label:Dynamic):Array<google.apps.script.contacts.PhoneField>;
	function getInitials(label:Dynamic):String;
	@:deprecated
	function setWorkFax(phone:String):Void;
	@:deprecated
	function setHomeFax(phone:String):Void;
	function getFullName(label:Dynamic):String;
	@:deprecated
	function getHomeFax(suffix:String):String;
	function addEmail(label:Dynamic,address:String):google.apps.script.contacts.EmailField;
	@:deprecated
	function getWorkAddress(key:String):String;
	@:deprecated
	function getWorkPhone(key:String):String;
	function getCompanies(label:Dynamic):Array<google.apps.script.contacts.CompanyField>;
	@:deprecated
	function setUserDefinedField(key:String,value:String):Void;
	function addPhone(label:Dynamic,number:String):google.apps.script.contacts.PhoneField;
	function addDate(label:Dynamic,month:google.apps.script.base.Month,day:Int,year:Int):google.apps.script.contacts.DateField;
	function getContactGroups(label:Dynamic):Array<google.apps.script.contacts.ContactGroup>;
	@:overload(function (label:Dynamic):Array<google.apps.script.contacts.IMField> {})
	function getIMs(label:Dynamic):Array<google.apps.script.contacts.IMField>;
	function removeFromGroup(group:google.apps.script.contacts.ContactGroup):google.apps.script.contacts.Contact;
	function getMaidenName(label:Dynamic):String;
	function getLastUpdated(label:Dynamic):Date;
	@:overload(function (label:Dynamic):Array<google.apps.script.contacts.EmailField> {})
	function getEmails(label:Dynamic):Array<google.apps.script.contacts.EmailField>;
	function addAddress(label:Dynamic,address:String):google.apps.script.contacts.AddressField;
	@:deprecated
	function setPager(phone:String):Void;
	function addToGroup(group:google.apps.script.contacts.ContactGroup):google.apps.script.contacts.Contact;
	function getNickname(label:Dynamic):String;
	@:overload(function (label:Dynamic):Array<google.apps.script.contacts.AddressField> {})
	function getAddresses(label:Dynamic,url:String):Array<google.apps.script.contacts.AddressField>;
	function getShortName(label:Dynamic):String;
	function setMaidenName(maidenName:String):google.apps.script.contacts.Contact;
	function getId(label:Dynamic):String;
	@:deprecated
	function setHomePhone(phone:String):Void;
	@:deprecated
	function setHomeAddress(addr:String):Void;
	function setGivenName(givenName:String):google.apps.script.contacts.Contact;
	@:overload(function (label:Dynamic):Array<google.apps.script.contacts.CustomField> {})
	function getCustomFields(label:Dynamic):Array<google.apps.script.contacts.CustomField>;
	@:deprecated
	function getUserDefinedField(key:String):String;
	function getNotes(label:Dynamic):String;
	@:deprecated
	function setPrimaryEmail(primaryEmail:String):Void;
	@:overload(function (label:Dynamic):Array<google.apps.script.contacts.UrlField> {})
	function getUrls(label:Dynamic):Array<google.apps.script.contacts.UrlField>;
	function getGivenName(label:Dynamic):String;
	function setNickname(nickname:String):google.apps.script.contacts.Contact;
	function setNotes(notes:String):google.apps.script.contacts.Contact;
	@:deprecated
	function setUserDefinedFields(o:Dynamic):Void;
	@:deprecated
	function setWorkAddress(addr:String):Void;
	function addIM(label:Dynamic,address:String):google.apps.script.contacts.IMField;
	function deleteContact(label:Dynamic,url:String):Void;
	@:deprecated
	function getHomePhone(suffix:String):String;
	function setFamilyName(familyName:String):google.apps.script.contacts.Contact;
}
