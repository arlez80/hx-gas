package google.apps.script.contacts;

@:native("UrlField")
extern class UrlField
{
	function getLabel(number:String):Dynamic;
	function isPrimary(number:String):Bool;
	function getAddress(number:String):String;
	function setAddress(address:String):google.apps.script.contacts.UrlField;
	@:overload(function (label:String):google.apps.script.contacts.UrlField {})
	function setLabel(field:google.apps.script.contacts.Field):google.apps.script.contacts.UrlField;
	function deleteUrlField(number:String):Void;
	function setAsPrimary(address:String):google.apps.script.contacts.UrlField;
}
