package google.apps.script.contacts;

@:native("IMField")
extern class IMField
{
	function getLabel(label:String):Dynamic;
	function isPrimary(label:String):Bool;
	function getAddress(label:String):String;
	function setAddress(address:String):google.apps.script.contacts.IMField;
	@:overload(function (label:String):google.apps.script.contacts.IMField {})
	function setLabel(field:google.apps.script.contacts.Field):google.apps.script.contacts.IMField;
	function setAsPrimary(address:String):google.apps.script.contacts.IMField;
	function deleteIMField(label:String):Void;
}
