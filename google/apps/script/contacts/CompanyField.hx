package google.apps.script.contacts;

@:native("CompanyField")
extern class CompanyField
{
	function getCompanyName(label:String):String;
	function isPrimary(label:String):Bool;
	function deleteCompanyField(label:String):Void;
	function setCompanyName(company:String):google.apps.script.contacts.CompanyField;
	function setJobTitle(title:String):google.apps.script.contacts.CompanyField;
	function getJobTitle(label:String):String;
	function setAsPrimary(label:String):google.apps.script.contacts.CompanyField;
}
