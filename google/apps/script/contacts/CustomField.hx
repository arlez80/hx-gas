package google.apps.script.contacts;

@:native("CustomField")
extern class CustomField
{
	function getLabel(name:String):Dynamic;
	function deleteCustomField(name:String):Void;
	@:overload(function (label:String):google.apps.script.contacts.CustomField {})
	function setLabel(field:google.apps.script.contacts.ExtendedField):google.apps.script.contacts.CustomField;
	function setValue(value:Dynamic):google.apps.script.contacts.CustomField;
	function getValue(name:String):Dynamic;
}
