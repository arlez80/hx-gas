package google.apps.script.contacts;

@:native("AddressField")
extern class AddressField
{
	function getLabel(name:String):Dynamic;
	function isPrimary(name:String):Bool;
	function getAddress(name:String):String;
	function setAddress(address:String):google.apps.script.contacts.AddressField;
	function deleteAddressField(name:String):Void;
	@:overload(function (label:String):google.apps.script.contacts.AddressField {})
	function setLabel(field:google.apps.script.contacts.Field):google.apps.script.contacts.AddressField;
	function setAsPrimary(address:String):google.apps.script.contacts.AddressField;
}
