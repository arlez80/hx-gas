package google.apps.script.utilities;

@:require(hx_gas)
@:native("DigestAlgorithm")
@:enum abstract DigestAlgorithm(String) from String to String
{
	var MD2;
	var MD5;
	var SHA_1;
	var SHA_256;
	var SHA_384;
	var SHA_512;
}

