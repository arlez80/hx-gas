package google.apps.script.utilities;

@:require(hx_gas)
@:native("MacAlgorithm")
@:enum abstract MacAlgorithm(String) from String to String
{
	var HMAC_MD5;
	var HMAC_SHA_1;
	var HMAC_SHA_256;
	var HMAC_SHA_384;
	var HMAC_SHA_512;
}

