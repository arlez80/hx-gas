package google.apps.script.utilities;

@:require(hx_gas)
@:native("Charset")
@:enum abstract Charset(String) from String to String
{
	var US_ASCII;
	var UTF_8;
}

