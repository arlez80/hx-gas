package google.apps.script.utilities;

@:require(hx_gas)
@:native("Utilities.Charset")
@:enum abstract Charset(String) from String to String
{
	var US_ASCII;
	var UTF_8;
}

@:require(hx_gas)
@:native("Utilities.DigestAlgorithm")
@:enum abstract DigestAlgorithm(String) from String to String
{
	var MD2;
	var MD5;
	var SHA_1;
	var SHA_256;
	var SHA_384;
	var SHA_512;
}

@:require(hx_gas)
@:native("Utilities.MacAlgorithm")
@:enum abstract MacAlgorithm(String) from String to String
{
	var HMAC_MD5;
	var HMAC_SHA_1;
	var HMAC_SHA_256;
	var HMAC_SHA_384;
	var HMAC_SHA_512;
}

@:require(hx_gas)
@:native("Utilities.RsaAlgorithm")
@:enum abstract RsaAlgorithm(String) from String to String
{
	var RSA_SHA_1;
	var RSA_SHA_256;
}

@:native("Utilities")
extern class Utilities
{
	@:overload(function (algorithm:google.apps.script.utilities.RsaAlgorithm,value:String,key:String,charset:google.apps.script.utilities.Charset):Array<Int> {})
	static function computeRsaSignature(algorithm:google.apps.script.utilities.RsaAlgorithm,value:String,key:String):Array<Int>;
	@:overload(function (data:String):String {})
	@:overload(function (data:String,charset:google.apps.script.utilities.Charset):String {})
	static function base64Encode(data:Array<Int>):String;
	@:overload(function (value:String,key:String):Array<Int> {})
	@:overload(function (value:String,key:String,charset:google.apps.script.utilities.Charset):Array<Int> {})
	static function computeHmacSha256Signature(value:Array<Int>,key:Array<Int>):Array<Int>;
	@:overload(function (value:String,key:String,charset:google.apps.script.utilities.Charset):Array<Int> {})
	static function computeRsaSha1Signature(value:String,key:String):Array<Int>;
	static function formatDate(date:Date,timeZone:String,format:String):String;
	@:overload(function (csv:String,delimiter:String):Array<Array<String>> {})
	static function parseCsv(csv:String):Array<Array<String>>;
	@:overload(function (encoded:String,charset:google.apps.script.utilities.Charset):Array<Int> {})
	static function base64Decode(encoded:String):Array<Int>;
	@:overload(function (encoded:String,charset:google.apps.script.utilities.Charset):Array<Int> {})
	static function base64DecodeWebSafe(encoded:String):Array<Int>;
	static function formatString(template:String,args:Dynamic):String;
	@:deprecated
	static function jsonParse(jsonString:String):Dynamic;
	@:overload(function (blob:google.apps.script.base.BlobSource,name:String):google.apps.script.base.Blob {})
	static function gzip(blob:google.apps.script.base.BlobSource):google.apps.script.base.Blob;
	static function unzip(blob:google.apps.script.base.BlobSource):Array<google.apps.script.base.Blob>;
	@:overload(function (algorithm:google.apps.script.utilities.MacAlgorithm,value:String,key:String):Array<Int> {})
	@:overload(function (algorithm:google.apps.script.utilities.MacAlgorithm,value:String,key:String,charset:google.apps.script.utilities.Charset):Array<Int> {})
	static function computeHmacSignature(algorithm:google.apps.script.utilities.MacAlgorithm,value:Array<Int>,key:Array<Int>):Array<Int>;
	@:deprecated
	static function jsonStringify(obj:Dynamic):String;
	@:overload(function (data:Array<Int>,contentType:String):google.apps.script.base.Blob {})
	@:overload(function (data:Array<Int>,contentType:String,name:String):google.apps.script.base.Blob {})
	@:overload(function (data:String):google.apps.script.base.Blob {})
	@:overload(function (data:String,contentType:String):google.apps.script.base.Blob {})
	@:overload(function (data:String,contentType:String,name:String):google.apps.script.base.Blob {})
	static function newBlob(data:Array<Int>):google.apps.script.base.Blob;
	@:overload(function (data:String):String {})
	@:overload(function (data:String,charset:google.apps.script.utilities.Charset):String {})
	static function base64EncodeWebSafe(data:Array<Int>):String;
	static function ungzip(blob:google.apps.script.base.BlobSource):google.apps.script.base.Blob;
	static function getUuid(template:String,args:Dynamic):String;
	static function sleep(milliseconds:Int):Void;
	@:overload(function (blobs:Array<google.apps.script.base.BlobSource>,name:String):google.apps.script.base.Blob {})
	static function zip(blobs:Array<google.apps.script.base.BlobSource>):google.apps.script.base.Blob;
	@:overload(function (value:String,key:String,charset:google.apps.script.utilities.Charset):Array<Int> {})
	static function computeRsaSha256Signature(value:String,key:String):Array<Int>;
	@:overload(function (algorithm:google.apps.script.utilities.DigestAlgorithm,value:String):Array<Int> {})
	@:overload(function (algorithm:google.apps.script.utilities.DigestAlgorithm,value:String,charset:google.apps.script.utilities.Charset):Array<Int> {})
	static function computeDigest(algorithm:google.apps.script.utilities.DigestAlgorithm,value:Array<Int>):Array<Int>;
}
