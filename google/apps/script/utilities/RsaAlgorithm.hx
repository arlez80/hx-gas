package google.apps.script.utilities;

@:require(hx_gas)
@:native("RsaAlgorithm")
@:enum abstract RsaAlgorithm(String) from String to String
{
	var RSA_SHA_1;
	var RSA_SHA_256;
}

