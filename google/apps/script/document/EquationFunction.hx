package google.apps.script.document;

@:native("EquationFunction")
extern class EquationFunction
{
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function getCode(child:google.apps.script.document.Element):String;
	function setAttributes(attributes:Dynamic):google.apps.script.document.EquationFunction;
	function copy(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.EquationFunction;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	function getType(child:google.apps.script.document.Element):google.apps.script.document.ElementType;
	function clear(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.EquationFunction;
	function getLinkUrl(child:google.apps.script.document.Element):String;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function merge(child:google.apps.script.document.Element):google.apps.script.document.EquationFunction;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.EquationFunction;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function getNextSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	function isAtDocumentEnd(child:google.apps.script.document.Element):Bool;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.EquationFunction;
	function setLinkUrl(url:String):google.apps.script.document.EquationFunction;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function editAsText(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Text;
	function getText(child:google.apps.script.document.Element):String;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	function getTextAlignment(child:google.apps.script.document.Element):google.apps.script.document.TextAlignment;
	function getPreviousSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
}
