package google.apps.script.document;

@:native("Table")
extern class Table
{
	function getChild(childIndex:Int):google.apps.script.document.Element;
	@:overload(function (childIndex:Int,tableRow:google.apps.script.document.TableRow):google.apps.script.document.TableRow {})
	function insertTableRow(childIndex:Int):google.apps.script.document.TableRow;
	function copy(tableRow:google.apps.script.document.TableRow):google.apps.script.document.Table;
	function removeChild(child:google.apps.script.document.Element):google.apps.script.document.Table;
	function getNumChildren(columnIndex:Int):Int;
	function setAttributes(attributes:Dynamic):google.apps.script.document.Table;
	function getType(rowIndex:Int):google.apps.script.document.ElementType;
	function getBorderColor(searchPattern:String,from:google.apps.script.document.RangeElement):String;
	function getLinkUrl(columnIndex:Int):String;
	function getNextSibling(columnIndex:Int):google.apps.script.document.Element;
	function setBorderColor(color:String):google.apps.script.document.Table;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.Table;
	function setBorderWidth(width:Float):google.apps.script.document.Table;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function setColumnWidth(columnIndex:Int,width:Float):google.apps.script.document.Table;
	function editAsText(tableRow:google.apps.script.document.TableRow):google.apps.script.document.Text;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function clear(tableRow:google.apps.script.document.TableRow):google.apps.script.document.Table;
	function getCell(rowIndex:Int,cellIndex:Int):google.apps.script.document.TableCell;
	function isAtDocumentEnd(childIndex:Int,tableRow:google.apps.script.document.TableRow):Bool;
	function getBorderWidth(searchPattern:String,from:google.apps.script.document.RangeElement):Float;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Table;
	function getRow(rowIndex:Int):google.apps.script.document.TableRow;
	function setLinkUrl(url:String):google.apps.script.document.Table;
	function removeRow(rowIndex:Int):google.apps.script.document.TableRow;
	@:overload(function (tableRow:google.apps.script.document.TableRow):google.apps.script.document.TableRow {})
	function appendTableRow(range:google.apps.script.document.Range):google.apps.script.document.TableRow;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function getText(rowIndex:Int):String;
	function getNumRows(columnIndex:Int):Int;
	function getParent(columnIndex:Int):google.apps.script.document.ContainerElement;
	function getTextAlignment(rowIndex:Int):google.apps.script.document.TextAlignment;
	function getColumnWidth(columnIndex:Int):Float;
	function getPreviousSibling(columnIndex:Int):google.apps.script.document.Element;
}
