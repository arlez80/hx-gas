package google.apps.script.document;

@:native("InlineImage")
extern class InlineImage
{
	function getHeight(contentType:String):Int;
	function getAltTitle(attributes:Dynamic):String;
	function getType(contentType:String):google.apps.script.document.ElementType;
	function getLinkUrl(contentType:String):String;
	function getNextSibling(contentType:String):google.apps.script.document.Element;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function removeFromParent(contentType:String):google.apps.script.document.InlineImage;
	function getParent(contentType:String):google.apps.script.document.ContainerElement;
	function merge(contentType:String):google.apps.script.document.InlineImage;
	function setAltDescription(description:String):google.apps.script.document.InlineImage;
	function setWidth(width:Int):google.apps.script.document.InlineImage;
	function setAttributes(attributes:Dynamic):google.apps.script.document.InlineImage;
	function setHeight(height:Int):google.apps.script.document.InlineImage;
	function copy(attributes:Dynamic):google.apps.script.document.InlineImage;
	function isAtDocumentEnd(contentType:String):Bool;
	function setAltTitle(title:String):google.apps.script.document.InlineImage;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function getAltDescription(attributes:Dynamic):String;
	function getAttributes(contentType:String):Dynamic;
	function setLinkUrl(url:String):google.apps.script.document.InlineImage;
	function getWidth(contentType:String):Int;
	function getPreviousSibling(contentType:String):google.apps.script.document.Element;
}
