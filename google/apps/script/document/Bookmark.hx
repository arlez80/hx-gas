package google.apps.script.document;

@:native("Bookmark")
extern class Bookmark
{
	function remove(url:String):Void;
	function getId(url:String):String;
	function getPosition(url:String):google.apps.script.document.Position;
}
