package google.apps.script.document;

@:native("TableCell")
extern class TableCell
{
	function setPaddingBottom(paddingBottom:Float):google.apps.script.document.TableCell;
	function setPaddingTop(paddingTop:Float):google.apps.script.document.TableCell;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function setBackgroundColor(color:String):google.apps.script.document.TableCell;
	function insertHorizontalRule(childIndex:Int):google.apps.script.document.HorizontalRule;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	@:overload(function (cells:Array<Array<String>>):google.apps.script.document.Table {})
	@:overload(function (table:google.apps.script.document.Table):google.apps.script.document.Table {})
	function appendTable(text:String):google.apps.script.document.Table;
	function getRowSpan(child:google.apps.script.document.Element):Int;
	function merge(childIndex:Int,table:google.apps.script.document.Table):google.apps.script.document.TableCell;
	@:overload(function (text:String):google.apps.script.document.ListItem {})
	function appendListItem(listItem:google.apps.script.document.ListItem):google.apps.script.document.ListItem;
	function setPaddingRight(paddingTop:Float):google.apps.script.document.TableCell;
	function getPaddingTop(child:google.apps.script.document.Element):Float;
	function getParentTable(child:google.apps.script.document.Element):google.apps.script.document.Table;
	function removeChild(child:google.apps.script.document.Element):google.apps.script.document.TableCell;
	@:overload(function (childIndex:Int,text:String):google.apps.script.document.Paragraph {})
	function insertParagraph(childIndex:Int,paragraph:google.apps.script.document.Paragraph):google.apps.script.document.Paragraph;
	function isAtDocumentEnd(childIndex:Int,table:google.apps.script.document.Table):Bool;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.TableCell;
	function getColSpan(child:google.apps.script.document.Element):Int;
	function setLinkUrl(url:String):google.apps.script.document.TableCell;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	function setPaddingLeft(paddingLeft:Float):google.apps.script.document.TableCell;
	function editAsText(table:google.apps.script.document.Table):google.apps.script.document.Text;
	function getText(child:google.apps.script.document.Element):String;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.TableCell;
	function getTextAlignment(child:google.apps.script.document.Element):google.apps.script.document.TextAlignment;
	function appendHorizontalRule(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.HorizontalRule;
	function getParentRow(child:google.apps.script.document.Element):google.apps.script.document.TableRow;
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function getPaddingBottom(child:google.apps.script.document.Element):Float;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	@:overload(function (childIndex:Int,text:String):google.apps.script.document.ListItem {})
	function insertListItem(childIndex:Int,listItem:google.apps.script.document.ListItem):google.apps.script.document.ListItem;
	function setText(text:String):google.apps.script.document.TableCell;
	function getVerticalAlignment(child:google.apps.script.document.Element):google.apps.script.document.VerticalAlignment;
	function getType(child:google.apps.script.document.Element):google.apps.script.document.ElementType;
	function clear(table:google.apps.script.document.Table):google.apps.script.document.TableCell;
	function getLinkUrl(child:google.apps.script.document.Element):String;
	function setVerticalAlignment(alignment:google.apps.script.document.VerticalAlignment):google.apps.script.document.TableCell;
	@:overload(function (image:google.apps.script.document.InlineImage):google.apps.script.document.InlineImage {})
	function appendImage(image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	function getBackgroundColor(searchPattern:String,from:google.apps.script.document.RangeElement):String;
	function getPaddingLeft(child:google.apps.script.document.Element):Float;
	function getPaddingRight(child:google.apps.script.document.Element):Float;
	function setWidth(width:Float):google.apps.script.document.TableCell;
	function setAttributes(attributes:Dynamic):google.apps.script.document.TableCell;
	function getNextSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	@:overload(function (childIndex:Int,cells:Array<Array<String>>):google.apps.script.document.Table {})
	@:overload(function (childIndex:Int,table:google.apps.script.document.Table):google.apps.script.document.Table {})
	function insertTable(childIndex:Int):google.apps.script.document.Table;
	@:overload(function (text:String):google.apps.script.document.Paragraph {})
	function appendParagraph(paragraph:google.apps.script.document.Paragraph):google.apps.script.document.Paragraph;
	@:overload(function (childIndex:Int,image:google.apps.script.document.InlineImage):google.apps.script.document.InlineImage {})
	function insertImage(childIndex:Int,image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function copy(table:google.apps.script.document.Table):google.apps.script.document.TableCell;
	function getWidth(child:google.apps.script.document.Element):Float;
	function getPreviousSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
}
