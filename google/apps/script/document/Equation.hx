package google.apps.script.document;

@:native("Equation")
extern class Equation
{
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function copy(range:google.apps.script.document.Range):google.apps.script.document.Equation;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	function getType(child:google.apps.script.document.Element):google.apps.script.document.ElementType;
	function clear(range:google.apps.script.document.Range):google.apps.script.document.Equation;
	function getLinkUrl(child:google.apps.script.document.Element):String;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	function merge(child:google.apps.script.document.Element):google.apps.script.document.Equation;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function setAttributes(attributes:Dynamic):google.apps.script.document.Equation;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function getNextSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	function isAtDocumentEnd(child:google.apps.script.document.Element):Bool;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Equation;
	function setLinkUrl(url:String):google.apps.script.document.Equation;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function editAsText(range:google.apps.script.document.Range):google.apps.script.document.Text;
	function getText(child:google.apps.script.document.Element):String;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.Equation;
	function getTextAlignment(child:google.apps.script.document.Element):google.apps.script.document.TextAlignment;
	function getPreviousSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
}
