package google.apps.script.document;

@:native("Range")
extern class Range
{
	@:deprecated
	function getSelectedElements(width:Int):Array<google.apps.script.document.RangeElement>;
	function getRangeElements(width:Int):Array<google.apps.script.document.RangeElement>;
}
