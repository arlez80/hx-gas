package google.apps.script.document;

@:require(hx_gas)
@:native("Attribute")
@:enum abstract Attribute(String) from String to String
{
	var BACKGROUND_COLOR;
	var BOLD;
	var BORDER_COLOR;
	var BORDER_WIDTH;
	var CODE;
	var FONT_FAMILY;
	var FONT_SIZE;
	var FOREGROUND_COLOR;
	var HEADING;
	var HEIGHT;
	var HORIZONTAL_ALIGNMENT;
	var INDENT_END;
	var INDENT_FIRST_LINE;
	var INDENT_START;
	var ITALIC;
	var GLYPH_TYPE;
	var LEFT_TO_RIGHT;
	var LINE_SPACING;
	var LINK_URL;
	var LIST_ID;
	var MARGIN_BOTTOM;
	var MARGIN_LEFT;
	var MARGIN_RIGHT;
	var MARGIN_TOP;
	var NESTING_LEVEL;
	var MINIMUM_HEIGHT;
	var PADDING_BOTTOM;
	var PADDING_LEFT;
	var PADDING_RIGHT;
	var PADDING_TOP;
	var PAGE_HEIGHT;
	var PAGE_WIDTH;
	var SPACING_AFTER;
	var SPACING_BEFORE;
	var STRIKETHROUGH;
	var UNDERLINE;
	var VERTICAL_ALIGNMENT;
	var WIDTH;
}

