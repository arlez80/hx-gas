package google.apps.script.document;

@:native("ContainerElement")
extern class ContainerElement
{
	function asTableOfContents(url:String):google.apps.script.document.TableOfContents;
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function setAttributes(attributes:Dynamic):google.apps.script.document.ContainerElement;
	function asTableCell(url:String):google.apps.script.document.TableCell;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	function getType(child:google.apps.script.document.Element):google.apps.script.document.ElementType;
	function clear(url:String):google.apps.script.document.ContainerElement;
	function getLinkUrl(child:google.apps.script.document.Element):String;
	function setLinkUrl(url:String):google.apps.script.document.ContainerElement;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	function asFootnoteSection(url:String):google.apps.script.document.FootnoteSection;
	function asTable(url:String):google.apps.script.document.Table;
	function merge(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function editAsText(url:String):google.apps.script.document.Text;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function asFooterSection(url:String):google.apps.script.document.FooterSection;
	function getNextSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	function isAtDocumentEnd(child:google.apps.script.document.Element):Bool;
	function asListItem(url:String):google.apps.script.document.ListItem;
	function asHeaderSection(url:String):google.apps.script.document.HeaderSection;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.ContainerElement;
	function asTableRow(url:String):google.apps.script.document.TableRow;
	function asParagraph(url:String):google.apps.script.document.Paragraph;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	function asEquation(url:String):google.apps.script.document.Equation;
	function getPreviousSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function getText(child:google.apps.script.document.Element):String;
	function copy(url:String):google.apps.script.document.ContainerElement;
	function getTextAlignment(child:google.apps.script.document.Element):google.apps.script.document.TextAlignment;
	function asBody(url:String):google.apps.script.document.Body;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
}
