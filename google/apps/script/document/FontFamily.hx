package google.apps.script.document;

@:require(hx_gas)
@:native("FontFamily")
@:deprecated
@:enum abstract FontFamily(String) from String to String
{
@:deprecated
	var AMARANTH;
@:deprecated
	var ARIAL;
@:deprecated
	var ARIAL_BLACK;
@:deprecated
	var ARIAL_NARROW;
@:deprecated
	var ARVO;
@:deprecated
	var CALIBRI;
@:deprecated
	var CAMBRIA;
@:deprecated
	var COMIC_SANS_MS;
@:deprecated
	var CONSOLAS;
@:deprecated
	var CORSIVA;
@:deprecated
	var COURIER_NEW;
@:deprecated
	var DANCING_SCRIPT;
@:deprecated
	var DROID_SANS;
@:deprecated
	var DROID_SERIF;
@:deprecated
	var GARAMOND;
@:deprecated
	var GEORGIA;
@:deprecated
	var GLORIA_HALLELUJAH;
@:deprecated
	var GREAT_VIBES;
@:deprecated
	var LOBSTER;
@:deprecated
	var MERRIWEATHER;
@:deprecated
	var PACIFICO;
@:deprecated
	var PHILOSOPHER;
@:deprecated
	var POIRET_ONE;
@:deprecated
	var QUATTROCENTO;
@:deprecated
	var ROBOTO;
@:deprecated
	var SHADOWS_INTO_LIGHT;
@:deprecated
	var SYNCOPATE;
@:deprecated
	var TAHOMA;
@:deprecated
	var TIMES_NEW_ROMAN;
@:deprecated
	var TREBUCHET_MS;
@:deprecated
	var UBUNTU;
@:deprecated
	var VERDANA;
}

