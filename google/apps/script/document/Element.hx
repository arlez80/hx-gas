package google.apps.script.document;

@:native("Element")
extern class Element
{
	function asTableOfContents(attributes:Dynamic):google.apps.script.document.TableOfContents;
	function setAttributes(attributes:Dynamic):google.apps.script.document.Element;
	function asTableCell(attributes:Dynamic):google.apps.script.document.TableCell;
	function asText(attributes:Dynamic):google.apps.script.document.Text;
	function getType(attributes:Dynamic):google.apps.script.document.ElementType;
	function asFooterSection(attributes:Dynamic):google.apps.script.document.FooterSection;
	function asEquationFunction(attributes:Dynamic):google.apps.script.document.EquationFunction;
	function getNextSibling(attributes:Dynamic):google.apps.script.document.Element;
	function asFootnoteSection(attributes:Dynamic):google.apps.script.document.FootnoteSection;
	function asTable(attributes:Dynamic):google.apps.script.document.Table;
	function merge(attributes:Dynamic):google.apps.script.document.Element;
	function getAttributes(attributes:Dynamic):Dynamic;
	function removeFromParent(attributes:Dynamic):google.apps.script.document.Element;
	function asEquationFunctionArgumentSeparator(attributes:Dynamic):google.apps.script.document.EquationFunctionArgumentSeparator;
	function asInlineDrawing(attributes:Dynamic):google.apps.script.document.InlineDrawing;
	function isAtDocumentEnd(attributes:Dynamic):Bool;
	function asHorizontalRule(attributes:Dynamic):google.apps.script.document.HorizontalRule;
	function asListItem(attributes:Dynamic):google.apps.script.document.ListItem;
	function asHeaderSection(attributes:Dynamic):google.apps.script.document.HeaderSection;
	function asFootnote(attributes:Dynamic):google.apps.script.document.Footnote;
	function asTableRow(attributes:Dynamic):google.apps.script.document.TableRow;
	function asEquation(attributes:Dynamic):google.apps.script.document.Equation;
	function asParagraph(attributes:Dynamic):google.apps.script.document.Paragraph;
	function asEquationSymbol(attributes:Dynamic):google.apps.script.document.EquationSymbol;
	function asPageBreak(attributes:Dynamic):google.apps.script.document.PageBreak;
	function asInlineImage(attributes:Dynamic):google.apps.script.document.InlineImage;
	function getPreviousSibling(attributes:Dynamic):google.apps.script.document.Element;
	function copy(attributes:Dynamic):google.apps.script.document.Element;
	function asBody(attributes:Dynamic):google.apps.script.document.Body;
	function getParent(attributes:Dynamic):google.apps.script.document.ContainerElement;
}
