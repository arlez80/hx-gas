package google.apps.script.document;

@:native("Text")
extern class Text
{
	@:overload(function (attributes:Dynamic):google.apps.script.document.Text {})
	function setAttributes(startOffset:Int,endOffsetInclusive:Int,attributes:Dynamic):google.apps.script.document.Text;
	function copy(text:String):google.apps.script.document.Text;
	@:overload(function (startOffset:Int,endOffsetInclusive:Int,underline:Bool):google.apps.script.document.Text {})
	function setUnderline(underline:Bool):google.apps.script.document.Text;
	@:overload(function (offset:Int):String {})
	function getForegroundColor(offset:Int):String;
	@:overload(function (offset:Int):Int {})
	function getFontSize(offset:Int):Int;
	@:overload(function (startOffset:Int,endOffsetInclusive:Int,bold:Bool):google.apps.script.document.Text {})
	function setBold(bold:Bool):google.apps.script.document.Text;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	@:overload(function (offset:Int):Bool {})
	function isBold(offset:Int,text:String):Bool;
	@:overload(function (color:String):google.apps.script.document.Text {})
	function setBackgroundColor(startOffset:Int,endOffsetInclusive:Int,color:String):google.apps.script.document.Text;
	function getType(offset:Int):google.apps.script.document.ElementType;
	@:overload(function (offset:Int):Bool {})
	function isStrikethrough(offset:Int):Bool;
	function deleteText(startOffset:Int,endOffsetInclusive:Int):google.apps.script.document.Text;
	@:overload(function (offset:Int):String {})
	function getLinkUrl(offset:Int):String;
	function insertText(offset:Int,text:String):google.apps.script.document.Text;
	@:overload(function (offset:Int):Bool {})
	function isItalic(offset:Int):Bool;
	@:overload(function (offset:Int):Bool {})
	function isUnderline(offset:Int):Bool;
	@:overload(function (offset:Int):String {})
	function getBackgroundColor(offset:Int):String;
	@:overload(function (fontFamilyName:String):google.apps.script.document.Text {})
	function setFontFamily(startOffset:Int,endOffsetInclusive:Int,fontFamilyName:String):google.apps.script.document.Text;
	function merge(offset:Int):google.apps.script.document.Text;
	@:overload(function (offset:Int):Dynamic {})
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function appendText(text:String):google.apps.script.document.Text;
	function editAsText(startOffset:Int,endOffsetInclusive:Int):google.apps.script.document.Text;
	function setText(text:String):google.apps.script.document.Text;
	function getNextSibling(offset:Int):google.apps.script.document.Element;
	function isAtDocumentEnd(offset:Int,text:String):Bool;
	@:overload(function (offset:Int):String {})
	function getFontFamily(offset:Int):String;
	@:overload(function (textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Text {})
	function setTextAlignment(startOffset:Int,endOffsetInclusive:Int,textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Text;
	@:overload(function (startOffset:Int,endOffsetInclusive:Int,strikethrough:Bool):google.apps.script.document.Text {})
	function setStrikethrough(strikethrough:Bool):google.apps.script.document.Text;
	@:overload(function (url:String):google.apps.script.document.Text {})
	function setLinkUrl(startOffset:Int,endOffsetInclusive:Int,url:String):google.apps.script.document.Text;
	@:overload(function (color:String):google.apps.script.document.Text {})
	function setForegroundColor(startOffset:Int,endOffsetInclusive:Int,color:String):google.apps.script.document.Text;
	@:overload(function (startOffset:Int,endOffsetInclusive:Int,italic:Bool):google.apps.script.document.Text {})
	function setItalic(italic:Bool):google.apps.script.document.Text;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	@:overload(function (startOffset:Int,endOffsetInclusive:Int,size:Int):google.apps.script.document.Text {})
	function setFontSize(size:Int):google.apps.script.document.Text;
	function removeFromParent(offset:Int):google.apps.script.document.Text;
	function getTextAttributeIndices(offset:Int):Array<Int>;
	function getText(offset:Int):String;
	function getParent(offset:Int):google.apps.script.document.ContainerElement;
	@:overload(function (offset:Int):google.apps.script.document.TextAlignment {})
	function getTextAlignment(offset:Int):google.apps.script.document.TextAlignment;
	function getPreviousSibling(offset:Int):google.apps.script.document.Element;
}
