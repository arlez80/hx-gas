package google.apps.script.document;

@:require(hx_gas)
@:native("DocumentApp.Attribute")
@:enum abstract Attribute(String) from String to String
{
	var BACKGROUND_COLOR;
	var BOLD;
	var BORDER_COLOR;
	var BORDER_WIDTH;
	var CODE;
	var FONT_FAMILY;
	var FONT_SIZE;
	var FOREGROUND_COLOR;
	var HEADING;
	var HEIGHT;
	var HORIZONTAL_ALIGNMENT;
	var INDENT_END;
	var INDENT_FIRST_LINE;
	var INDENT_START;
	var ITALIC;
	var GLYPH_TYPE;
	var LEFT_TO_RIGHT;
	var LINE_SPACING;
	var LINK_URL;
	var LIST_ID;
	var MARGIN_BOTTOM;
	var MARGIN_LEFT;
	var MARGIN_RIGHT;
	var MARGIN_TOP;
	var NESTING_LEVEL;
	var MINIMUM_HEIGHT;
	var PADDING_BOTTOM;
	var PADDING_LEFT;
	var PADDING_RIGHT;
	var PADDING_TOP;
	var PAGE_HEIGHT;
	var PAGE_WIDTH;
	var SPACING_AFTER;
	var SPACING_BEFORE;
	var STRIKETHROUGH;
	var UNDERLINE;
	var VERTICAL_ALIGNMENT;
	var WIDTH;
}

@:require(hx_gas)
@:native("DocumentApp.ElementType")
@:enum abstract ElementType(String) from String to String
{
	var BODY_SECTION;
	var COMMENT_SECTION;
	var DOCUMENT;
	var EQUATION;
	var EQUATION_FUNCTION;
	var EQUATION_FUNCTION_ARGUMENT_SEPARATOR;
	var EQUATION_SYMBOL;
	var FOOTER_SECTION;
	var FOOTNOTE;
	var FOOTNOTE_SECTION;
	var HEADER_SECTION;
	var HORIZONTAL_RULE;
	var INLINE_DRAWING;
	var INLINE_IMAGE;
	var LIST_ITEM;
	var PAGE_BREAK;
	var PARAGRAPH;
	var TABLE;
	var TABLE_CELL;
	var TABLE_OF_CONTENTS;
	var TABLE_ROW;
	var TEXT;
	var UNSUPPORTED;
}

@:require(hx_gas)
@:native("DocumentApp.FontFamily")
@:deprecated
@:enum abstract FontFamily(String) from String to String
{
@:deprecated
	var AMARANTH;
@:deprecated
	var ARIAL;
@:deprecated
	var ARIAL_BLACK;
@:deprecated
	var ARIAL_NARROW;
@:deprecated
	var ARVO;
@:deprecated
	var CALIBRI;
@:deprecated
	var CAMBRIA;
@:deprecated
	var COMIC_SANS_MS;
@:deprecated
	var CONSOLAS;
@:deprecated
	var CORSIVA;
@:deprecated
	var COURIER_NEW;
@:deprecated
	var DANCING_SCRIPT;
@:deprecated
	var DROID_SANS;
@:deprecated
	var DROID_SERIF;
@:deprecated
	var GARAMOND;
@:deprecated
	var GEORGIA;
@:deprecated
	var GLORIA_HALLELUJAH;
@:deprecated
	var GREAT_VIBES;
@:deprecated
	var LOBSTER;
@:deprecated
	var MERRIWEATHER;
@:deprecated
	var PACIFICO;
@:deprecated
	var PHILOSOPHER;
@:deprecated
	var POIRET_ONE;
@:deprecated
	var QUATTROCENTO;
@:deprecated
	var ROBOTO;
@:deprecated
	var SHADOWS_INTO_LIGHT;
@:deprecated
	var SYNCOPATE;
@:deprecated
	var TAHOMA;
@:deprecated
	var TIMES_NEW_ROMAN;
@:deprecated
	var TREBUCHET_MS;
@:deprecated
	var UBUNTU;
@:deprecated
	var VERDANA;
}

@:require(hx_gas)
@:native("DocumentApp.GlyphType")
@:enum abstract GlyphType(String) from String to String
{
	var BULLET;
	var HOLLOW_BULLET;
	var SQUARE_BULLET;
	var NUMBER;
	var LATIN_UPPER;
	var LATIN_LOWER;
	var ROMAN_UPPER;
	var ROMAN_LOWER;
}

@:require(hx_gas)
@:native("DocumentApp.HorizontalAlignment")
@:enum abstract HorizontalAlignment(String) from String to String
{
	var LEFT;
	var CENTER;
	var RIGHT;
	var JUSTIFY;
}

@:require(hx_gas)
@:native("DocumentApp.ParagraphHeading")
@:enum abstract ParagraphHeading(String) from String to String
{
	var NORMAL;
	var HEADING1;
	var HEADING2;
	var HEADING3;
	var HEADING4;
	var HEADING5;
	var HEADING6;
	var TITLE;
	var SUBTITLE;
}

@:require(hx_gas)
@:native("DocumentApp.PositionedLayout")
@:enum abstract PositionedLayout(String) from String to String
{
	var ABOVE_TEXT;
	var BREAK_BOTH;
	var BREAK_LEFT;
	var BREAK_RIGHT;
	var WRAP_TEXT;
}

@:require(hx_gas)
@:native("DocumentApp.TextAlignment")
@:enum abstract TextAlignment(String) from String to String
{
	var NORMAL;
	var SUPERSCRIPT;
	var SUBSCRIPT;
}

@:require(hx_gas)
@:native("DocumentApp.VerticalAlignment")
@:enum abstract VerticalAlignment(String) from String to String
{
	var BOTTOM;
	var CENTER;
	var TOP;
}

@:native("DocumentApp")
extern class DocumentApp
{
	static function openByUrl(url:String):google.apps.script.document.Document;
	static function openById(id:String):google.apps.script.document.Document;
	static function getActiveDocument(name:String):google.apps.script.document.Document;
	static function getUi(name:String):google.apps.script.base.Ui;
	static function create(name:String):google.apps.script.document.Document;
}
