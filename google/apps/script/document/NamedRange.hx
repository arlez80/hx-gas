package google.apps.script.document;

@:native("NamedRange")
extern class NamedRange
{
	function getName(textAlignment:google.apps.script.document.TextAlignment):String;
	function getRange(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Range;
	function getId(textAlignment:google.apps.script.document.TextAlignment):String;
	function remove(textAlignment:google.apps.script.document.TextAlignment):Void;
}
