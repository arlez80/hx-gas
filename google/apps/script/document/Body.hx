package google.apps.script.document;

@:native("Body")
extern class Body
{
	function getMarginBottom(paragraphHeading:google.apps.script.document.ParagraphHeading):Float;
	function getHeadingAttributes(paragraphHeading:google.apps.script.document.ParagraphHeading):Dynamic;
	function getMarginRight(paragraphHeading:google.apps.script.document.ParagraphHeading):Float;
	function getNumChildren(paragraphHeading:google.apps.script.document.ParagraphHeading):Int;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function insertHorizontalRule(childIndex:Int):google.apps.script.document.HorizontalRule;
	@:overload(function (cells:Array<Array<String>>):google.apps.script.document.Table {})
	@:overload(function (table:google.apps.script.document.Table):google.apps.script.document.Table {})
	function appendTable(text:String):google.apps.script.document.Table;
	function getMarginLeft(paragraphHeading:google.apps.script.document.ParagraphHeading):Float;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function getMarginTop(paragraphHeading:google.apps.script.document.ParagraphHeading):Float;
	function getTables(paragraphHeading:google.apps.script.document.ParagraphHeading):Array<google.apps.script.document.Table>;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function removeChild(child:google.apps.script.document.Element):google.apps.script.document.Body;
	function getListItems(paragraphHeading:google.apps.script.document.ParagraphHeading):Array<google.apps.script.document.ListItem>;
	@:deprecated
	function isAtDocumentEnd(textAlignment:google.apps.script.document.TextAlignment):Bool;
	function setPageHeight(pageHeight:Float):google.apps.script.document.Body;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Body;
	function setMarginLeft(marginLeft:Float):google.apps.script.document.Body;
	@:deprecated
	function setLinkUrl(url:String):google.apps.script.document.Body;
	@:overload(function (text:String):google.apps.script.document.ListItem {})
	function appendListItem(listItem:google.apps.script.document.ListItem):google.apps.script.document.ListItem;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	@:deprecated
	function getFootnotes(textAlignment:google.apps.script.document.TextAlignment):Array<google.apps.script.document.Footnote>;
	function getText(paragraphHeading:google.apps.script.document.ParagraphHeading):String;
	function editAsText(table:google.apps.script.document.Table):google.apps.script.document.Text;
	function getTextAlignment(paragraphHeading:google.apps.script.document.ParagraphHeading):google.apps.script.document.TextAlignment;
	function setMarginBottom(marginBottom:Float):google.apps.script.document.Body;
	function getParent(paragraphHeading:google.apps.script.document.ParagraphHeading):google.apps.script.document.ContainerElement;
	function getChild(childIndex:Int):google.apps.script.document.Element;
	@:overload(function (childIndex:Int,pageBreak:google.apps.script.document.PageBreak):google.apps.script.document.PageBreak {})
	function insertPageBreak(childIndex:Int):google.apps.script.document.PageBreak;
	function setText(text:String):google.apps.script.document.Body;
	function getParagraphs(paragraphHeading:google.apps.script.document.ParagraphHeading):Array<google.apps.script.document.Paragraph>;
	function getPageHeight(paragraphHeading:google.apps.script.document.ParagraphHeading):Float;
	function getType(paragraphHeading:google.apps.script.document.ParagraphHeading):google.apps.script.document.ElementType;
	function getImages(paragraphHeading:google.apps.script.document.ParagraphHeading):Array<google.apps.script.document.InlineImage>;
	function clear(table:google.apps.script.document.Table):google.apps.script.document.Body;
	@:deprecated
	function getLinkUrl(textAlignment:google.apps.script.document.TextAlignment):String;
	@:overload(function (childIndex:Int,text:String):google.apps.script.document.Paragraph {})
	function insertParagraph(childIndex:Int,paragraph:google.apps.script.document.Paragraph):google.apps.script.document.Paragraph;
	function getPageWidth(paragraphHeading:google.apps.script.document.ParagraphHeading):Float;
	@:overload(function (image:google.apps.script.document.InlineImage):google.apps.script.document.InlineImage {})
	function appendImage(image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	@:overload(function (pageBreak:google.apps.script.document.PageBreak):google.apps.script.document.PageBreak {})
	function appendPageBreak(text:String):google.apps.script.document.PageBreak;
	function setMarginTop(marginTop:Float):google.apps.script.document.Body;
	function setMarginRight(marginRight:Float):google.apps.script.document.Body;
	function setAttributes(attributes:Dynamic):google.apps.script.document.Body;
	@:overload(function (childIndex:Int,text:String):google.apps.script.document.ListItem {})
	function insertListItem(childIndex:Int,listItem:google.apps.script.document.ListItem):google.apps.script.document.ListItem;
	@:deprecated
	function getNextSibling(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
	@:overload(function (childIndex:Int,cells:Array<Array<String>>):google.apps.script.document.Table {})
	@:overload(function (childIndex:Int,table:google.apps.script.document.Table):google.apps.script.document.Table {})
	function insertTable(childIndex:Int):google.apps.script.document.Table;
	@:overload(function (text:String):google.apps.script.document.Paragraph {})
	function appendParagraph(paragraph:google.apps.script.document.Paragraph):google.apps.script.document.Paragraph;
	@:overload(function (childIndex:Int,image:google.apps.script.document.InlineImage):google.apps.script.document.InlineImage {})
	function insertImage(childIndex:Int,image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	function appendHorizontalRule(url:String):google.apps.script.document.HorizontalRule;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function copy(table:google.apps.script.document.Table):google.apps.script.document.Body;
	function setHeadingAttributes(paragraphHeading:google.apps.script.document.ParagraphHeading,attributes:Dynamic):google.apps.script.document.Body;
	function setPageWidth(pageWidth:Float):google.apps.script.document.Body;
	@:deprecated
	function getPreviousSibling(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
}
