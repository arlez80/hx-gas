package google.apps.script.document;

@:native("RangeElement")
extern class RangeElement
{
	function getEndOffsetInclusive(range:google.apps.script.document.Range):Int;
	function getStartOffset(range:google.apps.script.document.Range):Int;
	function isPartial(range:google.apps.script.document.Range):Bool;
	function getElement(range:google.apps.script.document.Range):google.apps.script.document.Element;
}
