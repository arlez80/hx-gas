package google.apps.script.document;

@:require(hx_gas)
@:native("HorizontalAlignment")
@:enum abstract HorizontalAlignment(String) from String to String
{
	var LEFT;
	var CENTER;
	var RIGHT;
	var JUSTIFY;
}

