package google.apps.script.document;

@:native("PageBreak")
extern class PageBreak
{
	function setAttributes(attributes:Dynamic):google.apps.script.document.PageBreak;
	function getNextSibling(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
	function isAtDocumentEnd(textAlignment:google.apps.script.document.TextAlignment):Bool;
	function removeFromParent(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.PageBreak;
	function getParent(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.ContainerElement;
	function copy(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.PageBreak;
	function getAttributes(textAlignment:google.apps.script.document.TextAlignment):Dynamic;
	function getType(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.ElementType;
	function getPreviousSibling(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
}
