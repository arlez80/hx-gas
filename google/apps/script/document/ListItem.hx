package google.apps.script.document;

@:native("ListItem")
extern class ListItem
{
	function copy(text:google.apps.script.document.Text):google.apps.script.document.ListItem;
	function getPositionedImage(id:String):google.apps.script.document.PositionedImage;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	function getIndentEnd(child:google.apps.script.document.Element):Float;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function setNestingLevel(nestingLevel:Int):google.apps.script.document.ListItem;
	function setLineSpacing(multiplier:Float):google.apps.script.document.ListItem;
	function insertHorizontalRule(childIndex:Int):google.apps.script.document.HorizontalRule;
	function setIndentEnd(indentEnd:Float):google.apps.script.document.ListItem;
	function getLineSpacing(child:google.apps.script.document.Element):Float;
	function getIndentStart(child:google.apps.script.document.Element):Float;
	function setHeading(heading:google.apps.script.document.ParagraphHeading):google.apps.script.document.ListItem;
	function getNestingLevel(child:google.apps.script.document.Element):Int;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	@:overload(function (text:google.apps.script.document.Text):google.apps.script.document.Text {})
	function appendText(text:String):google.apps.script.document.Text;
	function editAsText(text:google.apps.script.document.Text):google.apps.script.document.Text;
	function isLeftToRight(childIndex:Int,text:google.apps.script.document.Text):Bool;
	function removeChild(child:google.apps.script.document.Element):google.apps.script.document.ListItem;
	function setIndentStart(indentStart:Float):google.apps.script.document.ListItem;
	function isAtDocumentEnd(childIndex:Int,text:google.apps.script.document.Text):Bool;
	function setListId(listItem:google.apps.script.document.ListItem):google.apps.script.document.ListItem;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.ListItem;
	function merge(childIndex:Int,text:google.apps.script.document.Text):google.apps.script.document.ListItem;
	function getHeading(child:google.apps.script.document.Element):google.apps.script.document.ParagraphHeading;
	function setSpacingAfter(spacingAfter:Float):google.apps.script.document.ListItem;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	function getPositionedImages(id:String):Array<google.apps.script.document.PositionedImage>;
	function getSpacingAfter(id:String):Float;
	function getText(id:String):String;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.ListItem;
	function getTextAlignment(id:String):google.apps.script.document.TextAlignment;
	function appendHorizontalRule(image:google.apps.script.base.BlobSource):google.apps.script.document.HorizontalRule;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function setAttributes(attributes:Dynamic):google.apps.script.document.ListItem;
	@:overload(function (childIndex:Int,pageBreak:google.apps.script.document.PageBreak):google.apps.script.document.PageBreak {})
	function insertPageBreak(childIndex:Int):google.apps.script.document.PageBreak;
	function setText(text:String):Void;
	function setLeftToRight(leftToRight:Bool):google.apps.script.document.ListItem;
	function getType(id:String):google.apps.script.document.ElementType;
	function getSpacingBefore(id:String):Float;
	function clear(text:google.apps.script.document.Text):google.apps.script.document.ListItem;
	function getLinkUrl(child:google.apps.script.document.Element):String;
	function getGlyphType(child:google.apps.script.document.Element):google.apps.script.document.GlyphType;
	function setLinkUrl(url:String):google.apps.script.document.ListItem;
	@:overload(function (pageBreak:google.apps.script.document.PageBreak):google.apps.script.document.PageBreak {})
	function appendPageBreak(image:google.apps.script.document.InlineImage):google.apps.script.document.PageBreak;
	function setSpacingBefore(spacingBefore:Float):google.apps.script.document.ListItem;
	function removePositionedImage(id:String):Bool;
	function getNextSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	function getAlignment(searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.HorizontalAlignment;
	function setAlignment(alignment:google.apps.script.document.HorizontalAlignment):google.apps.script.document.ListItem;
	@:overload(function (image:google.apps.script.document.InlineImage):google.apps.script.document.InlineImage {})
	function appendInlineImage(image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	function getListId(child:google.apps.script.document.Element):String;
	@:overload(function (childIndex:Int,image:google.apps.script.document.InlineImage):google.apps.script.document.InlineImage {})
	function insertInlineImage(childIndex:Int,image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	function setGlyphType(glyphType:google.apps.script.document.GlyphType):google.apps.script.document.ListItem;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function getIndentFirstLine(child:google.apps.script.document.Element):Float;
	function addPositionedImage(image:google.apps.script.base.BlobSource):google.apps.script.document.PositionedImage;
	@:overload(function (childIndex:Int,text:google.apps.script.document.Text):google.apps.script.document.Text {})
	function insertText(childIndex:Int,text:String):google.apps.script.document.Text;
	function setIndentFirstLine(indentFirstLine:Float):google.apps.script.document.ListItem;
	function getPreviousSibling(id:String):google.apps.script.document.Element;
}
