package google.apps.script.document;

@:native("EquationSymbol")
extern class EquationSymbol
{
	function getCode(attributes:Dynamic):String;
	function setAttributes(attributes:Dynamic):google.apps.script.document.EquationSymbol;
	function getNextSibling(attributes:Dynamic):google.apps.script.document.Element;
	function isAtDocumentEnd(attributes:Dynamic):Bool;
	function removeFromParent(attributes:Dynamic):google.apps.script.document.EquationSymbol;
	function getPreviousSibling(attributes:Dynamic):google.apps.script.document.Element;
	function copy(attributes:Dynamic):google.apps.script.document.EquationSymbol;
	function merge(attributes:Dynamic):google.apps.script.document.EquationSymbol;
	function getAttributes(attributes:Dynamic):Dynamic;
	function getType(attributes:Dynamic):google.apps.script.document.ElementType;
	function getParent(attributes:Dynamic):google.apps.script.document.ContainerElement;
}
