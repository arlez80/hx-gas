package google.apps.script.document;

@:require(hx_gas)
@:native("ParagraphHeading")
@:enum abstract ParagraphHeading(String) from String to String
{
	var NORMAL;
	var HEADING1;
	var HEADING2;
	var HEADING3;
	var HEADING4;
	var HEADING5;
	var HEADING6;
	var TITLE;
	var SUBTITLE;
}

