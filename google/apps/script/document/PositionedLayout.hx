package google.apps.script.document;

@:require(hx_gas)
@:native("PositionedLayout")
@:enum abstract PositionedLayout(String) from String to String
{
	var ABOVE_TEXT;
	var BREAK_BOTH;
	var BREAK_LEFT;
	var BREAK_RIGHT;
	var WRAP_TEXT;
}

