package google.apps.script.document;

@:require(hx_gas)
@:native("GlyphType")
@:enum abstract GlyphType(String) from String to String
{
	var BULLET;
	var HOLLOW_BULLET;
	var SQUARE_BULLET;
	var NUMBER;
	var LATIN_UPPER;
	var LATIN_LOWER;
	var ROMAN_UPPER;
	var ROMAN_LOWER;
}

