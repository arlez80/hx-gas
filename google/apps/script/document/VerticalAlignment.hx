package google.apps.script.document;

@:require(hx_gas)
@:native("VerticalAlignment")
@:enum abstract VerticalAlignment(String) from String to String
{
	var BOTTOM;
	var CENTER;
	var TOP;
}

