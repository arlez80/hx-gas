package google.apps.script.document;

@:require(hx_gas)
@:native("ElementType")
@:enum abstract ElementType(String) from String to String
{
	var BODY_SECTION;
	var COMMENT_SECTION;
	var DOCUMENT;
	var EQUATION;
	var EQUATION_FUNCTION;
	var EQUATION_FUNCTION_ARGUMENT_SEPARATOR;
	var EQUATION_SYMBOL;
	var FOOTER_SECTION;
	var FOOTNOTE;
	var FOOTNOTE_SECTION;
	var HEADER_SECTION;
	var HORIZONTAL_RULE;
	var INLINE_DRAWING;
	var INLINE_IMAGE;
	var LIST_ITEM;
	var PAGE_BREAK;
	var PARAGRAPH;
	var TABLE;
	var TABLE_CELL;
	var TABLE_OF_CONTENTS;
	var TABLE_ROW;
	var TEXT;
	var UNSUPPORTED;
}

