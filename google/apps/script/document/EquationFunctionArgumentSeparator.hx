package google.apps.script.document;

@:native("EquationFunctionArgumentSeparator")
extern class EquationFunctionArgumentSeparator
{
	function setAttributes(attributes:Dynamic):google.apps.script.document.EquationFunctionArgumentSeparator;
	function getNextSibling(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
	function isAtDocumentEnd(textAlignment:google.apps.script.document.TextAlignment):Bool;
	function removeFromParent(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.EquationFunctionArgumentSeparator;
	function getParent(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.ContainerElement;
	function copy(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.EquationFunctionArgumentSeparator;
	function merge(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.EquationFunctionArgumentSeparator;
	function getAttributes(textAlignment:google.apps.script.document.TextAlignment):Dynamic;
	function getType(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.ElementType;
	function getPreviousSibling(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
}
