package google.apps.script.document;

@:native("RangeBuilder")
extern class RangeBuilder
{
	@:deprecated
	function getSelectedElements(range:google.apps.script.document.Range):Array<google.apps.script.document.RangeElement>;
	@:overload(function (startTextElement:google.apps.script.document.Text,startOffset:Int,endTextElementInclusive:google.apps.script.document.Text,endOffsetInclusive:Int):google.apps.script.document.RangeBuilder {})
	function addElementsBetween(startElement:google.apps.script.document.Element,endElementInclusive:google.apps.script.document.Element):google.apps.script.document.RangeBuilder;
	function getRangeElements(range:google.apps.script.document.Range):Array<google.apps.script.document.RangeElement>;
	function build(range:google.apps.script.document.Range):google.apps.script.document.Range;
	function addRange(range:google.apps.script.document.Range):google.apps.script.document.RangeBuilder;
	@:overload(function (textElement:google.apps.script.document.Text,startOffset:Int,endOffsetInclusive:Int):google.apps.script.document.RangeBuilder {})
	function addElement(element:google.apps.script.document.Element):google.apps.script.document.RangeBuilder;
}
