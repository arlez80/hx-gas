package google.apps.script.document;

@:native("InlineDrawing")
extern class InlineDrawing
{
	function setAttributes(attributes:Dynamic):google.apps.script.document.InlineDrawing;
	function getAltTitle(attributes:Dynamic):String;
	function isAtDocumentEnd(attributes:Dynamic):Bool;
	function setAltTitle(title:String):google.apps.script.document.InlineDrawing;
	function getNextSibling(attributes:Dynamic):google.apps.script.document.Element;
	function getAltDescription(attributes:Dynamic):String;
	function getType(attributes:Dynamic):google.apps.script.document.ElementType;
	function setAltDescription(description:String):google.apps.script.document.InlineDrawing;
	function getPreviousSibling(attributes:Dynamic):google.apps.script.document.Element;
	function getParent(attributes:Dynamic):google.apps.script.document.ContainerElement;
	function copy(attributes:Dynamic):google.apps.script.document.InlineDrawing;
	function merge(attributes:Dynamic):google.apps.script.document.InlineDrawing;
	function getAttributes(attributes:Dynamic):Dynamic;
	function removeFromParent(attributes:Dynamic):google.apps.script.document.InlineDrawing;
}
