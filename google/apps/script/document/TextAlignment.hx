package google.apps.script.document;

@:require(hx_gas)
@:native("TextAlignment")
@:enum abstract TextAlignment(String) from String to String
{
	var NORMAL;
	var SUPERSCRIPT;
	var SUBSCRIPT;
}

