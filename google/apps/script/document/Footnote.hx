package google.apps.script.document;

@:native("Footnote")
extern class Footnote
{
	function getFootnoteContents(url:String):google.apps.script.document.FootnoteSection;
	function setAttributes(attributes:Dynamic):google.apps.script.document.Footnote;
	function getNextSibling(url:String):google.apps.script.document.Element;
	function isAtDocumentEnd(url:String):Bool;
	function removeFromParent(url:String):google.apps.script.document.Footnote;
	function getPreviousSibling(url:String):google.apps.script.document.Element;
	function copy(url:String):google.apps.script.document.Footnote;
	function getAttributes(url:String):Dynamic;
	function getType(url:String):google.apps.script.document.ElementType;
	function getParent(url:String):google.apps.script.document.ContainerElement;
}
