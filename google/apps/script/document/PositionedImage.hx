package google.apps.script.document;

@:native("PositionedImage")
extern class PositionedImage
{
	function getHeight(contentType:String):Int;
	function getParagraph(contentType:String):google.apps.script.document.Paragraph;
	function setLayout(layout:google.apps.script.document.PositionedLayout):google.apps.script.document.PositionedImage;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function setTopOffset(offset:Float):google.apps.script.document.PositionedImage;
	function setHeight(height:Int):google.apps.script.document.PositionedImage;
	function getTopOffset(contentType:String):Float;
	function setLeftOffset(offset:Float):google.apps.script.document.PositionedImage;
	function setWidth(width:Int):google.apps.script.document.PositionedImage;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function getLeftOffset(contentType:String):Float;
	function getId(contentType:String):String;
	function getWidth(contentType:String):Int;
	function getLayout(contentType:String):google.apps.script.document.PositionedLayout;
}
