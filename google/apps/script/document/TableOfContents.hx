package google.apps.script.document;

@:native("TableOfContents")
extern class TableOfContents
{
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function copy(width:Float):google.apps.script.document.TableOfContents;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	function getType(child:google.apps.script.document.Element):google.apps.script.document.ElementType;
	function clear(width:Float):google.apps.script.document.TableOfContents;
	function getLinkUrl(child:google.apps.script.document.Element):String;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.TableOfContents;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function setAttributes(attributes:Dynamic):google.apps.script.document.TableOfContents;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function getNextSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	function isAtDocumentEnd(child:google.apps.script.document.Element):Bool;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.TableOfContents;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function editAsText(width:Float):google.apps.script.document.Text;
	function getText(child:google.apps.script.document.Element):String;
	function setLinkUrl(url:String):google.apps.script.document.TableOfContents;
	function getTextAlignment(child:google.apps.script.document.Element):google.apps.script.document.TextAlignment;
	function getPreviousSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
}
