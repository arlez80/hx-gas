package google.apps.script.document;

@:native("HorizontalRule")
extern class HorizontalRule
{
	function setAttributes(attributes:Dynamic):google.apps.script.document.HorizontalRule;
	function getNextSibling(url:String):google.apps.script.document.Element;
	function isAtDocumentEnd(url:String):Bool;
	function removeFromParent(url:String):google.apps.script.document.HorizontalRule;
	function getParent(url:String):google.apps.script.document.ContainerElement;
	function copy(url:String):google.apps.script.document.HorizontalRule;
	function getAttributes(url:String):Dynamic;
	function getType(url:String):google.apps.script.document.ElementType;
	function getPreviousSibling(url:String):google.apps.script.document.Element;
}
