package google.apps.script.document;

@:native("UnsupportedElement")
extern class UnsupportedElement
{
	function setAttributes(attributes:Dynamic):google.apps.script.document.UnsupportedElement;
	function getNextSibling(startOffset:Int,endOffsetInclusive:Int,underline:Bool):google.apps.script.document.Element;
	function isAtDocumentEnd(startOffset:Int,endOffsetInclusive:Int,underline:Bool):Bool;
	function removeFromParent(startOffset:Int,endOffsetInclusive:Int,underline:Bool):google.apps.script.document.UnsupportedElement;
	function getParent(startOffset:Int,endOffsetInclusive:Int,underline:Bool):google.apps.script.document.ContainerElement;
	function copy(startOffset:Int,endOffsetInclusive:Int,underline:Bool):google.apps.script.document.UnsupportedElement;
	function merge(startOffset:Int,endOffsetInclusive:Int,underline:Bool):google.apps.script.document.UnsupportedElement;
	function getAttributes(startOffset:Int,endOffsetInclusive:Int,underline:Bool):Dynamic;
	function getType(startOffset:Int,endOffsetInclusive:Int,underline:Bool):google.apps.script.document.ElementType;
	function getPreviousSibling(startOffset:Int,endOffsetInclusive:Int,underline:Bool):google.apps.script.document.Element;
}
