package google.apps.script.document;

@:native("FootnoteSection")
extern class FootnoteSection
{
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function setAttributes(attributes:Dynamic):google.apps.script.document.FootnoteSection;
	function copy(text:String):google.apps.script.document.FootnoteSection;
	function setText(text:String):google.apps.script.document.FootnoteSection;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	function getParagraphs(child:google.apps.script.document.Element):Array<google.apps.script.document.Paragraph>;
	function getType(child:google.apps.script.document.Element):google.apps.script.document.ElementType;
	function clear(text:String):google.apps.script.document.FootnoteSection;
	@:deprecated
	function getFootnotes(textAlignment:google.apps.script.document.TextAlignment):Array<google.apps.script.document.Footnote>;
	@:overload(function (childIndex:Int,text:String):google.apps.script.document.Paragraph {})
	function insertParagraph(childIndex:Int,paragraph:google.apps.script.document.Paragraph):google.apps.script.document.Paragraph;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.FootnoteSection;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function removeChild(child:google.apps.script.document.Element):google.apps.script.document.FootnoteSection;
	function getNextSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	@:deprecated
	function isAtDocumentEnd(textAlignment:google.apps.script.document.TextAlignment):Bool;
	@:deprecated
	function getLinkUrl(textAlignment:google.apps.script.document.TextAlignment):String;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.FootnoteSection;
	@:overload(function (text:String):google.apps.script.document.Paragraph {})
	function appendParagraph(paragraph:google.apps.script.document.Paragraph):google.apps.script.document.Paragraph;
	@:deprecated
	function setLinkUrl(url:String):google.apps.script.document.FootnoteSection;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function editAsText(text:String):google.apps.script.document.Text;
	function getText(child:google.apps.script.document.Element):String;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	function getTextAlignment(child:google.apps.script.document.Element):google.apps.script.document.TextAlignment;
	function getPreviousSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
}
