package google.apps.script.document;

@:native("Position")
extern class Position
{
	function getElement(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
	function getSurroundingTextOffset(textAlignment:google.apps.script.document.TextAlignment):Int;
	function getOffset(textAlignment:google.apps.script.document.TextAlignment):Int;
	function insertText(text:String):google.apps.script.document.Text;
	function insertBookmark(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Bookmark;
	function insertInlineImage(image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	function getSurroundingText(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Text;
}
