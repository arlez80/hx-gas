package google.apps.script.document;

@:native("Document")
extern class Document
{
	function getBookmarks(id:String):Array<google.apps.script.document.Bookmark>;
	function newRange(element:google.apps.script.document.Element,offset:Int):google.apps.script.document.RangeBuilder;
	function addNamedRange(name:String,range:google.apps.script.document.Range):google.apps.script.document.NamedRange;
	function saveAndClose(user:google.apps.script.base.User):Void;
	function addHeader(emailAddresses:Array<String>):google.apps.script.document.HeaderSection;
	function addFooter(emailAddresses:Array<String>):google.apps.script.document.FooterSection;
	function getEditors(id:String):Array<google.apps.script.base.User>;
	function getBody(contentType:String):google.apps.script.document.Body;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function addEditors(emailAddresses:Array<String>):google.apps.script.document.Document;
	function getViewers(name:String):Array<google.apps.script.base.User>;
	function getFooter(id:String):google.apps.script.document.FooterSection;
	function getFootnotes(id:String):Array<google.apps.script.document.Footnote>;
	function getUrl(name:String):String;
	function getSelection(name:String):google.apps.script.document.Range;
	function getNamedRangeById(id:String):google.apps.script.document.NamedRange;
	function addViewers(emailAddresses:Array<String>):google.apps.script.document.Document;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.document.Document {})
	function addViewer(emailAddress:String):google.apps.script.document.Document;
	function addBookmark(position:google.apps.script.document.Position):google.apps.script.document.Bookmark;
	function getId(id:String):String;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.document.Document {})
	function removeEditor(emailAddress:String):google.apps.script.document.Document;
	function getBookmark(id:String):google.apps.script.document.Bookmark;
	@:overload(function (name:String):Array<google.apps.script.document.NamedRange> {})
	function getNamedRanges(id:String):Array<google.apps.script.document.NamedRange>;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function getCursor(id:String):google.apps.script.document.Position;
	function setCursor(position:google.apps.script.document.Position):google.apps.script.document.Document;
	function getName(id:String):String;
	function setName(name:String):google.apps.script.document.Document;
	function newPosition(element:google.apps.script.document.Element,offset:Int):google.apps.script.document.Position;
	function getHeader(id:String):google.apps.script.document.HeaderSection;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.document.Document {})
	function addEditor(emailAddress:String):google.apps.script.document.Document;
	function setSelection(range:google.apps.script.document.Range):google.apps.script.document.Document;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.document.Document {})
	function removeViewer(emailAddress:String):google.apps.script.document.Document;
}
