package google.apps.script.document;

@:native("TableRow")
extern class TableRow
{
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function getParentTable(child:google.apps.script.document.Element):google.apps.script.document.Table;
	function setAttributes(attributes:Dynamic):google.apps.script.document.TableRow;
	function copy(tableCell:google.apps.script.document.TableCell):google.apps.script.document.TableRow;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	function getType(child:google.apps.script.document.Element):google.apps.script.document.ElementType;
	function clear(tableCell:google.apps.script.document.TableCell):google.apps.script.document.TableRow;
	function getLinkUrl(child:google.apps.script.document.Element):String;
	function getMinimumHeight(child:google.apps.script.document.Element):Int;
	function getNextSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.TableRow;
	@:overload(function (childIndex:Int,textContents:String):google.apps.script.document.TableCell {})
	@:overload(function (childIndex:Int,tableCell:google.apps.script.document.TableCell):google.apps.script.document.TableCell {})
	function insertTableCell(childIndex:Int):google.apps.script.document.TableCell;
	function getNumCells(child:google.apps.script.document.Element):Int;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	function editAsText(tableCell:google.apps.script.document.TableCell):google.apps.script.document.Text;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function removeChild(child:google.apps.script.document.Element):google.apps.script.document.TableRow;
	function setMinimumHeight(minHeight:Int):google.apps.script.document.TableRow;
	function getCell(cellIndex:Int):google.apps.script.document.TableCell;
	function isAtDocumentEnd(childIndex:Int,tableCell:google.apps.script.document.TableCell):Bool;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.TableRow;
	function merge(childIndex:Int,tableCell:google.apps.script.document.TableCell):google.apps.script.document.TableRow;
	function setLinkUrl(url:String):google.apps.script.document.TableRow;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function getText(child:google.apps.script.document.Element):String;
	@:overload(function (textContents:String):google.apps.script.document.TableCell {})
	@:overload(function (tableCell:google.apps.script.document.TableCell):google.apps.script.document.TableCell {})
	function appendTableCell(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.TableCell;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
	function getTextAlignment(child:google.apps.script.document.Element):google.apps.script.document.TextAlignment;
	function removeCell(cellIndex:Int):google.apps.script.document.TableCell;
	function getPreviousSibling(child:google.apps.script.document.Element):google.apps.script.document.Element;
}
