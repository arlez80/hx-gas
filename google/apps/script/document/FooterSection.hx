package google.apps.script.document;

@:native("FooterSection")
extern class FooterSection
{
	function getChild(childIndex:Int):google.apps.script.document.Element;
	function setAttributes(attributes:Dynamic):google.apps.script.document.FooterSection;
	@:overload(function (childIndex:Int,text:String):google.apps.script.document.ListItem {})
	function insertListItem(childIndex:Int,listItem:google.apps.script.document.ListItem):google.apps.script.document.ListItem;
	function setText(text:String):google.apps.script.document.FooterSection;
	function getNumChildren(child:google.apps.script.document.Element):Int;
	function getParagraphs(child:google.apps.script.document.Element):Array<google.apps.script.document.Paragraph>;
	@:overload(function (cells:Array<Array<String>>):google.apps.script.document.Table {})
	@:overload(function (table:google.apps.script.document.Table):google.apps.script.document.Table {})
	function appendTable(text:String):google.apps.script.document.Table;
	@:deprecated
	function getNextSibling(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
	function getType(child:google.apps.script.document.Element):google.apps.script.document.ElementType;
	function getImages(child:google.apps.script.document.Element):Array<google.apps.script.document.InlineImage>;
	function clear(table:google.apps.script.document.Table):google.apps.script.document.FooterSection;
	@:deprecated
	function getFootnotes(textAlignment:google.apps.script.document.TextAlignment):Array<google.apps.script.document.Footnote>;
	@:deprecated
	function getLinkUrl(textAlignment:google.apps.script.document.TextAlignment):String;
	function insertHorizontalRule(childIndex:Int):google.apps.script.document.HorizontalRule;
	@:overload(function (image:google.apps.script.document.InlineImage):google.apps.script.document.InlineImage {})
	function appendImage(image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	@:overload(function (elementType:google.apps.script.document.ElementType,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findElement(elementType:google.apps.script.document.ElementType):google.apps.script.document.RangeElement;
	function removeFromParent(child:google.apps.script.document.Element):google.apps.script.document.FooterSection;
	@:overload(function (text:String):google.apps.script.document.ListItem {})
	function appendListItem(listItem:google.apps.script.document.ListItem):google.apps.script.document.ListItem;
	function getTables(child:google.apps.script.document.Element):Array<google.apps.script.document.Table>;
	function getChildIndex(child:google.apps.script.document.Element):Int;
	function removeChild(child:google.apps.script.document.Element):google.apps.script.document.FooterSection;
	function getListItems(child:google.apps.script.document.Element):Array<google.apps.script.document.ListItem>;
	@:deprecated
	function isAtDocumentEnd(textAlignment:google.apps.script.document.TextAlignment):Bool;
	@:overload(function (childIndex:Int,cells:Array<Array<String>>):google.apps.script.document.Table {})
	@:overload(function (childIndex:Int,table:google.apps.script.document.Table):google.apps.script.document.Table {})
	function insertTable(childIndex:Int):google.apps.script.document.Table;
	function replaceText(searchPattern:String,replacement:String):google.apps.script.document.Element;
	function setTextAlignment(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.FooterSection;
	@:overload(function (text:String):google.apps.script.document.Paragraph {})
	function appendParagraph(paragraph:google.apps.script.document.Paragraph):google.apps.script.document.Paragraph;
	@:deprecated
	function setLinkUrl(url:String):google.apps.script.document.FooterSection;
	function getAttributes(searchPattern:String,from:google.apps.script.document.RangeElement):Dynamic;
	@:overload(function (childIndex:Int,text:String):google.apps.script.document.Paragraph {})
	function insertParagraph(childIndex:Int,paragraph:google.apps.script.document.Paragraph):google.apps.script.document.Paragraph;
	@:overload(function (childIndex:Int,image:google.apps.script.document.InlineImage):google.apps.script.document.InlineImage {})
	function insertImage(childIndex:Int,image:google.apps.script.base.BlobSource):google.apps.script.document.InlineImage;
	@:overload(function (searchPattern:String,from:google.apps.script.document.RangeElement):google.apps.script.document.RangeElement {})
	function findText(searchPattern:String):google.apps.script.document.RangeElement;
	function editAsText(table:google.apps.script.document.Table):google.apps.script.document.Text;
	function getText(child:google.apps.script.document.Element):String;
	@:deprecated
	function getPreviousSibling(textAlignment:google.apps.script.document.TextAlignment):google.apps.script.document.Element;
	function copy(table:google.apps.script.document.Table):google.apps.script.document.FooterSection;
	function getTextAlignment(child:google.apps.script.document.Element):google.apps.script.document.TextAlignment;
	function appendHorizontalRule(attributes:Dynamic):google.apps.script.document.HorizontalRule;
	function getParent(child:google.apps.script.document.Element):google.apps.script.document.ContainerElement;
}
