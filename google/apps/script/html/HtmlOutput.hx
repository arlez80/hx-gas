package google.apps.script.html;

@:native("HtmlOutput")
extern class HtmlOutput
{
	function setWidth(width:Int):google.apps.script.html.HtmlOutput;
	function getHeight(contentType:String):Int;
	function setHeight(height:Int):google.apps.script.html.HtmlOutput;
	function getFaviconUrl(contentType:String):String;
	function setContent(content:String):google.apps.script.html.HtmlOutput;
	function getContent(contentType:String):String;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function setTitle(title:String):google.apps.script.html.HtmlOutput;
	function asTemplate(addedContent:String):google.apps.script.html.HtmlTemplate;
	function append(addedContent:String):google.apps.script.html.HtmlOutput;
	function addMetaTag(name:String,content:String):google.apps.script.html.HtmlOutput;
	function setXFrameOptionsMode(mode:google.apps.script.html.XFrameOptionsMode):google.apps.script.html.HtmlOutput;
	function getTitle(contentType:String):String;
	function getWidth(contentType:String):Int;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function getMetaTags(contentType:String):Array<google.apps.script.html.HtmlOutputMetaTag>;
	function clear(addedContent:String):google.apps.script.html.HtmlOutput;
	function setFaviconUrl(iconUrl:String):google.apps.script.html.HtmlOutput;
	function appendUntrusted(addedContent:String):google.apps.script.html.HtmlOutput;
	function setSandboxMode(mode:google.apps.script.html.SandboxMode):google.apps.script.html.HtmlOutput;
}
