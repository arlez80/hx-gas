package google.apps.script.html;

@:native("HtmlOutputMetaTag")
extern class HtmlOutputMetaTag
{
	function getName(mode:google.apps.script.html.XFrameOptionsMode):String;
	function getContent(mode:google.apps.script.html.XFrameOptionsMode):String;
}
