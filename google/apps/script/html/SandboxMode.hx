package google.apps.script.html;

@:require(hx_gas)
@:native("SandboxMode")
@:enum abstract SandboxMode(String) from String to String
{
	var EMULATED;
	var IFRAME;
	var NATIVE;
}

