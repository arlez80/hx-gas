package google.apps.script.html;

@:require(hx_gas)
@:native("HtmlService.SandboxMode")
@:enum abstract SandboxMode(String) from String to String
{
	var EMULATED;
	var IFRAME;
	var NATIVE;
}

@:require(hx_gas)
@:native("HtmlService.XFrameOptionsMode")
@:enum abstract XFrameOptionsMode(String) from String to String
{
	var ALLOWALL;
	var DEFAULT;
}

@:native("HtmlService")
extern class HtmlService
{
	static function getUserAgent(filename:String):String;
	static function createHtmlOutputFromFile(filename:String):google.apps.script.html.HtmlOutput;
	@:overload(function (html:String):google.apps.script.html.HtmlTemplate {})
	static function createTemplate(blob:google.apps.script.base.BlobSource):google.apps.script.html.HtmlTemplate;
	@:overload(function (blob:google.apps.script.base.BlobSource):google.apps.script.html.HtmlOutput {})
	@:overload(function (html:String):google.apps.script.html.HtmlOutput {})
	static function createHtmlOutput(function:Dynamic):google.apps.script.html.HtmlOutput;
	static function createTemplateFromFile(filename:String):google.apps.script.html.HtmlTemplate;
}
