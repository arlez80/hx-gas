package google.apps.script.html;

@:native("HtmlTemplate")
extern class HtmlTemplate
{
	function getCode(mode:google.apps.script.html.XFrameOptionsMode):String;
	function getCodeWithComments(mode:google.apps.script.html.XFrameOptionsMode):String;
	function evaluate(mode:google.apps.script.html.XFrameOptionsMode):google.apps.script.html.HtmlOutput;
	function getRawContent(mode:google.apps.script.html.XFrameOptionsMode):String;
}
