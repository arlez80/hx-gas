package google.apps.script.html;

@:require(hx_gas)
@:native("XFrameOptionsMode")
@:enum abstract XFrameOptionsMode(String) from String to String
{
	var ALLOWALL;
	var DEFAULT;
}

