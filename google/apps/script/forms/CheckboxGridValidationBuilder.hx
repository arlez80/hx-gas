package google.apps.script.forms;

@:native("CheckboxGridValidationBuilder")
extern class CheckboxGridValidationBuilder
{
	function requireLimitOneResponsePerColumn(validation:google.apps.script.forms.CheckboxGridValidation):google.apps.script.forms.CheckboxGridValidationBuilder;
}
