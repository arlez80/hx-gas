package google.apps.script.forms;

@:native("ScaleItem")
extern class ScaleItem
{
	function getLowerBound(response:Int):Int;
	function getHelpText(response:Int):String;
	function setPoints(points:Int):google.apps.script.forms.ScaleItem;
	function getLeftLabel(response:Int):String;
	function setRequired(enabled:Bool):google.apps.script.forms.ScaleItem;
	function getIndex(response:Int):Int;
	function setTitle(title:String):google.apps.script.forms.ScaleItem;
	function createResponse(response:Int):google.apps.script.forms.ItemResponse;
	function setBounds(lower:Int,upper:Int):google.apps.script.forms.ScaleItem;
	function isRequired(response:Int):Bool;
	function getType(response:Int):google.apps.script.forms.ItemType;
	function getRightLabel(response:Int):String;
	function setHelpText(text:String):google.apps.script.forms.ScaleItem;
	function getTitle(response:Int):String;
	function setLabels(lower:String,upper:String):google.apps.script.forms.ScaleItem;
	function getPoints(response:Int):Int;
	function getGeneralFeedback(response:Int):google.apps.script.forms.QuizFeedback;
	function setGeneralFeedback(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.ScaleItem;
	function duplicate(response:Int):google.apps.script.forms.ScaleItem;
	function getId(response:Int):Int;
	function getUpperBound(response:Int):Int;
}
