package google.apps.script.forms;

@:native("SectionHeaderItem")
extern class SectionHeaderItem
{
	function getHelpText(title:String):String;
	function getTitle(title:String):String;
	function setTitle(title:String):google.apps.script.forms.SectionHeaderItem;
	function getIndex(title:String):Int;
	function duplicate(title:String):google.apps.script.forms.SectionHeaderItem;
	function getId(title:String):Int;
	function getType(title:String):google.apps.script.forms.ItemType;
	function setHelpText(text:String):google.apps.script.forms.SectionHeaderItem;
}
