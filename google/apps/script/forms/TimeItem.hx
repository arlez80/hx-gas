package google.apps.script.forms;

@:native("TimeItem")
extern class TimeItem
{
	function getPoints(hour:Int,minute:Int):Int;
	function getHelpText(hour:Int,minute:Int):String;
	function setPoints(points:Int):google.apps.script.forms.TimeItem;
	function getTitle(hour:Int,minute:Int):String;
	function setRequired(enabled:Bool):google.apps.script.forms.TimeItem;
	function getIndex(hour:Int,minute:Int):Int;
	function duplicate(hour:Int,minute:Int):google.apps.script.forms.TimeItem;
	function setGeneralFeedback(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.TimeItem;
	function isRequired(hour:Int,minute:Int):Bool;
	function getType(hour:Int,minute:Int):google.apps.script.forms.ItemType;
	function getGeneralFeedback(hour:Int,minute:Int):google.apps.script.forms.QuizFeedback;
	function createResponse(hour:Int,minute:Int):google.apps.script.forms.ItemResponse;
	function setHelpText(text:String):google.apps.script.forms.TimeItem;
	function getId(hour:Int,minute:Int):Int;
	function setTitle(title:String):google.apps.script.forms.TimeItem;
}
