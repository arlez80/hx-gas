package google.apps.script.forms;

@:native("QuizFeedbackBuilder")
extern class QuizFeedbackBuilder
{
	function copy(url:String,displayText:String):google.apps.script.forms.QuizFeedbackBuilder;
	function setText(text:String):google.apps.script.forms.QuizFeedbackBuilder;
	function build(url:String,displayText:String):google.apps.script.forms.QuizFeedback;
	@:overload(function (url:String,displayText:String):google.apps.script.forms.QuizFeedbackBuilder {})
	function addLink(url:String):google.apps.script.forms.QuizFeedbackBuilder;
}
