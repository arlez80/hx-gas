package google.apps.script.forms;

@:native("Form")
extern class Form
{
	function addPageBreakItem(emailAddresses:Array<String>):google.apps.script.forms.PageBreakItem;
	function collectsEmail(emailAddresses:Array<String>):Bool;
	function removeDestination(item:google.apps.script.forms.Item,toIndex:Int):google.apps.script.forms.Form;
	function getSummaryUrl(timestamp:Date):String;
	function setTitle(title:String):google.apps.script.forms.Form;
	@:overload(function (item:google.apps.script.forms.Item):js.Error {})
	function deleteItem(index:Int):js.Error;
	function setAllowResponseEdits(enabled:Bool):google.apps.script.forms.Form;
	function setShowLinkToRespondAgain(enabled:Bool):google.apps.script.forms.Form;
	function hasLimitOneResponsePerUser(timestamp:Date):Bool;
	function getItemById(id:Int):google.apps.script.forms.Item;
	function addTimeItem(emailAddresses:Array<String>):google.apps.script.forms.TimeItem;
	function addScaleItem(emailAddresses:Array<String>):google.apps.script.forms.ScaleItem;
	function setLimitOneResponsePerUser(enabled:Bool):google.apps.script.forms.Form;
	function addDateItem(title:String):google.apps.script.forms.DateItem;
	function setCustomClosedFormMessage(message:String):google.apps.script.forms.Form;
	function setIsQuiz(enabled:Bool):google.apps.script.forms.Form;
	function addMultipleChoiceItem(emailAddresses:Array<String>):google.apps.script.forms.MultipleChoiceItem;
	function isPublishingSummary(timestamp:Date):Bool;
	function addImageItem(emailAddresses:Array<String>):google.apps.script.forms.ImageItem;
	function setCollectEmail(collect:Bool):google.apps.script.forms.Form;
	function canEditResponse(emailAddresses:Array<String>):Bool;
	function submitGrades(responses:Array<google.apps.script.forms.FormResponse>):google.apps.script.forms.Form;
	function addTextItem(emailAddresses:Array<String>):google.apps.script.forms.TextItem;
	function addGridItem(emailAddresses:Array<String>):google.apps.script.forms.GridItem;
	function isAcceptingResponses(timestamp:Date):Bool;
	function hasRespondAgainLink(timestamp:Date):Bool;
	function setProgressBar(enabled:Bool):google.apps.script.forms.Form;
	function getPublishedUrl(itemType:google.apps.script.forms.ItemType):String;
	function getConfirmationMessage(responseId:String):String;
	function getEditors(responseId:String):Array<google.apps.script.base.User>;
	function getTitle(timestamp:Date):String;
	function getResponse(responseId:String):google.apps.script.forms.FormResponse;
	function deleteAllResponses(emailAddresses:Array<String>):google.apps.script.forms.Form;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.forms.Form {})
	function addEditor(emailAddress:String):google.apps.script.forms.Form;
	function addListItem(emailAddresses:Array<String>):google.apps.script.forms.ListItem;
	function addDurationItem(title:String):google.apps.script.forms.DurationItem;
	function getDestinationId(responseId:String):String;
	function setDestination(type:google.apps.script.forms.DestinationType,id:String):google.apps.script.forms.Form;
	function hasProgressBar(timestamp:Date):Bool;
	function setRequireLogin(requireLogin:Bool):google.apps.script.forms.Form;
	function getCustomClosedFormMessage(responseId:String):String;
	function addVideoItem(emailAddresses:Array<String>):google.apps.script.forms.VideoItem;
	function setDescription(description:String):google.apps.script.forms.Form;
	function requiresLogin(user:google.apps.script.base.User):Bool;
	function addEditors(emailAddresses:Array<String>):google.apps.script.forms.Form;
	function addDateTimeItem(title:String):google.apps.script.forms.DateTimeItem;
	@:overload(function (timestamp:Date):Array<google.apps.script.forms.FormResponse> {})
	function getResponses(responseId:String):Array<google.apps.script.forms.FormResponse>;
	function isQuiz(timestamp:Date):Bool;
	function getDescription(responseId:String):String;
	function createResponse(emailAddresses:Array<String>):google.apps.script.forms.FormResponse;
	function deleteResponse(responseId:String):google.apps.script.forms.Form;
	function getEditUrl(responseId:String):String;
	function getId(responseId:String):String;
	@:overload(function (item:google.apps.script.forms.Item,toIndex:Int):google.apps.script.forms.Item {})
	function moveItem(from:Int,to:Int):google.apps.script.forms.Item;
	function setShuffleQuestions(shuffle:Bool):google.apps.script.forms.Form;
	function addParagraphTextItem(emailAddresses:Array<String>):google.apps.script.forms.ParagraphTextItem;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.forms.Form {})
	function removeEditor(emailAddress:String):google.apps.script.forms.Form;
	@:overload(function (itemType:google.apps.script.forms.ItemType):Array<google.apps.script.forms.Item> {})
	function getItems(id:Int):Array<google.apps.script.forms.Item>;
	function addCheckboxGridItem(title:String):google.apps.script.forms.CheckboxGridItem;
	function setPublishingSummary(enabled:Bool):google.apps.script.forms.Form;
	function addCheckboxItem(title:String):google.apps.script.forms.CheckboxItem;
	function setAcceptingResponses(enabled:Bool):google.apps.script.forms.Form;
	function getShuffleQuestions(timestamp:Date):Bool;
	function getDestinationType(responseId:String):google.apps.script.forms.DestinationType;
	function setConfirmationMessage(message:String):google.apps.script.forms.Form;
	function addSectionHeaderItem(emailAddresses:Array<String>):google.apps.script.forms.SectionHeaderItem;
	function shortenFormUrl(url:String):String;
}
