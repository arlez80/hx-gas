package google.apps.script.forms;

@:require(hx_gas)
@:native("ItemType")
@:enum abstract ItemType(String) from String to String
{
	var CHECKBOX;
	var CHECKBOX_GRID;
	var DATE;
	var DATETIME;
	var DURATION;
	var GRID;
	var IMAGE;
	var LIST;
	var MULTIPLE_CHOICE;
	var PAGE_BREAK;
	var PARAGRAPH_TEXT;
	var SCALE;
	var SECTION_HEADER;
	var TEXT;
	var TIME;
	var VIDEO;
}

