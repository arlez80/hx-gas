package google.apps.script.forms;

@:native("VideoItem")
extern class VideoItem
{
	function getHelpText(title:String):String;
	function getAlignment(title:String):google.apps.script.forms.Alignment;
	function setTitle(title:String):google.apps.script.forms.VideoItem;
	function getIndex(title:String):Int;
	function duplicate(title:String):google.apps.script.forms.VideoItem;
	function getType(title:String):google.apps.script.forms.ItemType;
	function setHelpText(text:String):google.apps.script.forms.VideoItem;
	function getTitle(title:String):String;
	function setVideoUrl(youtubeUrl:String):google.apps.script.forms.VideoItem;
	function setWidth(width:Int):google.apps.script.forms.VideoItem;
	function getId(title:String):Int;
	function getWidth(title:String):Int;
	function setAlignment(alignment:google.apps.script.forms.Alignment):google.apps.script.forms.VideoItem;
}
