package google.apps.script.forms;

@:native("ParagraphTextValidationBuilder")
extern class ParagraphTextValidationBuilder
{
	function requireTextDoesNotContainPattern(pattern:String):google.apps.script.forms.ParagraphTextValidationBuilder;
	function requireTextLengthLessThanOrEqualTo(number:Int):google.apps.script.forms.ParagraphTextValidationBuilder;
	function requireTextContainsPattern(pattern:String):google.apps.script.forms.ParagraphTextValidationBuilder;
	function requireTextLengthGreaterThanOrEqualTo(number:Int):google.apps.script.forms.ParagraphTextValidationBuilder;
	function requireTextDoesNotMatchPattern(pattern:String):google.apps.script.forms.ParagraphTextValidationBuilder;
	function requireTextMatchesPattern(pattern:String):google.apps.script.forms.ParagraphTextValidationBuilder;
}
