package google.apps.script.forms;

@:require(hx_gas)
@:native("PageNavigationType")
@:enum abstract PageNavigationType(String) from String to String
{
	var CONTINUE;
	var GO_TO_PAGE;
	var RESTART;
	var SUBMIT;
}

