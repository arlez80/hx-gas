package google.apps.script.forms;

@:require(hx_gas)
@:native("FeedbackType")
@:enum abstract FeedbackType(String) from String to String
{
	var CORRECT;
	var INCORRECT;
	var GENERAL;
}

