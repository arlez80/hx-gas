package google.apps.script.forms;

@:native("GridItem")
extern class GridItem
{
	function setValidation(validation:google.apps.script.forms.GridValidation):google.apps.script.forms.GridItem;
	function getHelpText(responses:Array<String>):String;
	function getTitle(responses:Array<String>):String;
	function setTitle(title:String):google.apps.script.forms.GridItem;
	function setRequired(enabled:Bool):google.apps.script.forms.GridItem;
	function getIndex(responses:Array<String>):Int;
	function setColumns(columns:Array<String>):google.apps.script.forms.GridItem;
	function duplicate(responses:Array<String>):google.apps.script.forms.GridItem;
	function clearValidation(response:google.apps.script.forms.ItemResponse):google.apps.script.forms.GridItem;
	function isRequired(responses:Array<String>):Bool;
	function getType(responses:Array<String>):google.apps.script.forms.ItemType;
	function setHelpText(text:String):google.apps.script.forms.GridItem;
	function createResponse(responses:Array<String>):google.apps.script.forms.ItemResponse;
	function getColumns(responses:Array<String>):Array<String>;
	function getRows(responses:Array<String>):Array<String>;
	function getId(responses:Array<String>):Int;
	function setRows(rows:Array<String>):google.apps.script.forms.GridItem;
}
