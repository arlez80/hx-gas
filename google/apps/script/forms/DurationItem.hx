package google.apps.script.forms;

@:native("DurationItem")
extern class DurationItem
{
	function getPoints(hours:Int,minutes:Int,seconds:Int):Int;
	function getHelpText(hours:Int,minutes:Int,seconds:Int):String;
	function setPoints(points:Int):google.apps.script.forms.DurationItem;
	function getTitle(hours:Int,minutes:Int,seconds:Int):String;
	function setRequired(enabled:Bool):google.apps.script.forms.DurationItem;
	function getIndex(hours:Int,minutes:Int,seconds:Int):Int;
	function duplicate(hours:Int,minutes:Int,seconds:Int):google.apps.script.forms.DurationItem;
	function setGeneralFeedback(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.DurationItem;
	function isRequired(hours:Int,minutes:Int,seconds:Int):Bool;
	function getType(hours:Int,minutes:Int,seconds:Int):google.apps.script.forms.ItemType;
	function getGeneralFeedback(hours:Int,minutes:Int,seconds:Int):google.apps.script.forms.QuizFeedback;
	function createResponse(hours:Int,minutes:Int,seconds:Int):google.apps.script.forms.ItemResponse;
	function setHelpText(text:String):google.apps.script.forms.DurationItem;
	function getId(hours:Int,minutes:Int,seconds:Int):Int;
	function setTitle(title:String):google.apps.script.forms.DurationItem;
}
