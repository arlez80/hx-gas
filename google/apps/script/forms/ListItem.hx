package google.apps.script.forms;

@:native("ListItem")
extern class ListItem
{
	function getPoints(response:String):Int;
	function getHelpText(response:String):String;
	function getChoices(response:String):Array<google.apps.script.forms.Choice>;
	function getTitle(response:String):String;
	function getFeedbackForIncorrect(response:String):google.apps.script.forms.QuizFeedback;
	function setFeedbackForIncorrect(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.ListItem;
	function getIndex(response:String):Int;
	function setRequired(enabled:Bool):google.apps.script.forms.ListItem;
	function duplicate(response:String):google.apps.script.forms.ListItem;
	function setTitle(title:String):google.apps.script.forms.ListItem;
	function isRequired(response:String):Bool;
	function setPoints(points:Int):google.apps.script.forms.ListItem;
	function getType(response:String):google.apps.script.forms.ItemType;
	function getFeedbackForCorrect(response:String):google.apps.script.forms.QuizFeedback;
	function createResponse(response:String):google.apps.script.forms.ItemResponse;
	@:overload(function (value:String,isCorrect:Bool):google.apps.script.forms.Choice {})
	@:overload(function (value:String,navigationItem:google.apps.script.forms.PageBreakItem):google.apps.script.forms.Choice {})
	@:overload(function (value:String,navigationType:google.apps.script.forms.PageNavigationType):google.apps.script.forms.Choice {})
	function createChoice(value:String):google.apps.script.forms.Choice;
	function setChoiceValues(values:Array<String>):google.apps.script.forms.ListItem;
	function setHelpText(text:String):google.apps.script.forms.ListItem;
	function setChoices(choices:Array<google.apps.script.forms.Choice>):google.apps.script.forms.ListItem;
	function getId(response:String):Int;
	function setFeedbackForCorrect(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.ListItem;
}
