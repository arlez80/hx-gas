package google.apps.script.forms;

@:native("Choice")
extern class Choice
{
	function isCorrectAnswer(number:Int):Bool;
	function getGotoPage(number:Int):google.apps.script.forms.PageBreakItem;
	function getValue(number:Int):String;
	function getPageNavigationType(number:Int):google.apps.script.forms.PageNavigationType;
}
