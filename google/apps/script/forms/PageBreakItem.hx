package google.apps.script.forms;

@:native("PageBreakItem")
extern class PageBreakItem
{
	function getHelpText(enabled:Bool):String;
	function getTitle(enabled:Bool):String;
	function setTitle(title:String):google.apps.script.forms.PageBreakItem;
	function getType(enabled:Bool):google.apps.script.forms.ItemType;
	function getGoToPage(enabled:Bool):google.apps.script.forms.PageBreakItem;
	function getIndex(enabled:Bool):Int;
	function duplicate(enabled:Bool):google.apps.script.forms.PageBreakItem;
	@:overload(function (navigationType:google.apps.script.forms.PageNavigationType):google.apps.script.forms.PageBreakItem {})
	function setGoToPage(goToPageItem:google.apps.script.forms.PageBreakItem):google.apps.script.forms.PageBreakItem;
	function getId(enabled:Bool):Int;
	function getPageNavigationType(enabled:Bool):google.apps.script.forms.PageNavigationType;
	function setHelpText(text:String):google.apps.script.forms.PageBreakItem;
}
