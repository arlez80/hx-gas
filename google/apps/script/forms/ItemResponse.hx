package google.apps.script.forms;

@:native("ItemResponse")
extern class ItemResponse
{
	function setFeedback(feedback:Dynamic):google.apps.script.forms.ItemResponse;
	function getScore(width:Int):Dynamic;
	function getResponse(width:Int):Dynamic;
	function getItem(width:Int):google.apps.script.forms.Item;
	function setScore(score:Dynamic):google.apps.script.forms.ItemResponse;
	function getFeedback(width:Int):Dynamic;
}
