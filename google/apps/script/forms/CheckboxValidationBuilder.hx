package google.apps.script.forms;

@:native("CheckboxValidationBuilder")
extern class CheckboxValidationBuilder
{
	function requireSelectAtLeast(number:Int):google.apps.script.forms.CheckboxValidationBuilder;
	function requireSelectAtMost(number:Int):google.apps.script.forms.CheckboxValidationBuilder;
	function requireSelectExactly(number:Int):google.apps.script.forms.CheckboxValidationBuilder;
}
