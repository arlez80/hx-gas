package google.apps.script.forms;

@:native("GridValidationBuilder")
extern class GridValidationBuilder
{
	function requireLimitOneResponsePerColumn(validation:google.apps.script.forms.GridValidation):google.apps.script.forms.GridValidationBuilder;
}
