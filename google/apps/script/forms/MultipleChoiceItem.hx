package google.apps.script.forms;

@:native("MultipleChoiceItem")
extern class MultipleChoiceItem
{
	function getPoints(response:String):Int;
	function setPoints(points:Int):google.apps.script.forms.MultipleChoiceItem;
	function setFeedbackForIncorrect(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.MultipleChoiceItem;
	function getIndex(response:String):Int;
	function getType(response:String):google.apps.script.forms.ItemType;
	function getFeedbackForCorrect(response:String):google.apps.script.forms.QuizFeedback;
	function createResponse(response:String):google.apps.script.forms.ItemResponse;
	function setChoiceValues(values:Array<String>):google.apps.script.forms.MultipleChoiceItem;
	function setHelpText(text:String):google.apps.script.forms.MultipleChoiceItem;
	function getId(response:String):Int;
	function setTitle(title:String):google.apps.script.forms.MultipleChoiceItem;
	function getHelpText(response:String):String;
	function getChoices(response:String):Array<google.apps.script.forms.Choice>;
	function getFeedbackForIncorrect(response:String):google.apps.script.forms.QuizFeedback;
	function setRequired(enabled:Bool):google.apps.script.forms.MultipleChoiceItem;
	function duplicate(response:String):google.apps.script.forms.MultipleChoiceItem;
	function hasOtherOption(response:String):Bool;
	function isRequired(response:String):Bool;
	function getTitle(response:String):String;
	@:overload(function (value:String,isCorrect:Bool):google.apps.script.forms.Choice {})
	@:overload(function (value:String,navigationItem:google.apps.script.forms.PageBreakItem):google.apps.script.forms.Choice {})
	@:overload(function (value:String,navigationType:google.apps.script.forms.PageNavigationType):google.apps.script.forms.Choice {})
	function createChoice(value:String):google.apps.script.forms.Choice;
	function showOtherOption(enabled:Bool):google.apps.script.forms.MultipleChoiceItem;
	function setChoices(choices:Array<google.apps.script.forms.Choice>):google.apps.script.forms.MultipleChoiceItem;
	function setFeedbackForCorrect(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.MultipleChoiceItem;
}
