package google.apps.script.forms;

@:native("CheckboxGridItem")
extern class CheckboxGridItem
{
	function setValidation(validation:google.apps.script.forms.CheckboxGridValidation):google.apps.script.forms.CheckboxGridItem;
	function getHelpText(responses:Array<Array<String>>):String;
	function getTitle(responses:Array<Array<String>>):String;
	function setTitle(title:String):google.apps.script.forms.CheckboxGridItem;
	function setRequired(enabled:Bool):google.apps.script.forms.CheckboxGridItem;
	function getIndex(responses:Array<Array<String>>):Int;
	function setColumns(columns:Array<String>):google.apps.script.forms.CheckboxGridItem;
	function duplicate(responses:Array<Array<String>>):google.apps.script.forms.CheckboxGridItem;
	function clearValidation(url:String):google.apps.script.forms.CheckboxGridItem;
	function isRequired(responses:Array<Array<String>>):Bool;
	function getType(responses:Array<Array<String>>):google.apps.script.forms.ItemType;
	function setHelpText(text:String):google.apps.script.forms.CheckboxGridItem;
	function createResponse(responses:Array<Array<String>>):google.apps.script.forms.ItemResponse;
	function getColumns(responses:Array<Array<String>>):Array<String>;
	function getRows(responses:Array<Array<String>>):Array<String>;
	function getId(responses:Array<Array<String>>):Int;
	function setRows(rows:Array<String>):google.apps.script.forms.CheckboxGridItem;
}
