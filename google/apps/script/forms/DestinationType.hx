package google.apps.script.forms;

@:require(hx_gas)
@:native("DestinationType")
@:enum abstract DestinationType(String) from String to String
{
	var SPREADSHEET;
}

