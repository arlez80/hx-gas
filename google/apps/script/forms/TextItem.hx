package google.apps.script.forms;

@:native("TextItem")
extern class TextItem
{
	function getPoints(response:String):Int;
	function getHelpText(response:String):String;
	function setPoints(points:Int):google.apps.script.forms.TextItem;
	function getTitle(response:String):String;
	function setTitle(title:String):google.apps.script.forms.TextItem;
	function setRequired(enabled:Bool):google.apps.script.forms.TextItem;
	function getIndex(response:String):Int;
	function duplicate(response:String):google.apps.script.forms.TextItem;
	function clearValidation(title:String):google.apps.script.forms.TextItem;
	function isRequired(response:String):Bool;
	function getType(response:String):google.apps.script.forms.ItemType;
	function getGeneralFeedback(response:String):google.apps.script.forms.QuizFeedback;
	function setValidation(validation:google.apps.script.forms.TextValidation):google.apps.script.forms.TextItem;
	function createResponse(response:String):google.apps.script.forms.ItemResponse;
	function setHelpText(text:String):google.apps.script.forms.TextItem;
	function getId(response:String):Int;
	function setGeneralFeedback(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.TextItem;
}
