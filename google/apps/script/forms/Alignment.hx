package google.apps.script.forms;

@:require(hx_gas)
@:native("Alignment")
@:enum abstract Alignment(String) from String to String
{
	var LEFT;
	var CENTER;
	var RIGHT;
}

