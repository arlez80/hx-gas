package google.apps.script.forms;

@:native("DateItem")
extern class DateItem
{
	function getPoints(response:Date):Int;
	function getHelpText(response:Date):String;
	function setPoints(points:Int):google.apps.script.forms.DateItem;
	function getTitle(response:Date):String;
	function setRequired(enabled:Bool):google.apps.script.forms.DateItem;
	function getIndex(response:Date):Int;
	function duplicate(response:Date):google.apps.script.forms.DateItem;
	function setGeneralFeedback(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.DateItem;
	function isRequired(response:Date):Bool;
	function getType(response:Date):google.apps.script.forms.ItemType;
	function getId(response:Date):Int;
	function setHelpText(text:String):google.apps.script.forms.DateItem;
	function createResponse(response:Date):google.apps.script.forms.ItemResponse;
	function setIncludesYear(enableYear:Bool):google.apps.script.forms.DateItem;
	function getGeneralFeedback(response:Date):google.apps.script.forms.QuizFeedback;
	function includesYear(response:Date):Bool;
	function setTitle(title:String):google.apps.script.forms.DateItem;
}
