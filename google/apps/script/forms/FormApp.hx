package google.apps.script.forms;

@:require(hx_gas)
@:native("FormApp.Alignment")
@:enum abstract Alignment(String) from String to String
{
	var LEFT;
	var CENTER;
	var RIGHT;
}

@:require(hx_gas)
@:native("FormApp.DestinationType")
@:enum abstract DestinationType(String) from String to String
{
	var SPREADSHEET;
}

@:require(hx_gas)
@:native("FormApp.FeedbackType")
@:enum abstract FeedbackType(String) from String to String
{
	var CORRECT;
	var INCORRECT;
	var GENERAL;
}

@:require(hx_gas)
@:native("FormApp.ItemType")
@:enum abstract ItemType(String) from String to String
{
	var CHECKBOX;
	var CHECKBOX_GRID;
	var DATE;
	var DATETIME;
	var DURATION;
	var GRID;
	var IMAGE;
	var LIST;
	var MULTIPLE_CHOICE;
	var PAGE_BREAK;
	var PARAGRAPH_TEXT;
	var SCALE;
	var SECTION_HEADER;
	var TEXT;
	var TIME;
	var VIDEO;
}

@:require(hx_gas)
@:native("FormApp.PageNavigationType")
@:enum abstract PageNavigationType(String) from String to String
{
	var CONTINUE;
	var GO_TO_PAGE;
	var RESTART;
	var SUBMIT;
}

@:native("FormApp")
extern class FormApp
{
	static function openByUrl(url:String):google.apps.script.forms.Form;
	static function createTextValidation(title:String):google.apps.script.forms.TextValidationBuilder;
	static function createParagraphTextValidation(title:String):google.apps.script.forms.ParagraphTextValidationBuilder;
	static function getUi(title:String):google.apps.script.base.Ui;
	static function createCheckboxGridValidation(title:String):google.apps.script.forms.CheckboxGridValidationBuilder;
	static function openById(id:String):google.apps.script.forms.Form;
	static function getActiveForm(title:String):google.apps.script.forms.Form;
	static function createCheckboxValidation(title:String):google.apps.script.forms.CheckboxValidationBuilder;
	static function createGridValidation(title:String):google.apps.script.forms.GridValidationBuilder;
	static function create(title:String):google.apps.script.forms.Form;
	static function createFeedback(title:String):google.apps.script.forms.QuizFeedbackBuilder;
}
