package google.apps.script.forms;

@:native("CheckboxItem")
extern class CheckboxItem
{
	function getPoints(responses:Array<String>):Int;
	function setPoints(points:Int):google.apps.script.forms.CheckboxItem;
	function setValidation(validation:google.apps.script.forms.CheckboxValidation):google.apps.script.forms.CheckboxItem;
	function setFeedbackForIncorrect(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.CheckboxItem;
	function getIndex(responses:Array<String>):Int;
	function clearValidation(validation:google.apps.script.forms.CheckboxGridValidation):google.apps.script.forms.CheckboxItem;
	function getType(responses:Array<String>):google.apps.script.forms.ItemType;
	function getFeedbackForCorrect(responses:Array<String>):google.apps.script.forms.QuizFeedback;
	function createResponse(responses:Array<String>):google.apps.script.forms.ItemResponse;
	function setChoiceValues(values:Array<String>):google.apps.script.forms.CheckboxItem;
	function setHelpText(text:String):google.apps.script.forms.CheckboxItem;
	function getId(responses:Array<String>):Int;
	function setTitle(title:String):google.apps.script.forms.CheckboxItem;
	function getHelpText(responses:Array<String>):String;
	function getChoices(responses:Array<String>):Array<google.apps.script.forms.Choice>;
	function getFeedbackForIncorrect(responses:Array<String>):google.apps.script.forms.QuizFeedback;
	function setRequired(enabled:Bool):google.apps.script.forms.CheckboxItem;
	function duplicate(responses:Array<String>):google.apps.script.forms.CheckboxItem;
	function hasOtherOption(responses:Array<String>):Bool;
	function isRequired(responses:Array<String>):Bool;
	function getTitle(responses:Array<String>):String;
	@:overload(function (value:String,isCorrect:Bool):google.apps.script.forms.Choice {})
	function createChoice(value:String):google.apps.script.forms.Choice;
	function showOtherOption(enabled:Bool):google.apps.script.forms.CheckboxItem;
	function setChoices(choices:Array<google.apps.script.forms.Choice>):google.apps.script.forms.CheckboxItem;
	function setFeedbackForCorrect(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.CheckboxItem;
}
