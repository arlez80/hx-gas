package google.apps.script.forms;

@:native("QuizFeedback")
extern class QuizFeedback
{
	function getText(pattern:String):String;
	function getLinkUrls(pattern:String):Array<String>;
}
