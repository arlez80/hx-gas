package google.apps.script.forms;

@:native("ParagraphTextItem")
extern class ParagraphTextItem
{
	function getPoints(response:String):Int;
	function getHelpText(response:String):String;
	function setPoints(points:Int):google.apps.script.forms.ParagraphTextItem;
	function getTitle(response:String):String;
	function setTitle(title:String):google.apps.script.forms.ParagraphTextItem;
	function setRequired(enabled:Bool):google.apps.script.forms.ParagraphTextItem;
	function getIndex(response:String):Int;
	function duplicate(response:String):google.apps.script.forms.ParagraphTextItem;
	function clearValidation(title:String):google.apps.script.forms.ParagraphTextItem;
	function isRequired(response:String):Bool;
	function getType(response:String):google.apps.script.forms.ItemType;
	function getGeneralFeedback(response:String):google.apps.script.forms.QuizFeedback;
	function setValidation(validation:google.apps.script.forms.ParagraphTextValidation):google.apps.script.forms.ParagraphTextItem;
	function createResponse(response:String):google.apps.script.forms.ItemResponse;
	function setHelpText(text:String):google.apps.script.forms.ParagraphTextItem;
	function getId(response:String):Int;
	function setGeneralFeedback(feedback:google.apps.script.forms.QuizFeedback):google.apps.script.forms.ParagraphTextItem;
}
