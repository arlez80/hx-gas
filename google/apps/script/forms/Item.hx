package google.apps.script.forms;

@:native("Item")
extern class Item
{
	function asImageItem(width:Int):google.apps.script.forms.ImageItem;
	function setTitle(title:String):google.apps.script.forms.Item;
	function getIndex(width:Int):Int;
	function asParagraphTextItem(width:Int):google.apps.script.forms.ParagraphTextItem;
	function getType(width:Int):google.apps.script.forms.ItemType;
	function asTextItem(width:Int):google.apps.script.forms.TextItem;
	function asDateItem(width:Int):google.apps.script.forms.DateItem;
	function asSectionHeaderItem(width:Int):google.apps.script.forms.SectionHeaderItem;
	function asMultipleChoiceItem(width:Int):google.apps.script.forms.MultipleChoiceItem;
	function asVideoItem(width:Int):google.apps.script.forms.VideoItem;
	function setHelpText(text:String):google.apps.script.forms.Item;
	function getId(width:Int):Int;
	function asCheckboxGridItem(width:Int):google.apps.script.forms.CheckboxGridItem;
	function getHelpText(width:Int):String;
	function asDurationItem(width:Int):google.apps.script.forms.DurationItem;
	function asListItem(width:Int):google.apps.script.forms.ListItem;
	function asScaleItem(width:Int):google.apps.script.forms.ScaleItem;
	function asPageBreakItem(width:Int):google.apps.script.forms.PageBreakItem;
	function asDateTimeItem(width:Int):google.apps.script.forms.DateTimeItem;
	function asGridItem(width:Int):google.apps.script.forms.GridItem;
	function getTitle(width:Int):String;
	function asTimeItem(width:Int):google.apps.script.forms.TimeItem;
	function duplicate(width:Int):google.apps.script.forms.Item;
	function asCheckboxItem(width:Int):google.apps.script.forms.CheckboxItem;
}
