package google.apps.script.forms;

@:native("TextValidationBuilder")
extern class TextValidationBuilder
{
	function requireNumberLessThanOrEqualTo(number:Float):google.apps.script.forms.TextValidationBuilder;
	function requireNumberGreaterThan(number:Float):google.apps.script.forms.TextValidationBuilder;
	function requireNumber(validation:google.apps.script.forms.TextValidation):google.apps.script.forms.TextValidationBuilder;
	function requireTextIsUrl(pattern:String):google.apps.script.forms.TextValidationBuilder;
	function requireNumberNotBetween(start:Float,end:Float):google.apps.script.forms.TextValidationBuilder;
	function requireWholeNumber(pattern:String):google.apps.script.forms.TextValidationBuilder;
	function requireTextLengthGreaterThanOrEqualTo(number:Int):google.apps.script.forms.TextValidationBuilder;
	function requireTextDoesNotMatchPattern(pattern:String):google.apps.script.forms.TextValidationBuilder;
	function requireNumberNotEqualTo(number:Float):google.apps.script.forms.TextValidationBuilder;
	function requireTextDoesNotContainPattern(pattern:String):google.apps.script.forms.TextValidationBuilder;
	function requireNumberLessThan(number:Float):google.apps.script.forms.TextValidationBuilder;
	function requireNumberEqualTo(number:Float):google.apps.script.forms.TextValidationBuilder;
	function requireTextMatchesPattern(pattern:String):google.apps.script.forms.TextValidationBuilder;
	function requireNumberBetween(start:Float,end:Float):google.apps.script.forms.TextValidationBuilder;
	function requireTextLengthLessThanOrEqualTo(number:Int):google.apps.script.forms.TextValidationBuilder;
	function requireTextContainsPattern(pattern:String):google.apps.script.forms.TextValidationBuilder;
	function requireNumberGreaterThanOrEqualTo(number:Float):google.apps.script.forms.TextValidationBuilder;
	function requireTextIsEmail(pattern:String):google.apps.script.forms.TextValidationBuilder;
}
