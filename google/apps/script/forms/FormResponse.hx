package google.apps.script.forms;

@:native("FormResponse")
extern class FormResponse
{
	function getGradableResponseForItem(item:google.apps.script.forms.Item):google.apps.script.forms.ItemResponse;
	function withItemResponse(response:google.apps.script.forms.ItemResponse):google.apps.script.forms.FormResponse;
	function getEditResponseUrl(responses:Array<google.apps.script.forms.FormResponse>):String;
	function submit(item:google.apps.script.forms.Item):google.apps.script.forms.FormResponse;
	function getTimestamp(item:google.apps.script.forms.Item):Date;
	function withItemGrade(gradedResponse:google.apps.script.forms.ItemResponse):google.apps.script.forms.FormResponse;
	function getGradableItemResponses(responses:Array<google.apps.script.forms.FormResponse>):Array<google.apps.script.forms.ItemResponse>;
	function getItemResponses(item:google.apps.script.forms.Item):Array<google.apps.script.forms.ItemResponse>;
	function getResponseForItem(item:google.apps.script.forms.Item):google.apps.script.forms.ItemResponse;
	function toPrefilledUrl(item:google.apps.script.forms.Item):String;
	function getId(item:google.apps.script.forms.Item):String;
	function getRespondentEmail(item:google.apps.script.forms.Item):String;
}
