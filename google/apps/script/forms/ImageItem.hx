package google.apps.script.forms;

@:native("ImageItem")
extern class ImageItem
{
	function getHelpText(validation:google.apps.script.forms.GridValidation):String;
	function setImage(image:google.apps.script.base.BlobSource):google.apps.script.forms.ImageItem;
	function getAlignment(validation:google.apps.script.forms.GridValidation):google.apps.script.forms.Alignment;
	function setAlignment(alignment:google.apps.script.forms.Alignment):google.apps.script.forms.ImageItem;
	function getIndex(validation:google.apps.script.forms.GridValidation):Int;
	function duplicate(validation:google.apps.script.forms.GridValidation):google.apps.script.forms.ImageItem;
	function getType(validation:google.apps.script.forms.GridValidation):google.apps.script.forms.ItemType;
	function setHelpText(text:String):google.apps.script.forms.ImageItem;
	function getTitle(validation:google.apps.script.forms.GridValidation):String;
	function getImage(validation:google.apps.script.forms.GridValidation):google.apps.script.base.Blob;
	function setWidth(width:Int):google.apps.script.forms.ImageItem;
	function getId(validation:google.apps.script.forms.GridValidation):Int;
	function getWidth(validation:google.apps.script.forms.GridValidation):Int;
	function setTitle(title:String):google.apps.script.forms.ImageItem;
}
