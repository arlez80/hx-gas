package google.apps.script.gmail;

@:native("GmailAttachment")
extern class GmailAttachment
{
	function setContentTypeFromExtension(contentType:String):google.apps.script.base.Blob;
	@:overload(function (string:String,charset:String):google.apps.script.base.Blob {})
	function setDataFromString(string:String):google.apps.script.base.Blob;
	@:overload(function (charset:String):String {})
	function getDataAsString(contentType:String):String;
	function getSize(charset:String):Int;
	@:deprecated
	function getAllBlobs(name:String):Array<google.apps.script.base.Blob>;
	function getName(charset:String):String;
	function copyBlob(messages:Array<google.apps.script.gmail.GmailMessage>):google.apps.script.base.Blob;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function setBytes(data:Array<Int>):google.apps.script.base.Blob;
	function getBytes(contentType:String):Array<Int>;
	function setContentType(contentType:String):google.apps.script.base.Blob;
	function getContentType(contentType:String):String;
	function setName(name:String):google.apps.script.base.Blob;
	function isGoogleType(charset:String):Bool;
	function getHash(charset:String):String;
}
