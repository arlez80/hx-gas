package google.apps.script.gmail;

@:native("GmailDraft")
extern class GmailDraft
{
	function getMessage(name:String):google.apps.script.gmail.GmailMessage;
	function deleteDraft(name:String):Void;
	function getMessageId(name:String):String;
	@:overload(function (recipient:String,subject:String,body:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?from:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?replyTo:String}):google.apps.script.gmail.GmailDraft {})
	function update(recipient:String,subject:String,body:String):google.apps.script.gmail.GmailDraft;
	function send(name:String):google.apps.script.gmail.GmailMessage;
	function getId(name:String):String;
}
