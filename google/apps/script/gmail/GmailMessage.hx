package google.apps.script.gmail;

@:native("GmailMessage")
extern class GmailMessage
{
	function isStarred(options:Dynamic):Bool;
	function markRead(options:Dynamic):google.apps.script.gmail.GmailMessage;
	function getFrom(options:Dynamic):String;
	function getBody(options:Dynamic):String;
	@:overload(function (options:{?includeInlineImages:Bool,?includeAttachments:Bool}):Array<google.apps.script.gmail.GmailAttachment> {})
	function getAttachments(recipient:String,options:Dynamic):Array<google.apps.script.gmail.GmailAttachment>;
	function moveToTrash(options:Dynamic):google.apps.script.gmail.GmailMessage;
	function getDate(options:Dynamic):Date;
	function isUnread(options:Dynamic):Bool;
	function isInTrash(options:Dynamic):Bool;
	@:overload(function (body:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?from:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?noReply:Bool,?replyTo:String,?subject:String}):google.apps.script.gmail.GmailMessage {})
	function reply(body:String):google.apps.script.gmail.GmailMessage;
	function getCc(options:Dynamic):String;
	function getReplyTo(options:Dynamic):String;
	function isInChats(options:Dynamic):Bool;
	function getId(options:Dynamic):String;
	@:overload(function (body:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?from:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?replyTo:String,?subject:String}):google.apps.script.gmail.GmailDraft {})
	function createDraftReply(body:String):google.apps.script.gmail.GmailDraft;
	function getThread(options:Dynamic):google.apps.script.gmail.GmailThread;
	@:overload(function (recipient:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?from:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?noReply:Bool,?replyTo:String,?subject:String}):google.apps.script.gmail.GmailMessage {})
	function forward(recipient:String):google.apps.script.gmail.GmailMessage;
	function getRawContent(options:Dynamic):String;
	@:overload(function (body:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?from:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?replyTo:String,?subject:String}):google.apps.script.gmail.GmailDraft {})
	function createDraftReplyAll(body:String):google.apps.script.gmail.GmailDraft;
	function getBcc(options:Dynamic):String;
	function isInInbox(options:Dynamic):Bool;
	function getSubject(options:Dynamic):String;
	function getPlainBody(options:Dynamic):String;
	function getTo(options:Dynamic):String;
	function markUnread(options:Dynamic):google.apps.script.gmail.GmailMessage;
	function unstar(body:String,options:Dynamic):google.apps.script.gmail.GmailMessage;
	function refresh(options:Dynamic):google.apps.script.gmail.GmailMessage;
	function isDraft(options:Dynamic):Bool;
	@:overload(function (body:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?from:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?noReply:Bool,?replyTo:String,?subject:String}):google.apps.script.gmail.GmailMessage {})
	function replyAll(body:String):google.apps.script.gmail.GmailMessage;
	function isInPriorityInbox(options:Dynamic):Bool;
	function star(body:String,options:Dynamic):google.apps.script.gmail.GmailMessage;
}
