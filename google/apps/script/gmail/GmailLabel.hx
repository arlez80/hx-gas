package google.apps.script.gmail;

@:native("GmailLabel")
extern class GmailLabel
{
	function getUnreadCount(start:Int,max:Int):Int;
	function removeFromThreads(threads:Array<google.apps.script.gmail.GmailThread>):google.apps.script.gmail.GmailLabel;
	function removeFromThread(thread:google.apps.script.gmail.GmailThread):google.apps.script.gmail.GmailLabel;
	function addToThread(thread:google.apps.script.gmail.GmailThread):google.apps.script.gmail.GmailLabel;
	function addToThreads(threads:Array<google.apps.script.gmail.GmailThread>):google.apps.script.gmail.GmailLabel;
	@:overload(function (start:Int,max:Int):Array<google.apps.script.gmail.GmailThread> {})
	function getThreads(threads:Array<google.apps.script.gmail.GmailThread>):Array<google.apps.script.gmail.GmailThread>;
	function getName(threads:Array<google.apps.script.gmail.GmailThread>):String;
	function deleteLabel(threads:Array<google.apps.script.gmail.GmailThread>):js.Error;
}
