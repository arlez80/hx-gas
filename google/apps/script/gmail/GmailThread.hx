package google.apps.script.gmail;

@:native("GmailThread")
extern class GmailThread
{
	function getFirstMessageSubject(body:String,options:Dynamic):String;
	@:overload(function (body:String,options:{?cc:String,?bcc:String,?htmlBody:String,?name:String,?from:String,?replyTo:String,?noReply:Bool,?attachments:Array<google.apps.script.base.BlobSource>,?inlineImages:Dynamic}):google.apps.script.gmail.GmailThread {})
	function reply(body:String):google.apps.script.gmail.GmailThread;
	function markRead(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
	function isInTrash(body:String,options:Dynamic):Bool;
	function addLabel(label:google.apps.script.gmail.GmailLabel):google.apps.script.gmail.GmailThread;
	function getLabels(body:String,options:Dynamic):Array<google.apps.script.gmail.GmailLabel>;
	function moveToTrash(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
	function isUnread(body:String,options:Dynamic):Bool;
	function removeLabel(label:google.apps.script.gmail.GmailLabel):google.apps.script.gmail.GmailThread;
	function hasStarredMessages(body:String,options:Dynamic):Bool;
	function getId(body:String,options:Dynamic):String;
	function isInSpam(body:String,options:Dynamic):Bool;
	function getMessages(body:String,options:Dynamic):Array<google.apps.script.gmail.GmailMessage>;
	@:overload(function (body:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?from:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?replyTo:String,?subject:String}):google.apps.script.gmail.GmailDraft {})
	function createDraftReply(body:String):google.apps.script.gmail.GmailDraft;
	function isImportant(body:String,options:Dynamic):Bool;
	@:overload(function (body:String,options:{?cc:String,?bcc:String,?htmlBody:String,?name:String,?from:String,?replyTo:String,?noReply:Bool,?attachments:Array<google.apps.script.base.BlobSource>,?inlineImages:Dynamic}):google.apps.script.gmail.GmailThread {})
	function replyAll(body:String):google.apps.script.gmail.GmailThread;
	function getMessageCount(body:String,options:Dynamic):Int;
	@:overload(function (body:String,options:{?attachments:Array<google.apps.script.base.BlobSource>,?bcc:String,?cc:String,?from:String,?htmlBody:String,?inlineImages:Dynamic,?name:String,?replyTo:String,?subject:String}):google.apps.script.gmail.GmailDraft {})
	function createDraftReplyAll(body:String):google.apps.script.gmail.GmailDraft;
	function moveToArchive(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
	function isInInbox(body:String,options:Dynamic):Bool;
	function getLastMessageDate(body:String,options:Dynamic):Date;
	function moveToInbox(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
	function markImportant(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
	function getPermalink(body:String,options:Dynamic):String;
	function refresh(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
	function moveToSpam(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
	function isInChats(body:String,options:Dynamic):Bool;
	function isInPriorityInbox(body:String,options:Dynamic):Bool;
	function markUnimportant(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
	function markUnread(body:String,options:Dynamic):google.apps.script.gmail.GmailThread;
}
