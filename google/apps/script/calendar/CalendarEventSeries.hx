package google.apps.script.calendar;

@:native("CalendarEventSeries")
extern class CalendarEventSeries
{
	@:overload(function (includeOwner:Bool):Array<google.apps.script.calendar.EventGuest> {})
	function getGuestList(email:String):Array<google.apps.script.calendar.EventGuest>;
	function guestsCanSeeGuests(key:String):Bool;
	function getGuestByEmail(email:String):google.apps.script.calendar.EventGuest;
	function getMyStatus(includeOwner:Bool):google.apps.script.calendar.GuestStatus;
	function setGuestsCanModify(guestsCanModify:Bool):google.apps.script.calendar.CalendarEventSeries;
	function setTag(key:String,value:String):google.apps.script.calendar.CalendarEventSeries;
	function getDateCreated(key:String):Date;
	function getTag(key:String):String;
	function guestsCanInviteOthers(key:String):Bool;
	function getVisibility(key:String):google.apps.script.calendar.Visibility;
	function setTitle(title:String):google.apps.script.calendar.CalendarEventSeries;
	function getPopupReminders(includeOwner:Bool):Array<Int>;
	function guestsCanModify(key:String):Bool;
	function setLocation(location:String):google.apps.script.calendar.CalendarEventSeries;
	function setVisibility(visibility:google.apps.script.calendar.Visibility):google.apps.script.calendar.CalendarEventSeries;
	function getLocation(includeOwner:Bool):String;
	function removeAllReminders(key:String):google.apps.script.calendar.CalendarEventSeries;
	function removeGuest(email:String):google.apps.script.calendar.CalendarEventSeries;
	function addSmsReminder(minutesBefore:Int):google.apps.script.calendar.CalendarEventSeries;
	function setMyStatus(status:google.apps.script.calendar.GuestStatus):google.apps.script.calendar.CalendarEventSeries;
	function deleteEventSeries(minutesBefore:Int):Void;
	function setAnyoneCanAddSelf(anyoneCanAddSelf:Bool):google.apps.script.calendar.CalendarEventSeries;
	function getSmsReminders(includeOwner:Bool):Array<Int>;
	function getOriginalCalendarId(includeOwner:Bool):String;
	function anyoneCanAddSelf(minutesBefore:Int):Bool;
	function getLastUpdated(includeOwner:Bool):Date;
	function setDescription(description:String):google.apps.script.calendar.CalendarEventSeries;
	function getCreators(key:String):Array<String>;
	function resetRemindersToDefault(email:String):google.apps.script.calendar.CalendarEventSeries;
	function getDescription(key:String):String;
	function getAllTagKeys(key:String):Array<String>;
	function getEmailReminders(key:String):Array<Int>;
	function setGuestsCanSeeGuests(guestsCanSeeGuests:Bool):google.apps.script.calendar.CalendarEventSeries;
	function getId(includeOwner:Bool):String;
	function addPopupReminder(minutesBefore:Int):google.apps.script.calendar.CalendarEventSeries;
	function setGuestsCanInviteOthers(guestsCanInviteOthers:Bool):google.apps.script.calendar.CalendarEventSeries;
	@:overload(function (recurrence:google.apps.script.calendar.EventRecurrence,startTime:Date,endTime:Date):google.apps.script.calendar.CalendarEventSeries {})
	function setRecurrence(recurrence:google.apps.script.calendar.EventRecurrence,startDate:Date):google.apps.script.calendar.CalendarEventSeries;
	function addEmailReminder(minutesBefore:Int):google.apps.script.calendar.CalendarEventSeries;
	function deleteTag(key:String):google.apps.script.calendar.CalendarEventSeries;
	function getColor(key:String):String;
	function isOwnedByMe(key:String):Bool;
	function setColor(color:String):google.apps.script.calendar.CalendarEventSeries;
	function getTitle(key:String):String;
	function addGuest(email:String):google.apps.script.calendar.CalendarEventSeries;
}
