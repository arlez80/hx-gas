package google.apps.script.calendar;

@:native("EventGuest")
extern class EventGuest
{
	function getName(visibility:google.apps.script.calendar.Visibility):String;
	function getGuestStatus(visibility:google.apps.script.calendar.Visibility):google.apps.script.calendar.GuestStatus;
	function getAdditionalGuests(visibility:google.apps.script.calendar.Visibility):Int;
	function getEmail(visibility:google.apps.script.calendar.Visibility):String;
	@:deprecated
	function getStatus(visibility:google.apps.script.calendar.Visibility):String;
}
