package google.apps.script.calendar;

@:native("Calendar")
extern class Calendar
{
	function setHidden(hidden:Bool):google.apps.script.calendar.Calendar;
	@:overload(function (title:String,startDate:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEventSeries {})
	function createAllDayEventSeries(title:String,startDate:Date,recurrence:google.apps.script.calendar.EventRecurrence):google.apps.script.calendar.CalendarEventSeries;
	function setDescription(description:String):google.apps.script.calendar.Calendar;
	@:overload(function (title:String,startDate:Date,endDate:Date):google.apps.script.calendar.CalendarEvent {})
	@:overload(function (title:String,startDate:Date,endDate:Date,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEvent {})
	@:overload(function (title:String,date:Date,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEvent {})
	function createAllDayEvent(title:String,date:Date):google.apps.script.calendar.CalendarEvent;
	function isMyPrimaryCalendar(date:Date,options:Dynamic):Bool;
	function setTimeZone(timeZone:String):google.apps.script.calendar.Calendar;
	function getDescription(title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:Dynamic):String;
	@:overload(function (title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEventSeries {})
	function createEventSeries(title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence):google.apps.script.calendar.CalendarEventSeries;
	function getEventById(iCalId:String):google.apps.script.calendar.CalendarEvent;
	function getEventSeriesById(iCalId:String):google.apps.script.calendar.CalendarEventSeries;
	function unsubscribeFromCalendar(timeZone:String):js.Error;
	function getId(date:Date,options:Dynamic):String;
	@:overload(function (startTime:Date,endTime:Date,options:{?start:Int,?max:Int,?author:String,?search:String,?statusFilters[]:google.apps.script.calendar.GuestStatus}):Array<google.apps.script.calendar.CalendarEvent> {})
	function getEvents(startTime:Date,endTime:Date):Array<google.apps.script.calendar.CalendarEvent>;
	function getTimeZone(date:Date,options:Dynamic):String;
	function isOwnedByMe(date:Date,options:Dynamic):Bool;
	function getColor(title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:Dynamic):String;
	function isHidden(date:Date,options:Dynamic):Bool;
	function getName(date:Date,options:Dynamic):String;
	function setColor(color:String):google.apps.script.calendar.Calendar;
	@:overload(function (title:String,startTime:Date,endTime:Date,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEvent {})
	function createEvent(title:String,startTime:Date,endTime:Date):google.apps.script.calendar.CalendarEvent;
	function setName(name:String):google.apps.script.calendar.Calendar;
	function setSelected(selected:Bool):google.apps.script.calendar.Calendar;
	function isSelected(date:Date,options:Dynamic):Bool;
	function createEventFromDescription(description:String):google.apps.script.calendar.CalendarEvent;
	@:overload(function (date:Date,options:{?start:Int,?max:Int,?author:String,?search:String,?statusFilters[]:google.apps.script.calendar.GuestStatus}):Array<google.apps.script.calendar.CalendarEvent> {})
	function getEventsForDay(date:Date):Array<google.apps.script.calendar.CalendarEvent>;
	function deleteCalendar(title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:Dynamic):js.Error;
}
