package google.apps.script.calendar;

@:require(hx_gas)
@:native("CalendarApp.Color")
@:enum abstract Color(String) from String to String
{
	var BLUE;
	var BROWN;
	var CHARCOAL;
	var CHESTNUT;
	var GRAY;
	var GREEN;
	var INDIGO;
	var LIME;
	var MUSTARD;
	var OLIVE;
	var ORANGE;
	var PINK;
	var PLUM;
	var PURPLE;
	var RED;
	var RED_ORANGE;
	var SEA_BLUE;
	var SLATE;
	var TEAL;
	var TURQOISE;
	var YELLOW;
}

@:require(hx_gas)
@:native("CalendarApp.EventColor")
@:enum abstract EventColor(String) from String to String
{
	var PALE_BLUE;
	var PALE_GREEN;
	var MAUVE;
	var PALE_RED;
	var YELLOW;
	var ORANGE;
	var CYAN;
	var GRAY;
	var BLUE;
	var GREEN;
	var RED;
}

@:require(hx_gas)
@:native("CalendarApp.GuestStatus")
@:enum abstract GuestStatus(String) from String to String
{
	var INVITED;
	var MAYBE;
	var NO;
	var OWNER;
	var YES;
}

@:require(hx_gas)
@:native("CalendarApp.Month")
@:enum abstract Month(String) from String to String
{
	var JANUARY;
	var FEBRUARY;
	var MARCH;
	var APRIL;
	var MAY;
	var JUNE;
	var JULY;
	var AUGUST;
	var SEPTEMBER;
	var OCTOBER;
	var NOVEMBER;
	var DECEMBER;
}

@:require(hx_gas)
@:native("CalendarApp.Visibility")
@:enum abstract Visibility(String) from String to String
{
	var CONFIDENTIAL;
	var DEFAULT;
	var PRIVATE;
	var PUBLIC;
}

@:require(hx_gas)
@:native("CalendarApp.Weekday")
@:enum abstract Weekday(String) from String to String
{
	var SUNDAY;
	var MONDAY;
	var TUESDAY;
	var WEDNESDAY;
	var THURSDAY;
	var FRIDAY;
	var SATURDAY;
}

@:native("CalendarApp")
extern class CalendarApp
{
	static function setHidden(hidden:Bool):google.apps.script.calendar.Calendar;
	static function setDescription(description:String):google.apps.script.calendar.Calendar;
	static function getOwnedCalendarById(id:String):google.apps.script.calendar.Calendar;
	static function getDefaultCalendar(name:String):google.apps.script.calendar.Calendar;
	static function getCalendarsByName(name:String):Array<google.apps.script.calendar.Calendar>;
	@:overload(function (title:String,startDate:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEventSeries {})
	static function createAllDayEventSeries(title:String,startDate:Date,recurrence:google.apps.script.calendar.EventRecurrence):google.apps.script.calendar.CalendarEventSeries;
	static function createEventFromDescription(description:String):google.apps.script.calendar.CalendarEvent;
	static function getCalendarById(id:String):google.apps.script.calendar.Calendar;
	@:overload(function (title:String,startDate:Date,endDate:Date):google.apps.script.calendar.CalendarEvent {})
	@:overload(function (title:String,startDate:Date,endDate:Date,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEvent {})
	@:overload(function (title:String,date:Date,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEvent {})
	static function createAllDayEvent(title:String,date:Date):google.apps.script.calendar.CalendarEvent;
	@:overload(function (id:String,options:{?color:String,?hidden:Bool,?selected:Bool}):google.apps.script.calendar.Calendar {})
	static function subscribeToCalendar(id:String):google.apps.script.calendar.Calendar;
	static function isMyPrimaryCalendar(name:String):Bool;
	static function setTimeZone(timeZone:String):google.apps.script.calendar.Calendar;
	static function getDescription(name:String):String;
	@:overload(function (title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEventSeries {})
	static function createEventSeries(title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence):google.apps.script.calendar.CalendarEventSeries;
	static function getEventById(iCalId:String):google.apps.script.calendar.CalendarEvent;
	static function getOwnedCalendarsByName(name:String):Array<google.apps.script.calendar.Calendar>;
	static function getEventSeriesById(iCalId:String):google.apps.script.calendar.CalendarEventSeries;
	static function getId(date:Date,options:Dynamic):String;
	@:overload(function (startTime:Date,endTime:Date,options:{?start:Int,?max:Int,?author:String,?search:String,?statusFilters[]:google.apps.script.calendar.GuestStatus}):Array<google.apps.script.calendar.CalendarEvent> {})
	static function getEvents(startTime:Date,endTime:Date):Array<google.apps.script.calendar.CalendarEvent>;
	static function newRecurrence(name:String):google.apps.script.calendar.EventRecurrence;
	@:overload(function (name:String,options:{?location:String,?summary:String,?timeZone:String,?color:String,?hidden:Bool,?selected:Bool}):google.apps.script.calendar.Calendar {})
	static function createCalendar(name:String):google.apps.script.calendar.Calendar;
	static function getAllOwnedCalendars(title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:Dynamic):Array<google.apps.script.calendar.Calendar>;
	static function getTimeZone(name:String):String;
	static function isOwnedByMe(name:String):Bool;
	static function getColor(name:String):String;
	static function isHidden(name:String):Bool;
	static function getName(date:Date,options:Dynamic):String;
	static function setColor(color:String):google.apps.script.calendar.Calendar;
	@:overload(function (title:String,startTime:Date,endTime:Date,options:{?description:String,?location:String,?guests:String,?sendInvites:Bool}):google.apps.script.calendar.CalendarEvent {})
	static function createEvent(title:String,startTime:Date,endTime:Date):google.apps.script.calendar.CalendarEvent;
	static function setName(name:String):google.apps.script.calendar.Calendar;
	static function setSelected(selected:Bool):google.apps.script.calendar.Calendar;
	static function isSelected(name:String):Bool;
	static function getAllCalendars(title:String,startTime:Date,endTime:Date,recurrence:google.apps.script.calendar.EventRecurrence,options:Dynamic):Array<google.apps.script.calendar.Calendar>;
	@:overload(function (date:Date,options:{?start:Int,?max:Int,?author:String,?search:String,?statusFilters[]:google.apps.script.calendar.GuestStatus}):Array<google.apps.script.calendar.CalendarEvent> {})
	static function getEventsForDay(date:Date):Array<google.apps.script.calendar.CalendarEvent>;
}
