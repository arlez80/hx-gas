package google.apps.script.calendar;

@:require(hx_gas)
@:native("Color")
@:enum abstract Color(String) from String to String
{
	var BLUE;
	var BROWN;
	var CHARCOAL;
	var CHESTNUT;
	var GRAY;
	var GREEN;
	var INDIGO;
	var LIME;
	var MUSTARD;
	var OLIVE;
	var ORANGE;
	var PINK;
	var PLUM;
	var PURPLE;
	var RED;
	var RED_ORANGE;
	var SEA_BLUE;
	var SLATE;
	var TEAL;
	var TURQOISE;
	var YELLOW;
}

