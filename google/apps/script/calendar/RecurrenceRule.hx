package google.apps.script.calendar;

@:native("RecurrenceRule")
extern class RecurrenceRule
{
	function onlyOnWeekdays(days:Array<google.apps.script.base.Weekday>):google.apps.script.calendar.RecurrenceRule;
	function addDailyExclusion(timeZone:String):google.apps.script.calendar.RecurrenceRule;
	function interval(interval:Int):google.apps.script.calendar.RecurrenceRule;
	function addDateExclusion(date:Date):google.apps.script.calendar.EventRecurrence;
	function weekStartsOn(day:google.apps.script.base.Weekday):google.apps.script.calendar.RecurrenceRule;
	function onlyInMonths(months:Array<google.apps.script.base.Month>):google.apps.script.calendar.RecurrenceRule;
	function setTimeZone(timeZone:String):google.apps.script.calendar.EventRecurrence;
	function addDailyRule(timeZone:String):google.apps.script.calendar.RecurrenceRule;
	function times(times:Int):google.apps.script.calendar.RecurrenceRule;
	function addYearlyExclusion(date:Date):google.apps.script.calendar.RecurrenceRule;
	function onlyOnMonthDay(day:Int):google.apps.script.calendar.RecurrenceRule;
	function until(endDate:Date):google.apps.script.calendar.RecurrenceRule;
	function onlyOnWeeks(weeks:Array<Int>):google.apps.script.calendar.RecurrenceRule;
	function addWeeklyExclusion(date:Date):google.apps.script.calendar.RecurrenceRule;
	function onlyOnMonthDays(days:Array<Int>):google.apps.script.calendar.RecurrenceRule;
	function addMonthlyExclusion(date:Date):google.apps.script.calendar.RecurrenceRule;
	function onlyOnWeekday(day:google.apps.script.base.Weekday):google.apps.script.calendar.RecurrenceRule;
	function addYearlyRule(date:Date):google.apps.script.calendar.RecurrenceRule;
	function onlyInMonth(month:google.apps.script.base.Month):google.apps.script.calendar.RecurrenceRule;
	function addDate(date:Date):google.apps.script.calendar.EventRecurrence;
	function addMonthlyRule(date:Date):google.apps.script.calendar.RecurrenceRule;
	function onlyOnWeek(week:Int):google.apps.script.calendar.RecurrenceRule;
	function onlyOnYearDays(days:Array<Int>):google.apps.script.calendar.RecurrenceRule;
	function addWeeklyRule(date:Date):google.apps.script.calendar.RecurrenceRule;
	function onlyOnYearDay(day:Int):google.apps.script.calendar.RecurrenceRule;
}
