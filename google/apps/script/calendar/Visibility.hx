package google.apps.script.calendar;

@:require(hx_gas)
@:native("Visibility")
@:enum abstract Visibility(String) from String to String
{
	var CONFIDENTIAL;
	var DEFAULT;
	var PRIVATE;
	var PUBLIC;
}

