package google.apps.script.calendar;

@:require(hx_gas)
@:native("GuestStatus")
@:enum abstract GuestStatus(String) from String to String
{
	var INVITED;
	var MAYBE;
	var NO;
	var OWNER;
	var YES;
}

