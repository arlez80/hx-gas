package google.apps.script.calendar;

@:native("CalendarEvent")
extern class CalendarEvent
{
	@:overload(function (includeOwner:Bool):Array<google.apps.script.calendar.EventGuest> {})
	function getGuestList(email:String):Array<google.apps.script.calendar.EventGuest>;
	function guestsCanSeeGuests(key:String):Bool;
	function getGuestByEmail(email:String):google.apps.script.calendar.EventGuest;
	function getMyStatus(includeOwner:Bool):google.apps.script.calendar.GuestStatus;
	function setGuestsCanModify(guestsCanModify:Bool):google.apps.script.calendar.CalendarEvent;
	function setTag(key:String,value:String):google.apps.script.calendar.CalendarEvent;
	function getDateCreated(key:String):Date;
	function getTag(key:String):String;
	function guestsCanInviteOthers(key:String):Bool;
	function getVisibility(key:String):google.apps.script.calendar.Visibility;
	function setTitle(title:String):google.apps.script.calendar.CalendarEvent;
	function getEventSeries(key:String):google.apps.script.calendar.CalendarEventSeries;
	function getPopupReminders(includeOwner:Bool):Array<Int>;
	function guestsCanModify(key:String):Bool;
	function addEmailReminder(minutesBefore:Int):google.apps.script.calendar.CalendarEvent;
	function deleteEvent(minutesBefore:Int):Void;
	function setVisibility(visibility:google.apps.script.calendar.Visibility):google.apps.script.calendar.CalendarEvent;
	function getLocation(includeOwner:Bool):String;
	function removeAllReminders(key:String):google.apps.script.calendar.CalendarEvent;
	function removeGuest(email:String):google.apps.script.calendar.CalendarEvent;
	function addSmsReminder(minutesBefore:Int):google.apps.script.calendar.CalendarEvent;
	function setMyStatus(status:google.apps.script.calendar.GuestStatus):google.apps.script.calendar.CalendarEvent;
	function getEndTime(key:String):Date;
	function setAnyoneCanAddSelf(anyoneCanAddSelf:Bool):google.apps.script.calendar.CalendarEvent;
	function setAllDayDates(startDate:Date,endDate:Date):google.apps.script.calendar.CalendarEvent;
	function getAllDayEndDate(key:String):Date;
	function getOriginalCalendarId(includeOwner:Bool):String;
	function anyoneCanAddSelf(minutesBefore:Int):Bool;
	function getLastUpdated(includeOwner:Bool):Date;
	function setDescription(description:String):google.apps.script.calendar.CalendarEvent;
	function getCreators(key:String):Array<String>;
	function resetRemindersToDefault(email:String):google.apps.script.calendar.CalendarEvent;
	function getDescription(key:String):String;
	function getAllTagKeys(key:String):Array<String>;
	function getEmailReminders(key:String):Array<Int>;
	function setGuestsCanSeeGuests(guestsCanSeeGuests:Bool):google.apps.script.calendar.CalendarEvent;
	function getId(includeOwner:Bool):String;
	function addPopupReminder(minutesBefore:Int):google.apps.script.calendar.CalendarEvent;
	function setGuestsCanInviteOthers(guestsCanInviteOthers:Bool):google.apps.script.calendar.CalendarEvent;
	function getSmsReminders(includeOwner:Bool):Array<Int>;
	function setLocation(location:String):google.apps.script.calendar.CalendarEvent;
	function deleteTag(key:String):google.apps.script.calendar.CalendarEvent;
	function setAllDayDate(date:Date):google.apps.script.calendar.CalendarEvent;
	function getColor(key:String):String;
	function getStartTime(includeOwner:Bool):Date;
	function isRecurringEvent(key:String):Bool;
	function isOwnedByMe(key:String):Bool;
	function setColor(color:String):google.apps.script.calendar.CalendarEvent;
	function getTitle(key:String):String;
	function isAllDayEvent(key:String):Bool;
	function addGuest(email:String):google.apps.script.calendar.CalendarEvent;
	function setTime(startTime:Date,endTime:Date):google.apps.script.calendar.CalendarEvent;
	function getAllDayStartDate(key:String):Date;
}
