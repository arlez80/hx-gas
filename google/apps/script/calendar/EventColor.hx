package google.apps.script.calendar;

@:require(hx_gas)
@:native("EventColor")
@:enum abstract EventColor(String) from String to String
{
	var PALE_BLUE;
	var PALE_GREEN;
	var MAUVE;
	var PALE_RED;
	var YELLOW;
	var ORANGE;
	var CYAN;
	var GRAY;
	var BLUE;
	var GREEN;
	var RED;
}

