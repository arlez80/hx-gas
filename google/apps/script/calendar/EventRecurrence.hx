package google.apps.script.calendar;

@:native("EventRecurrence")
extern class EventRecurrence
{
	function addWeeklyExclusion(date:Date):google.apps.script.calendar.RecurrenceRule;
	function addYearlyRule(date:Date):google.apps.script.calendar.RecurrenceRule;
	function addDailyRule(visibility:google.apps.script.calendar.Visibility):google.apps.script.calendar.RecurrenceRule;
	function addDailyExclusion(visibility:google.apps.script.calendar.Visibility):google.apps.script.calendar.RecurrenceRule;
	function addMonthlyRule(date:Date):google.apps.script.calendar.RecurrenceRule;
	function addMonthlyExclusion(date:Date):google.apps.script.calendar.RecurrenceRule;
	function addYearlyExclusion(date:Date):google.apps.script.calendar.RecurrenceRule;
	function addDateExclusion(date:Date):google.apps.script.calendar.EventRecurrence;
	function setTimeZone(timeZone:String):google.apps.script.calendar.EventRecurrence;
	function addWeeklyRule(date:Date):google.apps.script.calendar.RecurrenceRule;
	function addDate(date:Date):google.apps.script.calendar.EventRecurrence;
}
