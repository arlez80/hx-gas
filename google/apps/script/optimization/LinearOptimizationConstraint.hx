package google.apps.script.optimization;

@:native("LinearOptimizationConstraint")
extern class LinearOptimizationConstraint
{
	function setCoefficient(variableName:String,coefficient:Float):google.apps.script.optimization.LinearOptimizationConstraint;
}
