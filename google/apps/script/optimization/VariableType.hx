package google.apps.script.optimization;

@:require(hx_gas)
@:native("VariableType")
@:enum abstract VariableType(String) from String to String
{
	var INTEGER;
	var CONTINUOUS;
}

