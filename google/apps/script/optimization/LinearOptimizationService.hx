package google.apps.script.optimization;

@:require(hx_gas)
@:native("LinearOptimizationService.Status")
@:enum abstract Status(String) from String to String
{
	var OPTIMAL;
	var FEASIBLE;
	var INFEASIBLE;
	var UNBOUNDED;
	var ABNORMAL;
	var MODEL_INVALID;
	var NOT_SOLVED;
}

@:require(hx_gas)
@:native("LinearOptimizationService.VariableType")
@:enum abstract VariableType(String) from String to String
{
	var INTEGER;
	var CONTINUOUS;
}

@:native("LinearOptimizationService")
extern class LinearOptimizationService
{
	static function createEngine(to:String,replyTo:String,subject:String,body:String):google.apps.script.optimization.LinearOptimizationEngine;
}
