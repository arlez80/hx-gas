package google.apps.script.optimization;

@:require(hx_gas)
@:native("Status")
@:enum abstract Status(String) from String to String
{
	var OPTIMAL;
	var FEASIBLE;
	var INFEASIBLE;
	var UNBOUNDED;
	var ABNORMAL;
	var MODEL_INVALID;
	var NOT_SOLVED;
}

