package google.apps.script.optimization;

@:native("LinearOptimizationSolution")
extern class LinearOptimizationSolution
{
	function getObjectiveValue(seconds:Float):Float;
	function isValid(variableName:String):Bool;
	function getVariableValue(variableName:String):Float;
	function getStatus(seconds:Float):google.apps.script.optimization.Status;
}
