package google.apps.script.optimization;

@:native("LinearOptimizationEngine")
extern class LinearOptimizationEngine
{
	function addConstraint(lowerBound:Float,upperBound:Float):google.apps.script.optimization.LinearOptimizationConstraint;
	@:overload(function (seconds:Float):google.apps.script.optimization.LinearOptimizationSolution {})
	function solve(variableName:String,coefficient:Float):google.apps.script.optimization.LinearOptimizationSolution;
	function setObjectiveCoefficient(variableName:String,coefficient:Float):google.apps.script.optimization.LinearOptimizationEngine;
	function setMinimization(name:String,lowerBound:Float,upperBound:Float,type:google.apps.script.optimization.VariableType,objectiveCoefficient:Float):google.apps.script.optimization.LinearOptimizationEngine;
	function setMaximization(name:String,lowerBound:Float,upperBound:Float,type:google.apps.script.optimization.VariableType,objectiveCoefficient:Float):google.apps.script.optimization.LinearOptimizationEngine;
	@:overload(function (name:String,lowerBound:Float,upperBound:Float,type:google.apps.script.optimization.VariableType):google.apps.script.optimization.LinearOptimizationEngine {})
	@:overload(function (name:String,lowerBound:Float,upperBound:Float,type:google.apps.script.optimization.VariableType,objectiveCoefficient:Float):google.apps.script.optimization.LinearOptimizationEngine {})
	function addVariable(name:String,lowerBound:Float,upperBound:Float):google.apps.script.optimization.LinearOptimizationEngine;
}
