package google.apps.script.properties;

@:native("ScriptProperties")
@:deprecated
extern class ScriptProperties
{
	@:deprecated
	static function setProperty(key:String,value:String):google.apps.script.properties.ScriptProperties;
	@:deprecated
	static function getProperties(key:String):Dynamic;
	@:deprecated
	static function getProperty(key:String):String;
	@:deprecated
	static function deleteAllProperties(variableName:String):google.apps.script.properties.ScriptProperties;
	@:deprecated
	static function deleteProperty(key:String):google.apps.script.properties.ScriptProperties;
	@:deprecated
	@:overload(function (properties:Dynamic,deleteAllOthers:Bool):google.apps.script.properties.ScriptProperties {})
	static function setProperties(properties:Dynamic):google.apps.script.properties.ScriptProperties;
	@:deprecated
	static function getKeys(key:String):Array<String>;
}
