package google.apps.script.properties;

@:native("UserProperties")
@:deprecated
extern class UserProperties
{
	@:deprecated
	static function setProperty(key:String,value:String):google.apps.script.properties.UserProperties;
	@:deprecated
	static function getProperties(key:String):Dynamic;
	@:deprecated
	static function getProperty(key:String):String;
	@:deprecated
	static function deleteAllProperties(key:String,value:String):google.apps.script.properties.UserProperties;
	@:deprecated
	static function deleteProperty(key:String):google.apps.script.properties.UserProperties;
	@:deprecated
	@:overload(function (properties:Dynamic,deleteAllOthers:Bool):google.apps.script.properties.UserProperties {})
	static function setProperties(properties:Dynamic):google.apps.script.properties.UserProperties;
	@:deprecated
	static function getKeys(key:String):Array<String>;
}
