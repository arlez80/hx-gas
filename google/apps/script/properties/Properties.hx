package google.apps.script.properties;

@:native("Properties")
extern class Properties
{
	function setProperty(key:String,value:String):google.apps.script.properties.Properties;
	function getProperties(key:String):Dynamic;
	function getProperty(key:String):String;
	function deleteAllProperties(key:String,value:String):google.apps.script.properties.Properties;
	function deleteProperty(key:String):google.apps.script.properties.Properties;
	@:overload(function (properties:Dynamic,deleteAllOthers:Bool):google.apps.script.properties.Properties {})
	function setProperties(properties:Dynamic):google.apps.script.properties.Properties;
	function getKeys(key:String):Array<String>;
}
