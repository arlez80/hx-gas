package google.apps.script.properties;

@:native("PropertiesService")
extern class PropertiesService
{
	static function getDocumentProperties(variableName:String):google.apps.script.properties.Properties;
	static function getScriptProperties(variableName:String):google.apps.script.properties.Properties;
	static function getUserProperties(variableName:String):google.apps.script.properties.Properties;
}
