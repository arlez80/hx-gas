package google.apps.script.sites;

@:require(hx_gas)
@:native("AttachmentType")
@:enum abstract AttachmentType(String) from String to String
{
	var WEB;
	var HOSTED;
}

