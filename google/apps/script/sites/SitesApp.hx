package google.apps.script.sites;

@:require(hx_gas)
@:native("SitesApp.AttachmentType")
@:enum abstract AttachmentType(String) from String to String
{
	var WEB;
	var HOSTED;
}

@:require(hx_gas)
@:native("SitesApp.PageType")
@:enum abstract PageType(String) from String to String
{
	var WEB_PAGE;
	var LIST_PAGE;
	var ANNOUNCEMENT;
	var ANNOUNCEMENTS_PAGE;
	var FILE_CABINET_PAGE;
}

@:native("SitesApp")
extern class SitesApp
{
	@:overload(function (domain:String,start:Int,max:Int):Array<google.apps.script.sites.Site> {})
	static function getAllSites(domain:String):Array<google.apps.script.sites.Site>;
	static function getActivePage(domain:String,name:String,title:String,summary:String):google.apps.script.sites.Page;
	@:overload(function (start:Int,max:Int):Array<google.apps.script.sites.Site> {})
	@:overload(function (domain:String):Array<google.apps.script.sites.Site> {})
	@:overload(function (domain:String,start:Int,max:Int):Array<google.apps.script.sites.Site> {})
	static function getSites(url:String):Array<google.apps.script.sites.Site>;
	static function copySite(domain:String,name:String,title:String,summary:String,site:google.apps.script.sites.Site):google.apps.script.sites.Site;
	static function getPageByUrl(url:String):google.apps.script.sites.Page;
	static function getActiveSite(domain:String,name:String,title:String,summary:String):google.apps.script.sites.Site;
	@:overload(function (domain:String,name:String):google.apps.script.sites.Site {})
	static function getSite(name:String):google.apps.script.sites.Site;
	static function createSite(domain:String,name:String,title:String,summary:String):google.apps.script.sites.Site;
	static function getSiteByUrl(url:String):google.apps.script.sites.Site;
}
