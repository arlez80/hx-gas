package google.apps.script.sites;

@:require(hx_gas)
@:native("PageType")
@:enum abstract PageType(String) from String to String
{
	var WEB_PAGE;
	var LIST_PAGE;
	var ANNOUNCEMENT;
	var ANNOUNCEMENTS_PAGE;
	var FILE_CABINET_PAGE;
}

