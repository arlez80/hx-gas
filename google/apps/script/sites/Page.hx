package google.apps.script.sites;

@:native("Page")
extern class Page
{
	@:overload(function (title:String,html:String,asDraft:Bool):google.apps.script.sites.Page {})
	function createAnnouncement(title:String,html:String):google.apps.script.sites.Page;
	function isDeleted(optOptions:Dynamic):Bool;
	function getIsDraft(options:Dynamic):Bool;
	function getUrl(optOptions:Dynamic):String;
	function createPageFromTemplate(title:String,name:String,template:google.apps.script.sites.Page):google.apps.script.sites.Page;
	function getTextContent(optOptions:Dynamic):String;
	function getPageType(optOptions:Dynamic):google.apps.script.sites.PageType;
	function getLastUpdated(options:Dynamic):Date;
	@:overload(function (query:String,options:{?type:Array<google.apps.script.sites.PageType>,?start:Int,?max:Int,?includeDrafts:Bool,?includeDeleted:Bool}):Array<google.apps.script.sites.Page> {})
	function search(query:String):Array<google.apps.script.sites.Page>;
	@:overload(function (options:{?type:Array<google.apps.script.sites.PageType>,?start:Int,?max:Int,?includeDrafts:Bool,?includeDeleted:Bool,?search:String}):Array<google.apps.script.sites.Page> {})
	function getChildren(name:String):Array<google.apps.script.sites.Page>;
	function createListPage(title:String,name:String,html:String,columnNames:Array<String>):google.apps.script.sites.Page;
	function deletePage(title:String,name:String,html:String):Void;
	function getColumns(options:Dynamic):Array<google.apps.script.sites.Column>;
	function getChildByName(name:String):google.apps.script.sites.Page;
	@:overload(function (optOptions:Dynamic):Array<google.apps.script.sites.Attachment> {})
	function getAttachments(optOptions:Dynamic):Array<google.apps.script.sites.Attachment>;
	function getAuthors(optOptions:Dynamic):Array<String>;
	function isTemplate(optOptions:Dynamic):Bool;
	function addColumn(name:String):google.apps.script.sites.Column;
	@:overload(function (optOptions:Dynamic):Array<google.apps.script.sites.Page> {})
	function getAnnouncements(options:Dynamic):Array<google.apps.script.sites.Page>;
	function publishAsTemplate(name:String):google.apps.script.sites.Page;
	function getHtmlContent(options:Dynamic):String;
	function getLastEdited(options:Dynamic):Date;
	function createAnnouncementsPage(title:String,name:String,html:String):google.apps.script.sites.Page;
	@:overload(function (optOptions:Dynamic):Array<google.apps.script.sites.ListItem> {})
	function getListItems(options:Dynamic):Array<google.apps.script.sites.ListItem>;
	function setTitle(title:String):google.apps.script.sites.Page;
	function createWebPage(title:String,name:String,html:String):google.apps.script.sites.Page;
	function addWebAttachment(title:String,description:String,url:String):google.apps.script.sites.Attachment;
	@:overload(function (blob:google.apps.script.base.BlobSource,description:String):google.apps.script.sites.Attachment {})
	function addHostedAttachment(blob:google.apps.script.base.BlobSource):google.apps.script.sites.Attachment;
	@:deprecated
	function getSelfLink(optOptions:Dynamic):String;
	function createFileCabinetPage(title:String,name:String,html:String):google.apps.script.sites.Page;
	@:deprecated
	@:overload(function (optOptions:Dynamic):Array<google.apps.script.sites.Comment> {})
	function getComments(content:String):Array<google.apps.script.sites.Comment>;
	function getName(optOptions:Dynamic):String;
	@:deprecated
	function getPageName(optOptions:Dynamic):String;
	function getTitle(optOptions:Dynamic):String;
	@:overload(function (options:{?type:Array<google.apps.script.sites.PageType>,?start:Int,?max:Int,?includeDrafts:Bool,?includeDeleted:Bool,?search:String}):Array<google.apps.script.sites.Page> {})
	function getAllDescendants(title:String,name:String,html:String):Array<google.apps.script.sites.Page>;
	function getDatePublished(options:Dynamic):Date;
	function setIsDraft(draft:Bool):google.apps.script.sites.Page;
	function setName(name:String):google.apps.script.sites.Page;
	function addListItem(values:Array<String>):google.apps.script.sites.ListItem;
	function setParent(parent:google.apps.script.sites.Page):google.apps.script.sites.Page;
	function setHtmlContent(html:String):google.apps.script.sites.Page;
	@:deprecated
	function addComment(content:String):google.apps.script.sites.Comment;
	function getParent(optOptions:Dynamic):google.apps.script.sites.Page;
}
