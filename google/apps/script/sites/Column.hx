package google.apps.script.sites;

@:native("Column")
extern class Column
{
	function getName(url:String):String;
	function deleteColumn(url:String):Void;
	function setName(name:String):google.apps.script.sites.Column;
	function getParent(url:String):google.apps.script.sites.Page;
}
