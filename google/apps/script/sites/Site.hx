package google.apps.script.sites;

@:native("Site")
extern class Site
{
	@:deprecated
	function createAnnouncement(title:String,html:String,parent:google.apps.script.sites.Page):google.apps.script.sites.Page;
	function getUrl(options:Dynamic):String;
	function getSummary(options:Dynamic):String;
	function getEditors(options:Dynamic):Array<google.apps.script.base.User>;
	@:deprecated
	function getFileCabinetPages(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.Page>;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.sites.Site {})
	function addViewer(emailAddress:String):google.apps.script.sites.Site;
	function getViewers(options:Dynamic):Array<google.apps.script.base.User>;
	@:deprecated
	function createComment(inReplyTo:String,html:String,parent:google.apps.script.sites.Page):google.apps.script.sites.Comment;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.sites.Site {})
	function removeEditor(emailAddress:String):google.apps.script.sites.Site;
	@:overload(function (query:String,options:{?type:Array<google.apps.script.sites.PageType>,?start:Int,?max:Int,?includeDrafts:Bool,?includeDeleted:Bool}):Array<google.apps.script.sites.Page> {})
	function search(query:String):Array<google.apps.script.sites.Page>;
	function setTitle(title:String):google.apps.script.sites.Site;
	function createAnnouncementsPage(title:String,name:String,html:String):google.apps.script.sites.Page;
	@:deprecated
	function getListItems(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.ListItem>;
	function createWebPage(title:String,name:String,html:String):google.apps.script.sites.Page;
	@:overload(function (options:{?type:Array<google.apps.script.sites.PageType>,?start:Int,?max:Int,?includeDrafts:Bool,?includeDeleted:Bool,?search:String}):Array<google.apps.script.sites.Page> {})
	function getChildren(name:String):Array<google.apps.script.sites.Page>;
	function setTheme(theme:String):google.apps.script.sites.Site;
	@:deprecated
	function getComments(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.Comment>;
	@:deprecated
	@:overload(function (user:google.apps.script.base.User):google.apps.script.sites.Site {})
	function addCollaborator(email:String):google.apps.script.sites.Site;
	@:deprecated
	function getWebPages(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.Page>;
	@:deprecated
	function createWebAttachment(title:String,url:String,parent:google.apps.script.sites.Page):google.apps.script.sites.Attachment;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.sites.Site {})
	function addEditor(emailAddress:String):google.apps.script.sites.Site;
	@:deprecated
	function getSiteName(title:String,url:String,parent:google.apps.script.sites.Page):String;
	@:deprecated
	@:overload(function (user:google.apps.script.base.User):google.apps.script.sites.Site {})
	function removeCollaborator(email:String):google.apps.script.sites.Site;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.sites.Site {})
	function removeOwner(email:String):google.apps.script.sites.Site;
	@:deprecated
	function getWebAttachments(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.Attachment>;
	function getTemplates(options:Dynamic):Array<google.apps.script.sites.Page>;
	function createPageFromTemplate(title:String,name:String,template:google.apps.script.sites.Page):google.apps.script.sites.Page;
	function getName(options:Dynamic):String;
	function addEditors(emailAddresses:Array<String>):google.apps.script.sites.Site;
	function createListPage(title:String,name:String,html:String,columnNames:Array<String>):google.apps.script.sites.Page;
	function getChildByName(name:String):google.apps.script.sites.Page;
	@:deprecated
	function getCollaborators(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.base.User>;
	@:deprecated
	function getAttachments(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.Attachment>;
	function getTheme(options:Dynamic):String;
	@:deprecated
	function createListItem(html:String,columnNames:Array<String>,values:Array<String>,parent:google.apps.script.sites.Page):google.apps.script.sites.ListItem;
	function addViewers(emailAddresses:Array<String>):google.apps.script.sites.Site;
	@:deprecated
	function getAnnouncements(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.Page>;
	@:deprecated
	function getAnnouncementsPages(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.Page>;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.sites.Site {})
	function addOwner(email:String):google.apps.script.sites.Site;
	function setSummary(summary:String):google.apps.script.sites.Site;
	function getOwners(options:Dynamic):Array<google.apps.script.base.User>;
	@:deprecated
	function getSelfLink(title:String,url:String,parent:google.apps.script.sites.Page):String;
	function createFileCabinetPage(title:String,name:String,html:String):google.apps.script.sites.Page;
	@:deprecated
	function getListPages(title:String,url:String,parent:google.apps.script.sites.Page):Array<google.apps.script.sites.Page>;
	function getTitle(options:Dynamic):String;
	@:overload(function (options:{?type:Array<google.apps.script.sites.PageType>,?start:Int,?max:Int,?includeDrafts:Bool,?includeDeleted:Bool,?search:String}):Array<google.apps.script.sites.Page> {})
	function getAllDescendants(title:String,name:String,html:String):Array<google.apps.script.sites.Page>;
	@:deprecated
	function deleteSite(title:String,url:String,parent:google.apps.script.sites.Page):Void;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.sites.Site {})
	function removeViewer(emailAddress:String):google.apps.script.sites.Site;
}
