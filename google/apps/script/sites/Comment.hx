package google.apps.script.sites;

@:native("Comment")
extern class Comment
{
	function getAuthorEmail(name:String):String;
	function setParent(parent:google.apps.script.sites.Page):google.apps.script.sites.Comment;
	function setContent(content:String):google.apps.script.sites.Comment;
	function getContent(name:String):String;
	function getDatePublished(name:String):Date;
	function getLastUpdated(name:String):Date;
	function getAuthorName(name:String):String;
	function deleteComment(name:String):Void;
	function getParent(name:String):google.apps.script.sites.Page;
}
