package google.apps.script.sites;

@:native("Attachment")
extern class Attachment
{
	function getUrl(contentType:String):String;
	function setFrom(blob:google.apps.script.base.BlobSource):google.apps.script.sites.Attachment;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function getLastUpdated(contentType:String):Date;
	function getDatePublished(contentType:String):Date;
	function deleteAttachment(domain:String,start:Int,max:Int):Void;
	function setUrl(url:String):google.apps.script.sites.Attachment;
	function setTitle(title:String):google.apps.script.sites.Attachment;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function getDescription(contentType:String):String;
	function getAttachmentType(contentType:String):google.apps.script.sites.AttachmentType;
	function setContentType(contentType:String):google.apps.script.sites.Attachment;
	function getContentType(contentType:String):String;
	function setDescription(description:String):google.apps.script.sites.Attachment;
	function setParent(parent:google.apps.script.sites.Page):google.apps.script.sites.Attachment;
	function getTitle(contentType:String):String;
	function getParent(contentType:String):google.apps.script.sites.Page;
}
