package google.apps.script.sites;

@:native("ListItem")
extern class ListItem
{
	function deleteListItem(parent:google.apps.script.sites.Page):Void;
	function getValueByIndex(index:Int):String;
	function setParent(parent:google.apps.script.sites.Page):google.apps.script.sites.ListItem;
	function setValueByName(name:String,value:String):google.apps.script.sites.ListItem;
	function getLastUpdated(parent:google.apps.script.sites.Page):Date;
	function getDatePublished(parent:google.apps.script.sites.Page):Date;
	function getValueByName(name:String):String;
	function setValueByIndex(index:Int,value:String):google.apps.script.sites.ListItem;
	function getParent(parent:google.apps.script.sites.Page):google.apps.script.sites.Page;
}
