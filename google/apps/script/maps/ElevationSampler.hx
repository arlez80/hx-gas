package google.apps.script.maps;

@:native("ElevationSampler")
extern class ElevationSampler
{
	@:overload(function (encodedPolyline:String,numSamples:Int):Dynamic {})
	function samplePath(points:Array<Float>,numSamples:Int):Dynamic;
	@:overload(function (encodedPolyline:String):Dynamic {})
	function sampleLocations(points:Array<Float>):Dynamic;
	function sampleLocation(latitude:Float,longitude:Float):Dynamic;
}
