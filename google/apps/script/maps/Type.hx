package google.apps.script.maps;

@:require(hx_gas)
@:native("Type")
@:enum abstract Type(String) from String to String
{
	var ROADMAP;
	var SATELLITE;
	var TERRAIN;
	var HYBRID;
}

