package google.apps.script.maps;

@:require(hx_gas)
@:native("StaticMapEnums.Color")
@:enum abstract Color(String) from String to String
{
	var BLACK;
	var BROWN;
	var GREEN;
	var PURPLE;
	var YELLOW;
	var BLUE;
	var GRAY;
	var ORANGE;
	var RED;
	var WHITE;
}

@:require(hx_gas)
@:native("StaticMapEnums.Format")
@:enum abstract Format(String) from String to String
{
	var PNG;
	var PNG8;
	var PNG32;
	var GIF;
	var JPG;
	var JPG_BASELINE;
}

@:require(hx_gas)
@:native("StaticMapEnums.MarkerSize")
@:enum abstract MarkerSize(String) from String to String
{
	var TINY;
	var MID;
	var SMALL;
}

@:require(hx_gas)
@:native("StaticMapEnums.Type")
@:enum abstract Type(String) from String to String
{
	var ROADMAP;
	var SATELLITE;
	var TERRAIN;
	var HYBRID;
}

@:native("StaticMapEnums")
extern class StaticMapEnums
{
}
