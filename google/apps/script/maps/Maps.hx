package google.apps.script.maps;

@:require(hx_gas)
@:native("Maps.DirectionFinderEnums")
@:enum abstract DirectionFinderEnums(String) from String to String
{
	var Avoid;
	var Mode;
}

@:require(hx_gas)
@:native("Maps.StaticMapEnums")
@:enum abstract StaticMapEnums(String) from String to String
{
	var Color;
	var Format;
	var MarkerSize;
	var Type;
}

@:native("Maps")
extern class Maps
{
	static function decodePolyline(polyline:String):Array<Float>;
	static function encodePolyline(points:Array<Float>):String;
	static function newGeocoder(points:Array<Float>):google.apps.script.maps.Geocoder;
	static function setAuthentication(clientId:String,signingKey:String):Void;
	static function newElevationSampler(points:Array<Float>):google.apps.script.maps.ElevationSampler;
	static function newStaticMap(points:Array<Float>):google.apps.script.maps.StaticMap;
	static function newDirectionFinder(points:Array<Float>):google.apps.script.maps.DirectionFinder;
}
