package google.apps.script.maps;

@:require(hx_gas)
@:native("Mode")
@:enum abstract Mode(String) from String to String
{
	var DRIVING;
	var WALKING;
	var BICYCLING;
	var TRANSIT;
}

