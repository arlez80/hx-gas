package google.apps.script.maps;

@:native("Geocoder")
extern class Geocoder
{
	function setBounds(swLatitude:Float,swLongitude:Float,neLatitude:Float,neLongitude:Float):google.apps.script.maps.Geocoder;
	function setLanguage(language:String):google.apps.script.maps.Geocoder;
	function geocode(address:String):Dynamic;
	@:overload(function (swLatitude:Float,swLongitude:Float,neLatitude:Float,neLongitude:Float):Dynamic {})
	function reverseGeocode(latitude:Float,longitude:Float):Dynamic;
	function setRegion(region:String):google.apps.script.maps.Geocoder;
}
