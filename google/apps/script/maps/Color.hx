package google.apps.script.maps;

@:require(hx_gas)
@:native("Color")
@:enum abstract Color(String) from String to String
{
	var BLACK;
	var BROWN;
	var GREEN;
	var PURPLE;
	var YELLOW;
	var BLUE;
	var GRAY;
	var ORANGE;
	var RED;
	var WHITE;
}

