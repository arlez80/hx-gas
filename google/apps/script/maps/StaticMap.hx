package google.apps.script.maps;

@:native("StaticMap")
extern class StaticMap
{
	function setSize(width:Int,height:Int):google.apps.script.maps.StaticMap;
	function endPath(address:String):google.apps.script.maps.StaticMap;
	function clearVisibles(address:String):google.apps.script.maps.StaticMap;
	function beginPath(address:String):google.apps.script.maps.StaticMap;
	function setMarkerStyle(size:String,color:String,label:String):google.apps.script.maps.StaticMap;
	function setFormat(format:String):google.apps.script.maps.StaticMap;
	@:overload(function (address:String):google.apps.script.maps.StaticMap {})
	function addMarker(latitude:Float,longitude:Float):google.apps.script.maps.StaticMap;
	function getMapUrl(contentType:String):String;
	function clearPaths(address:String):google.apps.script.maps.StaticMap;
	function setLanguage(language:String):google.apps.script.maps.StaticMap;
	function addPoint(latitude:Float,longitude:Float):google.apps.script.maps.StaticMap;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function getMapImage(contentType:String):Array<Int>;
	function setPathStyle(weight:Int,color:String,fillColor:String):google.apps.script.maps.StaticMap;
	@:overload(function (address:String):google.apps.script.maps.StaticMap {})
	function addVisible(latitude:Float,longitude:Float):google.apps.script.maps.StaticMap;
	@:overload(function (address:String):google.apps.script.maps.StaticMap {})
	function setCenter(latitude:Float,longitude:Float):google.apps.script.maps.StaticMap;
	function setMapType(mapType:String):google.apps.script.maps.StaticMap;
	@:overload(function (polyline:String):google.apps.script.maps.StaticMap {})
	function addPath(points:Array<Float>):google.apps.script.maps.StaticMap;
	function clearMarkers(address:String):google.apps.script.maps.StaticMap;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function setCustomMarkerStyle(imageUrl:String,useShadow:Bool):google.apps.script.maps.StaticMap;
	function setZoom(zoom:Int):google.apps.script.maps.StaticMap;
	function addAddress(address:String):google.apps.script.maps.StaticMap;
	function setMobile(useMobileTiles:Bool):google.apps.script.maps.StaticMap;
}
