package google.apps.script.maps;

@:native("DirectionFinder")
extern class DirectionFinder
{
	function setOptimizeWaypoints(optimizeOrder:Bool):google.apps.script.maps.DirectionFinder;
	@:overload(function (address:String):google.apps.script.maps.DirectionFinder {})
	function addWaypoint(latitude:Float,longitude:Float):google.apps.script.maps.DirectionFinder;
	function clearWaypoints(address:String):google.apps.script.maps.DirectionFinder;
	function getDirections(address:String):Dynamic;
	function setLanguage(language:String):google.apps.script.maps.DirectionFinder;
	function setArrive(time:Date):google.apps.script.maps.DirectionFinder;
	function setMode(mode:String):google.apps.script.maps.DirectionFinder;
	function setRegion(region:String):google.apps.script.maps.DirectionFinder;
	function setAvoid(avoid:String):google.apps.script.maps.DirectionFinder;
	function setAlternatives(useAlternatives:Bool):google.apps.script.maps.DirectionFinder;
	@:overload(function (address:String):google.apps.script.maps.DirectionFinder {})
	function setOrigin(latitude:Float,longitude:Float):google.apps.script.maps.DirectionFinder;
	@:overload(function (address:String):google.apps.script.maps.DirectionFinder {})
	function setDestination(latitude:Float,longitude:Float):google.apps.script.maps.DirectionFinder;
	function setDepart(time:Date):google.apps.script.maps.DirectionFinder;
}
