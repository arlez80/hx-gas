package google.apps.script.maps;

@:require(hx_gas)
@:native("MarkerSize")
@:enum abstract MarkerSize(String) from String to String
{
	var TINY;
	var MID;
	var SMALL;
}

