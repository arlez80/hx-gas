package google.apps.script.maps;

@:require(hx_gas)
@:native("DirectionFinderEnums.Avoid")
@:enum abstract Avoid(String) from String to String
{
	var TOLLS;
	var HIGHWAYS;
}

@:require(hx_gas)
@:native("DirectionFinderEnums.Mode")
@:enum abstract Mode(String) from String to String
{
	var DRIVING;
	var WALKING;
	var BICYCLING;
	var TRANSIT;
}

@:native("DirectionFinderEnums")
extern class DirectionFinderEnums
{
}
