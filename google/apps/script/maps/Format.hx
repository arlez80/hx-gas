package google.apps.script.maps;

@:require(hx_gas)
@:native("Format")
@:enum abstract Format(String) from String to String
{
	var PNG;
	var PNG8;
	var PNG32;
	var GIF;
	var JPG;
	var JPG_BASELINE;
}

