package google.apps.script.url_fetch;

@:native("HTTPResponse")
extern class HTTPResponse
{
	function getAllHeaders(url:String,params:Dynamic):Dynamic;
	@:overload(function (charset:String):String {})
	function getContentText(contentType:String):String;
	function getHeaders(charset:String):Dynamic;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function getContent(contentType:String):Array<Int>;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function getResponseCode(charset:String):Int;
}
