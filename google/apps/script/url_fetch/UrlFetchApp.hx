package google.apps.script.url_fetch;

@:native("UrlFetchApp")
extern class UrlFetchApp
{
	static function fetchAll(requests:Array<Dynamic>):Array<google.apps.script.url_fetch.HTTPResponse>;
	@:overload(function (url:String,params:Dynamic):google.apps.script.url_fetch.HTTPResponse {})
	static function fetch(url:String):google.apps.script.url_fetch.HTTPResponse;
	@:overload(function (url:String,params:Dynamic):Dynamic {})
	static function getRequest(url:String):Dynamic;
}
