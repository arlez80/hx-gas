package google.apps.script.jdbc;

@:native("JdbcClob")
extern class JdbcClob
{
	function getAppsScriptBlob(parameterName:String,val:String):google.apps.script.base.Blob;
	@:overload(function (search:String,start:Int):Int {})
	function position(search:google.apps.script.jdbc.JdbcClob,start:Int):Int;
	function free(parameterName:String,val:String):Void;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function truncate(length:Int):Void;
	function getSubString(position:Int,length:Int):String;
	function length(position:Int,length:Int):Int;
	@:overload(function (position:Int,blobSource:google.apps.script.base.BlobSource,offset:Int,len:Int):Int {})
	@:overload(function (position:Int,value:String):Int {})
	@:overload(function (position:Int,value:String,offset:Int,len:Int):Int {})
	function setString(position:Int,blobSource:google.apps.script.base.BlobSource):Int;
}
