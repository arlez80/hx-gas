package google.apps.script.jdbc;

@:native("JdbcResultSetMetaData")
extern class JdbcResultSetMetaData
{
	function isAutoIncrement(column:Int):Bool;
	function isReadOnly(column:Int):Bool;
	function getColumnCount(column:Int):Int;
	function isWritable(column:Int):Bool;
	function getColumnLabel(column:Int):String;
	function getScale(column:Int):Int;
	function getColumnType(column:Int):Int;
	function isCurrency(column:Int):Bool;
	function getColumnTypeName(column:Int):String;
	function isSigned(column:Int):Bool;
	function getTableName(column:Int):String;
	function isSearchable(column:Int):Bool;
	function isCaseSensitive(column:Int):Bool;
	function getPrecision(column:Int):Int;
	function getCatalogName(column:Int):String;
	function isNullable(column:Int):Int;
	function getColumnClassName(column:Int):String;
	function getColumnName(column:Int):String;
	function isDefinitelyWritable(column:Int):Bool;
	function getColumnDisplaySize(column:Int):Int;
	function getSchemaName(column:Int):String;
}
