package google.apps.script.jdbc;

@:native("JdbcSQLXML")
extern class JdbcSQLXML
{
	function setString(value:String):Void;
	function free(column:Int):Void;
	function getString(column:Int):String;
}
