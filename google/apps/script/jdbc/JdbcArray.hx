package google.apps.script.jdbc;

@:native("JdbcArray")
extern class JdbcArray
{
	function getBaseTypeName(index:Int,count:Int):String;
	function getBaseType(index:Int,count:Int):Int;
	function free(timestamp:String):Void;
	@:overload(function (index:Int,count:Int):Dynamic {})
	function getArray(timestamp:String):Dynamic;
	@:overload(function (index:Int,count:Int):google.apps.script.jdbc.JdbcResultSet {})
	function getResultSet(index:Int,count:Int):google.apps.script.jdbc.JdbcResultSet;
}
