package google.apps.script.jdbc;

@:native("JdbcSavepoint")
extern class JdbcSavepoint
{
	function getSavepointId(value:String):Int;
	function getSavepointName(value:String):String;
}
