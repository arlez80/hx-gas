package google.apps.script.jdbc;

@:native("JdbcTimestamp")
extern class JdbcTimestamp
{
	function getMinutes(when:google.apps.script.jdbc.JdbcTimestamp):Int;
	function getHours(when:google.apps.script.jdbc.JdbcTimestamp):Int;
	function getMonth(when:google.apps.script.jdbc.JdbcTimestamp):Int;
	function getTime(when:google.apps.script.jdbc.JdbcTimestamp):Int;
	function setNanos(nanoseconds:Int):Void;
	function getYear(when:google.apps.script.jdbc.JdbcTimestamp):Int;
	function getNanos(when:google.apps.script.jdbc.JdbcTimestamp):Int;
	function getSeconds(when:google.apps.script.jdbc.JdbcTimestamp):Int;
	function setYear(year:Int):Void;
	function getDate(when:google.apps.script.jdbc.JdbcTimestamp):Int;
	function setHours(hours:Int):Void;
	function after(when:google.apps.script.jdbc.JdbcTimestamp):Bool;
	function before(when:google.apps.script.jdbc.JdbcTimestamp):Bool;
	function setMonth(month:Int):Void;
	function setTime(milliseconds:Int):Void;
	function setMinutes(minutes:Int):Void;
	function setDate(date:Int):Void;
	function setSeconds(seconds:Int):Void;
}
