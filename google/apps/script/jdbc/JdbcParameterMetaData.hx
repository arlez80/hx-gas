package google.apps.script.jdbc;

@:native("JdbcParameterMetaData")
extern class JdbcParameterMetaData
{
	function isSigned(param:Int):Bool;
	function getParameterType(param:Int):Int;
	function getPrecision(param:Int):Int;
	function isNullable(param:Int):Int;
	function getScale(param:Int):Int;
	function getParameterMode(param:Int):Int;
	function getParameterClassName(param:Int):String;
	function getParameterCount(param:Int):Int;
	function getParameterTypeName(param:Int):String;
}
