package google.apps.script.jdbc;

@:native("Jdbc")
extern class Jdbc
{
	static function parseDate(date:String):google.apps.script.jdbc.JdbcDate;
	static function newTimestamp(milliseconds:Int):google.apps.script.jdbc.JdbcTimestamp;
	static function parseTimestamp(timestamp:String):google.apps.script.jdbc.JdbcTimestamp;
	static function parseTime(time:String):google.apps.script.jdbc.JdbcTime;
	static function newTime(milliseconds:Int):google.apps.script.jdbc.JdbcTime;
	static function newDate(milliseconds:Int):google.apps.script.jdbc.JdbcDate;
	@:overload(function (url:String,info:Dynamic):google.apps.script.jdbc.JdbcConnection {})
	@:overload(function (url:String,userName:String,password:String):google.apps.script.jdbc.JdbcConnection {})
	static function getCloudSqlConnection(url:String):google.apps.script.jdbc.JdbcConnection;
	@:overload(function (url:String,info:Dynamic):google.apps.script.jdbc.JdbcConnection {})
	@:overload(function (url:String,userName:String,password:String):google.apps.script.jdbc.JdbcConnection {})
	static function getConnection(url:String):google.apps.script.jdbc.JdbcConnection;
}
