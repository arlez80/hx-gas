package google.apps.script.jdbc;

@:native("JdbcRef")
extern class JdbcRef
{
	function setObject(object:Dynamic):Void;
	function getBaseTypeName(parameterIndex:Int,x:String):String;
	function getObject(parameterIndex:Int,x:String):Dynamic;
}
