package google.apps.script.jdbc;

@:native("JdbcBlob")
extern class JdbcBlob
{
	function getAppsScriptBlob(index:Int,count:Int):google.apps.script.base.Blob;
	function getBytes(position:Int,length:Int):Array<Int>;
	function free(index:Int,count:Int):Void;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function truncate(length:Int):Void;
	@:overload(function (pattern:google.apps.script.jdbc.JdbcBlob,start:Int):Int {})
	function position(pattern:Array<Int>,start:Int):Int;
	function length(position:Int,length:Int):Int;
	@:overload(function (position:Int,blobSource:google.apps.script.base.BlobSource,offset:Int,length:Int):Int {})
	@:overload(function (position:Int,bytes:Array<Int>):Int {})
	@:overload(function (position:Int,bytes:Array<Int>,offset:Int,length:Int):Int {})
	function setBytes(position:Int,blobSource:google.apps.script.base.BlobSource):Int;
}
