package google.apps.script.jdbc;

@:native("JdbcResultSet")
extern class JdbcResultSet
{
	function isClosed(columnLabel:String):Bool;
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcRef {})
	function getRef(columnIndex:Int):google.apps.script.jdbc.JdbcRef;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcRef):Void {})
	function updateRef(columnIndex:Int,x:google.apps.script.jdbc.JdbcRef):Void;
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcBlob {})
	function getBlob(columnIndex:Int):google.apps.script.jdbc.JdbcBlob;
	function moveToCurrentRow(columnLabel:String):Void;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcSQLXML):Void {})
	function updateSQLXML(columnIndex:Int,x:google.apps.script.jdbc.JdbcSQLXML):Void;
	function findColumn(columnLabel:String):Int;
	@:overload(function (columnLabel:String,x:Int):Void {})
	function updateInt(columnIndex:Int,x:Int):Void;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcTimestamp):Void {})
	function updateTimestamp(columnIndex:Int,x:google.apps.script.jdbc.JdbcTimestamp):Void;
	function close(row:Int):Void;
	@:overload(function (columnLabel:String):Array<Int> {})
	function getBytes(columnIndex:Int):Array<Int>;
	function setFetchDirection(direction:Int):Void;
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcArray {})
	function getArray(columnIndex:Int):google.apps.script.jdbc.JdbcArray;
	function isLast(columnLabel:String):Bool;
	function isAfterLast(columnLabel:String):Bool;
	function rowInserted(rows:Int):Bool;
	@:overload(function (columnLabel:String):Int {})
	function getLong(columnIndex:Int):Int;
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcRowId {})
	function getRowId(columnIndex:Int):google.apps.script.jdbc.JdbcRowId;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcArray):Void {})
	function updateArray(columnIndex:Int,x:google.apps.script.jdbc.JdbcArray):Void;
	@:overload(function (columnIndex:Int,timeZone:String):google.apps.script.jdbc.JdbcTime {})
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcTime {})
	@:overload(function (columnLabel:String,timeZone:String):google.apps.script.jdbc.JdbcTime {})
	function getTime(columnIndex:Int):google.apps.script.jdbc.JdbcTime;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcClob):Void {})
	function updateNClob(columnIndex:Int,x:google.apps.script.jdbc.JdbcClob):Void;
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcClob {})
	function getNClob(columnIndex:Int):google.apps.script.jdbc.JdbcClob;
	@:overload(function (columnIndex:Int,timeZone:String):google.apps.script.jdbc.JdbcTimestamp {})
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcTimestamp {})
	@:overload(function (columnLabel:String,timeZone:String):google.apps.script.jdbc.JdbcTimestamp {})
	function getTimestamp(columnIndex:Int):google.apps.script.jdbc.JdbcTimestamp;
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcClob {})
	function getClob(columnIndex:Int):google.apps.script.jdbc.JdbcClob;
	function clearWarnings(row:Int):Void;
	function last(columnLabel:String):Bool;
	@:overload(function (columnLabel:String):Int {})
	function getInt(columnIndex:Int):Int;
	@:overload(function (columnLabel:String):Float {})
	function getBigDecimal(columnIndex:Int):Float;
	function next(columnLabel:String):Bool;
	@:overload(function (columnLabel:String):Void {})
	function updateNull(columnIndex:Int):Void;
	function cancelRowUpdates(row:Int):Void;
	function first(columnLabel:String):Bool;
	function getStatement(columnLabel:String):google.apps.script.jdbc.JdbcStatement;
	function setFetchSize(rows:Int):Void;
	@:overload(function (columnLabel:String):Float {})
	function getDouble(columnIndex:Int):Float;
	@:overload(function (columnLabel:String,x:Array<Int>):Void {})
	function updateBytes(columnIndex:Int,x:Array<Int>):Void;
	function isBeforeFirst(columnLabel:String):Bool;
	function absolute(row:Int):Bool;
	function deleteRow(row:Int):Void;
	function beforeFirst(row:Int):Void;
	function moveToInsertRow(columnLabel:String):Void;
	@:overload(function (columnLabel:String):String {})
	function getNString(columnIndex:Int):String;
	function rowUpdated(rows:Int):Bool;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcTime):Void {})
	function updateTime(columnIndex:Int,x:google.apps.script.jdbc.JdbcTime):Void;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcClob):Void {})
	function updateClob(columnIndex:Int,x:google.apps.script.jdbc.JdbcClob):Void;
	function getHoldability(columnLabel:String):Int;
	@:overload(function (columnLabel:String,x:String):Void {})
	function updateString(columnIndex:Int,x:String):Void;
	function wasNull(columnLabel:String,x:google.apps.script.jdbc.JdbcTimestamp):Bool;
	function getType(columnLabel:String,timeZone:String):Int;
	@:overload(function (columnIndex:Int,timeZone:String):google.apps.script.jdbc.JdbcDate {})
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcDate {})
	@:overload(function (columnLabel:String,timeZone:String):google.apps.script.jdbc.JdbcDate {})
	function getDate(columnIndex:Int):google.apps.script.jdbc.JdbcDate;
	function getConcurrency(columnLabel:String):Int;
	@:overload(function (columnIndex:Int,x:Dynamic,scaleOrLength:Int):Void {})
	@:overload(function (columnLabel:String,x:Dynamic):Void {})
	@:overload(function (columnLabel:String,x:Dynamic,scaleOrLength:Int):Void {})
	function updateObject(columnIndex:Int,x:Dynamic):Void;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcDate):Void {})
	function updateDate(columnIndex:Int,x:google.apps.script.jdbc.JdbcDate):Void;
	@:overload(function (columnLabel:String):Int {})
	function getShort(columnIndex:Int):Int;
	@:overload(function (columnLabel:String,x:Int):Void {})
	function updateShort(columnIndex:Int,x:Int):Void;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcBlob):Void {})
	function updateBlob(columnIndex:Int,x:google.apps.script.jdbc.JdbcBlob):Void;
	@:overload(function (columnLabel:String,x:Float):Void {})
	function updateDouble(columnIndex:Int,x:Float):Void;
	function rowDeleted(rows:Int):Bool;
	@:overload(function (columnLabel:String):Float {})
	function getFloat(columnIndex:Int):Float;
	@:overload(function (columnLabel:String,x:Float):Void {})
	function updateBigDecimal(columnIndex:Int,x:Float):Void;
	function getFetchSize(columnLabel:String):Int;
	@:overload(function (columnLabel:String):google.apps.script.jdbc.JdbcSQLXML {})
	function getSQLXML(columnIndex:Int):google.apps.script.jdbc.JdbcSQLXML;
	@:overload(function (columnLabel:String,x:String):Void {})
	function updateNString(columnIndex:Int,x:String):Void;
	@:overload(function (columnLabel:String,x:Int):Void {})
	function updateByte(columnIndex:Int,x:Int):Void;
	function relative(rows:Int):Bool;
	function updateRow(columnLabel:String,x:google.apps.script.jdbc.JdbcRef):Void;
	@:overload(function (columnLabel:String):String {})
	function getString(columnIndex:Int):String;
	function previous(columnLabel:String):Bool;
	function getRow(columnLabel:String):Int;
	function getFetchDirection(columnLabel:String):Int;
	@:overload(function (columnLabel:String,x:google.apps.script.jdbc.JdbcRowId):Void {})
	function updateRowId(columnIndex:Int,x:google.apps.script.jdbc.JdbcRowId):Void;
	@:overload(function (columnLabel:String):Int {})
	function getByte(columnIndex:Int):Int;
	function isFirst(columnLabel:String):Bool;
	@:overload(function (columnLabel:String):Bool {})
	function getBoolean(columnIndex:Int):Bool;
	function afterLast(row:Int):Void;
	function getCursorName(columnLabel:String):String;
	@:overload(function (columnLabel:String):String {})
	function getURL(columnIndex:Int):String;
	@:overload(function (columnLabel:String,x:Int):Void {})
	function updateLong(columnIndex:Int,x:Int):Void;
	@:overload(function (columnLabel:String,x:Float):Void {})
	function updateFloat(columnIndex:Int,x:Float):Void;
	function getWarnings(columnLabel:String):Array<String>;
	function refreshRow(columnLabel:String):Void;
	function insertRow(columnLabel:String):Void;
	@:overload(function (columnLabel:String):Dynamic {})
	function getObject(columnIndex:Int):Dynamic;
	function getMetaData(columnLabel:String):google.apps.script.jdbc.JdbcResultSetMetaData;
	@:overload(function (columnLabel:String,x:Bool):Void {})
	function updateBoolean(columnIndex:Int,x:Bool):Void;
}
