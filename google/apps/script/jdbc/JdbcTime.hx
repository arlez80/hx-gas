package google.apps.script.jdbc;

@:native("JdbcTime")
extern class JdbcTime
{
	function getMinutes(when:google.apps.script.jdbc.JdbcTime):Int;
	function getHours(when:google.apps.script.jdbc.JdbcTime):Int;
	function setHours(hours:Int):Void;
	function after(when:google.apps.script.jdbc.JdbcTime):Bool;
	function getTime(when:google.apps.script.jdbc.JdbcTime):Int;
	function setMinutes(minutes:Int):Void;
	function setTime(milliseconds:Int):Void;
	function getSeconds(when:google.apps.script.jdbc.JdbcTime):Int;
	function before(when:google.apps.script.jdbc.JdbcTime):Bool;
	function setSeconds(seconds:Int):Void;
}
