package google.apps.script.jdbc;

@:native("JdbcDate")
extern class JdbcDate
{
	function getDate(when:google.apps.script.jdbc.JdbcDate):Int;
	function before(when:google.apps.script.jdbc.JdbcDate):Bool;
	function setMonth(month:Int):Void;
	function after(when:google.apps.script.jdbc.JdbcDate):Bool;
	function getTime(when:google.apps.script.jdbc.JdbcDate):Int;
	function setDate(date:Int):Void;
	function getYear(when:google.apps.script.jdbc.JdbcDate):Int;
	function setTime(milliseconds:Int):Void;
	function getMonth(when:google.apps.script.jdbc.JdbcDate):Int;
	function setYear(year:Int):Void;
}
