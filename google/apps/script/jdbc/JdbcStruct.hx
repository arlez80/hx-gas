package google.apps.script.jdbc;

@:native("JdbcStruct")
extern class JdbcStruct
{
	function getSQLTypeName(seconds:Int):String;
	function getAttributes(seconds:Int):Array<Dynamic>;
}
