package google.apps.script.jdbc;

@:native("JdbcRowId")
extern class JdbcRowId
{
	function getBytes(column:Int):Array<Int>;
}
