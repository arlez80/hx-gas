package google.apps.script.xml_service;

@:native("ProcessingInstruction")
extern class ProcessingInstruction
{
	function getTarget(omitEncoding:Bool):String;
	function detach(omitEncoding:Bool):google.apps.script.xml_service.Content;
	function getData(omitEncoding:Bool):String;
	function getParentElement(omitEncoding:Bool):google.apps.script.xml_service.Element;
	function getValue(omitEncoding:Bool):String;
}
