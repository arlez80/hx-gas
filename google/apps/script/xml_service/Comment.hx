package google.apps.script.xml_service;

@:native("Comment")
extern class Comment
{
	function getText(text:String):String;
	function detach(text:String):google.apps.script.xml_service.Content;
	function getValue(text:String):String;
	function getParentElement(text:String):google.apps.script.xml_service.Element;
	function setText(text:String):google.apps.script.xml_service.Comment;
}
