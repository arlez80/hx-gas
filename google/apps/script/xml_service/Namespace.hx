package google.apps.script.xml_service;

@:native("Namespace")
extern class Namespace
{
	function getURI(omitEncoding:Bool):String;
	function getPrefix(omitEncoding:Bool):String;
}
