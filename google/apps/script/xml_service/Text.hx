package google.apps.script.xml_service;

@:native("Text")
extern class Text
{
	function append(text:String):google.apps.script.xml_service.Text;
	function setText(text:String):google.apps.script.xml_service.Text;
	function detach(text:String):google.apps.script.xml_service.Content;
	function getValue(text:String):String;
	function getText(text:String):String;
	function getParentElement(text:String):google.apps.script.xml_service.Element;
}
