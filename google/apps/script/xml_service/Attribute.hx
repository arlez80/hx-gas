package google.apps.script.xml_service;

@:native("Attribute")
extern class Attribute
{
	function setNamespace(namespace:google.apps.script.xml_service.Namespace):google.apps.script.xml_service.Attribute;
	function setValue(value:String):google.apps.script.xml_service.Attribute;
	function setName(name:String):google.apps.script.xml_service.Attribute;
	function getName(xml:String):String;
	function getValue(xml:String):String;
	function getNamespace(xml:String):google.apps.script.xml_service.Namespace;
}
