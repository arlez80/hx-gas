package google.apps.script.xml_service;

@:native("Content")
extern class Content
{
	function asElement(text:String):google.apps.script.xml_service.Element;
	function asProcessingInstruction(text:String):google.apps.script.xml_service.ProcessingInstruction;
	function detach(text:String):google.apps.script.xml_service.Content;
	function asEntityRef(text:String):google.apps.script.xml_service.EntityRef;
	function asComment(text:String):google.apps.script.xml_service.Comment;
	function asCdata(text:String):google.apps.script.xml_service.Cdata;
	function asDocType(text:String):google.apps.script.xml_service.DocType;
	function asText(text:String):google.apps.script.xml_service.Text;
	function getValue(text:String):String;
	function getParentElement(text:String):google.apps.script.xml_service.Element;
	function getType(text:String):google.apps.script.xml_service.ContentType;
}
