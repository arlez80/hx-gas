package google.apps.script.xml_service;

@:native("EntityRef")
extern class EntityRef
{
	function detach(text:String):google.apps.script.xml_service.Content;
	function getSystemId(text:String):String;
	function getValue(text:String):String;
	function setSystemId(id:String):google.apps.script.xml_service.EntityRef;
	function getName(text:String):String;
	function setName(name:String):google.apps.script.xml_service.EntityRef;
	function getPublicId(text:String):String;
	function getParentElement(text:String):google.apps.script.xml_service.Element;
	function setPublicId(id:String):google.apps.script.xml_service.EntityRef;
}
