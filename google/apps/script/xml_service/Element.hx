package google.apps.script.xml_service;

@:native("Element")
extern class Element
{
	function cloneContent(index:Int,content:google.apps.script.xml_service.Content):Array<google.apps.script.xml_service.Content>;
	@:overload(function (name:String,namespace:google.apps.script.xml_service.Namespace):google.apps.script.xml_service.Element {})
	function getChild(name:String):google.apps.script.xml_service.Element;
	function getContentSize(index:Int):Int;
	function getValue(prefix:String):String;
	function getContent(index:Int):google.apps.script.xml_service.Content;
	@:overload(function (content:google.apps.script.xml_service.Content):Bool {})
	@:overload(function (index:Int):google.apps.script.xml_service.Content {})
	function removeContent(attributeName:String,namespace:google.apps.script.xml_service.Namespace):Array<google.apps.script.xml_service.Content>;
	function getName(index:Int):String;
	function getDocument(index:Int):google.apps.script.xml_service.Document;
	function getDescendants(index:Int):Array<google.apps.script.xml_service.Content>;
	@:overload(function (name:String):Array<google.apps.script.xml_service.Element> {})
	@:overload(function (name:String,namespace:google.apps.script.xml_service.Namespace):Array<google.apps.script.xml_service.Element> {})
	function getChildren(name:String,namespace:google.apps.script.xml_service.Namespace):Array<google.apps.script.xml_service.Element>;
	function getParentElement(prefix:String):google.apps.script.xml_service.Element;
	function isAncestorOf(other:google.apps.script.xml_service.Element):Bool;
	@:overload(function (name:String,namespace:google.apps.script.xml_service.Namespace):String {})
	function getChildText(name:String):String;
	function getQualifiedName(prefix:String):String;
	function getAllContent(index:Int,content:google.apps.script.xml_service.Content):Array<google.apps.script.xml_service.Content>;
	@:overload(function (name:String,namespace:google.apps.script.xml_service.Namespace):google.apps.script.xml_service.Attribute {})
	function getAttribute(name:String):google.apps.script.xml_service.Attribute;
	@:overload(function (attributeName:String):Bool {})
	@:overload(function (attributeName:String,namespace:google.apps.script.xml_service.Namespace):Bool {})
	function removeAttribute(attribute:google.apps.script.xml_service.Attribute):Bool;
	function getAttributes(name:String,namespace:google.apps.script.xml_service.Namespace):Array<google.apps.script.xml_service.Attribute>;
	function isRootElement(other:google.apps.script.xml_service.Element):Bool;
	function setText(text:String):google.apps.script.xml_service.Element;
	function detach(index:Int,content:google.apps.script.xml_service.Content):google.apps.script.xml_service.Content;
	function setNamespace(namespace:google.apps.script.xml_service.Namespace):google.apps.script.xml_service.Element;
	@:overload(function (name:String,value:String):google.apps.script.xml_service.Element {})
	@:overload(function (name:String,value:String,namespace:google.apps.script.xml_service.Namespace):google.apps.script.xml_service.Element {})
	function setAttribute(attribute:google.apps.script.xml_service.Attribute):google.apps.script.xml_service.Element;
	@:overload(function (prefix:String):google.apps.script.xml_service.Namespace {})
	function getNamespace(index:Int):google.apps.script.xml_service.Namespace;
	function setName(name:String):google.apps.script.xml_service.Element;
	@:overload(function (index:Int,content:google.apps.script.xml_service.Content):google.apps.script.xml_service.Element {})
	function addContent(content:google.apps.script.xml_service.Content):google.apps.script.xml_service.Element;
	function getText(prefix:String):String;
}
