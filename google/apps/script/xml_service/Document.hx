package google.apps.script.xml_service;

@:native("Document")
extern class Document
{
	function cloneContent(index:Int,content:google.apps.script.xml_service.Content):Array<google.apps.script.xml_service.Content>;
	function detachRootElement(index:Int,content:google.apps.script.xml_service.Content):google.apps.script.xml_service.Element;
	function getContentSize(index:Int):Int;
	function getContent(index:Int):google.apps.script.xml_service.Content;
	@:overload(function (content:google.apps.script.xml_service.Content):Bool {})
	@:overload(function (index:Int):google.apps.script.xml_service.Content {})
	function removeContent(index:Int):Array<google.apps.script.xml_service.Content>;
	function getRootElement(index:Int):google.apps.script.xml_service.Element;
	function getDescendants(index:Int):Array<google.apps.script.xml_service.Content>;
	function hasRootElement(index:Int):Bool;
	@:overload(function (index:Int,content:google.apps.script.xml_service.Content):google.apps.script.xml_service.Document {})
	function addContent(content:google.apps.script.xml_service.Content):google.apps.script.xml_service.Document;
	function setRootElement(element:google.apps.script.xml_service.Element):google.apps.script.xml_service.Document;
	function setDocType(docType:google.apps.script.xml_service.DocType):google.apps.script.xml_service.Document;
	function getDocType(index:Int):google.apps.script.xml_service.DocType;
	function getAllContent(index:Int,content:google.apps.script.xml_service.Content):Array<google.apps.script.xml_service.Content>;
}
