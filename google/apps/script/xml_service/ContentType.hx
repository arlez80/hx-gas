package google.apps.script.xml_service;

@:require(hx_gas)
@:native("ContentType")
@:enum abstract ContentType(String) from String to String
{
	var CDATA;
	var COMMENT;
	var DOCTYPE;
	var ELEMENT;
	var ENTITYREF;
	var PROCESSINGINSTRUCTION;
	var TEXT;
}

