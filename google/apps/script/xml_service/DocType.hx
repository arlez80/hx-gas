package google.apps.script.xml_service;

@:native("DocType")
extern class DocType
{
	function detach(text:String):google.apps.script.xml_service.Content;
	function getSystemId(text:String):String;
	function getInternalSubset(text:String):String;
	function setInternalSubset(data:String):google.apps.script.xml_service.DocType;
	function setSystemId(id:String):google.apps.script.xml_service.DocType;
	function getElementName(text:String):String;
	function getValue(text:String):String;
	function getPublicId(text:String):String;
	function setPublicId(id:String):google.apps.script.xml_service.DocType;
	function getParentElement(text:String):google.apps.script.xml_service.Element;
	function setElementName(name:String):google.apps.script.xml_service.DocType;
}
