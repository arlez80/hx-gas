package google.apps.script.xml_service;

@:native("Format")
extern class Format
{
	@:overload(function (element:google.apps.script.xml_service.Element):String {})
	function format(document:google.apps.script.xml_service.Document):String;
	function setIndent(indent:String):google.apps.script.xml_service.Format;
	function setOmitDeclaration(omitDeclaration:Bool):google.apps.script.xml_service.Format;
	function setLineSeparator(separator:String):google.apps.script.xml_service.Format;
	function setEncoding(encoding:String):google.apps.script.xml_service.Format;
	function setOmitEncoding(omitEncoding:Bool):google.apps.script.xml_service.Format;
}
