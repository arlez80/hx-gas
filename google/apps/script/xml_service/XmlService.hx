package google.apps.script.xml_service;

@:require(hx_gas)
@:native("XmlService.ContentType")
@:enum abstract ContentType(String) from String to String
{
	var CDATA;
	var COMMENT;
	var DOCTYPE;
	var ELEMENT;
	var ENTITYREF;
	var PROCESSINGINSTRUCTION;
	var TEXT;
}

@:native("XmlService")
extern class XmlService
{
	@:overload(function (rootElement:google.apps.script.xml_service.Element):google.apps.script.xml_service.Document {})
	static function createDocument(elementName:String,publicId:String,systemId:String):google.apps.script.xml_service.Document;
	static function getNoNamespace(prefix:String,uri:String):google.apps.script.xml_service.Namespace;
	static function getRawFormat(prefix:String,uri:String):google.apps.script.xml_service.Format;
	@:overload(function (name:String,namespace:google.apps.script.xml_service.Namespace):google.apps.script.xml_service.Element {})
	static function createElement(name:String):google.apps.script.xml_service.Element;
	static function parse(xml:String):google.apps.script.xml_service.Document;
	static function createCdata(text:String):google.apps.script.xml_service.Cdata;
	static function createText(text:String):google.apps.script.xml_service.Text;
	@:overload(function (prefix:String,uri:String):google.apps.script.xml_service.Namespace {})
	static function getNamespace(uri:String):google.apps.script.xml_service.Namespace;
	static function createComment(text:String):google.apps.script.xml_service.Comment;
	static function getPrettyFormat(prefix:String,uri:String):google.apps.script.xml_service.Format;
	@:overload(function (elementName:String,systemId:String):google.apps.script.xml_service.DocType {})
	@:overload(function (elementName:String,publicId:String,systemId:String):google.apps.script.xml_service.DocType {})
	static function createDocType(elementName:String):google.apps.script.xml_service.DocType;
	static function getCompactFormat(text:String):google.apps.script.xml_service.Format;
	static function getXmlNamespace(prefix:String,uri:String):google.apps.script.xml_service.Namespace;
}
