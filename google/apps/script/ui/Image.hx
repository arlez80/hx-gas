package google.apps.script.ui;

@:native("Image")
@:deprecated
extern class Image
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.Image;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.Image;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function setVisibleRect(left:Int,top:Int,width:Int,height:Int):google.apps.script.ui.Image;
	@:deprecated
	function setResource(resource:google.apps.script.ui.Component):google.apps.script.ui.Image;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.Image;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.Image;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function addErrorHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.Image;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.Image;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.Image;
	@:deprecated
	function setUrl(url:String):google.apps.script.ui.Image;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.Image;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.Image;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function addLoadHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.Image;
	@:deprecated
	function setUrlAndVisibleRect(url:String,left:Int,top:Int,width:Int,height:Int):google.apps.script.ui.Image;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Image;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.Image;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.Image;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.Image;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.Image;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.Image;
}
