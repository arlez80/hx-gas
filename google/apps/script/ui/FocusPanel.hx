package google.apps.script.ui;

@:native("FocusPanel")
@:deprecated
extern class FocusPanel
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.FocusPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.FocusPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.FocusPanel;
}
