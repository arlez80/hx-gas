package google.apps.script.ui;

@:native("RadioButton")
@:deprecated
extern class RadioButton
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.RadioButton;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function setText(text:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function setFormValue(formValue:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.RadioButton;
	@:deprecated
	function addValueChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.RadioButton;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.RadioButton;
	@:deprecated
	@:overload(function (value:Bool,fireEvents:Bool):google.apps.script.ui.RadioButton {})
	function setValue(value:Bool):google.apps.script.ui.RadioButton;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.RadioButton;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.RadioButton;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function setName(name:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.RadioButton;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.RadioButton;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.RadioButton;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.RadioButton;
}
