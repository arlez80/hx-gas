package google.apps.script.ui;

@:native("Grid")
@:deprecated
extern class Grid
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.Grid;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.Grid;
	@:deprecated
	function setText(row:Int,column:Int,text:String):google.apps.script.ui.Grid;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.Grid;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.Grid;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.Grid;
	@:deprecated
	function setCellPadding(padding:Int):google.apps.script.ui.Grid;
	@:deprecated
	function setColumnStyleAttribute(column:Int,attribute:String,value:String):google.apps.script.ui.Grid;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.Grid;
	@:deprecated
	function setColumnStyleAttributes(column:Int,attributes:Dynamic):google.apps.script.ui.Grid;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.Grid;
	@:deprecated
	function resize(rows:Int,columns:Int):google.apps.script.ui.Grid;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function setCellSpacing(spacing:Int):google.apps.script.ui.Grid;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.Grid;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Grid;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.Grid;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.Grid;
	@:deprecated
	function setBorderWidth(width:Int):google.apps.script.ui.Grid;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.Grid;
	@:deprecated
	function setWidget(row:Int,column:Int,widget:google.apps.script.ui.Widget):google.apps.script.ui.Grid;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.Grid;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.Grid;
	@:deprecated
	function setRowStyleAttribute(row:Int,attribute:String,value:String):google.apps.script.ui.Grid;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.Grid;
	@:deprecated
	@:overload(function (attributes:Dynamic):google.apps.script.ui.Grid {})
	function setStyleAttributes(row:Int,column:Int,attributes:Dynamic):google.apps.script.ui.Grid;
	@:deprecated
	@:overload(function (attribute:String,value:String):google.apps.script.ui.Grid {})
	function setStyleAttribute(row:Int,column:Int,attribute:String,value:String):google.apps.script.ui.Grid;
	@:deprecated
	function setRowStyleAttributes(row:Int,attributes:Dynamic):google.apps.script.ui.Grid;
}
