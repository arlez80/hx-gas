package google.apps.script.ui;

@:native("Hidden")
@:deprecated
extern class Hidden
{
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.Hidden;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.Hidden;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setValue(value:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.Hidden;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setID(id:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.Hidden;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setName(name:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setDefaultValue(value:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.Hidden;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.Hidden;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.Hidden;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.Hidden;
}
