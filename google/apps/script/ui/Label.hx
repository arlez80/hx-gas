package google.apps.script.ui;

@:native("Label")
@:deprecated
extern class Label
{
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.Label;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.Label;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Label;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.Label;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Label;
	@:deprecated
	function setText(text:String):google.apps.script.ui.Label;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.Label;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.Label;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Label;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.Label;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.Label;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Label;
	@:deprecated
	function setWordWrap(wordWrap:Bool):google.apps.script.ui.Label;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.Label;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Label;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.Label;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.Label;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Label;
	@:deprecated
	function setHorizontalAlignment(horizontalAlignment:google.apps.script.ui.HorizontalAlignment):google.apps.script.ui.Label;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.Label;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Label;
	@:deprecated
	function setDirection(direction:google.apps.script.ui.Component):google.apps.script.ui.Label;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.Label;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.Label;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.Label;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.Label;
}
