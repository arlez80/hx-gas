package google.apps.script.ui;

@:native("UiInstance")
@:deprecated
extern class UiInstance
{
	@:deprecated
	function setWidth(width:Int):google.apps.script.ui.UiInstance;
	@:deprecated
	function createClientHandler(label:String,asHtml:Bool):google.apps.script.ui.ClientHandler;
	@:deprecated
	function createTree(upText:String,downText:String):google.apps.script.ui.Tree;
	@:deprecated
	@:overload(function (text:String):google.apps.script.ui.TreeItem {})
	@:overload(function (child:google.apps.script.ui.Widget):google.apps.script.ui.TreeItem {})
	function createTreeItem(upText:String,downText:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function createFlowPanel(autoHide:Bool,modal:Bool):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.UiInstance;
	@:deprecated
	function createDateBox(label:String,asHtml:Bool):google.apps.script.ui.DateBox;
	@:deprecated
	function createPasswordTextBox(text:String,command:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setStandardsMode(standardsMode:Bool):google.apps.script.ui.UiInstance;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerSubmitHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function close(child:google.apps.script.ui.Widget):google.apps.script.ui.UiInstance;
	@:deprecated
	function createFlexTable(autoHide:Bool,modal:Bool):google.apps.script.ui.FlexTable;
	@:deprecated
	function createDecoratorPanel(label:String,asHtml:Bool):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function isStandardsMode(id:String):Bool;
	@:deprecated
	@:overload(function (html:String):google.apps.script.ui.ResetButton {})
	@:overload(function (html:String,clickHandler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton {})
	function createResetButton(name:String,label:String,asHtml:Bool):google.apps.script.ui.ResetButton;
	@:deprecated
	@:overload(function (html:String):google.apps.script.ui.SubmitButton {})
	function createSubmitButton(name:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function createVerticalPanel(child:google.apps.script.ui.Widget):google.apps.script.ui.VerticalPanel;
	@:deprecated
	function createSuggestBox(html:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function createDocsListDialog(autoHide:Bool,modal:Bool):google.apps.script.ui.DocsListDialog;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerFocusHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function createDatePicker(label:String,asHtml:Bool):google.apps.script.ui.DatePicker;
	@:deprecated
	@:overload(function (isMultipleSelect:Bool):google.apps.script.ui.ListBox {})
	function createListBox(text:String,wordWrap:Bool):google.apps.script.ui.ListBox;
	@:deprecated
	function createDecoratedTabPanel(label:String,asHtml:Bool):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function createSimplePanel(functionName:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function createSimpleRadioButton(name:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	@:overload(function (html:String):google.apps.script.ui.Button {})
	@:overload(function (html:String,clickHandler:google.apps.script.ui.Handler):google.apps.script.ui.Button {})
	function createButton(text:String,href:String):google.apps.script.ui.Button;
	@:deprecated
	@:overload(function (text:String):google.apps.script.ui.Label {})
	@:overload(function (text:String,wordWrap:Bool):google.apps.script.ui.Label {})
	function createLabel(text:String):google.apps.script.ui.Label;
	@:deprecated
	function add(child:google.apps.script.ui.Widget):google.apps.script.ui.UiInstance;
	@:deprecated
	@:overload(function (upText:String):google.apps.script.ui.ToggleButton {})
	@:overload(function (upText:String,clickHandler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton {})
	@:overload(function (upText:String,downText:String):google.apps.script.ui.ToggleButton {})
	function createToggleButton(html:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerMouseHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	@:overload(function (name:String):google.apps.script.ui.Hidden {})
	@:overload(function (name:String,value:String):google.apps.script.ui.Hidden {})
	function createHidden(html:String,wordWrap:Bool):google.apps.script.ui.Hidden;
	@:deprecated
	@:overload(function (vertical:Bool):google.apps.script.ui.MenuBar {})
	function createMenuBar(isMultipleSelect:Bool):google.apps.script.ui.MenuBar;
	@:deprecated
	function createTextArea(html:String):google.apps.script.ui.TextArea;
	@:deprecated
	function createFormPanel(child:google.apps.script.ui.Widget):google.apps.script.ui.FormPanel;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerLoadHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	@:overload(function (text:String):google.apps.script.ui.InlineLabel {})
	function createInlineLabel(url:String,left:Int,top:Int,width:Int,height:Int):google.apps.script.ui.InlineLabel;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerValueChangeHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	@:overload(function (caption:String):google.apps.script.ui.CaptionPanel {})
	@:overload(function (caption:String,asHtml:Bool):google.apps.script.ui.CaptionPanel {})
	function createCaptionPanel(html:String,clickHandler:google.apps.script.ui.Handler):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function createHorizontalPanel(name:String,value:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerCommand(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerInitializeHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function createSplitLayoutPanel(name:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	@:overload(function (autoHide:Bool):google.apps.script.ui.DialogBox {})
	@:overload(function (autoHide:Bool,modal:Bool):google.apps.script.ui.DialogBox {})
	function createDialogBox(label:String,asHtml:Bool):google.apps.script.ui.DialogBox;
	@:deprecated
	function createFileUpload(autoHide:Bool,modal:Bool):google.apps.script.ui.FileUpload;
	@:deprecated
	function createTextBox(html:String):google.apps.script.ui.TextBox;
	@:deprecated
	@:overload(function (componentName:String,optAdvancedArgs:Dynamic):google.apps.script.ui.Component {})
	function loadComponent(componentName:String):google.apps.script.ui.Component;
	@:deprecated
	@:overload(function (url:String):google.apps.script.ui.Image {})
	@:overload(function (url:String,left:Int,top:Int,width:Int,height:Int):google.apps.script.ui.Image {})
	function createImage(name:String,value:String):google.apps.script.ui.Image;
	@:deprecated
	@:overload(function (child:google.apps.script.ui.Widget):google.apps.script.ui.ScrollPanel {})
	function createScrollPanel(html:String,clickHandler:google.apps.script.ui.Handler):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function createTabBar(html:String):google.apps.script.ui.TabBar;
	@:deprecated
	@:overload(function (autoHide:Bool):google.apps.script.ui.PopupPanel {})
	@:overload(function (autoHide:Bool,modal:Bool):google.apps.script.ui.PopupPanel {})
	function createPopupPanel(text:String,command:google.apps.script.ui.Handler):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setHeight(height:Int):google.apps.script.ui.UiInstance;
	@:deprecated
	@:overload(function (child:google.apps.script.ui.Widget):google.apps.script.ui.FocusPanel {})
	function createFocusPanel(autoHide:Bool,modal:Bool):google.apps.script.ui.FocusPanel;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerKeyHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function createDecoratedStackPanel(label:String,asHtml:Bool):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function createStackPanel(name:String):google.apps.script.ui.StackPanel;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerSelectionHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	@:overload(function (upText:String):google.apps.script.ui.PushButton {})
	@:overload(function (upText:String,clickHandler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton {})
	@:overload(function (upText:String,downText:String):google.apps.script.ui.PushButton {})
	@:overload(function (upText:String,downText:String,clickHandler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton {})
	function createPushButton(autoHide:Bool,modal:Bool):google.apps.script.ui.PushButton;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function getId(id:String):String;
	@:deprecated
	function createDecoratedTabBar(label:String,asHtml:Bool):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerChangeHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	@:overload(function (html:String):google.apps.script.ui.HTML {})
	@:overload(function (html:String,wordWrap:Bool):google.apps.script.ui.HTML {})
	function createHTML(rows:Int,columns:Int):google.apps.script.ui.HTML;
	@:deprecated
	@:overload(function (text:String,command:google.apps.script.ui.Handler):google.apps.script.ui.MenuItem {})
	function createMenuItem(text:String,asHtml:Bool,command:google.apps.script.ui.Handler):google.apps.script.ui.MenuItem;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerScrollHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function getElementById(id:String):google.apps.script.ui.Component;
	@:deprecated
	function createSimpleCheckBox(functionName:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerClickHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function createMenuItemSeparator(text:String,command:google.apps.script.ui.Handler):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	@:overload(function (rows:Int,columns:Int):google.apps.script.ui.Grid {})
	function createGrid(child:google.apps.script.ui.Widget):google.apps.script.ui.Grid;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerBlurHandler(child:google.apps.script.ui.Widget):google.apps.script.ui.ServerHandler;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerCloseHandler(functionName:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function createAbsolutePanel(child:google.apps.script.ui.Widget):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	@:overload(function (text:String,href:String):google.apps.script.ui.Anchor {})
	function createAnchor(text:String,asHtml:Bool,href:String):google.apps.script.ui.Anchor;
	@:deprecated
	@:overload(function (name:String,label:String):google.apps.script.ui.RadioButton {})
	@:overload(function (name:String,label:String,asHtml:Bool):google.apps.script.ui.RadioButton {})
	function createRadioButton(name:String):google.apps.script.ui.RadioButton;
	@:deprecated
	@:overload(function (label:String):google.apps.script.ui.CheckBox {})
	@:overload(function (label:String,asHtml:Bool):google.apps.script.ui.CheckBox {})
	function createCheckBox(caption:String,asHtml:Bool):google.apps.script.ui.CheckBox;
	@:deprecated
	function createTabPanel(html:String):google.apps.script.ui.TabPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.UiInstance;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.UiInstance {})
	function remove(index:Int):google.apps.script.ui.UiInstance;
	@:deprecated
	@:overload(function (functionName:String):google.apps.script.ui.ServerHandler {})
	function createServerErrorHandler(functionName:String):google.apps.script.ui.ServerHandler;
}
