package google.apps.script.ui;

@:native("FileUpload")
@:deprecated
extern class FileUpload
{
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.FileUpload;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.FileUpload;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function addChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FileUpload;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.FileUpload;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.FileUpload;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function setName(name:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.FileUpload;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.FileUpload;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.FileUpload;
}
