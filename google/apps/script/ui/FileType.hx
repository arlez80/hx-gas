package google.apps.script.ui;

@:require(hx_gas)
@:native("FileType")
@:deprecated
@:enum abstract FileType(String) from String to String
{
@:deprecated
	var ALL;
@:deprecated
	var ALL_DOCS;
@:deprecated
	var DRAWINGS;
@:deprecated
	var DOCUMENTS;
@:deprecated
	var SPREADSHEETS;
@:deprecated
	var FOLDERS;
@:deprecated
	var RECENTLY_PICKED;
@:deprecated
	var PRESENTATIONS;
@:deprecated
	var FORMS;
@:deprecated
	var PHOTOS;
@:deprecated
	var PHOTO_ALBUMS;
@:deprecated
	var PDFS;
}

