package google.apps.script.ui;

@:native("HorizontalPanel")
@:deprecated
extern class HorizontalPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setCellWidth(widget:google.apps.script.ui.Widget,width:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setCellVerticalAlignment(widget:google.apps.script.ui.Widget,verticalAlignment:google.apps.script.ui.VerticalAlignment):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setVerticalAlignment(verticalAlignment:google.apps.script.ui.VerticalAlignment):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setBorderWidth(width:Int):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setCellHeight(widget:google.apps.script.ui.Widget,height:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setHorizontalAlignment(horizontalAlignment:google.apps.script.ui.HorizontalAlignment):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setCellHorizontalAlignment(widget:google.apps.script.ui.Widget,horizontalAlignment:google.apps.script.ui.HorizontalAlignment):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setSpacing(spacing:Int):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.HorizontalPanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.HorizontalPanel {})
	function remove(index:Int):google.apps.script.ui.HorizontalPanel;
}
