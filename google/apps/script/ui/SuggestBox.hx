package google.apps.script.ui;

@:native("SuggestBox")
@:deprecated
extern class SuggestBox
{
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setText(text:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SuggestBox;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function addSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SuggestBox;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setLimit(limit:Int):google.apps.script.ui.SuggestBox;
	@:deprecated
	function addValueChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SuggestBox;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setAutoSelectEnabled(autoSelectEnabled:Bool):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setPopupStyleName(styleName:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setAnimationEnabled(animationEnabled:Bool):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	@:overload(function (value:String,fireEvents:Bool):google.apps.script.ui.SuggestBox {})
	function setValue(value:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.SuggestBox;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.SuggestBox;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.SuggestBox;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.SuggestBox;
}
