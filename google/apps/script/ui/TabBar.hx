package google.apps.script.ui;

@:native("TabBar")
@:deprecated
extern class TabBar
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.TabBar;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.TabBar;
	@:deprecated
	function getType(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	function addSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TabBar;
	@:deprecated
	function setTabText(index:Int,text:String):google.apps.script.ui.TabBar;
	@:deprecated
	function selectTab(index:Int):google.apps.script.ui.TabBar;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.TabBar;
	@:deprecated
	function addBeforeSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TabBar;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.TabBar;
	@:deprecated
	function getTag(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	function getId(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.TabBar;
	@:deprecated
	@:overload(function (title:String,asHtml:Bool):google.apps.script.ui.TabBar {})
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.TabBar {})
	function addTab(title:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setTabEnabled(index:Int,enabled:Bool):google.apps.script.ui.TabBar;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.TabBar;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.TabBar;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.TabBar;
}
