package google.apps.script.ui;

@:native("DialogBox")
@:deprecated
extern class DialogBox
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setPreviewingAllNativeEvents(previewing:Bool):google.apps.script.ui.DialogBox;
	@:deprecated
	function setPopupPosition(left:Int,top:Int):google.apps.script.ui.DialogBox;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.DialogBox;
	@:deprecated
	function addCloseHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DialogBox;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setModal(modal:Bool):google.apps.script.ui.DialogBox;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.DialogBox;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setGlassStyleName(styleName:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setText(text:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setAnimationEnabled(animationEnabled:Bool):google.apps.script.ui.DialogBox;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function show(width:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function hide(styleName:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.DialogBox;
	@:deprecated
	function setGlassEnabled(enabled:Bool):google.apps.script.ui.DialogBox;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.DialogBox;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.DialogBox;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setPopupPositionAndShow(a:google.apps.script.ui.Component):google.apps.script.ui.DialogBox;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.DialogBox;
	@:deprecated
	function addAutoHidePartner(partner:google.apps.script.ui.Component):google.apps.script.ui.DialogBox;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.DialogBox;
	@:deprecated
	function setAutoHideEnabled(enabled:Bool):google.apps.script.ui.DialogBox;
}
