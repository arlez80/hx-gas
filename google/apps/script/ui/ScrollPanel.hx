package google.apps.script.ui;

@:native("ScrollPanel")
@:deprecated
extern class ScrollPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function addScrollHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setAlwaysShowScrollBars(alwaysShow:Bool):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setScrollPosition(position:Int):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.ScrollPanel;
	@:deprecated
	function setHorizontalScrollPosition(position:Int):google.apps.script.ui.ScrollPanel;
}
