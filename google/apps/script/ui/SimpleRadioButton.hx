package google.apps.script.ui;

@:native("SimpleRadioButton")
@:deprecated
extern class SimpleRadioButton
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setChecked(checked:Bool):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setName(name:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.SimpleRadioButton;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.SimpleRadioButton;
}
