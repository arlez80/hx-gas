package google.apps.script.ui;

@:native("SimplePanel")
@:deprecated
extern class SimplePanel
{
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.SimplePanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.SimplePanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.SimplePanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.SimplePanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.SimplePanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.SimplePanel;
}
