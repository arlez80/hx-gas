package google.apps.script.ui;

@:native("SplitLayoutPanel")
@:deprecated
extern class SplitLayoutPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function addEast(widget:google.apps.script.ui.Widget,width:Float):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function addWest(widget:google.apps.script.ui.Widget,width:Float):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function addNorth(widget:google.apps.script.ui.Widget,height:Float):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function getType(widget:google.apps.script.ui.Widget,width:Float):String;
	@:deprecated
	function clear(widget:google.apps.script.ui.Widget,width:Float):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function getTag(widget:google.apps.script.ui.Widget,width:Float):String;
	@:deprecated
	function getId(widget:google.apps.script.ui.Widget,width:Float):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function addSouth(widget:google.apps.script.ui.Widget,height:Float):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setWidgetMinSize(widget:google.apps.script.ui.Widget,minSize:Int):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.SplitLayoutPanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.SplitLayoutPanel {})
	function remove(index:Int):google.apps.script.ui.SplitLayoutPanel;
}
