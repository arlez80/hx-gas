package google.apps.script.ui;

@:native("SimpleCheckBox")
@:deprecated
extern class SimpleCheckBox
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setChecked(checked:Bool):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setName(name:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.SimpleCheckBox;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.SimpleCheckBox;
}
