package google.apps.script.ui;

@:native("ResetButton")
@:deprecated
extern class ResetButton
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.ResetButton;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function setText(text:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.ResetButton;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.ResetButton;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.ResetButton;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.ResetButton;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.ResetButton;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ResetButton;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.ResetButton;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.ResetButton;
}
