package google.apps.script.ui;

@:native("DecoratedTabPanel")
@:deprecated
extern class DecoratedTabPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function addBeforeSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setAnimationEnabled(animationEnabled:Bool):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,text:String):google.apps.script.ui.DecoratedTabPanel {})
	@:overload(function (widget:google.apps.script.ui.Widget,text:String,asHtml:Bool):google.apps.script.ui.DecoratedTabPanel {})
	@:overload(function (widget:google.apps.script.ui.Widget,tabWidget:google.apps.script.ui.Widget):google.apps.script.ui.DecoratedTabPanel {})
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function selectTab(index:Int):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.DecoratedTabPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.DecoratedTabPanel;
}
