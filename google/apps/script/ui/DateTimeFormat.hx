package google.apps.script.ui;

@:require(hx_gas)
@:native("DateTimeFormat")
@:deprecated
@:enum abstract DateTimeFormat(String) from String to String
{
@:deprecated
	var ISO_8601;
@:deprecated
	var RFC_2822;
@:deprecated
	var DATE_FULL;
@:deprecated
	var DATE_LONG;
@:deprecated
	var DATE_MEDIUM;
@:deprecated
	var DATE_SHORT;
@:deprecated
	var TIME_FULL;
@:deprecated
	var TIME_LONG;
@:deprecated
	var TIME_MEDIUM;
@:deprecated
	var TIME_SHORT;
@:deprecated
	var DATE_TIME_FULL;
@:deprecated
	var DATE_TIME_LONG;
@:deprecated
	var DATE_TIME_MEDIUM;
@:deprecated
	var DATE_TIME_SHORT;
@:deprecated
	var DAY;
@:deprecated
	var HOUR_MINUTE;
@:deprecated
	var HOUR_MINUTE_SECOND;
@:deprecated
	var HOUR24_MINUTE;
@:deprecated
	var HOUR24_MINUTE_SECOND;
@:deprecated
	var MINUTE_SECOND;
@:deprecated
	var MONTH;
@:deprecated
	var MONTH_ABBR;
@:deprecated
	var MONTH_ABBR_DAY;
@:deprecated
	var MONTH_DAY;
@:deprecated
	var MONTH_NUM_DAY;
@:deprecated
	var MONTH_WEEKDAY_DAY;
@:deprecated
	var YEAR;
@:deprecated
	var YEAR_MONTH;
@:deprecated
	var YEAR_MONTH_ABBR;
@:deprecated
	var YEAR_MONTH_ABBR_DAY;
@:deprecated
	var YEAR_MONTH_DAY;
@:deprecated
	var YEAR_MONTH_NUM;
@:deprecated
	var YEAR_MONTH_NUM_DAY;
@:deprecated
	var YEAR_MONTH_WEEKDAY_DAY;
@:deprecated
	var YEAR_QUARTER;
@:deprecated
	var YEAR_QUARTER_ABBR;
}

