package google.apps.script.ui;

@:native("Anchor")
@:deprecated
extern class Anchor
{
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.Anchor;
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.Anchor;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.Anchor;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.Anchor;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setText(text:String):google.apps.script.ui.Anchor;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.Anchor;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.Anchor;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function setWordWrap(wordWrap:Bool):google.apps.script.ui.Anchor;
	@:deprecated
	function setHref(href:String):google.apps.script.ui.Anchor;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setDirection(direction:google.apps.script.ui.Component):google.apps.script.ui.Anchor;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.Anchor;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.Anchor;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.Anchor;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setHorizontalAlignment(horizontalAlignment:google.apps.script.ui.HorizontalAlignment):google.apps.script.ui.Anchor;
	@:deprecated
	function setTarget(target:String):google.apps.script.ui.Anchor;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function setName(name:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setId(id:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.Anchor;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.Anchor;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Anchor;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.Anchor;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.Anchor;
}
