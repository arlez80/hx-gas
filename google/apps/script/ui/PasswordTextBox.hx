package google.apps.script.ui;

@:native("PasswordTextBox")
@:deprecated
extern class PasswordTextBox
{
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setCursorPos(position:Int):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setVisibleLength(length:Int):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setDirection(direction:google.apps.script.ui.Component):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setText(text:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setSelectionRange(position:Int,length:Int):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setTextAlignment(textAlign:google.apps.script.ui.Component):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setMaxLength(length:Int):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setReadOnly(readOnly:Bool):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addValueChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setName(name:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	@:overload(function (value:String,fireEvents:Bool):google.apps.script.ui.PasswordTextBox {})
	function setValue(value:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.PasswordTextBox;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.PasswordTextBox;
}
