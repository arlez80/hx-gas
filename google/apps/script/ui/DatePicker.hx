package google.apps.script.ui;

@:native("DatePicker")
@:deprecated
extern class DatePicker
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setCurrentMonth(date:Date):google.apps.script.ui.DatePicker;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setValue(date:Date):google.apps.script.ui.DatePicker;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function addValueChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DatePicker;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.DatePicker;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.DatePicker;
	@:deprecated
	function setName(name:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.DatePicker;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.DatePicker;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.DatePicker;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.DatePicker;
}
