package google.apps.script.ui;

@:native("Handler")
@:deprecated
extern class Handler
{
	@:deprecated
	function validateSum(widgets:Array<google.apps.script.ui.Widget>,sum:Int):google.apps.script.ui.Handler;
	@:deprecated
	function validateNotRange(widget:google.apps.script.ui.Widget,min:Float,max:Float):google.apps.script.ui.Handler;
	@:deprecated
	function validateOptions(widget:google.apps.script.ui.Widget,options:Array<String>):google.apps.script.ui.Handler;
	@:deprecated
	function getTag(width:String):String;
	@:deprecated
	function validateNotNumber(widget:google.apps.script.ui.Widget):google.apps.script.ui.Handler;
	@:deprecated
	function validateNotOptions(widget:google.apps.script.ui.Widget,options:Array<String>):google.apps.script.ui.Handler;
	@:deprecated
	function validateLength(widget:google.apps.script.ui.Widget,min:Int,max:Int):google.apps.script.ui.Handler;
	@:deprecated
	function validateNotSum(widgets:Array<google.apps.script.ui.Widget>,sum:Int):google.apps.script.ui.Handler;
	@:deprecated
	function getType(width:String):String;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,pattern:String,flags:String):google.apps.script.ui.Handler {})
	function validateMatches(widget:google.apps.script.ui.Widget,pattern:String):google.apps.script.ui.Handler;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,pattern:String,flags:String):google.apps.script.ui.Handler {})
	function validateNotMatches(widget:google.apps.script.ui.Widget,pattern:String):google.apps.script.ui.Handler;
	@:deprecated
	function validateEmail(widget:google.apps.script.ui.Widget):google.apps.script.ui.Handler;
	@:deprecated
	function validateRange(widget:google.apps.script.ui.Widget,min:Float,max:Float):google.apps.script.ui.Handler;
	@:deprecated
	function validateNotLength(widget:google.apps.script.ui.Widget,min:Int,max:Int):google.apps.script.ui.Handler;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.Handler;
	@:deprecated
	function validateInteger(widget:google.apps.script.ui.Widget):google.apps.script.ui.Handler;
	@:deprecated
	function validateNotInteger(widget:google.apps.script.ui.Widget):google.apps.script.ui.Handler;
	@:deprecated
	function validateNotEmail(widget:google.apps.script.ui.Widget):google.apps.script.ui.Handler;
	@:deprecated
	function getId(width:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.Handler;
	@:deprecated
	function validateNumber(widget:google.apps.script.ui.Widget):google.apps.script.ui.Handler;
}
