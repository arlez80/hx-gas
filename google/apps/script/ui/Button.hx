package google.apps.script.ui;

@:native("Button")
@:deprecated
extern class Button
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.Button;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.Button;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.Button;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.Button;
	@:deprecated
	function setText(text:String):google.apps.script.ui.Button;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.Button;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.Button;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.Button;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.Button;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.Button;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.Button;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.Button;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.Button;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.Button;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.Button;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.Button;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.Button;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.Button;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.Button;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Button;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.Button;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.Button;
}
