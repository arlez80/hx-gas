package google.apps.script.ui;

@:native("ToggleButton")
@:deprecated
extern class ToggleButton
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setText(text:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.ToggleButton;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setId(id:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ToggleButton;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setDown(down:Bool):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.ToggleButton;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.ToggleButton;
}
