package google.apps.script.ui;

@:native("FlowPanel")
@:deprecated
extern class FlowPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.FlowPanel;
	@:deprecated
	function insert(widget:google.apps.script.ui.Widget,beforeIndex:Int):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.FlowPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.FlowPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.FlowPanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.FlowPanel {})
	function remove(index:Int):google.apps.script.ui.FlowPanel;
}
