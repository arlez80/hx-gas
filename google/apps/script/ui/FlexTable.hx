package google.apps.script.ui;

@:native("FlexTable")
@:deprecated
extern class FlexTable
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function addCell(row:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function setText(row:Int,column:Int,text:String):google.apps.script.ui.FlexTable;
	@:deprecated
	@:overload(function (attributes:Dynamic):google.apps.script.ui.FlexTable {})
	function setStyleAttributes(row:Int,column:Int,attributes:Dynamic):google.apps.script.ui.FlexTable;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.FlexTable;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function removeCells(row:Int,column:Int,num:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function setCellPadding(padding:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function setColumnStyleAttribute(column:Int,attribute:String,value:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function setColumnStyleAttributes(column:Int,attributes:Dynamic):google.apps.script.ui.FlexTable;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FlexTable;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.FlexTable;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function setBorderWidth(width:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function setCellSpacing(spacing:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function insertRow(beforeRow:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function setWidget(row:Int,column:Int,widget:google.apps.script.ui.Widget):google.apps.script.ui.FlexTable;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function setRowStyleAttribute(row:Int,attribute:String,value:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function removeRow(row:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	@:overload(function (attribute:String,value:String):google.apps.script.ui.FlexTable {})
	function setStyleAttribute(row:Int,column:Int,attribute:String,value:String):google.apps.script.ui.FlexTable;
	@:deprecated
	function removeCell(row:Int,column:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function insertCell(beforeRow:Int,beforeColumn:Int):google.apps.script.ui.FlexTable;
	@:deprecated
	function setRowStyleAttributes(row:Int,attributes:Dynamic):google.apps.script.ui.FlexTable;
}
