package google.apps.script.ui;

@:native("HTML")
@:deprecated
extern class HTML
{
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.HTML;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.HTML;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.HTML;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.HTML;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.HTML;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.HTML;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.HTML;
	@:deprecated
	function setText(text:String):google.apps.script.ui.HTML;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.HTML;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.HTML;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.HTML;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.HTML;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.HTML;
	@:deprecated
	function setWordWrap(wordWrap:Bool):google.apps.script.ui.HTML;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.HTML;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.HTML;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.HTML;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.HTML;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.HTML;
	@:deprecated
	function setHorizontalAlignment(horizontalAlignment:google.apps.script.ui.HorizontalAlignment):google.apps.script.ui.HTML;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.HTML;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.HTML;
	@:deprecated
	function setDirection(direction:google.apps.script.ui.Component):google.apps.script.ui.HTML;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.HTML;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.HTML;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.HTML;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.HTML;
}
