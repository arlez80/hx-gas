package google.apps.script.ui;

@:native("MenuItem")
@:deprecated
extern class MenuItem
{
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.MenuItem;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function setSubMenu(subMenu:Dynamic):google.apps.script.ui.MenuItem;
	@:deprecated
	function setCommand(handler:google.apps.script.ui.Handler):google.apps.script.ui.MenuItem;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.MenuItem;
	@:deprecated
	function setText(text:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.MenuItem;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.MenuItem;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.MenuItem;
}
