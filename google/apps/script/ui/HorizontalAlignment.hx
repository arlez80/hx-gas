package google.apps.script.ui;

@:require(hx_gas)
@:native("HorizontalAlignment")
@:deprecated
@:enum abstract HorizontalAlignment(String) from String to String
{
@:deprecated
	var LEFT;
@:deprecated
	var RIGHT;
@:deprecated
	var CENTER;
@:deprecated
	var DEFAULT;
@:deprecated
	var JUSTIFY;
@:deprecated
	var LOCALE_START;
@:deprecated
	var LOCALE_END;
}

