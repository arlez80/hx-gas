package google.apps.script.ui;

@:native("InlineLabel")
@:deprecated
extern class InlineLabel
{
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setText(text:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.InlineLabel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.InlineLabel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setWordWrap(wordWrap:Bool):google.apps.script.ui.InlineLabel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setHorizontalAlignment(horizontalAlignment:google.apps.script.ui.HorizontalAlignment):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setDirection(direction:google.apps.script.ui.Component):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.InlineLabel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.InlineLabel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.InlineLabel;
}
