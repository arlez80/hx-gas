package google.apps.script.ui;

@:native("StackPanel")
@:deprecated
extern class StackPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.StackPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.StackPanel;
	@:deprecated
	@:overload(function (index:Int,text:String,asHtml:Bool):google.apps.script.ui.StackPanel {})
	function setStackText(index:Int,text:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.StackPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.StackPanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,text:String):google.apps.script.ui.StackPanel {})
	@:overload(function (widget:google.apps.script.ui.Widget,text:String,asHtml:Bool):google.apps.script.ui.StackPanel {})
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.StackPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.StackPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.StackPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.StackPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.StackPanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.StackPanel {})
	function remove(index:Int):google.apps.script.ui.StackPanel;
}
