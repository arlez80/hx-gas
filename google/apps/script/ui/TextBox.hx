package google.apps.script.ui;

@:native("TextBox")
@:deprecated
extern class TextBox
{
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.TextBox;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.TextBox;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.TextBox;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.TextBox;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.TextBox;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function setCursorPos(position:Int):google.apps.script.ui.TextBox;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setVisibleLength(length:Int):google.apps.script.ui.TextBox;
	@:deprecated
	function setDirection(direction:google.apps.script.ui.Component):google.apps.script.ui.TextBox;
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.TextBox;
	@:deprecated
	function setText(text:String):google.apps.script.ui.TextBox;
	@:deprecated
	function setSelectionRange(position:Int,length:Int):google.apps.script.ui.TextBox;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.TextBox;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function setTextAlignment(textAlign:google.apps.script.ui.Component):google.apps.script.ui.TextBox;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.TextBox;
	@:deprecated
	function setMaxLength(length:Int):google.apps.script.ui.TextBox;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.TextBox;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.TextBox;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.TextBox;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function addChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function setReadOnly(readOnly:Bool):google.apps.script.ui.TextBox;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.TextBox;
	@:deprecated
	function addValueChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.TextBox;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.TextBox;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.TextBox;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.TextBox;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextBox;
	@:deprecated
	function setName(name:String):google.apps.script.ui.TextBox;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.TextBox;
	@:deprecated
	@:overload(function (value:String,fireEvents:Bool):google.apps.script.ui.TextBox {})
	function setValue(value:String):google.apps.script.ui.TextBox;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.TextBox;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.TextBox;
}
