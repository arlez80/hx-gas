package google.apps.script.ui;

@:native("MenuBar")
@:deprecated
extern class MenuBar
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.MenuBar;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	@:overload(function (text:String,asHtml:Bool,command:google.apps.script.ui.Handler):google.apps.script.ui.MenuBar {})
	@:overload(function (text:String,asHtml:Bool,subMenu:Dynamic):google.apps.script.ui.MenuBar {})
	@:overload(function (text:String,command:google.apps.script.ui.Handler):google.apps.script.ui.MenuBar {})
	@:overload(function (text:String,subMenu:Dynamic):google.apps.script.ui.MenuBar {})
	function addItem(item:Dynamic):google.apps.script.ui.MenuBar;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setAnimationEnabled(animationEnabled:Bool):google.apps.script.ui.MenuBar;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.MenuBar;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function addCloseHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.MenuBar;
	@:deprecated
	@:overload(function (separator:Dynamic):google.apps.script.ui.MenuBar {})
	function addSeparator(text:String,subMenu:Dynamic):google.apps.script.ui.MenuBar;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.MenuBar;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.MenuBar;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.MenuBar;
	@:deprecated
	function setAutoOpen(autoOpen:Bool):google.apps.script.ui.MenuBar;
}
