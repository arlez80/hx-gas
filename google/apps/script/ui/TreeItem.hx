package google.apps.script.ui;

@:native("TreeItem")
@:deprecated
extern class TreeItem
{
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.TreeItem;
	@:deprecated
	@:overload(function (open:Bool,fireEvents:Bool):google.apps.script.ui.TreeItem {})
	function setState(open:Bool):google.apps.script.ui.TreeItem;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setText(text:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.TreeItem;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.TreeItem;
	@:deprecated
	@:overload(function (item:Dynamic):google.apps.script.ui.TreeItem {})
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.TreeItem {})
	function addItem(text:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setSelected(selected:Bool):google.apps.script.ui.TreeItem;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setUserObject(a:Dynamic):google.apps.script.ui.TreeItem;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.TreeItem;
	@:deprecated
	function setId(id:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.TreeItem;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.TreeItem;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.TreeItem;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.TreeItem;
}
