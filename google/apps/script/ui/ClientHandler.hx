package google.apps.script.ui;

@:native("ClientHandler")
@:deprecated
extern class ClientHandler
{
	@:deprecated
	function validateOptions(widget:google.apps.script.ui.Widget,options:Array<String>):google.apps.script.ui.ClientHandler;
	@:deprecated
	function forTargets(widgets:Dynamic):google.apps.script.ui.ClientHandler;
	@:deprecated
	function setText(text:String):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateLength(widget:google.apps.script.ui.Widget,min:Int,max:Int):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateNotSum(widgets:Array<google.apps.script.ui.Widget>,sum:Int):google.apps.script.ui.ClientHandler;
	@:deprecated
	function getType(widgets:Dynamic):String;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,pattern:String,flags:String):google.apps.script.ui.ClientHandler {})
	function validateMatches(widget:google.apps.script.ui.Widget,pattern:String):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateNotRange(widget:google.apps.script.ui.Widget,min:Float,max:Float):google.apps.script.ui.ClientHandler;
	@:deprecated
	function setValue(value:Bool):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateNotLength(widget:google.apps.script.ui.Widget,min:Int,max:Int):google.apps.script.ui.ClientHandler;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateInteger(widget:google.apps.script.ui.Widget):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateNotInteger(widget:google.apps.script.ui.Widget):google.apps.script.ui.ClientHandler;
	@:deprecated
	function getId(widgets:Dynamic):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateNumber(widget:google.apps.script.ui.Widget):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateNotNumber(widget:google.apps.script.ui.Widget):google.apps.script.ui.ClientHandler;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateSum(widgets:Array<google.apps.script.ui.Widget>,sum:Int):google.apps.script.ui.ClientHandler;
	@:deprecated
	function forEventSource(width:String):google.apps.script.ui.ClientHandler;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateNotOptions(widget:google.apps.script.ui.Widget,options:Array<String>):google.apps.script.ui.ClientHandler;
	@:deprecated
	@:overload(function (attribute:String,value:String):google.apps.script.ui.ClientHandler {})
	function setStyleAttribute(row:Int,column:Int,attribute:String,value:String):google.apps.script.ui.ClientHandler;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,pattern:String,flags:String):google.apps.script.ui.ClientHandler {})
	function validateNotMatches(widget:google.apps.script.ui.Widget,pattern:String):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateEmail(widget:google.apps.script.ui.Widget):google.apps.script.ui.ClientHandler;
	@:deprecated
	function getTag(widgets:Dynamic):String;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateRange(widget:google.apps.script.ui.Widget,min:Float,max:Float):google.apps.script.ui.ClientHandler;
	@:deprecated
	@:overload(function (attributes:Dynamic):google.apps.script.ui.ClientHandler {})
	function setStyleAttributes(row:Int,column:Int,attributes:Dynamic):google.apps.script.ui.ClientHandler;
	@:deprecated
	function validateNotEmail(widget:google.apps.script.ui.Widget):google.apps.script.ui.ClientHandler;
}
