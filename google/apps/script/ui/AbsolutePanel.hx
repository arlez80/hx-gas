package google.apps.script.ui;

@:native("AbsolutePanel")
@:deprecated
extern class AbsolutePanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setWidgetPosition(widget:google.apps.script.ui.Widget,left:Int,top:Int):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,left:Int,top:Int):google.apps.script.ui.AbsolutePanel {})
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.AbsolutePanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.AbsolutePanel {})
	function remove(index:Int):google.apps.script.ui.AbsolutePanel;
}
