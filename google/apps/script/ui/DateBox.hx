package google.apps.script.ui;

@:native("DateBox")
@:deprecated
extern class DateBox
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.DateBox;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.DateBox;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.DateBox;
	@:deprecated
	function addValueChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DateBox;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.DateBox;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setValue(date:Date):google.apps.script.ui.DateBox;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.DateBox;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.DateBox;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.DateBox;
	@:deprecated
	function showDatePicker(width:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setFireEventsForInvalid(fireEvents:Bool):google.apps.script.ui.DateBox;
	@:deprecated
	function setName(name:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.DateBox;
	@:deprecated
	function setFormat(dateTimeFormat:google.apps.script.ui.DateTimeFormat):google.apps.script.ui.DateBox;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.DateBox;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.DateBox;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.DateBox;
	@:deprecated
	function hideDatePicker(handler:google.apps.script.ui.Handler):google.apps.script.ui.DateBox;
}
