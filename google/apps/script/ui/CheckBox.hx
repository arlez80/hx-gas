package google.apps.script.ui;

@:native("CheckBox")
@:deprecated
extern class CheckBox
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.CheckBox;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function setText(text:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function setFormValue(formValue:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.CheckBox;
	@:deprecated
	function addValueChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.CheckBox;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.CheckBox;
	@:deprecated
	@:overload(function (value:Bool,fireEvents:Bool):google.apps.script.ui.CheckBox {})
	function setValue(value:Bool):google.apps.script.ui.CheckBox;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.CheckBox;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.CheckBox;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function setName(name:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.CheckBox;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.CheckBox;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.CheckBox;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.CheckBox;
}
