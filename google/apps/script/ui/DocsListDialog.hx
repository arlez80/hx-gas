package google.apps.script.ui;

@:native("DocsListDialog")
@:deprecated
extern class DocsListDialog
{
	@:deprecated
	function setHeight(height:Int):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function setMultiSelectEnabled(multiSelectEnabled:Bool):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function addCloseHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function setInitialView(fileType:google.apps.script.ui.FileType):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function addView(fileType:google.apps.script.ui.FileType):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function getType(fileType:google.apps.script.ui.FileType):String;
	@:deprecated
	function addSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function setDialogTitle(title:String):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function showDocsPicker(width:Int):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function setOAuthToken(oAuthToken:String):google.apps.script.ui.DocsListDialog;
	@:deprecated
	function getId(fileType:google.apps.script.ui.FileType):String;
	@:deprecated
	function setWidth(width:Int):google.apps.script.ui.DocsListDialog;
}
