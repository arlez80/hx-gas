package google.apps.script.ui;

@:require(hx_gas)
@:native("VerticalAlignment")
@:deprecated
@:enum abstract VerticalAlignment(String) from String to String
{
@:deprecated
	var TOP;
@:deprecated
	var MIDDLE;
@:deprecated
	var BOTTOM;
}

