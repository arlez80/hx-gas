package google.apps.script.ui;

@:native("DecoratedStackPanel")
@:deprecated
extern class DecoratedStackPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	@:overload(function (index:Int,text:String,asHtml:Bool):google.apps.script.ui.DecoratedStackPanel {})
	function setStackText(index:Int,text:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,text:String):google.apps.script.ui.DecoratedStackPanel {})
	@:overload(function (widget:google.apps.script.ui.Widget,text:String,asHtml:Bool):google.apps.script.ui.DecoratedStackPanel {})
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.DecoratedStackPanel;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.DecoratedStackPanel {})
	function remove(index:Int):google.apps.script.ui.DecoratedStackPanel;
}
