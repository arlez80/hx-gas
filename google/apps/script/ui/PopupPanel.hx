package google.apps.script.ui;

@:native("PopupPanel")
@:deprecated
extern class PopupPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setPreviewingAllNativeEvents(previewing:Bool):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setPopupPosition(left:Int,top:Int):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function addCloseHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setModal(modal:Bool):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.PopupPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setGlassStyleName(styleName:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setAnimationEnabled(animationEnabled:Bool):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function show(width:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function hide(styleName:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setGlassEnabled(enabled:Bool):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setPopupPositionAndShow(a:google.apps.script.ui.Component):google.apps.script.ui.PopupPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.PopupPanel;
	@:deprecated
	function addAutoHidePartner(partner:google.apps.script.ui.Component):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.PopupPanel;
	@:deprecated
	function setAutoHideEnabled(enabled:Bool):google.apps.script.ui.PopupPanel;
}
