package google.apps.script.ui;

@:native("ListBox")
@:deprecated
extern class ListBox
{
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.ListBox;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.ListBox;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	@:overload(function (text:String,value:String):google.apps.script.ui.ListBox {})
	function addItem(text:String):google.apps.script.ui.ListBox;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.ListBox;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.ListBox;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setSelectedIndex(index:Int):google.apps.script.ui.ListBox;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function setItemSelected(index:Int,selected:Bool):google.apps.script.ui.ListBox;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.ListBox;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.ListBox;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function addChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setItemText(index:Int,text:String):google.apps.script.ui.ListBox;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.ListBox;
	@:deprecated
	function removeItem(index:Int):google.apps.script.ui.ListBox;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.ListBox;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.ListBox;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.ListBox;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.ListBox;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.ListBox;
	@:deprecated
	function setVisibleItemCount(count:Int):google.apps.script.ui.ListBox;
	@:deprecated
	function setName(name:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.ListBox;
	@:deprecated
	function setValue(index:Int,value:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.ListBox;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.ListBox;
}
