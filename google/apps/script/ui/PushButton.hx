package google.apps.script.ui;

@:native("PushButton")
@:deprecated
extern class PushButton
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.PushButton;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.PushButton;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.PushButton;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.PushButton;
	@:deprecated
	function setText(text:String):google.apps.script.ui.PushButton;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.PushButton;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.PushButton;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.PushButton;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.PushButton;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.PushButton;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.PushButton;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.PushButton;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.PushButton;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.PushButton;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.PushButton;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.PushButton;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.PushButton;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.PushButton;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.PushButton;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.PushButton;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.PushButton;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.PushButton;
}
