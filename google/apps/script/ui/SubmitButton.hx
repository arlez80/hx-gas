package google.apps.script.ui;

@:native("SubmitButton")
@:deprecated
extern class SubmitButton
{
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setText(text:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.SubmitButton;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setHTML(html:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.SubmitButton;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.SubmitButton;
}
