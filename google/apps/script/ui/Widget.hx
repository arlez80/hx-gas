package google.apps.script.ui;

@:native("Widget")
@:deprecated
extern class Widget
{
	@:deprecated
	function getId(widgets:Array<google.apps.script.ui.Widget>,sum:Int):String;
	@:deprecated
	function getType(widgets:Array<google.apps.script.ui.Widget>,sum:Int):String;
}
