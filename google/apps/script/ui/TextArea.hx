package google.apps.script.ui;

@:native("TextArea")
@:deprecated
extern class TextArea
{
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.TextArea;
	@:deprecated
	function setEnabled(enabled:Bool):google.apps.script.ui.TextArea;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.TextArea;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.TextArea;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.TextArea;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function setCursorPos(position:Int):google.apps.script.ui.TextArea;
	@:deprecated
	function setSelectionRange(position:Int,length:Int):google.apps.script.ui.TextArea;
	@:deprecated
	function setDirection(direction:google.apps.script.ui.Component):google.apps.script.ui.TextArea;
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.TextArea;
	@:deprecated
	function setText(text:String):google.apps.script.ui.TextArea;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.TextArea;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function setTextAlignment(textAlign:google.apps.script.ui.Component):google.apps.script.ui.TextArea;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.TextArea;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.TextArea;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.TextArea;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.TextArea;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function addChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function setReadOnly(readOnly:Bool):google.apps.script.ui.TextArea;
	@:deprecated
	function setCharacterWidth(width:Int):google.apps.script.ui.TextArea;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.TextArea;
	@:deprecated
	function addValueChangeHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.TextArea;
	@:deprecated
	function addClickHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.TextArea;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.TextArea;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function setVisibleLines(lines:Int):google.apps.script.ui.TextArea;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.TextArea;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.TextArea;
	@:deprecated
	function setName(name:String):google.apps.script.ui.TextArea;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.TextArea;
	@:deprecated
	@:overload(function (value:String,fireEvents:Bool):google.apps.script.ui.TextArea {})
	function setValue(value:String):google.apps.script.ui.TextArea;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.TextArea;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.TextArea;
}
