package google.apps.script.ui;

@:native("DecoratorPanel")
@:deprecated
extern class DecoratorPanel
{
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.DecoratorPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.DecoratorPanel;
}
