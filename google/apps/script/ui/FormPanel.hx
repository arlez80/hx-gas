package google.apps.script.ui;

@:native("FormPanel")
@:deprecated
extern class FormPanel
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setMethod(method:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setAction(action:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.FormPanel;
	@:deprecated
	function getType(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function clear(handler:google.apps.script.ui.Handler):google.apps.script.ui.FormPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function addSubmitHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FormPanel;
	@:deprecated
	function getTag(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function getId(handler:google.apps.script.ui.Handler):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.FormPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setEncoding(encoding:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.FormPanel;
	@:deprecated
	function addSubmitCompleteHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.FormPanel;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.FormPanel;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.FormPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.FormPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.FormPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.FormPanel;
}
