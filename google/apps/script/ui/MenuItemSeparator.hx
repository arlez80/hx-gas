package google.apps.script.ui;

@:native("MenuItemSeparator")
@:deprecated
extern class MenuItemSeparator
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.MenuItemSeparator;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.MenuItemSeparator;
}
