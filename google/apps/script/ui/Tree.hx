package google.apps.script.ui;

@:native("Tree")
@:deprecated
extern class Tree
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.Tree;
	@:deprecated
	function addMouseMoveHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function addKeyDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.Tree;
	@:deprecated
	function addMouseUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.Tree;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.Tree;
	@:deprecated
	function addOpenHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function addKeyPressHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function addMouseOverHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.Tree;
	@:deprecated
	@:overload(function (item:Dynamic):google.apps.script.ui.Tree {})
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.Tree {})
	function addItem(text:String):google.apps.script.ui.Tree;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.Tree;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.Tree;
	@:deprecated
	function addMouseDownHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.Tree;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.Tree;
	@:deprecated
	function setAnimationEnabled(animationEnabled:Bool):google.apps.script.ui.Tree;
	@:deprecated
	function setAccessKey(accessKey:String):google.apps.script.ui.Tree;
	@:deprecated
	function addSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.Tree;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.Tree;
	@:deprecated
	function setFocus(focus:Bool):google.apps.script.ui.Tree;
	@:deprecated
	function addMouseOutHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function addKeyUpHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function setTabIndex(index:Int):google.apps.script.ui.Tree;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.Tree;
	@:deprecated
	function addCloseHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function addMouseWheelHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.Tree;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.Tree;
	@:deprecated
	@:overload(function (item:Dynamic,fireEvents:Bool):google.apps.script.ui.Tree {})
	function setSelectedItem(item:Dynamic):google.apps.script.ui.Tree;
	@:deprecated
	function addFocusHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.Tree;
	@:deprecated
	function addBlurHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.Tree;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.Tree;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.Tree;
}
