package google.apps.script.ui;

@:native("ServerHandler")
@:deprecated
extern class ServerHandler
{
	@:deprecated
	function validateNotNumber(widget:google.apps.script.ui.Widget):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateNotSum(widgets:Array<google.apps.script.ui.Widget>,sum:Int):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateLength(widget:google.apps.script.ui.Widget,min:Int,max:Int):google.apps.script.ui.ServerHandler;
	@:deprecated
	function getType(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,pattern:String,flags:String):google.apps.script.ui.ServerHandler {})
	function validateMatches(widget:google.apps.script.ui.Widget,pattern:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateNotRange(widget:google.apps.script.ui.Widget,min:Float,max:Float):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateRange(widget:google.apps.script.ui.Widget,min:Float,max:Float):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateNotLength(widget:google.apps.script.ui.Widget,min:Int,max:Int):google.apps.script.ui.ServerHandler;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateInteger(widget:google.apps.script.ui.Widget):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateNotInteger(widget:google.apps.script.ui.Widget):google.apps.script.ui.ServerHandler;
	@:deprecated
	function getId(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateNumber(widget:google.apps.script.ui.Widget):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateOptions(widget:google.apps.script.ui.Widget,options:Array<String>):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateSum(widgets:Array<google.apps.script.ui.Widget>,sum:Int):google.apps.script.ui.ServerHandler;
	@:deprecated
	function getTag(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	function validateNotOptions(widget:google.apps.script.ui.Widget,options:Array<String>):google.apps.script.ui.ServerHandler;
	@:deprecated
	@:overload(function (widget:google.apps.script.ui.Widget,pattern:String,flags:String):google.apps.script.ui.ServerHandler {})
	function validateNotMatches(widget:google.apps.script.ui.Widget,pattern:String):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateEmail(widget:google.apps.script.ui.Widget):google.apps.script.ui.ServerHandler;
	@:deprecated
	function addCallbackElement(widget:google.apps.script.ui.Widget):google.apps.script.ui.ServerHandler;
	@:deprecated
	function validateNotEmail(widget:google.apps.script.ui.Widget):google.apps.script.ui.ServerHandler;
	@:deprecated
	function setCallbackFunction(functionToInvoke:String):google.apps.script.ui.ServerHandler;
}
