package google.apps.script.ui;

@:native("CaptionPanel")
@:deprecated
extern class CaptionPanel
{
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setText(text:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function getType(styleName:String):String;
	@:deprecated
	function clear(styleName:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function getTag(styleName:String):String;
	@:deprecated
	function getId(styleName:String):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setCaptionText(text:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setContentWidget(widget:google.apps.script.ui.Widget):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.CaptionPanel;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.CaptionPanel;
}
