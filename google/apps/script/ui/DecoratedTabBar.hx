package google.apps.script.ui;

@:native("DecoratedTabBar")
@:deprecated
extern class DecoratedTabBar
{
	@:deprecated
	function setSize(width:String,height:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setPixelSize(width:Int,height:Int):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function addStyleDependentName(styleName:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setStyleName(styleName:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setHeight(height:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function getType(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	function addSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setTabText(index:Int,text:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function selectTab(index:Int):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setTag(tag:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function addBeforeSelectionHandler(handler:google.apps.script.ui.Handler):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setVisible(visible:Bool):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function getTag(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	function getId(widget:google.apps.script.ui.Widget):String;
	@:deprecated
	function setId(id:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setTitle(title:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setLayoutData(layout:Dynamic):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	@:overload(function (title:String,asHtml:Bool):google.apps.script.ui.DecoratedTabBar {})
	@:overload(function (widget:google.apps.script.ui.Widget):google.apps.script.ui.DecoratedTabBar {})
	function addTab(title:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setStylePrimaryName(styleName:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setWidth(width:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setTabEnabled(index:Int,enabled:Bool):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function addStyleName(styleName:String):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setStyleAttributes(attributes:Dynamic):google.apps.script.ui.DecoratedTabBar;
	@:deprecated
	function setStyleAttribute(attribute:String,value:String):google.apps.script.ui.DecoratedTabBar;
}
