package google.apps.script.ui;

@:native("Component")
@:deprecated
extern class Component
{
	@:deprecated
	function getId(width:String):String;
	@:deprecated
	function getType(width:String):String;
}
