package google.apps.script.data_studio;

@:require(hx_gas)
@:native("CommunityConnector.AggregationType")
@:enum abstract AggregationType(String) from String to String
{
	var AVG;
	var COUNT;
	var COUNT_DISTINCT;
	var MAX;
	var MIN;
	var SUM;
	var AUTO;
@:deprecated
	var NO_AGGREGATION;
}

@:require(hx_gas)
@:native("CommunityConnector.AuthType")
@:enum abstract AuthType(String) from String to String
{
	var NONE;
	var OAUTH2;
	var USER_PASS;
	var KEY;
	var USER_TOKEN;
}

@:require(hx_gas)
@:native("CommunityConnector.FieldType")
@:enum abstract FieldType(String) from String to String
{
	var YEAR;
	var YEAR_QUARTER;
	var YEAR_MONTH;
	var YEAR_WEEK;
	var YEAR_MONTH_DAY;
	var YEAR_MONTH_DAY_HOUR;
	var QUARTER;
	var MONTH;
	var WEEK;
	var MONTH_DAY;
	var DAY_OF_WEEK;
	var DAY;
	var HOUR;
	var MINUTE;
	var DURATION;
	var COUNTRY;
	var COUNTRY_CODE;
	var CONTINENT;
	var CONTINENT_CODE;
	var SUB_CONTINENT;
	var SUB_CONTINENT_CODE;
	var REGION;
	var REGION_CODE;
	var CITY;
	var CITY_CODE;
	var METRO;
	var METRO_CODE;
	var LATITUDE_LONGITUDE;
	var NUMBER;
	var PERCENT;
	var TEXT;
	var BOOLEAN;
	var URL;
	var CURRENCY_AED;
	var CURRENCY_ALL;
	var CURRENCY_ARS;
	var CURRENCY_AUD;
	var CURRENCY_BDT;
	var CURRENCY_BGN;
	var CURRENCY_BOB;
	var CURRENCY_BRL;
	var CURRENCY_CAD;
	var CURRENCY_CDF;
	var CURRENCY_CHF;
	var CURRENCY_CLP;
	var CURRENCY_CNY;
	var CURRENCY_COP;
	var CURRENCY_CRC;
	var CURRENCY_CZK;
	var CURRENCY_DKK;
	var CURRENCY_DOP;
	var CURRENCY_EGP;
	var CURRENCY_ETB;
	var CURRENCY_EUR;
	var CURRENCY_GBP;
	var CURRENCY_HKD;
	var CURRENCY_HRK;
	var CURRENCY_HUF;
	var CURRENCY_IDR;
	var CURRENCY_ILS;
	var CURRENCY_INR;
	var CURRENCY_IRR;
	var CURRENCY_ISK;
	var CURRENCY_JMD;
	var CURRENCY_JPY;
	var CURRENCY_KRW;
	var CURRENCY_LKR;
	var CURRENCY_LTL;
	var CURRENCY_MNT;
	var CURRENCY_MVR;
	var CURRENCY_MXN;
	var CURRENCY_MYR;
	var CURRENCY_NOK;
	var CURRENCY_NZD;
	var CURRENCY_PAB;
	var CURRENCY_PEN;
	var CURRENCY_PHP;
	var CURRENCY_PKR;
	var CURRENCY_PLN;
	var CURRENCY_RON;
	var CURRENCY_RSD;
	var CURRENCY_RUB;
	var CURRENCY_SAR;
	var CURRENCY_SEK;
	var CURRENCY_SGD;
	var CURRENCY_THB;
	var CURRENCY_TRY;
	var CURRENCY_TWD;
	var CURRENCY_TZS;
	var CURRENCY_UAH;
	var CURRENCY_USD;
	var CURRENCY_UYU;
	var CURRENCY_VEF;
	var CURRENCY_VND;
	var CURRENCY_YER;
	var CURRENCY_ZAR;
}

@:native("CommunityConnector")
extern class CommunityConnector
{
	function getConfig(name:String):google.apps.script.data_studio.Config;
	function newDebugError(name:String):google.apps.script.data_studio.DebugError;
	function getFields(name:String):google.apps.script.data_studio.Fields;
	function newUserError(name:String):google.apps.script.data_studio.UserError;
	function newAuthTypeResponse(name:String):google.apps.script.data_studio.GetAuthTypeResponse;
}
