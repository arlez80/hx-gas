package google.apps.script.data_studio;

@:native("DataStudioApp")
extern class DataStudioApp
{
	static function createCommunityConnector(label:String):google.apps.script.data_studio.CommunityConnector;
}
