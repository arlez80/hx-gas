package google.apps.script.data_studio;

@:native("Checkbox")
extern class Checkbox
{
	function setHelpText(helpText:String):google.apps.script.data_studio.Checkbox;
	function setName(name:String):google.apps.script.data_studio.Checkbox;
	function setId(id:String):google.apps.script.data_studio.Checkbox;
	function setAllowOverride(allowOverride:Bool):google.apps.script.data_studio.Checkbox;
}
