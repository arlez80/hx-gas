package google.apps.script.data_studio;

@:native("SelectMultiple")
extern class SelectMultiple
{
	function setHelpText(helpText:String):google.apps.script.data_studio.SelectMultiple;
	function setName(name:String):google.apps.script.data_studio.SelectMultiple;
	function addOption(optionBuilder:google.apps.script.data_studio.OptionBuilder):google.apps.script.data_studio.SelectMultiple;
	function setId(id:String):google.apps.script.data_studio.SelectMultiple;
	function setAllowOverride(allowOverride:Bool):google.apps.script.data_studio.SelectMultiple;
}
