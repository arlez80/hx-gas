package google.apps.script.data_studio;

@:require(hx_gas)
@:native("AggregationType")
@:enum abstract AggregationType(String) from String to String
{
	var AVG;
	var COUNT;
	var COUNT_DISTINCT;
	var MAX;
	var MIN;
	var SUM;
	var AUTO;
@:deprecated
	var NO_AGGREGATION;
}

