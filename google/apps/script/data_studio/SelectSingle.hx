package google.apps.script.data_studio;

@:native("SelectSingle")
extern class SelectSingle
{
	function setHelpText(helpText:String):google.apps.script.data_studio.SelectSingle;
	function setName(name:String):google.apps.script.data_studio.SelectSingle;
	function addOption(optionBuilder:google.apps.script.data_studio.OptionBuilder):google.apps.script.data_studio.SelectSingle;
	function setId(id:String):google.apps.script.data_studio.SelectSingle;
	function setAllowOverride(allowOverride:Bool):google.apps.script.data_studio.SelectSingle;
}
