package google.apps.script.data_studio;

@:native("UserError")
extern class UserError
{
	function setText(text:String):google.apps.script.data_studio.UserError;
	function throwException(text:String):Void;
	function setDebugText(text:String):google.apps.script.data_studio.UserError;
	function printJson(placeholder:String):String;
}
