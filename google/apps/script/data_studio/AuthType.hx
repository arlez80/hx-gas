package google.apps.script.data_studio;

@:require(hx_gas)
@:native("AuthType")
@:enum abstract AuthType(String) from String to String
{
	var NONE;
	var OAUTH2;
	var USER_PASS;
	var KEY;
	var USER_TOKEN;
}

