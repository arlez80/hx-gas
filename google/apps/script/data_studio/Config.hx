package google.apps.script.data_studio;

@:native("Config")
extern class Config
{
	function newSelectMultiple(name:String):google.apps.script.data_studio.SelectMultiple;
	function newTextInput(name:String):google.apps.script.data_studio.TextInput;
	function setDateRangeRequired(dateRangeRequired:Bool):google.apps.script.data_studio.Config;
	function newInfo(name:String):google.apps.script.data_studio.Info;
	function newCheckbox(name:String):google.apps.script.data_studio.Checkbox;
	function newTextArea(name:String):google.apps.script.data_studio.TextArea;
	function build(name:String):Dynamic;
	function newOptionBuilder(name:String):google.apps.script.data_studio.OptionBuilder;
	function printJson(name:String):String;
	function newSelectSingle(name:String):google.apps.script.data_studio.SelectSingle;
}
