package google.apps.script.data_studio;

@:native("GetAuthTypeResponse")
extern class GetAuthTypeResponse
{
	function setHelpUrl(helpUrl:String):google.apps.script.data_studio.GetAuthTypeResponse;
	function build(fieldId:String):Dynamic;
	function setAuthType(authType:google.apps.script.data_studio.AuthType):google.apps.script.data_studio.GetAuthTypeResponse;
	function printJson(fieldId:String):String;
}
