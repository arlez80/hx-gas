package google.apps.script.data_studio;

@:native("DebugError")
extern class DebugError
{
	function setText(text:String):google.apps.script.data_studio.DebugError;
	function throwException(text:String):Void;
	function printJson(dateRangeRequired:Bool):String;
}
