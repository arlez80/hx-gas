package google.apps.script.data_studio;

@:native("Fields")
extern class Fields
{
	function newMetric(fieldId:String):google.apps.script.data_studio.Field;
	function getDefaultDimension(ids:Array<String>):google.apps.script.data_studio.Field;
	function getFieldById(fieldId:String):google.apps.script.data_studio.Field;
	function asArray(type:google.apps.script.data_studio.FieldType):Array<google.apps.script.data_studio.Field>;
	function forIds(ids:Array<String>):google.apps.script.data_studio.Fields;
	function newDimension(fieldId:String):google.apps.script.data_studio.Field;
	function build(type:google.apps.script.data_studio.FieldType):Array<Dynamic>;
	function setDefaultMetric(fieldId:String):Void;
	function setDefaultDimension(fieldId:String):Void;
	function getDefaultMetric(ids:Array<String>):google.apps.script.data_studio.Field;
}
