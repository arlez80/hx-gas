package google.apps.script.data_studio;

@:native("Info")
extern class Info
{
	function setText(text:String):google.apps.script.data_studio.Info;
	function setId(id:String):google.apps.script.data_studio.Info;
}
