package google.apps.script.data_studio;

@:native("OptionBuilder")
extern class OptionBuilder
{
	function setLabel(label:String):google.apps.script.data_studio.OptionBuilder;
	function setValue(value:String):google.apps.script.data_studio.OptionBuilder;
}
