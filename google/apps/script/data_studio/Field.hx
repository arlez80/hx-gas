package google.apps.script.data_studio;

@:native("Field")
extern class Field
{
	function getIsReaggregatable(text:String):Bool;
	function setFormula(formula:String):google.apps.script.data_studio.Field;
	function setId(id:String):google.apps.script.data_studio.Field;
	function isDefault(text:String):Bool;
	function getName(text:String):String;
	function setDescription(description:String):google.apps.script.data_studio.Field;
	function setName(name:String):google.apps.script.data_studio.Field;
	function setType(type:google.apps.script.data_studio.FieldType):google.apps.script.data_studio.Field;
	function isHidden(text:String):Bool;
	function getType(text:String):google.apps.script.data_studio.FieldType;
	function getAggregation(text:String):google.apps.script.data_studio.AggregationType;
	function getGroup(text:String):String;
	function getDescription(text:String):String;
	function setIsReaggregatable(isReaggregatable:Bool):google.apps.script.data_studio.Field;
	function setAggregation(aggregation:google.apps.script.data_studio.AggregationType):google.apps.script.data_studio.Field;
	function setGroup(group:String):google.apps.script.data_studio.Field;
	function getFormula(text:String):String;
	function setIsHidden(isHidden:Bool):google.apps.script.data_studio.Field;
	function isMetric(text:String):Bool;
	function getId(text:String):String;
	function isDimension(text:String):Bool;
}
