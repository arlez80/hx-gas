package google.apps.script.data_studio;

@:native("TextArea")
extern class TextArea
{
	function setPlaceholder(placeholder:String):google.apps.script.data_studio.TextArea;
	function setHelpText(helpText:String):google.apps.script.data_studio.TextArea;
	function setName(name:String):google.apps.script.data_studio.TextArea;
	function setId(id:String):google.apps.script.data_studio.TextArea;
	function setAllowOverride(allowOverride:Bool):google.apps.script.data_studio.TextArea;
}
