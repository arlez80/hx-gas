package google.apps.script.data_studio;

@:native("TextInput")
extern class TextInput
{
	function setPlaceholder(placeholder:String):google.apps.script.data_studio.TextInput;
	function setHelpText(helpText:String):google.apps.script.data_studio.TextInput;
	function setName(name:String):google.apps.script.data_studio.TextInput;
	function setId(id:String):google.apps.script.data_studio.TextInput;
	function setAllowOverride(allowOverride:Bool):google.apps.script.data_studio.TextInput;
}
