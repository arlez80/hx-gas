package google.apps.script.language;

@:native("LanguageApp")
extern class LanguageApp
{
	@:overload(function (text:String,sourceLanguage:String,targetLanguage:String,advancedArgs:Dynamic):String {})
	static function translate(text:String,sourceLanguage:String,targetLanguage:String):String;
}
