package google.apps.script.script;

@:native("DocumentTriggerBuilder")
extern class DocumentTriggerBuilder
{
	function onOpen(day:google.apps.script.base.Weekday):google.apps.script.script.DocumentTriggerBuilder;
	function create(day:google.apps.script.base.Weekday):google.apps.script.script.Trigger;
}
