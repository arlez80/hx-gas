package google.apps.script.script;

@:require(hx_gas)
@:native("AuthorizationStatus")
@:enum abstract AuthorizationStatus(String) from String to String
{
	var REQUIRED;
	var NOT_REQUIRED;
}

