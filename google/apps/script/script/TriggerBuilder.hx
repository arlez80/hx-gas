package google.apps.script.script;

@:native("TriggerBuilder")
extern class TriggerBuilder
{
	@:overload(function (key:String):google.apps.script.script.DocumentTriggerBuilder {})
	function forDocument(document:google.apps.script.document.Document):google.apps.script.script.DocumentTriggerBuilder;
	function timeBased(emailId:String):google.apps.script.script.ClockTriggerBuilder;
	@:overload(function (key:String):google.apps.script.script.FormTriggerBuilder {})
	function forForm(form:google.apps.script.forms.Form):google.apps.script.script.FormTriggerBuilder;
	@:overload(function (key:String):google.apps.script.script.SpreadsheetTriggerBuilder {})
	function forSpreadsheet(sheet:google.apps.script.spreadsheet.Spreadsheet):google.apps.script.script.SpreadsheetTriggerBuilder;
	function forUserCalendar(emailId:String):google.apps.script.script.CalendarTriggerBuilder;
}
