package google.apps.script.script;

@:require(hx_gas)
@:native("EventType")
@:enum abstract EventType(String) from String to String
{
	var CLOCK;
	var ON_OPEN;
	var ON_EDIT;
	var ON_FORM_SUBMIT;
	var ON_CHANGE;
	var ON_EVENT_UPDATED;
}

