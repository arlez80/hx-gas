package google.apps.script.script;

@:require(hx_gas)
@:native("AuthMode")
@:enum abstract AuthMode(String) from String to String
{
	var NONE;
	var CUSTOM_FUNCTION;
	var LIMITED;
	var FULL;
}

