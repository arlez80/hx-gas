package google.apps.script.script;

@:native("Service")
extern class Service
{
	function isEnabled(day:google.apps.script.base.Weekday):Bool;
	function disable(day:google.apps.script.base.Weekday):Void;
	function getUrl(day:google.apps.script.base.Weekday):String;
}
