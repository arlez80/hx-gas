package google.apps.script.script;

@:native("StateTokenBuilder")
extern class StateTokenBuilder
{
	function withMethod(method:String):google.apps.script.script.StateTokenBuilder;
	function createToken(day:google.apps.script.base.Weekday):String;
	function withArgument(name:String,value:String):google.apps.script.script.StateTokenBuilder;
	function withTimeout(seconds:Int):google.apps.script.script.StateTokenBuilder;
}
