package google.apps.script.script;

@:require(hx_gas)
@:native("TriggerSource")
@:enum abstract TriggerSource(String) from String to String
{
	var SPREADSHEETS;
	var CLOCK;
	var FORMS;
	var DOCUMENTS;
	var CALENDAR;
}

