package google.apps.script.script;

@:native("CalendarTriggerBuilder")
extern class CalendarTriggerBuilder
{
	function onEventUpdated(functionName:String):google.apps.script.script.CalendarTriggerBuilder;
	function create(functionName:String):google.apps.script.script.Trigger;
}
