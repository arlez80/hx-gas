package google.apps.script.script;

@:native("AuthorizationInfo")
extern class AuthorizationInfo
{
	function getAuthorizationStatus(functionName:String):google.apps.script.script.AuthorizationStatus;
	function getAuthorizationUrl(functionName:String):String;
}
