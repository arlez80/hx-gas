package google.apps.script.script;

@:native("Trigger")
extern class Trigger
{
	function getHandlerFunction(seconds:Int):String;
	function getTriggerSource(seconds:Int):google.apps.script.script.TriggerSource;
	function getEventType(seconds:Int):google.apps.script.script.EventType;
	function getTriggerSourceId(seconds:Int):String;
	function getUniqueId(seconds:Int):String;
}
