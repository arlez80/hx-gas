package google.apps.script.script;

@:native("FormTriggerBuilder")
extern class FormTriggerBuilder
{
	function onFormSubmit(day:google.apps.script.base.Weekday):google.apps.script.script.FormTriggerBuilder;
	function onOpen(day:google.apps.script.base.Weekday):google.apps.script.script.FormTriggerBuilder;
	function create(day:google.apps.script.base.Weekday):google.apps.script.script.Trigger;
}
