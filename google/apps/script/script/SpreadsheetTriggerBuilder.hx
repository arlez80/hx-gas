package google.apps.script.script;

@:native("SpreadsheetTriggerBuilder")
extern class SpreadsheetTriggerBuilder
{
	function onEdit(day:google.apps.script.base.Weekday):google.apps.script.script.SpreadsheetTriggerBuilder;
	function onFormSubmit(day:google.apps.script.base.Weekday):google.apps.script.script.SpreadsheetTriggerBuilder;
	function onChange(day:google.apps.script.base.Weekday):google.apps.script.script.SpreadsheetTriggerBuilder;
	function onOpen(day:google.apps.script.base.Weekday):google.apps.script.script.SpreadsheetTriggerBuilder;
	function create(day:google.apps.script.base.Weekday):google.apps.script.script.Trigger;
}
