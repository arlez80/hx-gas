package google.apps.script.script;

@:require(hx_gas)
@:native("ScriptApp.AuthMode")
@:enum abstract AuthMode(String) from String to String
{
	var NONE;
	var CUSTOM_FUNCTION;
	var LIMITED;
	var FULL;
}

@:require(hx_gas)
@:native("ScriptApp.AuthorizationStatus")
@:enum abstract AuthorizationStatus(String) from String to String
{
	var REQUIRED;
	var NOT_REQUIRED;
}

@:require(hx_gas)
@:native("ScriptApp.EventType")
@:enum abstract EventType(String) from String to String
{
	var CLOCK;
	var ON_OPEN;
	var ON_EDIT;
	var ON_FORM_SUBMIT;
	var ON_CHANGE;
	var ON_EVENT_UPDATED;
}

@:require(hx_gas)
@:native("ScriptApp.InstallationSource")
@:enum abstract InstallationSource(String) from String to String
{
	var APPS_MARKETPLACE_DOMAIN_ADD_ON;
	var NONE;
	var WEB_STORE_ADD_ON;
}

@:require(hx_gas)
@:native("ScriptApp.TriggerSource")
@:enum abstract TriggerSource(String) from String to String
{
	var SPREADSHEETS;
	var CLOCK;
	var FORMS;
	var DOCUMENTS;
	var CALENDAR;
}

@:require(hx_gas)
@:native("ScriptApp.Weekday")
@:enum abstract Weekday(String) from String to String
{
	var SUNDAY;
	var MONDAY;
	var TUESDAY;
	var WEDNESDAY;
	var THURSDAY;
	var FRIDAY;
	var SATURDAY;
}

@:native("ScriptApp")
extern class ScriptApp
{
	static function newStateToken(spreadsheet:google.apps.script.spreadsheet.Spreadsheet):google.apps.script.script.StateTokenBuilder;
	static function newTrigger(functionName:String):google.apps.script.script.TriggerBuilder;
	static function getAuthorizationInfo(authMode:google.apps.script.script.AuthMode):google.apps.script.script.AuthorizationInfo;
	static function getInstallationSource(authMode:google.apps.script.script.AuthMode):google.apps.script.script.InstallationSource;
	@:deprecated
	static function getScriptTriggers(functionName:String):Array<google.apps.script.script.Trigger>;
	@:overload(function (form:google.apps.script.forms.Form):Array<google.apps.script.script.Trigger> {})
	@:overload(function (spreadsheet:google.apps.script.spreadsheet.Spreadsheet):Array<google.apps.script.script.Trigger> {})
	static function getUserTriggers(document:google.apps.script.document.Document):Array<google.apps.script.script.Trigger>;
	static function deleteTrigger(trigger:google.apps.script.script.Trigger):Void;
	static function invalidateAuth(spreadsheet:google.apps.script.spreadsheet.Spreadsheet):js.Error;
	static function getOAuthToken(authMode:google.apps.script.script.AuthMode):String;
	static function getService(authMode:google.apps.script.script.AuthMode):google.apps.script.script.Service;
	static function getProjectTriggers(authMode:google.apps.script.script.AuthMode):Array<google.apps.script.script.Trigger>;
	static function getScriptId(authMode:google.apps.script.script.AuthMode):String;
	@:deprecated
	static function getProjectKey(functionName:String):String;
}
