package google.apps.script.script;

@:native("ClockTriggerBuilder")
extern class ClockTriggerBuilder
{
	function atDate(year:Int,month:Int,day:Int):google.apps.script.script.ClockTriggerBuilder;
	function at(date:Date):google.apps.script.script.ClockTriggerBuilder;
	function onMonthDay(day:Int):google.apps.script.script.ClockTriggerBuilder;
	function inTimezone(timezone:String):google.apps.script.script.ClockTriggerBuilder;
	function onWeekDay(day:google.apps.script.base.Weekday):google.apps.script.script.ClockTriggerBuilder;
	function atHour(hour:Int):google.apps.script.script.ClockTriggerBuilder;
	function after(durationMilliseconds:Int):google.apps.script.script.ClockTriggerBuilder;
	function create(hour:Int):google.apps.script.script.Trigger;
	function everyDays(n:Int):google.apps.script.script.ClockTriggerBuilder;
	function everyWeeks(n:Int):google.apps.script.script.ClockTriggerBuilder;
	function nearMinute(minute:Int):google.apps.script.script.ClockTriggerBuilder;
	function everyMinutes(n:Int):google.apps.script.script.ClockTriggerBuilder;
	function everyHours(n:Int):google.apps.script.script.ClockTriggerBuilder;
}
