package google.apps.script.script;

@:require(hx_gas)
@:native("InstallationSource")
@:enum abstract InstallationSource(String) from String to String
{
	var APPS_MARKETPLACE_DOMAIN_ADD_ON;
	var NONE;
	var WEB_STORE_ADD_ON;
}

