package google.apps.script.groups;

@:require(hx_gas)
@:native("GroupsApp.Role")
@:enum abstract Role(String) from String to String
{
	var OWNER;
	var MANAGER;
	var MEMBER;
	var INVITED;
	var PENDING;
}

@:native("GroupsApp")
extern class GroupsApp
{
	static function getGroupByEmail(email:String):google.apps.script.groups.Group;
	static function getGroups(email:String):Array<google.apps.script.groups.Group>;
}
