package google.apps.script.groups;

@:native("Group")
extern class Group
{
	function getEmail(email:String):String;
	@:overload(function (user:google.apps.script.base.User):google.apps.script.groups.Role {})
	function getRole(email:String):google.apps.script.groups.Role;
	@:overload(function (user:google.apps.script.base.User):Bool {})
	function hasUser(email:String):Bool;
	function getGroups(email:String):Array<google.apps.script.groups.Group>;
	@:overload(function (email:String):Bool {})
	function hasGroup(group:google.apps.script.groups.Group):Bool;
	function getUsers(user:google.apps.script.base.User):Array<google.apps.script.base.User>;
}
