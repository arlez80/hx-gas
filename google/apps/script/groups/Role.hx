package google.apps.script.groups;

@:require(hx_gas)
@:native("Role")
@:enum abstract Role(String) from String to String
{
	var OWNER;
	var MANAGER;
	var MEMBER;
	var INVITED;
	var PENDING;
}

