package google.apps.script.charts;

@:native("ScatterChartBuilder")
extern class ScatterChartBuilder
{
	function setBackgroundColor(cssValue:String):google.apps.script.charts.ScatterChartBuilder;
	function setTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ScatterChartBuilder;
	function setColors(cssValues:Array<String>):google.apps.script.charts.ScatterChartBuilder;
	function setDataViewDefinition(dataViewDefinition:google.apps.script.charts.DataViewDefinition):google.apps.script.charts.ScatterChartBuilder;
	function setXAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ScatterChartBuilder;
	function setYAxisRange(start:Float,end:Float):google.apps.script.charts.ScatterChartBuilder;
	function setXAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ScatterChartBuilder;
	function setPointStyle(style:google.apps.script.charts.PointStyle):google.apps.script.charts.ScatterChartBuilder;
	function setYAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ScatterChartBuilder;
	function setXAxisLogScale(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ScatterChartBuilder;
	function setTitle(chartTitle:String):google.apps.script.charts.ScatterChartBuilder;
	function setXAxisRange(start:Float,end:Float):google.apps.script.charts.ScatterChartBuilder;
	function setYAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ScatterChartBuilder;
	function setDataSourceUrl(url:String):google.apps.script.charts.ScatterChartBuilder;
	function setLegendPosition(position:google.apps.script.charts.Position):google.apps.script.charts.ScatterChartBuilder;
	function setXAxisTitle(title:String):google.apps.script.charts.ScatterChartBuilder;
	function setLegendTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ScatterChartBuilder;
	function setYAxisTitle(title:String):google.apps.script.charts.ScatterChartBuilder;
	function setDimensions(width:Int,height:Int):google.apps.script.charts.ScatterChartBuilder;
	function setOption(option:String,value:Dynamic):google.apps.script.charts.ScatterChartBuilder;
	function build(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.Chart;
	function setYAxisLogScale(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ScatterChartBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.ScatterChartBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.ScatterChartBuilder;
}
