package google.apps.script.charts;

@:native("DashboardPanel")
extern class DashboardPanel
{
	@:deprecated
	function add(widget:google.apps.script.ui.Widget):google.apps.script.charts.DashboardPanel;
	function setId(id:String):google.apps.script.charts.DashboardPanel;
	function getId(id:String):String;
	function getType(id:String):String;
}
