package google.apps.script.charts;

@:native("DataViewDefinitionBuilder")
extern class DataViewDefinitionBuilder
{
	function build(row:Int,column:Int,value:Dynamic):google.apps.script.charts.DataViewDefinition;
	function setColumns(columns:Array<Dynamic>):google.apps.script.charts.DataViewDefinitionBuilder;
}
