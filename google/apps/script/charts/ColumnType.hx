package google.apps.script.charts;

@:require(hx_gas)
@:native("ColumnType")
@:enum abstract ColumnType(String) from String to String
{
	var DATE;
	var NUMBER;
	var STRING;
}

