package google.apps.script.charts;

@:native("NumberRangeFilterBuilder")
extern class NumberRangeFilterBuilder
{
	function setFilterColumnLabel(columnLabel:String):google.apps.script.charts.NumberRangeFilterBuilder;
	function setLabelSeparator(labelSeparator:String):google.apps.script.charts.NumberRangeFilterBuilder;
	function setMaxValue(maxValue:Int):google.apps.script.charts.NumberRangeFilterBuilder;
	function setShowRangeValues(showRangeValues:Bool):google.apps.script.charts.NumberRangeFilterBuilder;
	function setMinValue(minValue:Int):google.apps.script.charts.NumberRangeFilterBuilder;
	function setLabelStacking(orientation:google.apps.script.charts.Orientation):google.apps.script.charts.NumberRangeFilterBuilder;
	function setFilterColumnIndex(columnIndex:Int):google.apps.script.charts.NumberRangeFilterBuilder;
	function setTicks(ticks:Int):google.apps.script.charts.NumberRangeFilterBuilder;
	function build(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.Control;
	function setOrientation(orientation:google.apps.script.charts.Orientation):google.apps.script.charts.NumberRangeFilterBuilder;
	function setLabel(label:String):google.apps.script.charts.NumberRangeFilterBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.NumberRangeFilterBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.NumberRangeFilterBuilder;
}
