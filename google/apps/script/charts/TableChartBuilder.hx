package google.apps.script.charts;

@:native("TableChartBuilder")
extern class TableChartBuilder
{
	@:overload(function (pageSize:Int):google.apps.script.charts.TableChartBuilder {})
	@:overload(function (pageSize:Int,startPage:Int):google.apps.script.charts.TableChartBuilder {})
	function enablePaging(enablePaging:Bool):google.apps.script.charts.TableChartBuilder;
	function enableSorting(enableSorting:Bool):google.apps.script.charts.TableChartBuilder;
	function useAlternatingRowStyle(alternate:Bool):google.apps.script.charts.TableChartBuilder;
	function setInitialSortingAscending(column:Int):google.apps.script.charts.TableChartBuilder;
	function setDataViewDefinition(dataViewDefinition:google.apps.script.charts.DataViewDefinition):google.apps.script.charts.TableChartBuilder;
	function setDataSourceUrl(url:String):google.apps.script.charts.TableChartBuilder;
	function setFirstRowNumber(number:Int):google.apps.script.charts.TableChartBuilder;
	function setDimensions(width:Int,height:Int):google.apps.script.charts.TableChartBuilder;
	function setOption(option:String,value:Dynamic):google.apps.script.charts.TableChartBuilder;
	function build(realtimeTrigger:Bool):google.apps.script.charts.Chart;
	function setInitialSortingDescending(column:Int):google.apps.script.charts.TableChartBuilder;
	function enableRtlTable(rtlEnabled:Bool):google.apps.script.charts.TableChartBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.TableChartBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.TableChartBuilder;
	function showRowNumberColumn(showRowNumber:Bool):google.apps.script.charts.TableChartBuilder;
}
