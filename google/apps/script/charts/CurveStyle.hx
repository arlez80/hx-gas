package google.apps.script.charts;

@:require(hx_gas)
@:native("CurveStyle")
@:enum abstract CurveStyle(String) from String to String
{
	var NORMAL;
	var SMOOTH;
}

