package google.apps.script.charts;

@:require(hx_gas)
@:native("Charts.ChartHiddenDimensionStrategy")
@:enum abstract ChartHiddenDimensionStrategy(String) from String to String
{
	var IGNORE_BOTH;
	var IGNORE_ROWS;
	var IGNORE_COLUMNS;
	var SHOW_BOTH;
}

@:require(hx_gas)
@:native("Charts.ChartMergeStrategy")
@:enum abstract ChartMergeStrategy(String) from String to String
{
	var MERGE_COLUMNS;
	var MERGE_ROWS;
}

@:require(hx_gas)
@:native("Charts.ChartType")
@:enum abstract ChartType(String) from String to String
{
	var TIMELINE;
	var AREA;
	var BAR;
	var BUBBLE;
	var CANDLESTICK;
	var COLUMN;
	var COMBO;
	var GAUGE;
	var GEO;
	var HISTOGRAM;
	var RADAR;
	var LINE;
	var ORG;
	var PIE;
	var SCATTER;
	var SPARKLINE;
	var STEPPED_AREA;
	var TABLE;
	var TREEMAP;
	var WATERFALL;
}

@:require(hx_gas)
@:native("Charts.ColumnType")
@:enum abstract ColumnType(String) from String to String
{
	var DATE;
	var NUMBER;
	var STRING;
}

@:require(hx_gas)
@:native("Charts.CurveStyle")
@:enum abstract CurveStyle(String) from String to String
{
	var NORMAL;
	var SMOOTH;
}

@:require(hx_gas)
@:native("Charts.MatchType")
@:enum abstract MatchType(String) from String to String
{
	var EXACT;
	var PREFIX;
	var ANY;
}

@:require(hx_gas)
@:native("Charts.Orientation")
@:enum abstract Orientation(String) from String to String
{
	var HORIZONTAL;
	var VERTICAL;
}

@:require(hx_gas)
@:native("Charts.PickerValuesLayout")
@:enum abstract PickerValuesLayout(String) from String to String
{
	var ASIDE;
	var BELOW;
	var BELOW_WRAPPING;
	var BELOW_STACKED;
}

@:require(hx_gas)
@:native("Charts.PointStyle")
@:enum abstract PointStyle(String) from String to String
{
	var NONE;
	var TINY;
	var MEDIUM;
	var LARGE;
	var HUGE;
}

@:require(hx_gas)
@:native("Charts.Position")
@:enum abstract Position(String) from String to String
{
	var TOP;
	var RIGHT;
	var BOTTOM;
	var NONE;
}

@:native("Charts")
extern class Charts
{
	static function newCategoryFilter(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.CategoryFilterBuilder;
	static function newDataViewDefinition(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.DataViewDefinitionBuilder;
	static function newBarChart(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.BarChartBuilder;
	static function newTableChart(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.TableChartBuilder;
	static function newDashboardPanel(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.DashboardPanelBuilder;
	static function newDataTable(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.DataTableBuilder;
	static function newNumberRangeFilter(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.NumberRangeFilterBuilder;
	static function newScatterChart(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.ScatterChartBuilder;
	static function newStringFilter(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.StringFilterBuilder;
	static function newLineChart(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.LineChartBuilder;
	static function newPieChart(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.PieChartBuilder;
	static function newTextStyle(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.TextStyleBuilder;
	static function newAreaChart(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.AreaChartBuilder;
	static function newColumnChart(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.ColumnChartBuilder;
}
