package google.apps.script.charts;

@:native("StringFilterBuilder")
extern class StringFilterBuilder
{
	function setFilterColumnLabel(columnLabel:String):google.apps.script.charts.StringFilterBuilder;
	function setLabelSeparator(labelSeparator:String):google.apps.script.charts.StringFilterBuilder;
	function setRealtimeTrigger(realtimeTrigger:Bool):google.apps.script.charts.StringFilterBuilder;
	function setFilterColumnIndex(columnIndex:Int):google.apps.script.charts.StringFilterBuilder;
	function setCaseSensitive(caseSensitive:Bool):google.apps.script.charts.StringFilterBuilder;
	function build(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.Control;
	function setMatchType(matchType:google.apps.script.charts.MatchType):google.apps.script.charts.StringFilterBuilder;
	function setLabelStacking(orientation:google.apps.script.charts.Orientation):google.apps.script.charts.StringFilterBuilder;
	function setLabel(label:String):google.apps.script.charts.StringFilterBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.StringFilterBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.StringFilterBuilder;
}
