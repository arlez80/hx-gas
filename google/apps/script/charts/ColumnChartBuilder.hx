package google.apps.script.charts;

@:native("ColumnChartBuilder")
extern class ColumnChartBuilder
{
	function setBackgroundColor(cssValue:String):google.apps.script.charts.ColumnChartBuilder;
	function setTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ColumnChartBuilder;
	function setTitle(chartTitle:String):google.apps.script.charts.ColumnChartBuilder;
	function setDataViewDefinition(dataViewDefinition:google.apps.script.charts.DataViewDefinition):google.apps.script.charts.ColumnChartBuilder;
	function setXAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ColumnChartBuilder;
	function setStacked(start:Float,end:Float):google.apps.script.charts.ColumnChartBuilder;
	function reverseCategories(option:String):google.apps.script.charts.ColumnChartBuilder;
	function setXAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ColumnChartBuilder;
	function setYAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ColumnChartBuilder;
	function setColors(cssValues:Array<String>):google.apps.script.charts.ColumnChartBuilder;
	function setRange(start:Float,end:Float):google.apps.script.charts.ColumnChartBuilder;
	function setYAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ColumnChartBuilder;
	function setDataSourceUrl(url:String):google.apps.script.charts.ColumnChartBuilder;
	function useLogScale(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ColumnChartBuilder;
	function setLegendTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.ColumnChartBuilder;
	function setLegendPosition(position:google.apps.script.charts.Position):google.apps.script.charts.ColumnChartBuilder;
	function setYAxisTitle(title:String):google.apps.script.charts.ColumnChartBuilder;
	function setDimensions(width:Int,height:Int):google.apps.script.charts.ColumnChartBuilder;
	function setOption(option:String,value:Dynamic):google.apps.script.charts.ColumnChartBuilder;
	function build(option:String):google.apps.script.charts.Chart;
	function setXAxisTitle(title:String):google.apps.script.charts.ColumnChartBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.ColumnChartBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.ColumnChartBuilder;
}
