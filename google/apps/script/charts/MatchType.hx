package google.apps.script.charts;

@:require(hx_gas)
@:native("MatchType")
@:enum abstract MatchType(String) from String to String
{
	var EXACT;
	var PREFIX;
	var ANY;
}

