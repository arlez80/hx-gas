package google.apps.script.charts;

@:require(hx_gas)
@:native("Position")
@:enum abstract Position(String) from String to String
{
	var TOP;
	var RIGHT;
	var BOTTOM;
	var NONE;
}

