package google.apps.script.charts;

@:native("PieChartBuilder")
extern class PieChartBuilder
{
	function setBackgroundColor(cssValue:String):google.apps.script.charts.PieChartBuilder;
	function setTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.PieChartBuilder;
	function setColors(cssValues:Array<String>):google.apps.script.charts.PieChartBuilder;
	function setDataViewDefinition(dataViewDefinition:google.apps.script.charts.DataViewDefinition):google.apps.script.charts.PieChartBuilder;
	function setDataSourceUrl(url:String):google.apps.script.charts.PieChartBuilder;
	function set3D(ticks:Int):google.apps.script.charts.PieChartBuilder;
	function setLegendPosition(position:google.apps.script.charts.Position):google.apps.script.charts.PieChartBuilder;
	function setLegendTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.PieChartBuilder;
	function reverseCategories(ticks:Int):google.apps.script.charts.PieChartBuilder;
	function setDimensions(width:Int,height:Int):google.apps.script.charts.PieChartBuilder;
	function setOption(option:String,value:Dynamic):google.apps.script.charts.PieChartBuilder;
	function build(ticks:Int):google.apps.script.charts.Chart;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.PieChartBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.PieChartBuilder;
	function setTitle(chartTitle:String):google.apps.script.charts.PieChartBuilder;
}
