package google.apps.script.charts;

@:require(hx_gas)
@:native("PickerValuesLayout")
@:enum abstract PickerValuesLayout(String) from String to String
{
	var ASIDE;
	var BELOW;
	var BELOW_WRAPPING;
	var BELOW_STACKED;
}

