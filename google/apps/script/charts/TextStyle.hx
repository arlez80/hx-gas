package google.apps.script.charts;

@:native("TextStyle")
extern class TextStyle
{
	function getColor(alternate:Bool):String;
	function getFontSize(alternate:Bool):Float;
	function getFontName(alternate:Bool):String;
}
