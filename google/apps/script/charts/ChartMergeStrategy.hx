package google.apps.script.charts;

@:require(hx_gas)
@:native("ChartMergeStrategy")
@:enum abstract ChartMergeStrategy(String) from String to String
{
	var MERGE_COLUMNS;
	var MERGE_ROWS;
}

