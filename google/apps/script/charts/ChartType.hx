package google.apps.script.charts;

@:require(hx_gas)
@:native("ChartType")
@:enum abstract ChartType(String) from String to String
{
	var TIMELINE;
	var AREA;
	var BAR;
	var BUBBLE;
	var CANDLESTICK;
	var COLUMN;
	var COMBO;
	var GAUGE;
	var GEO;
	var HISTOGRAM;
	var RADAR;
	var LINE;
	var ORG;
	var PIE;
	var SCATTER;
	var SPARKLINE;
	var STEPPED_AREA;
	var TABLE;
	var TREEMAP;
	var WATERFALL;
}

