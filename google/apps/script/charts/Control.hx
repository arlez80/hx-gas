package google.apps.script.charts;

@:native("Control")
extern class Control
{
	function setId(id:String):google.apps.script.charts.Control;
	function getId(textStyle:google.apps.script.charts.TextStyle):String;
	function getType(textStyle:google.apps.script.charts.TextStyle):String;
}
