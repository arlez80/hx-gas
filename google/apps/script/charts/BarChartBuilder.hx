package google.apps.script.charts;

@:native("BarChartBuilder")
extern class BarChartBuilder
{
	function setBackgroundColor(cssValue:String):google.apps.script.charts.BarChartBuilder;
	function setTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setTitle(chartTitle:String):google.apps.script.charts.BarChartBuilder;
	function setDataViewDefinition(dataViewDefinition:google.apps.script.charts.DataViewDefinition):google.apps.script.charts.BarChartBuilder;
	function setXAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setStacked(start:Float,end:Float):google.apps.script.charts.BarChartBuilder;
	function reverseCategories(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setXAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setYAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setColors(cssValues:Array<String>):google.apps.script.charts.BarChartBuilder;
	function setRange(start:Float,end:Float):google.apps.script.charts.BarChartBuilder;
	function setYAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setDataSourceUrl(url:String):google.apps.script.charts.BarChartBuilder;
	function useLogScale(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setLegendTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setLegendPosition(position:google.apps.script.charts.Position):google.apps.script.charts.BarChartBuilder;
	function setYAxisTitle(title:String):google.apps.script.charts.BarChartBuilder;
	function setDimensions(width:Int,height:Int):google.apps.script.charts.BarChartBuilder;
	function setOption(option:String,value:Dynamic):google.apps.script.charts.BarChartBuilder;
	function build(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.Chart;
	function reverseDirection(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.BarChartBuilder;
	function setXAxisTitle(title:String):google.apps.script.charts.BarChartBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.BarChartBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.BarChartBuilder;
}
