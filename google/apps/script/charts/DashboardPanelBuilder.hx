package google.apps.script.charts;

@:native("DashboardPanelBuilder")
extern class DashboardPanelBuilder
{
	function build(controls:Array<google.apps.script.charts.Control>,charts:Array<google.apps.script.charts.Chart>):google.apps.script.charts.DashboardPanel;
	@:overload(function (source:google.apps.script.charts.DataTableSource):google.apps.script.charts.DashboardPanelBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.DashboardPanelBuilder;
	@:overload(function (controls:Array<google.apps.script.charts.Control>,charts:Array<google.apps.script.charts.Chart>):google.apps.script.charts.DashboardPanelBuilder {})
	function bind(control:google.apps.script.charts.Control,chart:google.apps.script.charts.Chart):google.apps.script.charts.DashboardPanelBuilder;
}
