package google.apps.script.charts;

@:native("AreaChartBuilder")
extern class AreaChartBuilder
{
	function setBackgroundColor(cssValue:String):google.apps.script.charts.AreaChartBuilder;
	function setTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.AreaChartBuilder;
	function setTitle(chartTitle:String):google.apps.script.charts.AreaChartBuilder;
	function setDataViewDefinition(dataViewDefinition:google.apps.script.charts.DataViewDefinition):google.apps.script.charts.AreaChartBuilder;
	function setXAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.AreaChartBuilder;
	function setStacked(start:Float,end:Float):google.apps.script.charts.AreaChartBuilder;
	function reverseCategories(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.AreaChartBuilder;
	function setXAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.AreaChartBuilder;
	function setPointStyle(style:google.apps.script.charts.PointStyle):google.apps.script.charts.AreaChartBuilder;
	function setYAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.AreaChartBuilder;
	function setColors(cssValues:Array<String>):google.apps.script.charts.AreaChartBuilder;
	function setRange(start:Float,end:Float):google.apps.script.charts.AreaChartBuilder;
	function setYAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.AreaChartBuilder;
	function setDataSourceUrl(url:String):google.apps.script.charts.AreaChartBuilder;
	function useLogScale(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.AreaChartBuilder;
	function setLegendTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.AreaChartBuilder;
	function setLegendPosition(position:google.apps.script.charts.Position):google.apps.script.charts.AreaChartBuilder;
	function setYAxisTitle(title:String):google.apps.script.charts.AreaChartBuilder;
	function setDimensions(width:Int,height:Int):google.apps.script.charts.AreaChartBuilder;
	function setOption(option:String,value:Dynamic):google.apps.script.charts.AreaChartBuilder;
	function build(updateType:google.apps.script.card_service.UpdateDraftBodyType):google.apps.script.charts.Chart;
	function setXAxisTitle(title:String):google.apps.script.charts.AreaChartBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.AreaChartBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.AreaChartBuilder;
}
