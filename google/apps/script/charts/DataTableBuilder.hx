package google.apps.script.charts;

@:native("DataTableBuilder")
extern class DataTableBuilder
{
	function addColumn(type:google.apps.script.charts.ColumnType,label:String):google.apps.script.charts.DataTableBuilder;
	function build(values:Array<Dynamic>):google.apps.script.charts.DataTable;
	function setValue(row:Int,column:Int,value:Dynamic):google.apps.script.charts.DataTableBuilder;
	function addRow(values:Array<Dynamic>):google.apps.script.charts.DataTableBuilder;
}
