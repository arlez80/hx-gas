package google.apps.script.charts;

@:native("DataTableSource")
extern class DataTableSource
{
	function getDataTable(fontSize:Float):google.apps.script.charts.DataTable;
}
