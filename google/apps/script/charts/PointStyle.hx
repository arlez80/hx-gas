package google.apps.script.charts;

@:require(hx_gas)
@:native("PointStyle")
@:enum abstract PointStyle(String) from String to String
{
	var NONE;
	var TINY;
	var MEDIUM;
	var LARGE;
	var HUGE;
}

