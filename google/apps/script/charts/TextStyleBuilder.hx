package google.apps.script.charts;

@:native("TextStyleBuilder")
extern class TextStyleBuilder
{
	function build(alternate:Bool):google.apps.script.charts.TextStyle;
	function setFontSize(fontSize:Float):google.apps.script.charts.TextStyleBuilder;
	function setColor(cssValue:String):google.apps.script.charts.TextStyleBuilder;
	function setFontName(fontName:String):google.apps.script.charts.TextStyleBuilder;
}
