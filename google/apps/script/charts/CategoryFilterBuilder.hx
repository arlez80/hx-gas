package google.apps.script.charts;

@:native("CategoryFilterBuilder")
extern class CategoryFilterBuilder
{
	function setFilterColumnLabel(columnLabel:String):google.apps.script.charts.CategoryFilterBuilder;
	function setValues(values:Array<String>):google.apps.script.charts.CategoryFilterBuilder;
	function setLabelSeparator(labelSeparator:String):google.apps.script.charts.CategoryFilterBuilder;
	function setAllowTyping(allowTyping:Bool):google.apps.script.charts.CategoryFilterBuilder;
	function setAllowMultiple(allowMultiple:Bool):google.apps.script.charts.CategoryFilterBuilder;
	function build(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.Control;
	function setLabelStacking(orientation:google.apps.script.charts.Orientation):google.apps.script.charts.CategoryFilterBuilder;
	function setSelectedValuesLayout(layout:google.apps.script.charts.PickerValuesLayout):google.apps.script.charts.CategoryFilterBuilder;
	function setAllowNone(allowNone:Bool):google.apps.script.charts.CategoryFilterBuilder;
	function setFilterColumnIndex(columnIndex:Int):google.apps.script.charts.CategoryFilterBuilder;
	function setSortValues(sortValues:Bool):google.apps.script.charts.CategoryFilterBuilder;
	function setLabel(label:String):google.apps.script.charts.CategoryFilterBuilder;
	function setCaption(caption:String):google.apps.script.charts.CategoryFilterBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.CategoryFilterBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.CategoryFilterBuilder;
}
