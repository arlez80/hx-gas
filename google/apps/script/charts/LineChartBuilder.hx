package google.apps.script.charts;

@:native("LineChartBuilder")
extern class LineChartBuilder
{
	function setBackgroundColor(cssValue:String):google.apps.script.charts.LineChartBuilder;
	function setTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.LineChartBuilder;
	function setColors(cssValues:Array<String>):google.apps.script.charts.LineChartBuilder;
	function setDataViewDefinition(dataViewDefinition:google.apps.script.charts.DataViewDefinition):google.apps.script.charts.LineChartBuilder;
	function setXAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.LineChartBuilder;
	function useLogScale(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.LineChartBuilder;
	function reverseCategories(columns:Array<Dynamic>):google.apps.script.charts.LineChartBuilder;
	function setXAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.LineChartBuilder;
	function setPointStyle(style:google.apps.script.charts.PointStyle):google.apps.script.charts.LineChartBuilder;
	function setYAxisTitleTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.LineChartBuilder;
	function setTitle(chartTitle:String):google.apps.script.charts.LineChartBuilder;
	function setRange(start:Float,end:Float):google.apps.script.charts.LineChartBuilder;
	function setCurveStyle(style:google.apps.script.charts.CurveStyle):google.apps.script.charts.LineChartBuilder;
	function setDataSourceUrl(url:String):google.apps.script.charts.LineChartBuilder;
	function setYAxisTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.LineChartBuilder;
	function setLegendTextStyle(textStyle:google.apps.script.charts.TextStyle):google.apps.script.charts.LineChartBuilder;
	function setLegendPosition(position:google.apps.script.charts.Position):google.apps.script.charts.LineChartBuilder;
	function setYAxisTitle(title:String):google.apps.script.charts.LineChartBuilder;
	function setDimensions(width:Int,height:Int):google.apps.script.charts.LineChartBuilder;
	function setOption(option:String,value:Dynamic):google.apps.script.charts.LineChartBuilder;
	function build(columns:Array<Dynamic>):google.apps.script.charts.Chart;
	function setXAxisTitle(title:String):google.apps.script.charts.LineChartBuilder;
	@:overload(function (table:google.apps.script.charts.DataTableSource):google.apps.script.charts.LineChartBuilder {})
	function setDataTable(tableBuilder:google.apps.script.charts.DataTableBuilder):google.apps.script.charts.LineChartBuilder;
}
