package google.apps.script.charts;

@:native("ChartOptions")
extern class ChartOptions
{
	function get(option:String):Dynamic;
}
