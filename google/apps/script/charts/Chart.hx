package google.apps.script.charts;

@:native("Chart")
extern class Chart
{
	function getOptions(contentType:String):google.apps.script.charts.ChartOptions;
	function setId(id:String):google.apps.script.charts.Chart;
	function getBlob(contentType:String):google.apps.script.base.Blob;
	function getAs(contentType:String):google.apps.script.base.Blob;
	function getId(contentType:String):String;
	function getType(contentType:String):String;
}
