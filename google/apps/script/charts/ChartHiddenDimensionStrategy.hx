package google.apps.script.charts;

@:require(hx_gas)
@:native("ChartHiddenDimensionStrategy")
@:enum abstract ChartHiddenDimensionStrategy(String) from String to String
{
	var IGNORE_BOTH;
	var IGNORE_ROWS;
	var IGNORE_COLUMNS;
	var SHOW_BOTH;
}

