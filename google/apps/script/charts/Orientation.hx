package google.apps.script.charts;

@:require(hx_gas)
@:native("Orientation")
@:enum abstract Orientation(String) from String to String
{
	var HORIZONTAL;
	var VERTICAL;
}

